﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Parser.Tests.HoldemManagerLogParserTests
{
	[TestClass]
	public partial class HoldemManagerLogParserTest
	{
		[TestMethod]
		public void TestParseLines_SkipsEmptyLine()
		{
			var parser = new HoldemManagerLogParser();

			var input = new[] { "" };

			parser.ParseLines(input);
		}

		[TestMethod]
		[ExpectedException(typeof(FailedToParseLineException))]
		public void TestParseLines_ThrowsOnInvalidInput()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "123" };
			parser.ParseLines(input);
		}

		[TestMethod]
		public void TestParseLines_SeparatesGamesCorrectly()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Game started at: 2013/4/29 17:39:10",
				"Game started at: 2013/4/29 17:39:10"
			};

			var result = parser.ParseLines(input);

			Assert.AreEqual(2, result.Count());
		}

		[TestMethod]
		public void TestParseLines_SeparatesGamesCorrectly_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"***** Hand History for Game 12866338403 *****",
				"***** Hand History for Game 12866338404 *****"
			};

			var result = parser.ParseLines(input);

			Assert.AreEqual(2, result.Count());
		}

		[TestMethod]
		public void TestParseLines_SeparatesGamesCorrectly_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"PokerStars Game #000094119102: Hold'em No Limit ($0.1/$0.2 USD) - 2015/03/30 08:47:49",
				"PokerStars Game #000094119105: Hold'em No Limit ($0.1/$0.2 USD) - 2015/03/30 08:47:49"
			};

			var result = parser.ParseLines(input);

			Assert.AreEqual(2, result.Count());
		}

		[TestMethod]
		public void TestParseLines_WorksWithoutStartingLine()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] {"Seat 1 is the button"};
			var result = parser.ParseLines(input);

			Assert.AreEqual(1, result.Count());
		}
	}
}
