﻿using System;
using System.Linq;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Parser.Tests.HoldemManagerLogParserTests
{
	public partial class HoldemManagerLogParserTest
	{
		[TestMethod]
		public void TestParseGame_Type2_Line01()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "***** Hand History for Game 12866338403 *****" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("12866338403", game.GameNumber);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line02()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "$25 USD NL Texas Hold'em - Thursday, April 11, 20:13:44 MSKS 2013" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new DateTime(2013, 4, 11, 20, 13, 44), game.StartTime);
			Assert.AreEqual(0.25, game.BigBlind);
			Assert.AreEqual(0.1, game.SmallBlind);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line02_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "$25 USD NL Texas Hold'em - Thursday, April 01, 20:13:44 MSKS 2013" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new DateTime(2013, 4, 1, 20, 13, 44), game.StartTime);
			Assert.AreEqual(0.25, game.BigBlind);
			Assert.AreEqual(0.1, game.SmallBlind);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line02_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "$25 USD NL Texas Hold'em - Thursday, April 1, 20:13:44 MSKS 2013" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new DateTime(2013, 4, 1, 20, 13, 44), game.StartTime);
			Assert.AreEqual(0.25, game.BigBlind);
			Assert.AreEqual(0.1, game.SmallBlind);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line02_3()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "$25 USD NL Texas Hold'em - Wednesday, April 10, 19:56:55 MSKS 2013" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new DateTime(2013, 4, 10, 19, 56, 55), game.StartTime);
			Assert.AreEqual(0.25, game.BigBlind);
			Assert.AreEqual(0.1, game.SmallBlind);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line03()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] {"Table Zinder (Real Money)"};
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type2_Line03_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Table North York (Real Money)" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type2_Line04()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2 is the button" };
			var game = parser.ParseLines(input).Single();

			//Assert.AreEqual(Position.BU, game.GetChair(2).Position);
			//Assert.AreEqual(Position.BU, game.GetPosition(2));
			//Assert.AreEqual(true, game.GetChair(2).IsDealer);
			Assert.AreEqual(2, game.DealerChair);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line05()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Total number of players : 8/9 " };
			var game = parser.ParseLines(input).Single();

			
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: Angelika1988 ( $27.08 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Angelika1988", game.GetChair(1).Name);
			Assert.AreEqual(27.08, game.GetChair(1).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: Angelika 1988 ( $27.08 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Angelika 1988", game.GetChair(1).Name);
			Assert.AreEqual(27.08, game.GetChair(1).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 5: oezer5 ( $25 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("oezer5", game.GetChair(5).Name);
			Assert.AreEqual(25.0, game.GetChair(5).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06_3()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 5: oezer5 ( $25 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("oezer5", game.GetChair(5).Name);
			Assert.AreEqual(25.0, game.GetChair(5).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06_4()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2: Sh.A.O. ( $25 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Sh.A.O.", game.GetChair(2).Name);
			Assert.AreEqual(25.0, game.GetChair(2).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06_5()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2: _-Pavel-_ ( $25 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("_-Pavel-_", game.GetChair(2).Name);
			Assert.AreEqual(25.0, game.GetChair(2).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line06_6()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2: _-Pavel-_ ( $25 USD )" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("_-Pavel-_", game.GetChair(2).Name);
			Assert.AreEqual(25.0, game.GetChair(2).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line07()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx ( $25 USD )",
				"Rylanx posts small blind [$0.10 USD]."
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.1, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line07_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx ( $25 USD )",
				"Rylanx posts small blind [$0.10 USD]."
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.1, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line08_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Hsrhsrhsr52 ( $25 USD )",
				"Hsrhsrhsr52 posts big blind [$0.25 USD]."
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Hsrhsrhsr52", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.25, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line08_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: kellermanes ( $25 USD )",
				"kellermanes posts big blind + dead [0,35 $]."
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("kellermanes", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.35, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line08_3()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "** Dealing down cards **" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type2_Line09()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx ( $25 USD )",
				"Dealt to Rylanx [  5d 5h ]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("5d, 5h"), game.GetChair("Rylanx").Hand);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line10()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: oezer5 ( $25 USD )",
				"oezer5 folds"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("oezer5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Fold, game.Actions.Single().Action);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line11()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: iHell ( $25 USD )",
				"iHell raises [$0.75 USD]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("iHell", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Raise, game.Actions.Single().Action);
			Assert.AreEqual(0.75, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line12()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: iHell ( $25 USD )",
				"iHell calls [$1.75 USD]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("iHell", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Call, game.Actions.Single().Action);
			Assert.AreEqual(1.75, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line13()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx ( $25 USD )",
				"Rylanx bets [$3 USD]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Bet, game.Actions.Single().Action);
			Assert.AreEqual(3.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line14()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx ( $25 USD )",
				"Rylanx is all-In  [$16.46 USD]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Allin, game.Actions.Single().Action);
			Assert.AreEqual(16.46, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line15()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: dragoslav111 ( $25 USD )",
				"dragoslav111 checks"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("dragoslav111", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Check, game.Actions.Single().Action);
		}



		[TestMethod]
		public void TestParseGame_Type2_Line16()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "** Dealing Flop ** [ 4d, Ks, 3d ]" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("4d, Ks, 3d"), game.Board);
			Assert.AreEqual(BetRound.Flop, game.BetRound);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line17()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"** Dealing Flop ** [ 4c, Ks, 3d ]",
				"** Dealing Turn ** [ 4d ]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("4d, Ks, 3d, 4c"), game.Board);
			Assert.AreEqual(BetRound.Turn, game.BetRound);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line18()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"** Dealing Flop ** [ 4c, Ks, 3d ]",
				"** Dealing Turn ** [ 4d ]",
				"** Dealing River ** [ 2h ]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("4d, Ks, 3d, 4c, 2h"), game.Board);
			Assert.AreEqual(BetRound.River, game.BetRound);
		}

		[TestMethod]
		public void TestParseGame_Type2_Line19_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Rylanx shows [ 5d, 5h ]two pairs, Fives and Threes." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line20_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "iHell wins $53.37 USD from the main pot with two pairs, Aces and Kings." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type1_Line20_1_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Hummeren347 wins $7.77 USD from the side pot 1 with three of a kind, Fives." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line20_2_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "alexxutzu1992 wins $36.81 USD" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type2_Line21_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Rylanx will be using his time bank for this hand." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line22_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "marcische doesn't show [ As, 9c ]high card Ace." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line23_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Plutus5 has joined the table." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line24_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "zaxosdimi did not respond in time" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line25_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "alexxutzu1992 does not show cards." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line26_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "AnDrIx58 has left the table." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line27_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "irek40pl is sitting out" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line28_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Volchara21 could not respond in time.(disconnected)" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line29_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Your time bank will be activated in 6 secs. If you do not want it to be used, please act now." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line30_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "iHell is disconnected. We will wait for iHell to reconnect for a maximum of 24 seconds." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line31_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "nordars has been reconnected and has 20 seconds to act." };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type2_Line32_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Bekilly is reconnected and has 20 seconds to respond." };
			var game = parser.ParseLines(input).Single();

		}

		//[TestMethod]
		//public void TestParseGame_Type2_Line33_skip()
		//{
		//	var parser = new HoldemManagerLogParser();
		//	var input = new[] { "AnDrIx58: some chat message" };
		//	var game = parser.ParseLines(input).Single();

		//}
	}
}
