﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Parser.Tests.HoldemManagerLogParserTests
{
	public partial class HoldemManagerLogParserTest
	{
		[TestMethod]
		public void TestParseGame_Type3_Line01()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "PokerStars Game #000094119102: Hold'em No Limit ($0.1/$0.2 USD) - 2015/03/30 08:47:49" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new DateTime(2015, 3, 30, 8, 47, 49), game.StartTime);
			Assert.AreEqual("000094119102", game.GameNumber);
			Assert.AreEqual(0.1, game.SmallBlind);
			Assert.AreEqual(0.2, game.BigBlind);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line02()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Table '*Jackpot* 29  2791' 6-max Seat #2 is the button" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(2, game.DealerChair);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line03()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2: duru_duru ($3.76 in chips) " };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(3.76, game.GetChair(2).Balance);
			Assert.AreEqual("duru_duru", game.GetChair(2).Name);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line04()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 4: Rylanx ($20 in chips) ",
				"Rylanx: posts big blind $0.2 "
			};
			var game = parser.ParseLines(input).Single();

			var action = game.Actions.Single();

			Assert.AreEqual(4, action.Chair.ChairNumber);
			Assert.AreEqual(PlayerActionEnum.BigBlind, action.Action);
			Assert.AreEqual(0.2, action.Amount);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line04_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 4: TRY_HRD_FISHH ($13.94 in chips) ",
				"TRY_HRD_FISHH: folds"
			};
			var game = parser.ParseLines(input).Single();

			var action = game.Actions.Single();

			Assert.AreEqual(4, action.Chair.ChairNumber);
			Assert.AreEqual(PlayerActionEnum.Fold, action.Action);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line04_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 6: kostia5 ($7.45 in chips) ",
				"kostia5: raises 0.54 to $0.64"
			};
			var game = parser.ParseLines(input).Single();

			var action = game.Actions.Single();

			Assert.AreEqual(6, action.Chair.ChairNumber);
			Assert.AreEqual(PlayerActionEnum.Raise, action.Action);
			Assert.AreEqual(0.64, action.Amount);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line05()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 4: Rylanx ($20 in chips) ",
				"Dealt to Rylanx [7d 7c]"
			};
			var game = parser.ParseLines(input).Single();

			var chair = game.GetChair(4);

			Assert.AreEqual(new CardMask("7d, 7c"), chair.Hand);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line05_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 6: kostia5 ($7.45 in chips) ",
				"kostia5: shows [6c Ad] (a pair of, Aces)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("6c Ad"), game.GetChair("kostia5").Hand);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line06()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "*** FLOP *** [Qh 2h Kc]" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("Qh 2h Kc"), game.Board);
		}

		[TestMethod]
		public void TestParseGame_Type3_Line07()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Uncalled bet ($0.4) returned to limon777" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line08()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "*** HOLE CARDS ***" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line09()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "*** SHOW DOWN ***" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line10()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "*** SUMMARY ***" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line11()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Total pot $2.43 | Rake 0.14" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line11_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Total pot $16.65 Main pot $8.83. Side pot $7.82 | Rake 0.98" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line11_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Total pot $15.72 Main pot $7.25. Side pot #1 $1.53. Side pot #2 $6.94 | Rake 0.91" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line11_3()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Total pot ErrorPot | Rake 0" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line12()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Board [2c As 4s Jh 5h]" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line12_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Board []" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line13()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2: tonki_-_ (small blind) folded before Flop" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line14()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 3: ucnobi1977 (big blind) mucked" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line15()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 5: Rylanx folded on the Flop" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line16()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 6: kostia5  (button) showed [6c Ad] and won ($2.29) with a pair of, Aces" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line16_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: kastiladato (small blind) showed [8s Ac] and lost with a flush, Ace high" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line17()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 5: Rylanx folded before Flop (didn't bet)" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line18()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 6: mishka88888 (big blind) collected ($1.35)" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line18_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "kostia5 collected $2.29 from pot" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line18_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Rylanx collected $7.36 from side pot-1" };
			var game = parser.ParseLines(input).Single();

		}

		[TestMethod]
		public void TestParseGame_Type3_Line19()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "ucnobi1977: doesn't show hand" };
			var game = parser.ParseLines(input).Single();

		}
	}
}
