﻿using System;
using System.Linq;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Parser.Tests.HoldemManagerLogParserTests
{
	public partial class HoldemManagerLogParserTest
	{
		[TestMethod]
		public void TestParseGame_Type1_Line01()
		{
			var parser = new HoldemManagerLogParser();
			var input = new []{ "Game started at: 2013/4/29 17:39:10" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new DateTime(2013, 4, 29, 17, 39, 10), game.StartTime);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line02()
		{
			var parser = new HoldemManagerLogParser();
			var input = new []{ "Game ID: 159243907 0.10/0.25 Tantalite   (JP) - 6 (Hold'em)" };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("159243907", game.GameNumber);
			Assert.AreEqual(0.1, game.SmallBlind);
			Assert.AreEqual(0.25, game.BigBlind);
			Assert.AreEqual("Tantalite   (JP) - 6 (Hold'em)", game.Title);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line02_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new []{"Game ID: 159243907 1/2 Tantalite   (JP) - 6 (Hold'em)"};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("159243907", game.GameNumber);
			Assert.AreEqual(1.0, game.SmallBlind);
			Assert.AreEqual(2.0, game.BigBlind);
			Assert.AreEqual("Tantalite   (JP) - 6 (Hold'em)", game.Title);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line03()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1 is the button" };
			var game = parser.ParseLines(input).Single();

			//Assert.AreEqual(Position.BU, game.GetChair(1).Position);
			//Assert.AreEqual(Position.BU, game.GetPosition(1));
			//Assert.AreEqual(true, game.GetChair(1).IsDealer);
			Assert.AreEqual(1, game.DealerChair);
		}


		[TestMethod]
		public void TestParseGame_Type1_Line04()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: GangRaked (94.05)." };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("GangRaked", game.GetChair(1).Name);
			Assert.AreEqual(94.05, game.GetChair(1).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line04_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: GangRaked (94)." };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("GangRaked", game.GetChair(1).Name);
			Assert.AreEqual(94, game.GetChair(1).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line04_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: Gang Raked (94.05)." };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Gang Raked", game.GetChair(1).Name);
			Assert.AreEqual(94.05, game.GetChair(1).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line04_3()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 4: *TheGrinder* (14.28)." };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("*TheGrinder*", game.GetChair(4).Name);
			Assert.AreEqual(14.28, game.GetChair(4).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line04_4()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 1: metzler2013 (9.27) (chop is on)." };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("metzler2013", game.GetChair(1).Name);
			Assert.AreEqual(9.27, game.GetChair(1).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line04_5()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Seat 2: gloryAA (101.18)." };
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("gloryAA", game.GetChair(2).Name);
			Assert.AreEqual(101.18, game.GetChair(2).Balance);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line05()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr143550 (100).",
				"Player bkr143550 has small blind (0.10)"
			};
			var game = parser.ParseLines(input).Single();

			//Assert.AreEqual("bkr143550", game.Actions.Single().PlayerName);
			Assert.AreEqual("bkr143550", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.10, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line05_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr143550 (100).",
				"Player bkr143550 has small blind (10)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("bkr143550", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, game.Actions.Single().Action);
			Assert.AreEqual(10.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line05_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr 143550 (100).",
				"Player bkr 143550 has small blind (10)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("bkr 143550", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, game.Actions.Single().Action);
			Assert.AreEqual(10.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line06()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx (100).",
				"Player Rylanx has big blind (0.25)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.25, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line06_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx (100).",
				"Player Rylanx has big blind (25)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, game.Actions.Single().Action);
			Assert.AreEqual(25.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line06_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylan x (100).",
				"Player Rylan x has big blind (0.25)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylan x", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, game.Actions.Single().Action);
			Assert.AreEqual(0.25, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line07()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr143550 (100).",
				"Player bkr143550 received a card."
			};
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line07_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr1 43550 (100).",
				"Player bkr1 43550 received a card."
			};
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line07_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Totti! (100).",
				"Player Totti! received a card."
			};
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line08()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx (100).",
				"Player Rylanx received card: [5c]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("5c"), game.GetChair("Rylanx").Hand);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line08_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylan x (100).",
				"Player Rylan x received card: [5c]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("5c"), game.GetChair("Rylan x").Hand);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line08_2()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylan x (100).",
				"Player Rylan x received card: [5c]",
				"Player Rylan x received card: [5d]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(new CardMask("5c 5d"), game.GetChair("Rylan x").Hand);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: butroneador (100).",
				"Player butroneador folds"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("butroneador", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Fold, game.Actions.Single().Action);
			Assert.AreEqual(0.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_01()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: butro neador (100).",
				"Player butro neador folds"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("butro neador", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Fold, game.Actions.Single().Action);
			Assert.AreEqual(0.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_02()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: pureluck5 (100).",
				"Player pureluck5 raises (0.75)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("pureluck5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Raise, game.Actions.Single().Action);
			Assert.AreEqual(0.75, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_03()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: pureluck5 (100).",
				"Player pureluck5 raises (7)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("pureluck5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Raise, game.Actions.Single().Action);
			Assert.AreEqual(7.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_04()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: pure luck5 (100).",
				"Player pure luck5 raises (7)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("pure luck5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Raise, game.Actions.Single().Action);
			Assert.AreEqual(7.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_05()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr143550 (100).",
				"Player bkr143550 calls (0.65)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("bkr143550", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Call, game.Actions.Single().Action);
			Assert.AreEqual(0.65, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_06()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr143550 (100).",
				"Player bkr143550 calls (5)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("bkr143550", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Call, game.Actions.Single().Action);
			Assert.AreEqual(5.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_07()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: bkr 143550 (100).",
				"Player bkr 143550 calls (0.65)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("bkr 143550", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Call, game.Actions.Single().Action);
			Assert.AreEqual(0.65, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_08()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx (100).",
				"Player Rylanx checks"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Check, game.Actions.Single().Action);
			Assert.AreEqual(0.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_09()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylan x (100).",
				"Player Rylan x checks"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylan x", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Check, game.Actions.Single().Action);
			Assert.AreEqual(0.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_10()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: pureluck5 (100).",
				"Player pureluck5 bets (2.2)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("pureluck5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Bet, game.Actions.Single().Action);
			Assert.AreEqual(2.2, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_11()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: pureluck5 (100).",
				"Player pureluck5 bets (2)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("pureluck5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Bet, game.Actions.Single().Action);
			Assert.AreEqual(2.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_12()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: pure luck5 (100).",
				"Player pure luck5 bets (2)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("pure luck5", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Bet, game.Actions.Single().Action);
			Assert.AreEqual(2.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_13()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx (100).",
				"Player Rylanx allin (28.62)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Allin, game.Actions.Single().Action);
			Assert.AreEqual(28.62, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_14()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylanx (100).",
				"Player Rylanx allin (28)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylanx", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Allin, game.Actions.Single().Action);
			Assert.AreEqual(28.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_15()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: Rylan x (100).",
				"Player Rylan x allin (28)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("Rylan x", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Allin, game.Actions.Single().Action);
			Assert.AreEqual(28.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_16()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: LuNaTicFriNge (100).",
				"Player LuNaTicFriNge caps (4)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("LuNaTicFriNge", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Cap, game.Actions.Single().Action);
			Assert.AreEqual(4.0, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line09_17()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: 3puppies (100).",
				"Player 3puppies straddle (0.50)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("3puppies", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Straddle, game.Actions.Single().Action);
			Assert.AreEqual(0.5, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line10()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: KW11 (100).",
				"Player KW11 posts (0.10) as a dead bet"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("KW11", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Post, game.Actions.Single().Action);
			Assert.AreEqual(0.1, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line10_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"Seat 1: KW11 (100).",
				"Player KW11 posts (0.25)"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual("KW11", game.Actions.Single().Chair.Name);
			Assert.AreEqual(PlayerActionEnum.Post, game.Actions.Single().Action);
			Assert.AreEqual(0.25, game.Actions.Single().Amount);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line12()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"*** FLOP ***: [7d 2h 5h]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(BetRound.Flop, game.BetRound);
			Assert.AreEqual(new CardMask("7d, 2h, 5h"), game.Board);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line16()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"*** TURN ***: [7d 2h 5h] [4s]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(BetRound.Turn, game.BetRound);
			Assert.AreEqual(new CardMask("7d, 2h, 5h, 4s"), game.Board);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line16_1()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"*** FLOP ***: [10d 10s 10c]",
				"*** TURN ***: [10d 10s 10c] [10h]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(BetRound.Turn, game.BetRound);
			Assert.AreEqual(new CardMask("Td Tc Ts Th"), game.Board);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line17()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[]
			{
				"*** FLOP ***: [7d 2h 5h]",
				"*** TURN ***: [7d 2h 5h] [4s]",
				"*** RIVER ***: [7d 2h 5h 4s] [3h]"
			};
			var game = parser.ParseLines(input).Single();

			Assert.AreEqual(BetRound.River, game.BetRound);
			Assert.AreEqual(new CardMask("7d 2h 5h 4s 3h"), game.Board);
		}

		[TestMethod]
		public void TestParseGame_Type1_Line19_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "------Summary------" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line19_1_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "------ Summary ------" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line21_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Pot: 68.24. Rake 3. BBJ 0.25" };
			var game = parser.ParseLines(input).Single();
		}


		[TestMethod]
		public void TestParseGame_Type1_Line22_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Pot: 68.24. Rake 3. BBJ 0.25" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line23_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Pot: 57.36. Rake 2" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line24_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Board: [7d 2h 5h 4s 3h]" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line25_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player Bluffing Every Hand does not show cards.Bets: 0. Collects: 0. Wins: 0." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line27_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player Rylanx shows: One pair of Js [Jh Jc]. Bets: 25.95. Collects: 0. Loses: 25.95." };
			var game = parser.ParseLines(input).Single();
		}
		
		[TestMethod]
		public void TestParseGame_Type1_Line28_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "*Player cricket170 shows: One pair of As [As Ad]. Bets: 25.95. Collects: 49.15. Wins: 23.20." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line29_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "*Player johnnypanic mucks (does not show cards). Bets: 11.25. Collects: 21.37. Wins: 10.12." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line29_1_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player Rylanx mucks (does not show: [Qh Qc]). Bets: 9.75. Collects: 0. Loses: 9.75." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line30_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Game ended at: 2013/5/6 3:51:40" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line31_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Uncalled bet (0.03) returned to cricket170" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line32_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player johnnypanic mucks cards" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line33_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player LuNaTicFriNge is timed out." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line34_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player Trephine sitting out" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line35_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Player RaTaTatTat wait BB" };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line36_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "Game #12869299350 starts." };
			var game = parser.ParseLines(input).Single();
		}

		[TestMethod]
		public void TestParseGame_Type1_Line37_skip()
		{
			var parser = new HoldemManagerLogParser();
			var input = new[] { "#Game No : 12869299350" };
			var game = parser.ParseLines(input).Single();
		}
	}
}
