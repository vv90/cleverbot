﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;

namespace CleverBot.Brain
{
	public class PreflopLearnerResult
	{
		public double Raise { get; set; }
		public double Call { get; set; }
		public double Fold { get; set; }

		public PreflopLearnerResult(double[] resultParameters)
		{
			if(resultParameters.Length != 3)
				throw new ArgumentException("Invalid parameters array length");

			Raise = resultParameters[0];
			Call = resultParameters[1];
			Fold = resultParameters[2];
		}

		public PlayerActionEnum SelectAction()
		{
			if (Raise > Call && Raise > Fold)
				return PlayerActionEnum.Raise;
			else if (Call > Fold)
				return PlayerActionEnum.Call;
			else
				return PlayerActionEnum.Fold;
		}
	}

	public class PreflopLearnerParameter
	{
		public string Name { get; set; }
		public Func<Game, double> NormalizationFunction { get; set; }

		public PreflopLearnerParameter(string name, Func<Game, double> normalizationFunction)
		{
			Name = name;
			NormalizationFunction = normalizationFunction;
		}
	}

	public class PreflopLearner
	{
		private PreflopLearnerParameter[] _parameters;
		private NeuralNetwork _neuralNetwork;

		public int MaxEpochs { get; set; }
		public double LearnRate { get; set; }

		public double Momentum { get; set; }

		public int NumHiddenLayers { get; private set; }

		public PreflopLearner(params PreflopLearnerParameter[] parameters)
			: this(parameters.Length, parameters)
		{ }

		public PreflopLearner(int numHiddenLayers, params PreflopLearnerParameter[] parameters)
			: this(numHiddenLayers, 80, 0.05, 0.01, parameters)
		{ }

		public PreflopLearner(int numHiddenLayers, int maxEpochs, double learnRate, double momentum,
			params PreflopLearnerParameter[] parameters)
		{
			MaxEpochs = maxEpochs;
			LearnRate = learnRate;
			Momentum = momentum;
			NumHiddenLayers = numHiddenLayers;
			_parameters = parameters;
			_neuralNetwork = new NeuralNetwork(parameters.Length, NumHiddenLayers, 3);
		}

		private double[] GetParameterValues(Game game)
		{
			var normalizedParameters = _parameters.Select(f => f.NormalizationFunction(game)).ToArray();

			return normalizedParameters;
		}

		//private Dictionary<Game, PlayerAction> PrepareGames(Game[] games)
		//{
		//	var preparedGames = new Dictionary<Game, PlayerAction>();

		//	for (int i = 0; i < games.Length; i++)
		//	{
		//		var heroChair = games[i].GetHeroChair();
		//		var heroActions = games[i].Actions.Where(a =>
		//			a.Chair.ChairNumber == heroChair.ChairNumber && a.BetRound == BetRound.Preflop);

		//		// generate learning input for each hero action in the game
		//		foreach (var ha in heroActions)
		//		{
		//			PlayerAction currentHeroAction = ha;
		//			var actionsBeforeHero = games[i].Actions.TakeWhile(a => a != currentHeroAction).ToList();
		//			var gameStateBeforeHeroAction = games[i].Clone();
		//			gameStateBeforeHeroAction.Actions = actionsBeforeHero;
		//			//gameStateBeforeHeroAction.Actions.Add(currentHeroAction);
		//			preparedGames.Add(gameStateBeforeHeroAction, currentHeroAction);
		//		}
		//	}

		//	return preparedGames;
		//}

		private double[] NormalizeHeroAction(PlayerAction action)
		{
			var actions = new double[3];

			switch (action.Action)
			{
				case PlayerActionEnum.Bet:
				case PlayerActionEnum.Raise:
				case PlayerActionEnum.Cap:
				case PlayerActionEnum.Allin:
					actions[0] = 1.0;
					break;
				case PlayerActionEnum.Call:
					actions[1] = 1.0;
					break;
				case PlayerActionEnum.Fold:
				case PlayerActionEnum.Check:
				case PlayerActionEnum.SmallBlind:
				case PlayerActionEnum.BigBlind:
				case PlayerActionEnum.Straddle:
				case PlayerActionEnum.Post:
					actions[2] = 1.0;
					break;
			}

			return actions;
		}

		private double[][] GenerateTrainData(Game[] games)
		{
			var preparedGames = GameExtentions.PrepareGames(games);

			var trainData = preparedGames
				.Select(g => GetParameterValues(g.Key).Concat(NormalizeHeroAction(g.Value)).ToArray())
				.ToArray();


			return trainData;
		}

		public void Train(IEnumerable<Game> games)
		{
			double[][] trainData = GenerateTrainData(games.ToArray());

			_neuralNetwork.Train(trainData, MaxEpochs, LearnRate, Momentum);
		}

		public PreflopLearnerResult ComputeOutputs(Game game)
		{
			var returnParameters = _neuralNetwork.ComputeOutputs(GetParameterValues(game));

			return new PreflopLearnerResult(returnParameters);
		}

		public double Accuracy(IEnumerable<Game> games)
		{
			double[][] testData = GenerateTrainData(games.ToArray());

			return _neuralNetwork.Accuracy(testData);
		}

		public double[] GetWeights()
		{
			return _neuralNetwork.GetWeights();
		}

		public void SetWeights(double[] weights)
		{
			_neuralNetwork.SetWeights(weights);
		}

		public void LogResults(IEnumerable<Game> games, string logFileName, bool logFailedOnly = false)
		{
			var preparedGames = GameExtentions.PrepareGames(games.ToArray());


			using (var file = new StreamWriter(File.Create(logFileName)))
			{

				foreach (var g in preparedGames)
				{
					var game = g.Key;
					var heroAction = g.Value;

					

					var gameParameters = _parameters.Zip(GetParameterValues(game), 
						(param, val) => new KeyValuePair<string, double>(param.Name, val)).ToArray();
					var heroActionParameters = new PreflopLearnerResult(NormalizeHeroAction(heroAction));

					

					var output = ComputeOutputs(game);
					var isOutputCorrect = heroActionParameters.SelectAction() == output.SelectAction();

					if (!logFailedOnly || !isOutputCorrect)
					{
						file.WriteLine("\n---- New Game {0} ----", game.GameNumber);
						foreach (var line in game.CreateLog())
							file.WriteLine(line);

						file.WriteLine();

						foreach (var action in game.Actions)
							file.WriteLine(action.ToString());

						file.WriteLine(string.Join(", ", gameParameters.Select(p => string.Format("{0}: {1:0.00}", p.Key, p.Value))));

						file.WriteLine("Expected Raise: {0:0.00}, Call: {1:0.00}, Fold: {2:0.00}",
							heroActionParameters.Raise, heroActionParameters.Call, heroActionParameters.Fold);


						file.WriteLine("Actual   Raise: {0:0.00}, Call: {1:0.00}, Fold: {2:0.00}",
							output.Raise, output.Call, output.Fold);
					}
				}
			}
		}
	}
}
