﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Common.Utilities;

namespace CleverBot.Brain
{
	public static class GameExtentions
	{
		public static Dictionary<Game, PlayerAction> PrepareGames(Game[] games, params BetRound[] betRounds)
		{
			var preparedGames = new Dictionary<Game, PlayerAction>();

			for (int i = 0; i < games.Length; i++)
			{
				var heroChair = games[i].GetHeroChair();
				var heroActions = games[i].Actions.Where(a =>
					a.Chair.ChairNumber == heroChair.ChairNumber && betRounds.Contains(a.BetRound));

				// generate learning input for each hero action in the game
				foreach (var ha in heroActions)
				{
					PlayerAction currentHeroAction = ha;

					if (currentHeroAction.Action == PlayerActionEnum.BigBlind ||
					    currentHeroAction.Action == PlayerActionEnum.SmallBlind)
					{
						continue;
					}

					var actionsBeforeHero = games[i].Actions.TakeWhile(a => a != currentHeroAction).ToList();
					var gameStateBeforeHeroAction = games[i].Clone();
					gameStateBeforeHeroAction.Actions = actionsBeforeHero;
					
					preparedGames.Add(gameStateBeforeHeroAction, currentHeroAction);
				}
			}

			return preparedGames;
		}

		public static double NormalizedLastOpponentAction(this Game game)
		{
			var lastActionScore = 0;
			var maxAmount = 0.0;
			foreach (var a in game.Actions)
			{
				switch (a.Action)
				{
					case PlayerActionEnum.Fold:
					case PlayerActionEnum.Check:
					case PlayerActionEnum.SmallBlind:
					case PlayerActionEnum.BigBlind:
					case PlayerActionEnum.Straddle:
					case PlayerActionEnum.Post:
						break;
					case PlayerActionEnum.Call:
						if (lastActionScore == 0)
						{
							lastActionScore = 1;
						}
						break;
					case PlayerActionEnum.Bet:
					case PlayerActionEnum.Raise:
					case PlayerActionEnum.Cap:
						if (lastActionScore == 0)
						{
							lastActionScore = 1;
						}
						lastActionScore += 1;
						break;
					case PlayerActionEnum.Allin:
						if (maxAmount < a.Amount)
						{
							lastActionScore += 1;
						}
						break;
					default:
						throw new NotImplementedException();
				}

				if (maxAmount < a.Amount)
				{
					maxAmount = a.Amount;
				}
			}

			var parameterValue = (double) lastActionScore;

			return parameterValue;
		}

		public static double NormalizedHand(this Game game)
		{
			var hand = game.GetHeroChair().Hand;
			var handClass = HandClassification.ClassifyHand(hand);
			var parameterValue = (double)handClass / 100;
			return parameterValue;
		}

		public static double NormalizedPosition(this Game game)
		{
			return game.DealerOffset(game.HeroChair);
		}

		public static double NormalizedIsPocketPair(this Game game)
		{
			var hand = game.GetHeroChair().Hand;

			return hand.IsAllSameRank() ? 1 : 0;
		}
	}
}
