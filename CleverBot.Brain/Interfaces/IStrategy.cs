﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.Brain.Interfaces
{
	public interface IStrategy
	{
		PlayerAction Think(Game currentGame);
	}
}
