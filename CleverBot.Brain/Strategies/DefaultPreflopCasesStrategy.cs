﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;

namespace CleverBot.Brain.Strategies
{
	public class DefaultPreflopCasesStrategy : CasesStrategyBase, IStrategy
	{
		private struct PreflopRangesSelection
		{
			public string ThreeBet;
			public string Raise;
			public string Call15;
			public string FourBet;
			public string FiveBet;
			public string SixBet;
		}

		private PreflopRangesSelection SelectPreflopRanges(Game currentGame)
		{
			var heroChair = currentGame.GetHeroChair();
			var heroPosition = PositionUtils.GetPosition(
				currentGame.Chairs.Count, currentGame.DealerOffset(heroChair.ChairNumber));
			var hand = heroChair.Hand;

			var lastAgressiveAction = currentGame.Actions.LastOrDefault(a => a.Action == PlayerActionEnum.Raise) 
				?? currentGame.Actions.LastOrDefault();

			var opponentChair = lastAgressiveAction == null 
				? null 
				: lastAgressiveAction.Chair;

			if(opponentChair == null)
				throw new NotImplementedException();

			var opponentPosition = PositionUtils.GetPosition(
				currentGame.Chairs.Count, 
				currentGame.DealerOffset(opponentChair.ChairNumber));

			var ranges = new PreflopRangesSelection();

			ranges.FourBet = "QQ+, AK";
			ranges.FiveBet = "KK+";
			ranges.SixBet = "QQ+, AK";

			switch (heroPosition)
			{
				case Position.BU:
					switch (opponentPosition)
					{
						case Position.BU:
							break;

						case Position.CO:
							ranges.ThreeBet = "TT+, AQs+, AQo+";
							ranges.Raise = "A2s+, A7o+, 22+, 45s+, 75s-J9s, K9s+, Q9s+, Broadway";
							ranges.Call15 = "22-99";
							break;
						case Position.MP:
						case Position.UTG:
						case Position.BB:
						case Position.SB:
							ranges.ThreeBet = "JJ+, AQs+, AQo+";
							ranges.Raise = "A2s+, A7o+, 22+, 45s+, 75s-J9s, K9s+, Q9s+, Broadway";
							ranges.Call15 = "22-TT";

							break;
					}
					break;
				case Position.CO:
					switch (opponentPosition)
					{
						case Position.CO:
							break;

						case Position.BU:
						case Position.MP:
						case Position.UTG:
						case Position.BB:
						case Position.SB:
							ranges.ThreeBet = "JJ+, AQo+, AQs+";
							ranges.Raise = "A2s+, A9o+, 22+, 45s+, Broadway";
							ranges.Call15 = "22-TT";

							break;
					}
					break;
				case Position.MP:
					switch (opponentPosition)
					{
						case Position.MP:
							break;

						case Position.BU:
						case Position.CO:
						case Position.UTG:
						case Position.BB:
						case Position.SB:
							ranges.ThreeBet = "JJ+, AQo+, AQs+";
							ranges.Raise = "ATs+, ATo+, 22+, QJs+, KJs+, KQo+";
							ranges.Call15 = "22-TT";

							break;
					}
					break;
				case Position.UTG:
					switch (opponentPosition)
					{
						case Position.UTG:
							break;

						case Position.BU:
						case Position.CO:
						case Position.MP:
						case Position.BB:
						case Position.SB:
							ranges.Raise = "ATs+, AJo+, 22+, KQs";

							break;
					}
					break;
				case Position.BB:
				case Position.SB:
					switch (opponentPosition)
					{
						case Position.MP:
						case Position.UTG:
							ranges.ThreeBet = "JJ+, AQs+, AQo+";
							ranges.Raise = "A2s+, A9o+, 22+, 45s+, Broadway";
							ranges.Call15 = "22-TT";
							break;

						case Position.BB:
						case Position.BU:
						case Position.CO:
						case Position.SB:
							ranges.ThreeBet = "TT+, AQs+, AQo+";
							ranges.Raise = "A2s+, A9o+, 22+, 45s+, Broadway";
							ranges.Call15 = "22-99";

							break;
					}
					break;
			}

			return ranges;
		}

		private bool CheckRange(CardMask hand, string rangeString)
		{
			if (string.IsNullOrWhiteSpace(rangeString))
				return false;

			var range = HandRange.Parse(rangeString);
			return range.IndexOf(hand) > -1;
		}

		public PlayerAction Think(Game currentGame)
		{
			var ranges = SelectPreflopRanges(currentGame);

			var heroChair = currentGame.GetHeroChair();
			var hand = heroChair.Hand;

			var lastAction = currentGame.Actions.Last(a => 
				a.Action != PlayerActionEnum.Fold);

			var numberOfBets = currentGame.Actions
				.Where(a => a.BetRound == BetRound.Preflop)
				.Count(a => a.IsAgressive());

			var act = currentGame.Chairs
				.Select(c => currentGame.Actions.LastOrDefault(a => 
					a.Chair.ChairNumber == c.ChairNumber &&
					a.Chair.ChairNumber != currentGame.HeroChair &&
					a != lastAction &&
					a.Amount > 0))
				.Where(a => a != null).ToArray();

			var pot = currentGame.Chairs
				.Select(c => currentGame.Actions.LastOrDefault(a => 
					a.Chair.ChairNumber == c.ChairNumber &&
					a.Chair.ChairNumber != currentGame.HeroChair &&
					a != lastAction &&
					a.Amount > 0))
				.Where(a => a != null)
				.Sum(a => a.Amount);

			var raiseAmount = lastAction.Amount*3 + pot;

			var effectiveStack = Math.Min(
				currentGame.Chairs
					.Where(c => c.ChairNumber != currentGame.HeroChair)
					.Max(c => c.Balance), 
				heroChair.Balance);

			if(numberOfBets > 5 && CheckRange(hand, ranges.SixBet))
				return new PlayerAction(heroChair, PlayerActionEnum.Allin);

			if(numberOfBets == 5 && CheckRange(hand, ranges.SixBet))
				return new PlayerAction(heroChair, PlayerActionEnum.Allin);


			if (numberOfBets == 4 && CheckRange(hand, ranges.FiveBet))
				return new PlayerAction(heroChair, PlayerActionEnum.Allin);

			if (numberOfBets == 3 && CheckRange(hand, ranges.FourBet))
				return lastAction.RaiseAction(heroChair, raiseAmount);

			if (numberOfBets == 2 && CheckRange(hand, ranges.ThreeBet))
				return lastAction.RaiseAction(heroChair, raiseAmount);

			if (numberOfBets == 2 && CheckRange(hand, ranges.Call15) &&
				effectiveStack >= currentGame.BigBlind * 15)
				return lastAction.CallAction(heroChair);

			if (numberOfBets == 1 && CheckRange(hand, ranges.Raise))
				return lastAction.RaiseAction(heroChair, raiseAmount);


			
			return new PlayerAction(heroChair, PlayerActionEnum.Fold);
		}
	}
}
