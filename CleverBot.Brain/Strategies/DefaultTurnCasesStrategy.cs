﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.PokerEvalImport;

namespace CleverBot.Brain.Strategies
{
	public class DefaultTurnCasesStrategy : CasesStrategyBase, IStrategy
	{
		private struct TurnHandTypesSelection
		{
			public string Bet;
			public string Raise;
			public string Call;
			public string CallUnderBet;
			public string CallOverBet;
			public string CallMinRaise;
			public string ThreeBet;
		}

		public PlayerAction Think(Game currentGame)
		{
			var heroChair = currentGame.GetHeroChair();
			var handType = EvalType(heroChair.Hand, currentGame.Board);
			var selectedHandTypes = SelectHandTypes(currentGame);

			var lastAction = currentGame.Actions.LastOrDefault(a => 
				a.BetRound == BetRound.Turn && 
				a.IsAgressive());

			var numberOfBets = currentGame.Actions
				.Where(a => a.BetRound == BetRound.Turn)
				.Count(a => a.IsAgressive());

			var pot = currentGame.Chairs.Select(c => currentGame.Actions.LastOrDefault(a =>
					a.Chair.ChairNumber == c.ChairNumber && a != lastAction))
				.Where(a => a != null)
				.Sum(a => a.Amount);

			var betAmount = BetAmount(currentGame);

			if (lastAction == null)
			{
				return CheckHand(handType, selectedHandTypes.Bet)
					? new PlayerAction(heroChair, PlayerActionEnum.Bet, betAmount, BetRound.Turn)
					: new PlayerAction(heroChair, PlayerActionEnum.Check, 0, BetRound.Turn);
			}

			if (numberOfBets >= 2 && CheckHand(handType, selectedHandTypes.ThreeBet))
			{
				return lastAction.RaiseAction(heroChair, betAmount);
			}

			if (numberOfBets == 2 && CheckHand(handType, selectedHandTypes.CallMinRaise))
			{
				var secondToLastAction = currentGame.Actions
					.Where(a => a.BetRound == BetRound.Turn)
					.Last(a => a.IsAgressive() && a != lastAction);

				if (Math.Abs(lastAction.Amount - secondToLastAction.Amount*2) < 0.001)
				{
					return lastAction.CallAction(heroChair);
				}
			}

			if (numberOfBets == 1 && CheckHand(handType, selectedHandTypes.Raise))
			{
				return lastAction.RaiseAction(heroChair, betAmount);
			}

			if (numberOfBets == 1 && lastAction.Amount > pot && CheckHand(handType, selectedHandTypes.CallOverBet))
			{
				return lastAction.CallAction(heroChair);
			}

			if (numberOfBets == 1 && lastAction.Amount <= pot/2 && CheckHand(handType, selectedHandTypes.CallUnderBet))
			{
				return lastAction.CallAction(heroChair);
			}

			if (numberOfBets == 1 && lastAction.Amount <= pot && CheckHand(handType, selectedHandTypes.Call))
			{
				return lastAction.CallAction(heroChair);
			}

			return new PlayerAction(heroChair, PlayerActionEnum.Fold, 0, BetRound.Turn);
		}

		private TurnHandTypesSelection SelectHandTypes(Game currentGame)
		{
			var handTypes = new TurnHandTypesSelection();

			if (Is3BetBank(currentGame))
			{
				handTypes.Bet = "FirstPair+, StraightDraw, FlushDraw, GutShot";
				handTypes.Raise = "FirstPair+";
				handTypes.Call = "SecondPair";
				handTypes.CallUnderBet = "StraightDraw, FlushDraw";
				handTypes.ThreeBet = "FirstPair+";
			}
			else if (IsWithInitiative(currentGame))
			{
				handTypes.Bet = "FirstPair+, StraightDraw, FlushDraw";
				handTypes.Raise = "NutsTwoPairs+";
				handTypes.Call = "FirstPair, TwoPairs, StraightDraw, FlushDraw";
				handTypes.CallOverBet = "TwoPairs";
				handTypes.CallUnderBet = "SecondPair, FirstPair, TwoPairs, StraightDraw, FlushDraw";
				handTypes.CallMinRaise = "FlushDraw";
				handTypes.ThreeBet = "NutsTwoPairs+";
			}
			else
			{
				handTypes.Bet = "Set+";
				handTypes.Raise = "Set+";
				handTypes.ThreeBet = "Set+";
			}

			return handTypes;
		}
	}
}
