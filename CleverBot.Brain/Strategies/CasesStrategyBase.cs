﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.PokerEvalImport;

namespace CleverBot.Brain.Strategies
{
	public class CasesStrategyBase
	{
		[Flags]
		public enum CasesHandType
		{
			Nothing = 0,
			GutShot = (1 << 0),
			StraightDraw = (1 << 1),
			FlushDraw = (1 << 2),
			ThirdPair = (1 << 3),
			SecondPair = (1 << 4),
			FirstPair = (1 << 5),
			TwoPairs = (1 << 6),
			NutsTwoPairs = (1 << 7),
			Trips = (1 << 8),
			Set = (1 << 9),
			Straight = (1 << 10),
			Flush = (1 << 11),
			FullHouse = (1 << 12),
			FourOfAKind = (1 << 13),
			StraightFlush = (1 << 14)
		}

		protected bool CheckHand(CasesHandType handType, string handsString)
		{
			var selectedTypes = ParseHandTypes(handsString);
			return (handType & selectedTypes) != 0;
		}

		protected double BetAmount(Game currentGame)
		{
			var lastAggressiveAction = currentGame.Actions.LastOrDefault(
				a => a.IsAgressive() && a.BetRound == currentGame.BetRound);

			var previousRoundPot = currentGame.Chairs
				.Select(c => currentGame.Actions.LastOrDefault(
					a => a.Chair.ChairNumber == c.ChairNumber &&
					a.Amount > 0 &&
					a.BetRound < currentGame.BetRound))
				.Where(a => a != null)
				.Sum(a => a.Amount);

			var currentRoundPot = previousRoundPot +
				currentGame.Chairs.Select(c =>
					currentGame.Actions.LastOrDefault(a =>
						a.Chair.ChairNumber == c.ChairNumber &&
						a.Amount > 0 &&
						a.BetRound == currentGame.BetRound &&
						a.Chair.ChairNumber != currentGame.HeroChair &&
						a != lastAggressiveAction))
				.Where(a => a != null)
				.Sum(a => a.Amount);

			return lastAggressiveAction == null
				? previousRoundPot * 3/4
				: currentRoundPot + lastAggressiveAction.Amount * 3;
		}

		protected CasesHandType ParseHandTypes(string handTypesString)
		{
			if (string.IsNullOrWhiteSpace(handTypesString))
				return CasesHandType.Nothing;

			var items = handTypesString.Split(',').Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();

			var parsedTypes = CasesHandType.Nothing;

			foreach (var item in items)
			{
				if (item.EndsWith("+"))
				{
					var parsedItem = Enum.Parse(typeof(CasesHandType), item.Replace("+", ""));
					for (var i = (int)parsedItem; i <= (int)CasesHandType.StraightFlush; i *= 2)
					{
						parsedTypes |= (CasesHandType)i;
					}
				}
				else
				{
					parsedTypes |= (CasesHandType)Enum.Parse(typeof(CasesHandType), item.Replace("+", ""));
				}
			}

			return parsedTypes;
		}

		public bool IsWithInitiative(Game currentGame)
		{
			var lastAggressiveAction = currentGame.Actions.LastOrDefault(a =>
				a.BetRound == BetRound.Preflop &&
				a.IsAgressive() &&
				a.Action != PlayerActionEnum.BigBlind);

			var withInitiative = (lastAggressiveAction != null &&
				lastAggressiveAction.Chair.ChairNumber == currentGame.HeroChair);

			return withInitiative;
		}

		public bool Is3BetBank(Game currentGame)
		{
			var is3BetBank = currentGame.Actions
				.Where(a => a.BetRound == BetRound.Preflop)
				.Count(a => a.IsAgressive()) >= 3;

			return is3BetBank;
		}

		public CasesHandType EvalType(CardMask hand, CardMask board)
		{
			var combinedType = PokerEval.EvalType(hand | board);
			var boardType = PokerEval.EvalType(board);
			var boardRanks = board.GetRanks().OrderByDescending(r => r).ToArray();
			var handRanks = hand.GetRanks().OrderByDescending(r => r).ToArray();
			var combinedValue = PokerEval.Eval_N(hand | board);
			var combinedValueRanks = PokerEval.GetRanks(combinedValue).ToList();

			var oneCardOuts = new Dictionary<HandType, Dictionary<CardMask, int>>
			{
				{ HandType.StraightFlush, new Dictionary<CardMask, int>() },
				{ HandType.FourOfAKind, new Dictionary<CardMask, int>() },
				{ HandType.FullHouse, new Dictionary<CardMask, int>() },
				{ HandType.Flush, new Dictionary<CardMask, int>() },
				{ HandType.Straight, new Dictionary<CardMask, int>() },
				{ HandType.ThreeOfAKind, new Dictionary<CardMask, int>() },
				{ HandType.TwoPairs, new Dictionary<CardMask, int>() },
				{ HandType.Pair, new Dictionary<CardMask, int>() },
				{ HandType.Nothing, new Dictionary<CardMask, int>() }
			};

			var twoCardOuts = new Dictionary<HandType, Dictionary<CardMask, int>>
			{
				{ HandType.StraightFlush, new Dictionary<CardMask, int>() },
				{ HandType.FourOfAKind, new Dictionary<CardMask, int>() },
				{ HandType.FullHouse, new Dictionary<CardMask, int>() },
				{ HandType.Flush, new Dictionary<CardMask, int>() },
				{ HandType.Straight, new Dictionary<CardMask, int>() },
				{ HandType.ThreeOfAKind, new Dictionary<CardMask, int>() },
				{ HandType.TwoPairs, new Dictionary<CardMask, int>() },
				{ HandType.Pair, new Dictionary<CardMask, int>() },
				{ HandType.Nothing, new Dictionary<CardMask, int>() }
			};

			var combinedOneCardOuts = new Dictionary<HandType, Dictionary<CardMask, int>>
			{
				{ HandType.StraightFlush, new Dictionary<CardMask, int>() },
				{ HandType.FourOfAKind, new Dictionary<CardMask, int>() },
				{ HandType.FullHouse, new Dictionary<CardMask, int>() },
				{ HandType.Flush, new Dictionary<CardMask, int>() },
				{ HandType.Straight, new Dictionary<CardMask, int>() },
				{ HandType.ThreeOfAKind, new Dictionary<CardMask, int>() },
				{ HandType.TwoPairs, new Dictionary<CardMask, int>() },
				{ HandType.Pair, new Dictionary<CardMask, int>() },
				{ HandType.Nothing, new Dictionary<CardMask, int>() }
			};

			var maxPokerValue = combinedValue;
			var minPokerValue = combinedValue;

			foreach (var c in CardMask.MaskStringDictionary.Values)
			{
				if ((c & (hand | board)).NumCards() > 0)
					continue;

				var pokerValue = PokerEval.Eval_N(c | board);

				oneCardOuts[PokerEval.GetType(pokerValue)].Add(c, pokerValue);
			}

			foreach (var h in HandRange.StdRange())
			{
				if ((h & (hand | board)).NumCards() > 0)
					continue;

				var pokerValue = PokerEval.Eval_N((h | board));

				if (pokerValue > maxPokerValue)
					maxPokerValue = pokerValue;

				if (pokerValue < minPokerValue)
					minPokerValue = pokerValue;

				twoCardOuts[PokerEval.GetType(pokerValue)].Add(h, pokerValue);
			}

			foreach (var c in CardMask.MaskStringDictionary.Values)
			{
				if ((c & (hand | board)).NumCards() > 0)
					continue;

				var pokerValue = PokerEval.Eval_N(c | hand | board);

				combinedOneCardOuts[PokerEval.GetType(pokerValue)].Add(c, pokerValue);
			}

			if (combinedType >= HandType.StraightFlush && boardType != HandType.StraightFlush)
			{
				if (oneCardOuts[HandType.StraightFlush].Count > 0 &&
					oneCardOuts[HandType.StraightFlush].All(i => i.Value >= combinedValue))
				{
					return CasesHandType.TwoPairs;
				}

				return CasesHandType.StraightFlush;
			}

			if (maxPokerValue == minPokerValue)
				return CasesHandType.StraightFlush;

			if (combinedType == HandType.FourOfAKind && boardType != HandType.FourOfAKind)
			{
				return CasesHandType.FourOfAKind;
			}

			if (combinedType == HandType.FourOfAKind && boardType == HandType.FourOfAKind &&
				handRanks.Contains(combinedValueRanks[3]))
			{
				var possibleBetterKickers = twoCardOuts[HandType.FourOfAKind]
					.Where(i => i.Value > combinedValue)
					.GroupBy(i => PokerEval.GetRanks(i.Value)[3])
					.Select(g => g.Key)
					.ToArray();

				if (possibleBetterKickers.Length == 0)
					return CasesHandType.FourOfAKind;
				if (possibleBetterKickers.Length == 1)
					return CasesHandType.FirstPair;

				return CasesHandType.Nothing;
			}

			if (combinedType == HandType.FullHouse && boardType != HandType.FullHouse)
			{

				// three of a kind on the board
				if (board.NumCards() == boardRanks.Length + 2)
				{
					var fullHouseRank = combinedValueRanks[4];
					var kickerRank = combinedValueRanks[3];

					if (handRanks.Any(r => r == fullHouseRank))
						return CasesHandType.FullHouse;

					if (boardRanks.Where(r => r != fullHouseRank).Any(r => r > kickerRank)) 
						return CasesHandType.SecondPair;

					if (kickerRank >= CardRank.Jack)
						return CasesHandType.TwoPairs;
					else 
						return CasesHandType.FirstPair;
				}
				else
				{
					return CasesHandType.FullHouse;
				}
			}

			if (combinedType == HandType.FullHouse && boardType == HandType.FullHouse && 
				handRanks.Contains(combinedValueRanks[3]) && handRanks.Length == 1)
			{
				return combinedValueRanks[3] > CardRank.Ten
					? CasesHandType.TwoPairs
					: CasesHandType.FirstPair;
			}

			if (combinedType == HandType.Flush)
			{
				if (oneCardOuts[HandType.Flush].Any())
				{
					var betterFlushCount = twoCardOuts[HandType.Flush]
						.Where(i => i.Value > combinedValue)
						.GroupBy(i => PokerEval.GetRanks(i.Value)[4])
						.Count();

					if (oneCardOuts[HandType.Straight].Any())
					{
						if (betterFlushCount == 0)
							return CasesHandType.FirstPair;

						return CasesHandType.Nothing;
					}

					if (betterFlushCount == 0)
						return CasesHandType.Flush;

					if (betterFlushCount == 1)
						return CasesHandType.TwoPairs;

					if (betterFlushCount == 2)
						return CasesHandType.FirstPair;

					if (betterFlushCount == 3)
						return CasesHandType.SecondPair;

					if (betterFlushCount == 4)
						return CasesHandType.ThirdPair;

					return CasesHandType.Nothing;
				}

				return CasesHandType.Flush;
			}

			if (combinedType == HandType.Straight )
			{
				if (oneCardOuts[HandType.Flush].Any())
					return CasesHandType.Nothing;

				if (oneCardOuts[HandType.Straight].Any() &&
					oneCardOuts[HandType.Straight].All(i => i.Value >= combinedValue))
				{
					return CasesHandType.TwoPairs;
				}

				if (twoCardOuts[HandType.Flush].Any())
				{
					return CasesHandType.TwoPairs;
				}

				return CasesHandType.Straight;
			}

			if (combinedType == HandType.ThreeOfAKind && boardType != HandType.ThreeOfAKind)
			{
				if (oneCardOuts[HandType.Flush].Any())
				{
					return hand.IsAllSameRank() ? CasesHandType.FlushDraw : CasesHandType.Nothing;
				}
				if (oneCardOuts[HandType.Straight].Any())
				{
					return hand.IsAllSameRank() ? CasesHandType.FlushDraw : CasesHandType.Nothing;
				}

				return hand.IsAllSameRank() ? CasesHandType.Set : CasesHandType.Trips;

			}

			if (combinedType == HandType.TwoPairs && boardType != HandType.TwoPairs)
			{
				if (oneCardOuts[HandType.Flush].Any() || oneCardOuts[HandType.Straight].Any())
					return CasesHandType.Nothing;

				// if there are no pairs on the board
				if (board.NumCards() == boardRanks.Length)
				{
					var isNuts = handRanks.Contains(boardRanks[0]);
					if (twoCardOuts[HandType.Flush].Count == 0 &&
					    twoCardOuts[HandType.Straight].Count == 0)
					{
						return isNuts ? CasesHandType.NutsTwoPairs : CasesHandType.TwoPairs;
					}
					else if (twoCardOuts[HandType.Flush].Count != 0)
					{
						return isNuts ? CasesHandType.TwoPairs : CasesHandType.FirstPair;
					}
					else
					{
						return CasesHandType.TwoPairs;
					}
				}

				// if there is one pair on the board
				if (board.NumCards() == boardRanks.Length + 1)
				{
					var boardPairRank = PokerEval.GetRanks(PokerEval.Eval_N(board))[4];
					var pairRank = combinedValueRanks[4] == boardPairRank ? combinedValueRanks[3] : combinedValueRanks[4];

					if(combinedValueRanks[4] > boardPairRank && combinedValueRanks[3] > boardPairRank)
						return CasesHandType.TwoPairs;

					if (pairRank >= boardRanks[0])
						return CasesHandType.FirstPair;
					else if (pairRank >= boardRanks[1])
						return CasesHandType.SecondPair;
					else
						return CasesHandType.ThirdPair;
				}

			}

			if (combinedType == HandType.TwoPairs && boardType == HandType.TwoPairs &&
			    (handRanks.Contains(combinedValueRanks[4]) || handRanks.Contains(combinedValueRanks[3])))
			{
				return CasesHandType.SecondPair;
			}

			if (combinedType == HandType.Pair && boardType != HandType.Pair)
			{
				var isFlushPossible = twoCardOuts[HandType.Flush].Count != 0 && board.NumCards() == 3;

				if (oneCardOuts[HandType.Flush].Any() || oneCardOuts[HandType.Straight].Any())
					return CasesHandType.Nothing;

				var pairRank = combinedValueRanks[4];

				if (combinedOneCardOuts[HandType.Flush].Count == 0 || board.NumCards() == 5)
				{
					if (pairRank >= boardRanks[0])
						return isFlushPossible ? CasesHandType.SecondPair : CasesHandType.FirstPair;
					else if (pairRank >= boardRanks[1])
						return isFlushPossible ? CasesHandType.ThirdPair : CasesHandType.SecondPair;
					else
						return isFlushPossible ? CasesHandType.Nothing : CasesHandType.ThirdPair;
				}
				else
				{
					return CasesHandType.FlushDraw;
				}
			}

			// check for oesd and gutshot
			

			if (combinedOneCardOuts[HandType.Flush].Any())
			{
				if (twoCardOuts[HandType.Flush].Any())
				{
					var lowerCombinedFlushValue = combinedOneCardOuts[HandType.Flush].Min(i => i.Value);

					var possibleBetterFlushGroups = twoCardOuts[HandType.Flush]
						.Where(i => i.Value > lowerCombinedFlushValue)
						.GroupBy(i => i.Key.GetRanks().Max());

					var possibleBetterFlushGroupsCount = possibleBetterFlushGroups.Count();

					if (possibleBetterFlushGroupsCount == 0)
						return CasesHandType.FlushDraw;

					if (possibleBetterFlushGroupsCount == 1)
						return CasesHandType.GutShot;

					return CasesHandType.Nothing;

				}

				return CasesHandType.FlushDraw;
			}

			var straightOutsCount = combinedOneCardOuts[HandType.Straight]
				.GroupBy(i => i.Key.GetRanks()[0])
				.Count();

			if (straightOutsCount == 1)
			{
				if (twoCardOuts[HandType.Straight].Any() &&
					twoCardOuts[HandType.Straight].Max(i => i.Value) > combinedOneCardOuts[HandType.Straight].Max(i => i.Value))
				{
					return CasesHandType.Nothing;
				}

				return twoCardOuts[HandType.Flush].Any() ? CasesHandType.Nothing : CasesHandType.GutShot;
			}

			if (straightOutsCount == 2)
			{
				if (twoCardOuts[HandType.Straight].Any() &&
					twoCardOuts[HandType.Straight].Max(i => i.Value) > combinedOneCardOuts[HandType.Straight].Max(i => i.Value))
				{
					return twoCardOuts[HandType.Flush].Any() ? CasesHandType.Nothing : CasesHandType.GutShot;
				}

				return twoCardOuts[HandType.Flush].Any() ? CasesHandType.GutShot : CasesHandType.StraightDraw;
			}



			return CasesHandType.Nothing;
		}
	}
}
