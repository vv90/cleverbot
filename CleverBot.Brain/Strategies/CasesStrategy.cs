﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;

namespace CleverBot.Brain.Strategies
{
	public class CasesStrategy : IStrategy
	{
		private DefaultPreflopCasesStrategy _preflopStrategy = new DefaultPreflopCasesStrategy();
		private DefaultFlopCasesStrategy _flopStrategy = new DefaultFlopCasesStrategy();
		private DefaultTurnCasesStrategy _turnStrategy = new DefaultTurnCasesStrategy();
		private DefaultRiverCasesStrategy _riverStrategy = new DefaultRiverCasesStrategy();

		public PlayerAction Think(Game currentGame)
		{
			PlayerAction action = null;
			switch (currentGame.BetRound)
			{
				case BetRound.Preflop:
					action = _preflopStrategy.Think(currentGame);
					break;
				case BetRound.Flop:
					action = _flopStrategy.Think(currentGame);
					break;
				case BetRound.Turn:
					action = _turnStrategy.Think(currentGame);
					break;
				case BetRound.River:
					action = _riverStrategy.Think(currentGame);
					break;
			}

			if (action == null)
				throw new ArgumentException("Current game bet round is not valid");

			if (action.Action == PlayerActionEnum.Fold)
			{
				var lastAgressiveAction = currentGame.Actions.LastOrDefault(a => a.IsAgressive());
				var lastHeroAction = currentGame.Actions.LastOrDefault(a => a.Chair.ChairNumber == currentGame.HeroChair);

				if (lastAgressiveAction == null)
				{
					action = new PlayerAction(currentGame.GetHeroChair(), PlayerActionEnum.Check, 0, currentGame.BetRound);
				}
				else if (lastHeroAction != null && Math.Abs(lastAgressiveAction.Amount - lastHeroAction.Amount) < 0.001)
				{
					action = new PlayerAction(currentGame.GetHeroChair(), PlayerActionEnum.Check, 0, currentGame.BetRound);
				}
			}

			if (action.Amount >= currentGame.GetHeroChair().CurrentBalance)
			{
				action.Action = PlayerActionEnum.Allin;
				action.Amount = currentGame.GetHeroChair().CurrentBalance;
			}

			return action;
		}
	}
}
