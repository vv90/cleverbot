﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Common.Utilities;

namespace CleverBot.Brain.Strategies
{
	public class NeuralNetworkPreflopStrategy : IStrategy
	{
		public PlayerAction Think(Game currentGame)
		{
			//var learner = new NeuralNetworkPreflopLearner();

			//learner.SetWeights(new []
			//{
			//	0.62663117060939,
			//	0.28375687756083,
			//	0.348811148065134,
			//	-0.225656480814301,
			//	-0.249948227335724,
			//	-0.0756125305652677,
			//	-0.107139853490277,
			//	-0.886853403279601,
			//	0.11488510493642,
			//	0.652567522656542,
			//	0.400753054863226,
			//	0.659815839737598,
			//	-0.35720111784798,
			//	-1.02085321784035,
			//	-0.897340912799765,
			//	0.603613759780564,
			//	-0.502985701589133,
			//	0.0149092773870468,
			//	0.672554802966044,
			//	-0.952579407654425,
			//	0.192125650692645,
			//	0.582094808258276,
			//	-0.711726249646207,
			//	0.0341485579944995,
			//	0.686066030815266,
			//	0.197450862614148,
			//	0.405167404347987,
			//	-1.33008223001364,
			//	0.757376141609628,
			//	-0.810834750153997,
			//	0.823477544860845
			//});

			//var lastAction = currentGame.Actions.LastOrDefault();
			//var heroChair = currentGame.GetHeroChair();

			//var result = learner.ComputeOutputs(currentGame);

			//if (result[0] > result[1] && result[0] > result[2])
			//{
			//	return lastAction == null
			//		? new PlayerAction(heroChair, PlayerActionEnum.Bet, currentGame.BigBlind*4)
			//		: new PlayerAction(heroChair, PlayerActionEnum.Raise, lastAction.Amount*3);
			//} else if (result[1] > result[2])
			//{
			//	return lastAction == null
			//		? new PlayerAction(heroChair, PlayerActionEnum.Check, 0)
			//		: new PlayerAction(heroChair, PlayerActionEnum.Call, lastAction.Amount);
			//}
			//else
			//{
			//	return new PlayerAction(heroChair, PlayerActionEnum.Fold, 0);
			//}
			return new PlayerAction();
		}
	}

	//public class NeuralNetworkPreflopLearner
	//{
	//	private static readonly HandRange StdRange = HandRange.StdRange();

	//	private NeuralNetwork _neuralNetwork = new NeuralNetwork(3, 4, 3);
		

	//	private double NormasizeHand(CardMask hand)
	//	{
	//		//var handIndex = StdRange.IndexOf(hand);
	//		var handClass = HandClassification.ClassifyHand(hand);
	//		var parameterValue = (double)handClass / 100;
	//		return parameterValue;
	//	}

	//	//private double NormalizePosition(Position position)
	//	//{
	//	//	var parameterValue = (double)position;
	//	//	return parameterValue;
	//	//}

	//	private double NormalizeOpponentAction(IEnumerable<PlayerAction> actions)
	//	{
	//		var lastActionScore = 0;
	//		var maxAmount = 0.0;
	//		foreach (var a in actions)
	//		{
	//			switch (a.Action)
	//			{
	//				case PlayerActionEnum.Fold:
	//				case PlayerActionEnum.Check:
	//				case PlayerActionEnum.SmallBlind:
	//				case PlayerActionEnum.BigBlind:
	//				case PlayerActionEnum.Straddle:
	//				case PlayerActionEnum.Post:
	//					break;
	//				case PlayerActionEnum.Call:
	//					if (lastActionScore == 0)
	//					{
	//						lastActionScore = 1;
	//					}
	//					break;
	//				case PlayerActionEnum.Bet:
	//				case PlayerActionEnum.Raise:
	//				case PlayerActionEnum.Cap:
	//					if (lastActionScore == 0)
	//					{
	//						lastActionScore = 1;
	//					}
	//					lastActionScore += 1;
	//					break;
	//				case PlayerActionEnum.Allin:
	//					if (maxAmount < a.Amount)
	//					{
	//						lastActionScore += 1;
	//					}
	//					break;
	//				default:
	//					throw new NotImplementedException();
	//			}

	//			if (maxAmount < a.Amount)
	//			{
	//				maxAmount = a.Amount;
	//			}
	//		}

	//		var parameterValue = (double) lastActionScore;

	//		return parameterValue;
	//	}

	//	private double[] NormalizeAction(PlayerAction action)
	//	{
	//		var actions = new double[3];

	//		switch (action.Action)
	//		{
	//			case PlayerActionEnum.Bet:
	//			case PlayerActionEnum.Raise:
	//			case PlayerActionEnum.Cap:
	//			case PlayerActionEnum.Allin:
	//				actions[0] = 1.0;
	//				break;
	//			case PlayerActionEnum.Call:
	//				actions[1] = 1.0;
	//				break;
	//			case PlayerActionEnum.Fold:
	//			case PlayerActionEnum.Check:
	//			case PlayerActionEnum.SmallBlind:
	//			case PlayerActionEnum.BigBlind:
	//			case PlayerActionEnum.Straddle:
	//			case PlayerActionEnum.Post:
	//				actions[2] = 1.0;
	//				break;
	//		}

	//		return actions;
	//	}


	//	public double[] GetParameterValues(Game game)
	//	{
	//		var heroChair = game.GetHeroChair();
	//		double handParameter = NormasizeHand(heroChair.Hand);
	//		double positionParameter = game.DealerOffset(heroChair.ChairNumber);
	//		double lastActionParameter = NormalizeOpponentAction(game.Actions);

	//		return new[]
	//		{
	//			handParameter, 
	//			positionParameter, 
	//			lastActionParameter
	//		};
	//	}

	//	private double[] GetTrainingParameterValues(Game game, PlayerAction heroAction)
	//	{
	//		var heroChair = game.GetHeroChair();
	//		double handParameter = NormasizeHand(heroChair.Hand);
	//		double positionParameter = game.DealerOffset(heroChair.ChairNumber);
	//		double lastActionParameter = NormalizeOpponentAction(game.Actions);
	//		double[] heroActionParameters = NormalizeAction(heroAction);

	//		return new[]
	//		{
	//			handParameter, 
	//			positionParameter, 
	//			lastActionParameter, 
	//			heroActionParameters[0], 
	//			heroActionParameters[1], 
	//			heroActionParameters[2]
	//		};
	//	}

	//	public double[][] GenerateTrainData(Game[] games)
	//	{
	//		var preparedGames = PrepareGames(games);

	//		var trainData = preparedGames.Select(g => GetTrainingParameterValues(g.Key, g.Value)).ToArray();

	//		//var trainData = new List<double[]>();
	//		//for (int i = 0; i < games.Length; i++)
	//		//{
	//		//	var heroChair = games[i].GetHeroChair();
	//		//	var heroActions = games[i].Actions.Where(a => 
	//		//		a.Chair.ChairNumber == heroChair.ChairNumber && a.BetRound == BetRound.Preflop);

	//		//	// generate learning input for each hero action in the game
	//		//	foreach (var ha in heroActions)
	//		//	{
	//		//		PlayerAction currentHeroAction = ha;
	//		//		var actionsBeforeHero = games[i].Actions.TakeWhile(a => a != currentHeroAction).ToList();
	//		//		var gameStateBeforeHeroAction = games[i].Clone();
	//		//		gameStateBeforeHeroAction.Actions = actionsBeforeHero;
	//		//		trainData.Add(GetTrainingParameterValues(gameStateBeforeHeroAction));
	//		//	}

	//		//}

	//		return trainData;
	//	}

	//	private Dictionary<Game, PlayerAction> PrepareGames(Game[] games)
	//	{
	//		var preparedGames = new Dictionary<Game, PlayerAction>();

	//		for (int i = 0; i < games.Length; i++)
	//		{
	//			var heroChair = games[i].GetHeroChair();
	//			var heroActions = games[i].Actions.Where(a =>
	//				a.Chair.ChairNumber == heroChair.ChairNumber && a.BetRound == BetRound.Preflop);

	//			// generate learning input for each hero action in the game
	//			foreach (var ha in heroActions)
	//			{
	//				PlayerAction currentHeroAction = ha;
	//				var actionsBeforeHero = games[i].Actions.TakeWhile(a => a != currentHeroAction).ToList();
	//				var gameStateBeforeHeroAction = games[i].Clone();
	//				gameStateBeforeHeroAction.Actions = actionsBeforeHero;
	//				//gameStateBeforeHeroAction.Actions.Add(currentHeroAction);
	//				preparedGames.Add(gameStateBeforeHeroAction, currentHeroAction);
	//			}
	//		}

	//		return preparedGames;
	//	}

	//	public void Train(IEnumerable<Game> games)
	//	{

	//		int maxEpochs = 80;
	//		double learnRate = 0.05;
	//		double momentum = 0.01;
	//		double[][] trainData = GenerateTrainData(games.ToArray());

	//		_neuralNetwork.Train(trainData, maxEpochs, learnRate, momentum);
	//	}

	//	public double Accuracy(IEnumerable<Game> games)
	//	{
	//		double[][] testData = GenerateTrainData(games.ToArray());

	//		return _neuralNetwork.Accuracy(testData);
	//	}

	//	public double[] GetCurrentWeights()
	//	{
	//		return _neuralNetwork.GetWeights();
	//	}

	//	public void SetWeights(double[] weights)
	//	{
	//		_neuralNetwork.SetWeights(weights);
	//	}

	//	public double[] ComputeOutputs(Game game)
	//	{
	//		return _neuralNetwork.ComputeOutputs(GetParameterValues(game));
	//	}

	//	public void LogResults(IEnumerable<Game> games)
	//	{
	//		var preparedGames = PrepareGames(games.ToArray());


	//		using (var file = new StreamWriter(File.Create("NeuralNetworkLog.txt")))
	//		{

	//			foreach (var g in preparedGames)
	//			{
	//				var game = g.Key;
	//				var heroAction = g.Value;

	//				file.WriteLine("\n---- New Game ----");
	//				foreach (var line in game.CreateLog())
	//					file.WriteLine(line);

	//				file.WriteLine();

	//				foreach(var action in game.Actions)
	//					file.WriteLine(action.ToString());

	//				var parameters = GetTrainingParameterValues(game, heroAction);
	//				file.WriteLine("Hand: {0:0.00}, Position: {1:0.00}, Last action: {2:0.00}", 
	//					parameters[0], parameters[1], parameters[2]);
	//				file.WriteLine("Expected Raise: {0:0.00}, Call: {1:0.00}, Fold: {2:0.00}", 
	//					parameters[3], parameters[4], parameters[5]);

	//				var output = ComputeOutputs(game);
	//				file.WriteLine("Actual   Raise: {0:0.00}, Call: {1:0.00}, Fold: {2:0.00}", 
	//					output[0], output[1], output[2]);
	//			}
	//		}
	//	}
	//}
}
