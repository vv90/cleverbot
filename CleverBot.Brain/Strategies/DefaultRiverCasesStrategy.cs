﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;

namespace CleverBot.Brain.Strategies
{
	public class DefaultRiverCasesStrategy : CasesStrategyBase, IStrategy
	{
		private struct RiverHandTypesSelection
		{
			public string Bet;
			public string Raise;
			public string Call;
			public string ThreeBet;
		}

		public PlayerAction Think(Game currentGame)
		{
			var heroChair = currentGame.GetHeroChair();
			var handType = EvalType(heroChair.Hand, currentGame.Board);
			var selectedHandTypes = SelectHandTypes(currentGame);

			var lastAction = currentGame.Actions.LastOrDefault(a =>
				a.BetRound == BetRound.River &&
				a.IsAgressive());

			var numberOfBets = currentGame.Actions
				.Where(a => a.BetRound == BetRound.River)
				.Count(a => a.IsAgressive());

			var betAmount = BetAmount(currentGame);

			if (lastAction == null)
			{
				return CheckHand(handType, selectedHandTypes.Bet)
					? new PlayerAction(heroChair, PlayerActionEnum.Bet, betAmount, BetRound.River)
					: new PlayerAction(heroChair, PlayerActionEnum.Check, 0, BetRound.River);
			}

			if (numberOfBets >= 2 && CheckHand(handType, selectedHandTypes.ThreeBet))
			{
				return lastAction.RaiseAction(heroChair, betAmount);
			}

			if (numberOfBets == 1 && CheckHand(handType, selectedHandTypes.Raise))
			{
				return lastAction.RaiseAction(heroChair, betAmount);
			}

			if (numberOfBets == 1 && CheckHand(handType, selectedHandTypes.Call))
			{
				return lastAction.CallAction(heroChair);
			}

			

			return new PlayerAction(heroChair, PlayerActionEnum.Fold, 0, BetRound.River);
		}

		private RiverHandTypesSelection SelectHandTypes(Game currentGame)
		{
			var handTypes = new RiverHandTypesSelection();


			if (Is3BetBank(currentGame))
			{
				handTypes.Bet = "FirstPair+";
				handTypes.Raise = "NutsTwoPairs+";
				handTypes.Call = "FirstPair";
				handTypes.ThreeBet = "Straight+";
			}
			else if (IsWithInitiative(currentGame))
			{
				handTypes.Bet = "TwoPairs+";
				handTypes.Raise = "NutsTwoPairs+";
				handTypes.Call = "TwoPairs";
				handTypes.ThreeBet = "Set+";
			}
			else
			{
				handTypes.Bet = "Set+";
				handTypes.Raise = "Set+";
				handTypes.ThreeBet = "Set+";
			}

			return handTypes;
		}
	}
}
