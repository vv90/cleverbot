﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Common.Utilities;
using CleverBot.PokerEvalImport;

namespace CleverBot.Brain.Strategies
{
	public class DefaultFlopCasesStrategy : CasesStrategyBase, IStrategy
	{
		private struct FlopHandTypesSelection
		{
			public string Bet;
			public string RaiseDonk;
			public string CallDonk;
			public string CallRaise33bbOrLess;
			public string CallRaise16bbOrLess;
			public string CallRaise8bbOrLess;
			public string ThreeBet;
			public string ThreeBet55bbOrLess;
			public string FallbackTypes;
		}
		public PlayerAction Think(Game currentGame)
		{
			var heroChair = currentGame.GetHeroChair();
			var handType = EvalType(heroChair.Hand, currentGame.Board);
			var selectedHandTypes = SelectFlopTypes(currentGame);

			var lastAction = currentGame.Actions.LastOrDefault(a => 
				a.BetRound == BetRound.Flop &&
				a.IsAgressive());

			var betAmount = BetAmount(currentGame);


			var numberOfBets = currentGame.Actions
				.Where(a => a.BetRound == BetRound.Flop)
				.Count(a => a.IsAgressive());

			
			// Special cases

			if (lastAction == null)
			{
				return CheckHand(handType, selectedHandTypes.Bet)
					? new PlayerAction(heroChair, PlayerActionEnum.Bet, betAmount, BetRound.Flop)
					: new PlayerAction(heroChair, PlayerActionEnum.Check, 0, BetRound.Flop);
			}

			////////////////


			if (numberOfBets >= 3 && CheckHand(handType, selectedHandTypes.ThreeBet))
				return lastAction.RaiseAction(heroChair, betAmount);

			if (numberOfBets == 2 &&
			    currentGame.EffectiveStack() <= currentGame.BigBlind*33 &&
			    CheckHand(handType, selectedHandTypes.CallRaise33bbOrLess))
			{
				return lastAction.CallAction(heroChair);
			}

			if (numberOfBets == 2 &&
				currentGame.EffectiveStack() <= currentGame.BigBlind * 16 &&
				CheckHand(handType, selectedHandTypes.CallRaise16bbOrLess))
			{
				return lastAction.CallAction(heroChair);
			}

			if (numberOfBets == 2 &&
				currentGame.EffectiveStack() <= currentGame.BigBlind * 8 &&
				CheckHand(handType, selectedHandTypes.CallRaise8bbOrLess))
			{
				return lastAction.CallAction(heroChair);
			}

			if (numberOfBets == 2 &&
			    currentGame.EffectiveStack() <= currentGame.BigBlind*55 &&
			    CheckHand(handType, selectedHandTypes.ThreeBet55bbOrLess))
			{
				return lastAction.RaiseAction(heroChair, betAmount);
			}

			if (numberOfBets == 2 && CheckHand(handType, selectedHandTypes.ThreeBet))
				return lastAction.RaiseAction(heroChair, betAmount);

			if (numberOfBets == 1 && CheckHand(handType, selectedHandTypes.CallDonk))
				return lastAction.CallAction(heroChair);

			if (numberOfBets == 1 && CheckHand(handType, selectedHandTypes.RaiseDonk))
				return lastAction.RaiseAction(heroChair, betAmount);

			if (numberOfBets == 0 && CheckHand(handType, selectedHandTypes.Bet))
				return new PlayerAction(heroChair, PlayerActionEnum.Bet, betAmount, BetRound.Flop);

			if (CheckHand(handType, selectedHandTypes.FallbackTypes))
				return lastAction.RaiseAction(heroChair, betAmount);

			return Math.Abs(lastAction.Amount) < 0.001 
				? new PlayerAction(heroChair, PlayerActionEnum.Check, 0, BetRound.Flop)
				: new PlayerAction(heroChair, PlayerActionEnum.Fold, 0, BetRound.Flop);
		}

		private FlopHandTypesSelection SelectFlopTypes(Game currentGame)
		{

			var handTypes = new FlopHandTypesSelection
			{
				FallbackTypes = "Trips+"
			};

			if (Is3BetBank(currentGame))
			{
				handTypes.Bet = "ThirdPair+, GutShot, StraightDraw, FlushDraw";
				handTypes.RaiseDonk = "FirstPair+, StraightDraw, FlushDraw";
				handTypes.CallDonk = "SecondPair";
				handTypes.ThreeBet = "FirstPair+, StraightDraw, FlushDraw";
			}
			else if (IsWithInitiative(currentGame))
			{
				handTypes.Bet = "ThirdPair+, GutShot, StraightDraw, FlushDraw";
				handTypes.RaiseDonk = "NutsTwoPairs+";
				handTypes.CallDonk = "SecondPair, FirstPair, StraightDraw, FlushDraw, TwoPairs";
				handTypes.CallRaise33bbOrLess = "FirstPair+";
				handTypes.CallRaise16bbOrLess = "SecondPair+";
				handTypes.CallRaise8bbOrLess = "ThirdPair+";
				handTypes.ThreeBet = "NutsTwoPairs+";
				handTypes.ThreeBet55bbOrLess = "TwoPairs+";
			}
			else
			{
				handTypes.Bet = "Set+";
				handTypes.RaiseDonk = "Set+";
			}

			return handTypes;
		}

		
	}
}
