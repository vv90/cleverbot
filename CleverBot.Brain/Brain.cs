﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Interfaces;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;

namespace CleverBot.Brain
{
	public class Brain
	{
		private readonly IStrategy _strategy;


		public Brain(IStrategy strategy)
		{
			_strategy = strategy;
		}

		public PlayerAction Think(Game currentGame)
		{
			return _strategy.Think(currentGame);
		}
	}
}
