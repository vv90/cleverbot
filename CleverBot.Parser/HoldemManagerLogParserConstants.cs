﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CleverBot.Common;

namespace CleverBot.Parser
{
	public class GamePropertyParseSettings
	{
		public GameProperty GameProperty { get; set; }
		public string Pattern { get; set; }
		public Func<string, string> ParseTransform { get; set; }

		public GamePropertyParseSettings()
		{
			ParseTransform = s => s;
		}
	}

	public partial class HoldemManagerLogParser
	{
		private const string CurrencyPattern =
			@"(?:\s*[\$]?\s*[\d]{1,}[\.,]?[\d]{0,2}\s*(?:USD)?\$?\s*)";

		private const string IntPattern = @"[\d]+";

		private const string CardMaskPattern =
			@"(?:[\s,\[\]]*" +
			@"(?:2h|3h|4h|5h|6h|7h|8h|9h|Th|10h|Jh|Qh|Kh|Ah|" +
			@"2d|3d|4d|5d|6d|7d|8d|9d|Td|10d|Jd|Qd|Kd|Ad|" +
			@"2c|3c|4c|5c|6c|7c|8c|9c|Tc|10c|Jc|Qc|Kc|Ac|" +
			@"2s|3s|4s|5s|6s|7s|8s|9s|Ts|10s|Js|Qs|Ks|As)[\s,\[\]]*)+";

		private const string DateTimePattern =
			@"(?:(?:[\d]{4}/[\d]{1,2}/[\d]{1,2} [\d]{1,2}:[\d]{1,2}:[\d]{1,2})|" +

			@"(?:(?:Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), " +
			@"(?:January|February|March|April|May|June|July|August|September|October|November|December) \d{1,2}, " + 
			@"\d{2}:\d{2}:\d{2} [A-Z]+ \d{4}))";

		private const string PlayerActionPattern =
			@"(?:posts small blind|posts big blind|has small blind|has big blind|straddle|posts|calls|bets|raises|caps|allin|is all-In|folds|checks)";

		private const string PlayerNamePattern = @"[\w\s.\*_\-!?]+";

		private static readonly Func<string, string> CurrencyParseTransform =
			val => val.Replace("$", "").Replace("USD", "").Replace(',', '.');

		private static readonly Func<string, string> DateTimeParseTransform =
			val =>
			{
				var regex = new Regex(
					@"(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday), " +
					@"(January|February|March|April|May|June|July|August|September|October|November|December) (\d{1,2}), "+
					@"(\d{2}:\d{2}:\d{2}) ([A-Z]+) (\d{4})");
				if (regex.IsMatch(val))
				{
					val = regex.Replace(val, @"$2 $3, $6, $4");
				}
				return val;
			};

		private static readonly string[] NewGameIndicatorPatterns =
		{
			@"^(?:Game started at: {GameStartDateTime})$",
			@"^(?:\*\*\*\*\* Hand History for Game {GameId} \*\*\*\*\*)$",
			@"^(?:PokerStars Game #{GameId}: Hold'em No Limit \({SmallBlind}\/{BigBlind} USD\) - {GameStartDateTime})$"
		};

		private static readonly string[] SuppressParsingIndicatorStrings =
		{
			"------ Summary ------"
		};

		private static readonly string[] LinePatterns =
		{
			// Type 1
			@"^(?:Game started at: {GameStartDateTime})$",
			@"^(?:Game ID: {GameId} {SmallBlind}/{BigBlind} {GameTitle})$",
			@"^(?:Seat {ChairNumber}: {PlayerName} \({Balance}\)(?: \(chop is on\))?\.)$",
			@"^(?:Seat {DealerChair} is the button)$",
			@"^(?:Player {PlayerName} {Action}[\s]*(?:\({BetAmount}\))(?: as a dead bet)?)$",
			@"^(?:Player {PlayerName} {Action}[\s]*)$",
			@"^(?:Player {PlayerName} received a card.)$",
			@"^(?:Player {PlayerName} received card: \[{Hand}\])$",
			@"^(?:\*\*\* {IgnoreBetRound} \*\*\*: {Board})$",

			// Type 2
			@"^(?:\*\*\*\*\* Hand History for Game {GameId} \*\*\*\*\*)$",
			@"^(?:{GameLimit} NL Texas Hold'em - {GameStartDateTime})$",
			@"^(?:Table [\w\s()-]+)$",
			@"^(?:Total number of players : \d/\d )$",
			@"^(?:Seat {ChairNumber}: {PlayerName} \( {Balance} \))$",
			@"^(?:{PlayerName} {Action}[+\w\s]*\[{BetAmount}\]\.?)$",
			@"^(?:{PlayerName} {Action})$",
			@"^(?:\*\* Dealing down cards \*\*)$",
			@"^(?:Dealt to {PlayerName} \[{Hand}\])$",
			@"^(?:\*\* Dealing {IgnoreBetRound} \*\* \[{Board}\])$",

			// Type 3
			@"^(?:PokerStars Game #{GameId}: Hold'em No Limit \({SmallBlind}\/{BigBlind} USD\) - {GameStartDateTime})$",
			@"^(?:Table '.*' 6-max Seat #{DealerChair} is the button)$",
			@"^(?:Seat {ChairNumber}: {PlayerName} \({Balance} in chips\)\s*)$",
			@"^(?:{PlayerName}: {Action} {IgnoreCurrency} to {BetAmount})$",
			@"^(?:{PlayerName}: {Action} {BetAmount})$",
			@"^(?:{PlayerName}: {Action})$",
			@"^(?:Dealt to {PlayerName} \[{Hand}\])$",
			@"^(?:{PlayerName}: shows \[{Hand}\] \([\w\s,]*\))$",
			@"^(?:\*\*\* {IgnoreBetRound} \*\*\* \[{Board}\])$",

			// Type 1 ignored
			@"^(?:-+\s?Summary\s?-+)$",
			@"^(?:Pot: {IgnoreCurrency}. Rake {IgnoreCurrency}. BBJ {IgnoreCurrency})$",
			@"^(?:Pot: {IgnoreCurrency}. Rake {IgnoreCurrency})$",
			@"^(?:Board: \[{IgnoreCardMask}\])$",
			@"^(?:Player {IgnorePlayerName} does not show cards.Bets: {IgnoreCurrency}. Collects: {IgnoreCurrency}. Wins: {IgnoreCurrency}.)$",
			@"^(?:\*?Player {IgnorePlayerName} shows:[\w\s]+\[{IgnoreCardMask}\]. Bets: {IgnoreCurrency}. Collects: {IgnoreCurrency}. (?:Loses|Wins): {IgnoreCurrency}.)$",
			@"^(?:\*?Player {IgnorePlayerName} mucks \(does not show cards\). Bets: {IgnoreCurrency}. Collects: {IgnoreCurrency}. (?:Loses|Wins): {IgnoreCurrency}.)$",
			@"^(?:\*?Player {IgnorePlayerName} mucks \(does not show: \[{IgnoreCardMask}\]\). Bets: {IgnoreCurrency}. Collects: {IgnoreCurrency}. (?:Loses|Wins): {IgnoreCurrency}.)$",
			@"^(?:Game ended at: {IgnoreDateTime})$",
			@"^(?:Uncalled bet \({IgnoreCurrency}\) returned to {IgnorePlayerName})$",
			@"^(?:Player {IgnorePlayerName} mucks cards)$",
			@"^(?:Player {IgnorePlayerName} is timed out.)$",
			@"^(?:Player {IgnorePlayerName} sitting out)$",
			@"^(?:Player {IgnorePlayerName} wait BB)$",
			@"^(?:Game #\d+ starts.)$",
			@"^(?:#Game No : \d+)$",

			// Type 2 ignored
			@"^(?:{IgnorePlayerName} shows \[{IgnoreCardMask}\][\w\s,.]+)$",
			@"^(?:{IgnorePlayerName} wins {IgnoreCurrency} from the main pot with [\w\s,.]+)$",
			@"^(?:{IgnorePlayerName} wins {IgnoreCurrency} from the side pot \d+ with[\w\s.,]+)$",
			@"^(?:{IgnorePlayerName} wins {IgnoreCurrency})$",
			@"^(?:{IgnorePlayerName} will be using his time bank for this hand.)$",
			@"^(?:{IgnorePlayerName} doesn't show \[{IgnoreCardMask}\][\w\s,.]+)$",
			@"^(?:{IgnorePlayerName} has joined the table.)$",
			@"^(?:{IgnorePlayerName} did not respond in time)$",
			@"^(?:{IgnorePlayerName} does not show cards.)$",
			@"^(?:{IgnorePlayerName} has left the table.)$",
			@"^(?:{IgnorePlayerName} is sitting out)$",
			@"^(?:{IgnorePlayerName} could not respond in time.\(disconnected\))$",
			@"^(?:Your time bank will be activated in 6 secs. If you do not want it to be used, please act now.)$",
			@"^(?:{IgnorePlayerName} is disconnected. We will wait for {IgnorePlayerName} to reconnect for a maximum of \d+ seconds.)$",
			@"^(?:{IgnorePlayerName} has been reconnected and has \d+ seconds to act.)$",
			@"^(?:{IgnorePlayerName}is reconnected and has \d+ seconds to respond.)$",
			//@"^(?:{IgnorePlayerName}: .*)$",

			// Type 3 ignored
			@"^(?:\*\*\* HOLE CARDS \*\*\*)$",
			@"^(?:\*\*\* SHOW DOWN \*\*\*)$",
			@"^(?:\*\*\* SUMMARY \*\*\*)$",
			@"^(?:Total pot {IgnoreCurrency} \| Rake {IgnoreCurrency})$",
			@"^(?:Total pot {IgnoreCurrency} Main pot {IgnoreCurrency}. Side pot {IgnoreCurrency} \| Rake {IgnoreCurrency})$",
			@"^(?:Total pot {IgnoreCurrency} Main pot {IgnoreCurrency}.(?: Side pot #{IgnoreInt} {IgnoreCurrency}[\.]?)+ \| Rake {IgnoreCurrency})$",
			@"^(?:Total pot ErrorPot \| Rake {IgnoreCurrency})$",
			@"^(?:Board \[{IgnoreCardMask}\])$",
			@"^(?:Board \[\])$",
			@"^(?:Seat {IgnoreInt}: {IgnorePlayerName} (?:\([\w\s]*\) )?folded (?:before|on the) {IgnoreBetRound}(?: \(didn't bet\))?)$",
			@"^(?:Seat {IgnoreInt}: {IgnorePlayerName} (?:\([\w\s]*\) )?mucked)$",
			@"^(?:Seat {IgnoreInt}: {IgnorePlayerName} (?:\([\w\s]*\) )?showed \[{IgnoreCardMask}\] and won \({IgnoreCurrency}\) with [\w\s,]*)$",
			@"^(?:Seat {IgnoreInt}: {IgnorePlayerName} (?:\([\w\s]*\) )?showed \[{IgnoreCardMask}\] and lost with [\w\s,]*)$",
			@"^(?:Seat {IgnoreInt}: {IgnorePlayerName} (?:\([\w\s]*\) )?collected \({IgnoreCurrency}\))$",
			@"^(?:{IgnorePlayerName} collected {IgnoreCurrency} from pot)$",
			@"^(?:{IgnorePlayerName} collected {IgnoreCurrency} from side pot-{IgnoreInt})$",
			@"^(?:{IgnorePlayerName}: doesn't show hand)$",
		};

		private static readonly Dictionary<string, GamePropertyParseSettings> ConstantsDictionary =
			new Dictionary<string, GamePropertyParseSettings>
		{
			{
				"GameId",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.GameId,
					Pattern = IntPattern
				}
			},
			{
				"GameStartDateTime",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.GameStartDateTime,
					Pattern = DateTimePattern,
					ParseTransform = DateTimeParseTransform
				}
			},
			{
				"GameTitle",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.GameTitle,
					Pattern = ".+"
				}
			},
			{
				"GameLimit",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.GameLimit,
					Pattern = CurrencyPattern,
					ParseTransform = CurrencyParseTransform
				}
			},
			{
				"SmallBlind",
				new GamePropertyParseSettings{
					GameProperty = GameProperty.SmallBlind,
					Pattern = CurrencyPattern,
					ParseTransform = CurrencyParseTransform
				}
			},
			{
				"BigBlind",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.BigBlind,
					Pattern = CurrencyPattern,
					ParseTransform = CurrencyParseTransform
				}
			},
			{
				"Balance",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.Balance,
					Pattern = CurrencyPattern,
					ParseTransform = CurrencyParseTransform
				}
			},
			{
				"ChairNumber",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.ChairNumber,
					Pattern = IntPattern
				}
			},
			{
				"PlayerName",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.PlayerName,
					Pattern = PlayerNamePattern
				}
			},
			{
				"DealerChair",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.DealerChair,
					Pattern = IntPattern
				}
			},
			{
				"Action",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.Action,
					Pattern = PlayerActionPattern
				}
			},
			{
				"BetAmount",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.BetAmount,
					Pattern = CurrencyPattern,
					ParseTransform = CurrencyParseTransform
				}
			},
			{
				"Hand",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.Hand,
					Pattern = CardMaskPattern
				}
			},
			{
				"Board",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.Board,
					Pattern = CardMaskPattern
				}
			},
			{
				"IgnoreBetRound",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.NoProperty,
					Pattern = @"(?:FLOP|TURN|RIVER|Flop|Turn|River)"
				}
			},
			{
				"IgnoreCardMask",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.NoProperty,
					Pattern = CardMaskPattern
				}
			},
			{
				"IgnorePlayerName",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.NoProperty,
					Pattern = PlayerNamePattern
				}
			},
			{
				"IgnoreCurrency",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.NoProperty,
					Pattern = CurrencyPattern
				}
			},
			{
				"IgnoreDateTime",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.NoProperty,
					Pattern = DateTimePattern
				}
			},
			{
				"IgnoreInt",
				new GamePropertyParseSettings
				{
					GameProperty = GameProperty.NoProperty,
					Pattern = IntPattern
				}
			}
		};
	}
}
