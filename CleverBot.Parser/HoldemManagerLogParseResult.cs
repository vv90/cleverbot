﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.Parser
{
	public class HoldemManagerLogParseResult
	{
		public int LinesParsed { get; set; }
		public Dictionary<int, string> FailedLines { get; private set; }

		public bool IsSuccess { get { return !FailedLines.Any(); } }

		public List<Game> Games { get; set; }

		public HoldemManagerLogParseResult()
		{
			FailedLines = new Dictionary<int, string>();
			Games = new List<Game>();
		}
	}
}
