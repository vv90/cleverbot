﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

//namespace CleverBot.Parser
//{
//	public class ReplayFrameInfo
//	{
//		public int Id { get; set; }
//		public string GameNumber { get; set; }
//		public string GameTitle { get; set; }
//		public int DealerChair { get; set; }
//		public string Hand { get; set; }
//		public DateTime Time { get; set; }
//		public int FrameNumber { get; set; }

//		public override string ToString()
//		{
//			return string.Format("[{0}] {1}, {2}, {3}", FrameNumber, Time, Hand, GameTitle);
//		}
//	}

//	public class OpenHoldemLogParser
//	{
//		public List<ReplayFrameInfo> Parse(string fileName)
//		{
//			var parsedFrames = new List<ReplayFrameInfo>();

//			using (var reader = new StreamReader(File.OpenRead(fileName)))
//			{
//				string currentGameNumber = null;
//				int currentGameDealerChair = 0;
//				string currentGameHand = null;
//				string currentGameTitle = null;
//				var gameRegex = new Regex(@"HAND RESET \(num: (\d+) dealer: (\d) cards: ([23456789TJQKAcdhs]{4})\): (.+)");
//				var replayFrameRegex = new Regex(@"(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}) - \[CReplayFrame\] Shooting frame \[(\d+)\]");

//				while (!reader.EndOfStream)
//				{
//					var currentLine = reader.ReadLine();

//					if (currentLine == null)
//						continue;

//					var gameMatch = gameRegex.Match(currentLine);

//					if (gameMatch.Success)
//					{
//						currentGameNumber = gameMatch.Groups[1].Value;
//						currentGameDealerChair = int.Parse(gameMatch.Groups[2].Value);
//						currentGameHand = gameMatch.Groups[3].Value;
//						currentGameTitle = gameMatch.Groups[4].Value;
//						continue;
//					}

//					var replayFrameMatch = replayFrameRegex.Match(currentLine);
//					if (replayFrameMatch.Success)
//					{
//						var time = DateTime.Parse(replayFrameMatch.Groups[1].Value);
//						var frameNumber = int.Parse(replayFrameMatch.Groups[2].Value);

//						var replayFrame = new ReplayFrameInfo
//						{
//							GameNumber = currentGameNumber,
//							GameTitle = currentGameTitle,
//							DealerChair = currentGameDealerChair,
//							Hand = currentGameHand,
//							Time = time,
//							FrameNumber = frameNumber
//						};

//						parsedFrames.Add(replayFrame);
//					}
//				}
//			}

//			return parsedFrames;
//		}
//	}
//}
