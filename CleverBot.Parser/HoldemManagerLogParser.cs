﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Common.Utilities;
using CleverBot.Parser.TypeConverters;

namespace CleverBot.Parser
{
	public enum LogType
	{
		Type1 = 1,
		Type2 = 2
	}

	public partial class HoldemManagerLogParser
	{
		public event EventHandler<LineParsedEventArgs> LineParsedEvent;
		public event EventHandler<LineParseErrorEventAgrs> LineParseErrorEvent;
		public event EventHandler<NewGameParsingStartedEventArgs> NewGameParsingStartedEvent;


		private static readonly List<Regex> ProcessedNewGameIndicatorPatterns = new List<Regex>();
		private static readonly Dictionary<Regex, string> ProcessedLinePatterns = new Dictionary<Regex, string>();

		static HoldemManagerLogParser()
		{
			TypeDescriptor.AddAttributes(typeof (PlayerActionEnum), 
				new TypeConverterAttribute(typeof(PlayerActionEnumTypeConverter)));

			TypeDescriptor.AddAttributes(typeof (CardMask), 
				new TypeConverterAttribute(typeof (CardMaskTypeConverter)));

			//TypeDescriptor.AddAttributes(typeof (Position),
			//	new TypeConverterAttribute(typeof (PositionTypeConverter)));

			TypeDescriptor.AddAttributes(typeof (DateTime),
				new TypeConverterAttribute(typeof (DateTimeCustomTypeConverter)));

			var parseConstantRegex = new Regex(@"\{[\w]*\}");

			foreach (var originalLine in NewGameIndicatorPatterns)
			{
				var regexPattern = originalLine;

				foreach (Match m in parseConstantRegex.Matches(originalLine))
				{
					var val = m.Value;
					regexPattern = regexPattern.Replace(val, ConstantsDictionary[val.Trim('{', '}')].Pattern);
				}

				ProcessedNewGameIndicatorPatterns.Add(new Regex(regexPattern));
			}

			foreach (var originalLine in LinePatterns)
			{
				var regexPattern = originalLine;

				foreach (Match m in parseConstantRegex.Matches(originalLine))
				{
					var val = m.Value;
					regexPattern = regexPattern.Replace(val, ConstantsDictionary[val.Trim('{', '}')].Pattern);
				}

				ProcessedLinePatterns.Add(new Regex(regexPattern), originalLine);
			}
		}

		private GameBuilder _gameBuilder = new GameBuilder(new Game());
		private readonly List<Game> _games = new List<Game>();
		private bool suppressParsing = false;

		public HoldemManagerLogParser()
		{
			_games.Add(_gameBuilder.Game);
		}

		public HoldemManagerLogParseResult Import(string filePath)
		{
			using (var file = File.OpenText(filePath))
			{
				var lineNumber = 0;
				var parseResult = new HoldemManagerLogParseResult();

				while (!file.EndOfStream)
				{
					lineNumber += 1;

					var line = file.ReadLine();

					if (line == null)
						continue;

					try
					{
						ParseSingleLine(line, lineNumber);

						if (LineParsedEvent != null)
						{
							LineParsedEvent(this, new LineParsedEventArgs
							{
								LineNumber = lineNumber,
								Line = line
							});
						}
					}
					catch (HoldemManagerLogParseException ex)
					{
						parseResult.FailedLines.Add(lineNumber, line);
						if (LineParseErrorEvent != null)
						{
							LineParseErrorEvent(this, new LineParseErrorEventAgrs
							{
								LineNumber = lineNumber,
								Line = line,
								Message = ex.Message
							});
						}
					}
				}

				parseResult.LinesParsed = lineNumber + 1;
				parseResult.Games = _games;

				return parseResult;
			}
		}

		public IEnumerable<Game> ParseLines(string[] lines)
		{
			for (int i = 0; i < lines.Length; i++)
			{
				ParseSingleLine(lines[i], i);
			}

			return _games;
		}

		private void ParseSingleLine(string line, int lineNumber)
		{
			if (string.IsNullOrWhiteSpace(line))
				return;

			if (SuppressParsingIndicatorStrings.Any(s => s == line))
			{
				suppressParsing = true;
			}

			if ((ProcessedNewGameIndicatorPatterns.Any(p => p.IsMatch(line))))
			{
				suppressParsing = false;
				if (_gameBuilder.AssignmentCount > 0)
				{
					_gameBuilder = new GameBuilder();
					_games.Add(_gameBuilder.Game);
				}

				if (NewGameParsingStartedEvent != null)
				{
					NewGameParsingStartedEvent(this, 
						new NewGameParsingStartedEventArgs { Game = _gameBuilder.Game });
				}
			}

			if (suppressParsing)
				return;

			var matchingLine = ProcessedLinePatterns.FirstOrDefault(l => l.Key.IsMatch(line));

			if (matchingLine.Key == null && matchingLine.Value == null)
			{
				throw new FailedToParseLineException(lineNumber, line, "Line not recognized");
			}

			var values = ExtractValues(matchingLine.Value, line);

			_gameBuilder.SetProperties(values.ToArray());
		}

		private static IEnumerable<GamePropertyValue> ExtractValues(string template, string str)
		{
			var keys = new List<GamePropertyParseSettings>();

			var constantMatches = Regex.Matches(template, @"\{[\w]+\}");
			for (int i = 0; i < constantMatches.Count; i++)
			{
				keys.Add(ConstantsDictionary[constantMatches[i].Value.Trim('{', '}')]);
			}

			string pattern = template;
			

			foreach (var c in ConstantsDictionary)
			{
				if (Regex.IsMatch(pattern, string.Format("{{{0}}}", c.Key)))
				{
					pattern = Regex.Replace(pattern, string.Format("{{{0}}}", c.Key), 
						string.Format("({0})", c.Value.Pattern));
				}
			}

			var r = new Regex(pattern);
			Match m = r.Match(str);

			var result = new List<GamePropertyValue>();

			// parse values
			for (int i = 1; i < m.Groups.Count; i++)
			{
				var gamePropertyParseSettings = keys[i - 1];
				var valueString = m.Groups[i].Value;

				result.Add(new GamePropertyValue(gamePropertyParseSettings.GameProperty, TypeDescriptor
					.GetConverter(gamePropertyParseSettings.GameProperty.Type)
					.ConvertFromString(gamePropertyParseSettings.ParseTransform(valueString))
				));
			}

			return result;
		}
	}
}
