﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.Parser
{
	public class LineParsedEventArgs : EventArgs
	{
		public int LineNumber { get; set; }
		public string Line { get; set; }
		
	}

	public class NewGameParsingStartedEventArgs : EventArgs
	{
		public Game Game { get; set; }
	}

	public class LineParseErrorEventAgrs : LineParsedEventArgs
	{
		public string Message { get; set; }
	}
}
