﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using CleverBot.Common.Enums;

namespace CleverBot.Parser.TypeConverters
{
	public class PlayerActionEnumTypeConverter : TypeConverter
	{

		private static readonly Dictionary<string, PlayerActionEnum> PlayerActionsEnumDictionary =
			new Dictionary<string, PlayerActionEnum>
			{
				//folds|checks|has small blind|has big blind|straddle|posts|calls|bets|raises|caps|allin
				{"folds", PlayerActionEnum.Fold},
				{"checks", PlayerActionEnum.Check},
				{"has small blind", PlayerActionEnum.SmallBlind},
				{"has big blind", PlayerActionEnum.BigBlind},
				{"straddle", PlayerActionEnum.Straddle},
				{"posts", PlayerActionEnum.Post},
				{"calls", PlayerActionEnum.Call},
				{"bets", PlayerActionEnum.Bet},
				{"raises", PlayerActionEnum.Raise},
				{"caps", PlayerActionEnum.Cap},
				{"allin", PlayerActionEnum.Allin},

				{"posts small blind", PlayerActionEnum.SmallBlind},
				{"posts big blind", PlayerActionEnum.BigBlind},
				{"is all-In", PlayerActionEnum.Allin}
			};

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				var strValue = (string) value;
				if (PlayerActionsEnumDictionary.ContainsKey(strValue))
				{
					return PlayerActionsEnumDictionary[strValue];
				}
			}

			return base.ConvertFrom(context, culture, value);
		}
	}
}
