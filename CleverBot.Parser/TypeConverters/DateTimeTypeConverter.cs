﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.Parser.TypeConverters
{
	public class DateTimeCustomTypeConverter : DateTimeConverter
	{

		private static readonly string[] CustomDateTimeFormats =
		{
			"dddd, MMMM d, HH':'mm':'ss yyyy"
		};

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			var stringValue = value as string;
			if (stringValue != null)
			{
				DateTime dateTime;
				var success = DateTime.TryParse(stringValue, out dateTime);

				if (!success)
				{
					foreach (var formatString in CustomDateTimeFormats)
					{
						success = DateTime.TryParseExact(stringValue, formatString, 
							CultureInfo.InvariantCulture, DateTimeStyles.None , out dateTime);

						if (success)
						{
							return dateTime;
						}
					}
				}
			}

			return base.ConvertFrom(context, culture, value);
		}
	}
}
