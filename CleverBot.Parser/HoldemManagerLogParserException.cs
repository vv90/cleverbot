﻿using System;

namespace CleverBot.Parser
{
	public class HoldemManagerLogParseException : Exception
	{
		public HoldemManagerLogParseException()
			: base()
		{
		}

		public HoldemManagerLogParseException(string message)
			: base(message)
		{
		}
	}

	public class FailedToParseLineException : HoldemManagerLogParseException
	{
		public string FailedLine { get; private set; }
		public int LineNumber { get; private set; }
		public FailedToParseLineException(int lineNumber, string failedLine, string message = "")
			: base(string.Format("Failed to parse log line: {0} {1}", lineNumber, message))
		{
			FailedLine = failedLine;
			LineNumber = lineNumber;
		}
	}
}
