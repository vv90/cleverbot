//******************************************************************************
//
// This file is part of the OpenHoldem project
//   Download page:         http://code.google.com/p/openholdembot/
//   Forums:                http://www.maxinmontreal.com/forums/index.php
//   Licensed under GPL v3: http://www.gnu.org/licenses/gpl.html
//
//******************************************************************************
//
// Purpose: Very simple user-DLL as a starting-point
//
// Required OpenHoldem version: 7.7.6
//
//******************************************************************************

// Needs to be defined here, before #include "user.h"
// to generate proper export- and inport-definitions
#define USER_DLL

// #define OPT_DEMO_OUTPUT if you are a beginner 
// who wants to see some message-boxes with output of game-states, etc.
// It is disabled upon request, 
//   * as it is not really needed
//   * as some DLL-users don't have MFC (atlstr.h) installed
// http://www.maxinmontreal.com/forums/viewtopic.php?f=156&t=16232
#undef OPT_DEMO_OUTPUT
//#define OPT_DEMO_OUTPUT


#include "user.h"
#include <conio.h>
#include <windows.h>

#ifdef OPT_DEMO_OUTPUT
#include <atlstr.h>
#endif OPT_DEMO_OUTPUT

using namespace System;

/////////////////////////////////////
//card macros
#define RANK(c)			((c>>4)&0x0f)
#define SUIT(c)			((c>>0)&0x0f)
#define ISCARDBACK(c)	(c==0xff)
#define ISUNKNOWN(c)	(c==0)
/////////////////////////////////////

//Handling the lookup of dll$symbols
DLL_IMPLEMENTS double __stdcall ProcessQuery(const char* pquery) {
	if (pquery == NULL)
		return 0;
	if (strncmp(pquery, "dll$test", 8) == 0) {
		return GetSymbol("random");
	}
	if (strncmp(pquery, "dll$spend", 9) == 0) {
		return GetSymbol("f$spend");
	}
	if (strncmp(pquery, "dll$recurse", 11) == 0) {
		return GetSymbol("dll$mynumber");
	}
	if (strncmp(pquery, "dll$mynumber", 12) == 0) {
		return 12345.67;
	}
	return 0;
}


//double process_query(const char* pquery)
//{
//	if (pquery == NULL)
//		return 0;
//	
//	System::String^ query = gcnew String(pquery);
//
//	return CleverBot::OHConnectivity::MainController::GetInstance(System::IntPtr(getsym), System::IntPtr(gethandnum))->ProcessQuery(query);
//	
//}
//
//double process_state(holdem_state* pstate)
//{
//	if (pstate != NULL)
//	{
//		m_holdem_state[(++m_ndx) & 0xff] = *pstate;
//
//		System::IntPtr^ href = gcnew System::IntPtr(pstate);
//		CleverBot::OHConnectivity::MainController::GetInstance(System::IntPtr(getsym), System::IntPtr(gethandnum))->UpdateState(*href);
//
//	}
//	return 0;
//}

// OnLoad and OnUnload()
//   called once and at the beginning of a session
//   when the DLL gets loaded / unloaded
//   Do initilization / finalization here.

DLL_IMPLEMENTS void __stdcall DLLOnLoad() {
#ifdef OPT_DEMO_OUTPUT
	MessageBox(NULL, _T("event-load"), _T("MESSAGE"), MB_OK);
#endif OPT_DEMO_OUTPUT
}

DLL_IMPLEMENTS void __stdcall DLLOnUnLoad() {
#ifdef OPT_DEMO_OUTPUT
	MessageBox(NULL, _T("event-unload"), _T("MESSAGE"), MB_OK);
#endif OPT_DEMO_OUTPUT
}

#pragma unmanaged

// DLL entry point
//   Technically required, but don't do anything here.
//   Initializations belong into the OnLoad() function,
//   where they get executed at run-time.
//   Doing things here at load-time is a bad idea,
//   as some functionalitz might not be properly initialized   
//   (including error/handling).
BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call)	
	{
		case DLL_PROCESS_ATTACH:
#ifdef OPT_DEMO_OUTPUT
			AllocConsole();
#endif OPT_DEMO_OUTPUT
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
#ifdef OPT_DEMO_OUTPUT
			FreeConsole();
#endif OPT_DEMO_OUTPUT
			break;
	}
	return TRUE;
}
///////////////////////////////////////////////////// 
//WINHOLDEM RUNTIME ENTRY POINT 
///////////////////////////////////////////////////// 
//USERDLL_API double process_message(const char* pmessage, const void* param)
//{
//	if (pmessage == NULL)
//	{
//		return 0;
//	}
//
//	if (param == NULL)
//	{
//		return 0;
//	}
//
//	if (strcmp(pmessage, "state") == 0)
//	{
//		return process_state((holdem_state*)param);
//	}
//
//	if (strcmp(pmessage, "query") == 0)
//	{
//		return process_query((const char*)param);
//	}
//
//	return 0;
//}


///////////////////////////////// 
//DLLMAIN 
///////////////////////////////// 
//BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
//{
//
//	switch (ul_reason_for_call)
//	{
//		case DLL_PROCESS_ATTACH:
//			break;
//		case DLL_THREAD_ATTACH:
//			break;
//		case DLL_THREAD_DETACH:
//			break;
//		case DLL_PROCESS_DETACH:
//			break;
//	}
//
//	return TRUE;
//
//}

#pragma managed