﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Data.Entities;

namespace CleverBot.Data
{
	public class CleverBotDbContext : DbContext
	{
		public DbSet<GameEntity> Games { get; set; }
		public DbSet<ChairEntity> Chairs { get; set; }
		public DbSet<ActionEntity> Actions { get; set; }
		public DbSet<ReplayFrameInfo> ReplayFrames { get; set; }

		public CleverBotDbContext() : base("Name=CleverBotDatabase")
		{
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			base.OnModelCreating(modelBuilder);
		}
	}
}
