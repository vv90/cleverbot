﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CleverBot.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace CleverBot.Data
{
	public class CardMaskJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof (CardMask).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return new CardMask((string) reader.Value);
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			var cardMask = (CardMask) value;

			//writer.WriteStartObject();
			serializer.Serialize(writer, cardMask.ToString());
			//writer.WriteEndObject();
		}
	}

	public class CardMaskJsonContractResolver : DefaultContractResolver
	{
		protected override JsonContract CreateContract(Type objectType)
		{
		    JsonContract contract = base.CreateContract(objectType);
		
		    // this will only be called once and then cached
		    if (objectType == typeof(CardMask))
				contract.Converter = new CardMaskJsonConverter();
			else if (objectType.IsEnum)
				contract.Converter = new StringEnumConverter();


			return contract;
		}
	}
}
