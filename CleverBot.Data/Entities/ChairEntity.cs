﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.Data.Entities
{
	public class ChairEntity
	{
		public int Id { get; set; }
		public int ChairNumber { get; set; }
		public string HandString { get; set; }
		public string Name { get; set; }
		public double Balance { get; set; }

		//public int GameId { get; set; }
		//public GameEntity Game { get; set; }

		public ChairEntity()
		{
		}

		public ChairEntity(Chair chair)
		{
			ChairNumber = chair.ChairNumber;
			HandString = chair.Hand.ToString();
			Name = chair.Name;
			Balance = chair.Balance;
		}
	}
}
