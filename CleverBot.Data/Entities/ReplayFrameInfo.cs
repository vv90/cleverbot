﻿using System;

namespace CleverBot.Data.Entities
{
	public class ReplayFrameInfo
	{
		public int Id { get; set; }
		public string GameNumber { get; set; }
		public string GameTitle { get; set; }
		public int DealerChair { get; set; }
		public string Hand { get; set; }
		public DateTime Time { get; set; }
		public int FrameNumber { get; set; }

		public override string ToString()
		{
			return string.Format("[{0}] {1}, {2}, {3}", FrameNumber, Time, Hand, GameTitle);
		}
	}
}
