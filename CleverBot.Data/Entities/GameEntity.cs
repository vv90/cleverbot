﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.Data.Entities
{
	public class GameEntity
	{
		public int Id { get; set; }
		public string GameNumber { get; set; }
		public string Title { get; set; }
		public DateTime StartDateTime { get; set; }
		public double BigBlind { get; set; }
		public double SmallBlind { get; set; }
		public int DealerChair { get; set; }
		public int HeroChair { get; set; }
		public string BoardString { get; set; }
		public bool IsScraped { get; set; }
		public List<ChairEntity> Chairs { get; set; }
		public List<ActionEntity> Actions { get; set; }

		public GameEntity()
		{
		}

		public GameEntity(Game game, bool isScraped)
		{
			GameNumber = game.GameNumber;
			Title = game.Title;
			StartDateTime = game.StartTime;
			BigBlind = game.BigBlind;
			SmallBlind = game.SmallBlind;
			DealerChair = game.DealerChair;
			HeroChair = game.HeroChair;
			BoardString = game.Board.ToString();
			IsScraped = isScraped;

			Chairs = game.Chairs.Select(c => new ChairEntity(c)).ToList();
			Actions = game.Actions.Select(a => new ActionEntity(a)).ToList();
		}
	}
}
