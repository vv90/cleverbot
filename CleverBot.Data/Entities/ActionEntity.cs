﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.Data.Entities
{
	public class ActionEntity
	{
		public int Id { get; set; }
		public string BetRound { get; set; }
		public string ActionName { get; set; }
		public double Amount { get; set; }
		public int ChairNumber { get; set; }
		//public int GameId { get; set; }
		//public GameEntity Game { get; set; }

		//public int ChairId { get; set; }
		//public ChairEntity Chair { get; set; }

		public ActionEntity()
		{
		}

		public ActionEntity(PlayerAction action)
		{
			BetRound = action.BetRound.ToString();
			ActionName = action.Action.ToString();
			Amount = action.Amount;
			ChairNumber = action.Chair.ChairNumber;
		}
	}
}
