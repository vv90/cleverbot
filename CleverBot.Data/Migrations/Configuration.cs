﻿using System.Data.Entity.Migrations;

namespace CleverBot.Data.Migrations
{
	public class Configuration : DbMigrationsConfiguration<CleverBotDbContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
			AutomaticMigrationDataLossAllowed = true;
		}
	}
}
