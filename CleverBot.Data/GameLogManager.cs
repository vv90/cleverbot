﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using CleverBot.Common;
using CleverBot.Data.Entities;
using Newtonsoft.Json;

namespace CleverBot.Data
{
	public class GameLogManager
	{

		public GameLogManager()
		{
		}

		public void WriteToFile(string fileName, IEnumerable<Game> gameLog)
		{
			var data = string.Format("[{0}]", string.Join(", ", 
				gameLog.Select(g => JsonConvert.SerializeObject(g, Formatting.Indented, 
					new JsonSerializerSettings { ContractResolver = new CardMaskJsonContractResolver()}))));

			File.WriteAllText(fileName, data);
		}

		public void WriteToFile(string fileName, Game game)
		{
			var games = new List<Game>();

			if (File.Exists(fileName))
			{
				games.AddRange(ReadFromFile(fileName));
			}
			
			games.Add(game);
			WriteToFile(fileName, games);
		}

		public IEnumerable<Game> ReadFromFile(string fileName)
		{
			var data = File.ReadAllText(fileName);

			return JsonConvert.DeserializeObject<IEnumerable<Game>>(data, 
				new JsonSerializerSettings { ContractResolver = new CardMaskJsonContractResolver() });
		}

		public void WriteToDatabase(Game game, bool isScraped)
		{
			using (var context = new CleverBotDbContext())
			{
				var gameEntity = new GameEntity(game, isScraped);

				context.Games.Add(gameEntity);
				context.SaveChanges();
			}
		}

		public void WriteToDatabase(IEnumerable<Game> games)
		{
			using (var context = new CleverBotDbContext())
			{
				foreach (var game in games)
				{
					var gameEntity = new GameEntity(game, false);
					context.Games.Add(gameEntity);

				}
				context.SaveChanges();
			}
		}
	}
}
