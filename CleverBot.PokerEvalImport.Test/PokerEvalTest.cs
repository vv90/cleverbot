﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Utilities;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.PokerEvalImport.Test
{
	[TestClass]
    public class PokerEvalTest
    {
		[TestMethod]
		public void TestGetRanks()
		{
			// value for "Ac, Qh, Jh, Td, 9d"
			var ranks = PokerEval.GetRanks(829831);

			Assert.AreEqual(CardRank.Ace, ranks[4]);
			Assert.AreEqual(CardRank.Queen, ranks[3]);
			Assert.AreEqual(CardRank.Jack, ranks[2]);
			Assert.AreEqual(CardRank.Ten, ranks[1]);
			Assert.AreEqual(CardRank.Nine, ranks[0]);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_Nothing()
		{
			// value for "Ac, Kd, 6s, Jc, 2h"
			var type = PokerEval.GetType(833856);

			Assert.AreEqual(HandType.Nothing, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_Pair()
		{
			// value for "6s, 6c, Ac, Kd, 2h"
			var type = PokerEval.GetType(17091328);

			Assert.AreEqual(HandType.Pair, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_TwoPairs()
		{
			// value for "6s, 6c, Kc, Kd, 2h"
			var type = PokerEval.GetType(34291712);

			Assert.AreEqual(HandType.TwoPairs, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_ThreeOfAKind()
		{
			// value for "6s, 6c, 6d, Kd, 2h"
			var type = PokerEval.GetType(50638848);

			Assert.AreEqual(HandType.ThreeOfAKind, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_Straight()
		{
			// value for "6s, 7c, 8d, 9d, Th"
			var type = PokerEval.GetType(67633152);

			Assert.AreEqual(HandType.Straight, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_Flush()
		{
			// value for "Kc, 2c, 8c, 9c, Tc"
			var type = PokerEval.GetType(84641632);

			Assert.AreEqual(HandType.Flush, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_FullHouse()
		{
			// value for "Kc, Kd, 8c, 8h, 8s"
			var type = PokerEval.GetType(101101668);

			Assert.AreEqual(HandType.FullHouse, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_FourOfAKind()
		{
			// value for "Kc, Kd, Kh, Ks, 2s"
			var type = PokerEval.GetType(118161408);

			Assert.AreEqual(HandType.FourOfAKind, type);
		}

		[TestMethod]
		public void TestGetType_ReturnsCorrectType_StraightFlush()
		{
			// value for "5c, 6c, 7c, 8c, 9c"
			var type = PokerEval.GetType(134676480);

			Assert.AreEqual(HandType.StraightFlush, type);
		}
    }
}
