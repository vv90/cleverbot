﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain;
using CleverBot.Common;
using CleverBot.Data;
using System.IO;

namespace CleverBot.ConsoleApp
{
	public class TrainNeuralNetworkFeature : FeatureBase
	{
		private double _trainToTestRatio;

		private PreflopLearner _learner;

		public TrainNeuralNetworkFeature( 
			int numHiddenLayers, 
			int maxEpochs, 
			double learnRate, 
			double momentum,
			double trainToTestRatio,
			IEnumerable<PreflopLearnerParameter> parameters)
		{
			_trainToTestRatio = trainToTestRatio;
			
			_learner = new PreflopLearner(numHiddenLayers, maxEpochs, learnRate, momentum, parameters.ToArray());
		}

		private void ImportGames(string jsonLogFileName, string heroName, out List<Game> trainingSet, out List<Game> testingSet)
		{
			trainingSet = new List<Game>();
			testingSet = new List<Game>();

			var games = ImportGames(jsonLogFileName, heroName);

			var random = new Random();
			foreach (var game in games)
			{
				if (random.Next(100) < (_trainToTestRatio * 100))
					trainingSet.Add(game);
				else
					testingSet.Add(game);
			}
		}

		public void Run(string jsonLogFileName, string heroName, out double inSampleAccuracy, out double outOfSampleAccuracy)
		{
			List<Game> trainingSet;
			List<Game> testingSet;

			ImportGames(jsonLogFileName, heroName, out trainingSet, out testingSet);

			
			_learner.Train(trainingSet);
			inSampleAccuracy = _learner.Accuracy(trainingSet);
			_learner.LogResults(trainingSet, "FailedCases.txt", true);
			outOfSampleAccuracy = testingSet.Count > 0 ? _learner.Accuracy(testingSet) : 0;
		}

		public void SaveWeights(string fileName)
		{
			File.WriteAllText(fileName, string.Join(",\n", _learner.GetWeights()));
		}

	}
}
