﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Contexts;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CleverBot.Brain;
using CleverBot.Common;
using CleverBot.Common.Utilities;
using CleverBot.Data;
using CleverBot.Parser;
using CleverBot.Brain.Strategies;
using CleverBot.Data.Entities;
using CleverBot.PokerEvalImport;

namespace CleverBot.ConsoleApp
{
	public class Program
	{
		private class MenuOption
		{
			public MenuOption(int optionNumber, string optionLabel, Action action)
			{
				OptionNumber = optionNumber;
				OptionLabel = optionLabel;
				Action = action;
			}

			public int OptionNumber { get; set; }
			public string OptionLabel { get; set; }
			public Action Action { get; set; }
		}

		private static void DisplayMenu(string menuHeader, IEnumerable<MenuOption> menuOptions)
		{
			Console.WriteLine("{0}", menuHeader);
			var orderedOptions = menuOptions.OrderBy(o => o.OptionNumber).ToArray();
			foreach (var option in orderedOptions)
			{
				Console.WriteLine("[{0}] - {1}", option.OptionNumber, option.OptionLabel);
			}

			MenuOption selectedOption = null;
			while (selectedOption == null)
			{
				var input = Console.ReadLine();
				int parsedInput;
				var parseSuccessful = int.TryParse(input, out parsedInput);

				if (parseSuccessful)
				{
					selectedOption = orderedOptions.FirstOrDefault(o => o.OptionNumber == parsedInput);
				}

				if (selectedOption == null)
				{
					Console.WriteLine("Incorrect input. Please input an option number from the options list");
				}
			}

			selectedOption.Action();
		}

		private static void Main(string[] args)
		{
			Database.SetInitializer(new MigrateDatabaseToLatestVersion<CleverBotDbContext, Data.Migrations.Configuration>());

			var isContinue = true;
			while (isContinue)
			{
				DisplayMenu("Select a task:", new[]
				{
					new MenuOption(1, "Parse HM log file", ParseGames),
					new MenuOption(2, "Train neural network", TrainNeuralNetwork),
					new MenuOption(3, "Test strategy", TestStrategy), 
					new MenuOption(4, "Transform table map", TransformTableMap), 
					new MenuOption(5, "Run game construction tests", RunGameTests), 
					new MenuOption(6, "Parse OpenHoldem log", ParseOpenHoldemLog),
					new MenuOption(7, "Exit", () => { isContinue = false; }), 
				});
			}

			Console.WriteLine("Press any key to exit...");

			Console.Read();
		}

		private static void TestStrategy()
		{
			var feature = new TestStrategyFeature(new DefaultPreflopCasesStrategy());

			feature.Run("NL10_JSON.js", "Rylanx");
			feature.SaveLog("FailedCases.txt");
		}

		private static void TransformTableMap()
		{
			Console.WriteLine("Table map to transform:");
			var tableMapPath = Console.ReadLine();

			var lines = File.ReadAllLines(tableMapPath);
			for(int j = 0; j < lines.Length; j++)
			{
				if (!lines[j].StartsWith("r$"))
					continue;

				var regionRecord = lines[j];

				int sizeRecordsStartIndex = 0;
				int sizeRecordsEndIndex = 0;
				int sizeRecordsCount = 0;
				bool isReadingSizeRecord = false;
				for (int i = regionRecord.IndexOf(" "); i < regionRecord.Length; i++)
				{
					if (regionRecord[i] == ' ')
					{
						if (sizeRecordsCount == 4)
						{
							sizeRecordsEndIndex = i;
							break;
						}
						isReadingSizeRecord = false;
					}

					if(!isReadingSizeRecord && regionRecord[i] != ' ')
					{
						if (sizeRecordsStartIndex == 0)
						{
							sizeRecordsStartIndex = i;
						}

						isReadingSizeRecord = true;
						sizeRecordsCount += 1;
					}
				}

				var sizeRecords = new string(regionRecord
					.Skip(sizeRecordsStartIndex)
					.Take(sizeRecordsEndIndex - sizeRecordsStartIndex)
					.ToArray())
					.Split(' ')
					.Where(s => !string.IsNullOrWhiteSpace(s))
					.Select(int.Parse)
					.ToArray();

				sizeRecords[0] = sizeRecords[0] - 8;
				sizeRecords[1] = sizeRecords[1] - 30;
				sizeRecords[2] = sizeRecords[2] - 8;
				sizeRecords[3] = sizeRecords[3] - 30;

				var updatedRecord = regionRecord
					.Remove(sizeRecordsStartIndex, sizeRecordsEndIndex - sizeRecordsStartIndex)
					.Insert(sizeRecordsStartIndex, string.Join(" ", sizeRecords));

				lines[j] = updatedRecord;
			}

			File.WriteAllLines("TransformedTM.tm", lines);
		}

		private static void GenerateHandOrdering()
		{
			var lines = File.ReadAllLines("RawOrdering.txt");
			var handClassificationDictionary = new Dictionary<CardMask, int>();

			for(int i = 0; i < lines.Length; i++)
			{
				var l = lines[i];
				string handsString;
				if (l.StartsWith("("))
				{
					handsString = l.Trim('(', ')');
					handsString += "s";
				} 
				else if (l[0] == l[1])
				{
					handsString = l;
				}
				else
				{
					handsString = l + "o";
				}

				var parsedRange = HandRange.Parse(handsString);

				foreach(var m in parsedRange)
					handClassificationDictionary.Add(m, 169 - i);
			}

			File.WriteAllLines("Ordering.txt", handClassificationDictionary.Select(m => string.Format("{{ new CardMask(\"{0}\"), {1}}},", m.Key, m.Value)));
		}

		private static void PrintOutStdRange()
		{
			var stdRange = HandRange.StdRange();

			var handValues = new double[stdRange.Count];

			var allCards = CardMask.MaskStringDictionary.Values.ToArray();

			var numFinished = 0;
			Parallel.For(0, stdRange.Count, i =>
			{
				var hand = stdRange.Get(i);
				ulong sum = 0UL;

				for (int c1 = 0; c1 < allCards.Length; c1++)
					for (int c2 = c1 + 1; c2 < allCards.Length; c2++)
						for (int c3 = c2 + 1; c3 < allCards.Length; c3++)
							for (int c4 = c3 + 1; c4 < allCards.Length; c4++)
								for (int c5 = c4 + 1; c5 < allCards.Length; c5++)
								{
									var mask = allCards[c1] | allCards[c2] | allCards[c3] | allCards[c4] | allCards[c5];

									if ((mask & hand) != CardMask.Empty)
										continue;

									mask |= hand;
									sum += (ulong) PokerEval.Eval_N(mask);
								}

				
				
				var average = (double) sum/allCards.Length;
				handValues[i] = average;
				Console.Write("\r{0:0.00} %", (double) numFinished/stdRange.Count*100);
				Interlocked.Increment(ref numFinished);
			});
			Console.WriteLine();
			var handValuesDitcionary = new Dictionary<CardMask, double>();
			for(int i = 0; i < stdRange.Count; i++)
				handValuesDitcionary.Add(stdRange.Get(i), handValues[i]);

			File.WriteAllLines("ShowdownAverageRange.txt", handValuesDitcionary
				.OrderBy(v => v.Value)
				.Select(v => string.Format("{0}\t{1}", v.Key, v.Value)));
		}

		private static void TrainNeuralNetwork()
		{
			var numHiddenLayers = 4;
			var maxEpochs = 90;
			var learnRate = 0.1;
			var momentum = 0.05;
			var trainToTestRatio = 0.3;
			var normalizations = new[]
				{
					new PreflopLearnerParameter("Hand", GameExtentions.NormalizedHand),
					new PreflopLearnerParameter("Position", GameExtentions.NormalizedPosition),
					new PreflopLearnerParameter("Last Opponent Action", GameExtentions.NormalizedLastOpponentAction),
					new PreflopLearnerParameter("IsPocketPair", GameExtentions.NormalizedIsPocketPair), 
				};

			var feature = new TrainNeuralNetworkFeature(
				numHiddenLayers, maxEpochs, learnRate, momentum, trainToTestRatio, normalizations);

			double inSampleAccuracy;
			double outOfSampleAccuracy;

			feature.Run("NL10_JSON.js", "Rylanx", out inSampleAccuracy, out outOfSampleAccuracy);

			Console.WriteLine("In-Sample accuracy: {0}", inSampleAccuracy);
			Console.WriteLine("Out-of-Sample accuracy: {0}", outOfSampleAccuracy);

			DisplayMenu("Do you want to save the result weights?", new[]
			{
				new MenuOption(1, "Yes", () => 
				{
					var weightsFile = "weights.txt";
					feature.SaveWeights(weightsFile);
					Console.WriteLine("Weights are saved into \"{0}\"", weightsFile);
				}), 
				new MenuOption(2, "No", () => { }),
			});
		}

		private static void ParseGames()
		{
			var importer = new HoldemManagerLogParser();
			var percentCompleted = 0;



			Console.Write("Holdem Manager log file name: ");
			var fileName = Console.ReadLine();
			try
			{
				if (string.IsNullOrEmpty(fileName))
				{
					throw new ArgumentNullException();
				}
				var totalLines = File.ReadLines(fileName).Count();


				importer.LineParseErrorEvent += (sender, eventArgs) =>
				{
					Console.WriteLine("Error parsing line {0}: \"{1}\" {2}", 
						eventArgs.LineNumber, eventArgs.Line, eventArgs.Message);
					Console.ReadLine();
				};

				importer.LineParsedEvent += (sender, eventArgs) =>
				{
					if ((int)(100 * (double)eventArgs.LineNumber / totalLines) != percentCompleted)
					{
						percentCompleted = (int)(100 * (double)eventArgs.LineNumber / totalLines);
						Console.Write("\r{0} %      ", percentCompleted);
					}
				};

				var result = importer.Import(fileName);

				Console.WriteLine("Parsing complete");
				Console.WriteLine("Lines: {0}, Games: {1}, Errors: {2}.", result.LinesParsed, result.Games.Count,
					result.FailedLines.Count);

				var gameLog = new GameLogManager();

				Console.WriteLine("Saving parsed games...");
				gameLog.WriteToFile(string.Format("{0}_JSON.js", fileName.Replace(".txt", "")), result.Games);
				//gameLog.WriteToDatabase(result.Games);
			}
			catch (ArgumentNullException ex)
			{
				Console.WriteLine(ex.Message);
			}
			catch (FileNotFoundException ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		private static string TestGame(GameEntity expected, GameEntity actual)
		{
			if (expected.GameNumber != actual.GameNumber)
				return "GameNumber";

			if (Math.Abs(expected.BigBlind - actual.BigBlind) > 0.001)
				return "BigBlind";

			if (Math.Abs(expected.SmallBlind - actual.SmallBlind) > 0.001)
				return "SmallBlind";

			if (expected.DealerChair != actual.DealerChair)
				return "DealerChair";

			if (expected.HeroChair != actual.HeroChair)
				return "HeroChair";

			var actualBoard = new CardMask(actual.BoardString);
			var expectedBoard = new CardMask(expected.BoardString);

			if ((actualBoard & expectedBoard) != actualBoard)
				return "Board";

			foreach (var expectedChair in expected.Chairs)
			{
				var actualChair = actual.Chairs.SingleOrDefault(c => c.ChairNumber == expectedChair.ChairNumber);

				if (actualChair == null)
					return string.Format("Chair {0} not found", expectedChair.ChairNumber);

				var expectedHand = new CardMask(expectedChair.HandString);
				var actualHand = new CardMask(actualChair.HandString);
				if (expectedHand.NumCards() != 0 && actualHand.NumCards() != 0 && (expectedHand & actualHand) != actualHand)
					return string.Format("Chair {0} Hand", expectedChair.ChairNumber);

				if (Math.Abs(expectedChair.Balance - actualChair.Balance) > 0.001)
					return string.Format("Chair {0} Balance", expectedChair.ChairNumber);
			}

			for (int i = 0; i < expected.Actions.Count; i++)
			{
				var expectedAction = expected.Actions[i];
				if (expectedAction.ChairNumber == expected.HeroChair && expectedAction.ActionName == "Fold")
					break;

				if (actual.Actions.Count < i + 1)
					return string.Format("Missing action {0}", i);

				var actualAction = actual.Actions[i];

				if (expectedAction.BetRound != actualAction.BetRound)
					return string.Format("Action {0} BetRound", i);

				if (expectedAction.ActionName != actualAction.ActionName)
					return string.Format("Action {0} ActionName", i);

				if (Math.Abs(expectedAction.Amount - actualAction.Amount) > 0.001)
					return string.Format("Action {0} Amount", i);

				if (expectedAction.ChairNumber != actualAction.ChairNumber)
					return string.Format("Action {0} ChairNumber", i);
			}

			return null;
		}


		private static bool CheckReplayFrame(ReplayFrameInfo replayFrame, GameEntity game, string heroName, TimeSpan tolerance)
		{
			var replayFrameHand = new CardMask(string.Format("{0}, {1}", 
				new String(replayFrame.Hand.Take(2).ToArray()), 
				new String(replayFrame.Hand.Skip(2).Take(2).ToArray())));

			var gameHand = new CardMask(game.Chairs.First(c => c.Name == heroName).HandString);

			var timeDifference = replayFrame.Time.Subtract(game.StartDateTime);

			return replayFrameHand == gameHand && timeDifference.TotalMilliseconds > 0 && timeDifference < tolerance;
		}

		private static void RunGameTests()
		{
			Console.WriteLine("Hero name:");
			var heroName = Console.ReadLine();
			using (var context = new CleverBotDbContext())
			{
				var gamesParsedFromLog = context.Games
					.Include(g => g.Chairs)
					.Include(g => g.Actions)
					.Where(g => g.IsScraped == false).ToArray();

				var availableReplayFrames = context.ReplayFrames
					.ToArray();

				var tested = 0;
				var missing = 0;
				var failed = 0;

				foreach (var game in gamesParsedFromLog)
				{
					var parsedGame = game;
					tested += 1;

					var scrapedGames = context.Games
						.Include(g => g.Chairs)
						.Include(g => g.Actions)
						.Where(g => g.IsScraped && g.GameNumber == parsedGame.GameNumber).ToArray();

					if (scrapedGames.Length == 0)
					{
						missing += 1;
						Console.WriteLine("No scraped games with GameNumber = {0} found", parsedGame.GameNumber);
						Console.WriteLine("[{0}] - {1} - {2}",
							string.Join(", ", parsedGame.Chairs.Where(c => !string.IsNullOrWhiteSpace(c.HandString)).Select(c => c.HandString)),
							parsedGame.StartDateTime.ToString("g"),
							parsedGame.Title);
						Console.ReadKey();
					}

					var passedGame = scrapedGames.FirstOrDefault(g => TestGame(parsedGame, g) == null);

					if (passedGame == null)
					{
						failed += 1;
						Console.WriteLine("All games with GameNumber = {0} failed tests", parsedGame.GameNumber);
						Console.WriteLine("[{0}] - {1} - {2}", 
							string.Join(", ", parsedGame.Chairs.Where(c => !string.IsNullOrWhiteSpace(c.HandString)).Select(c => c.HandString)),
							parsedGame.StartDateTime.ToString("g"),
							parsedGame.Title);
						var replayFrames = availableReplayFrames
							.Where(r => CheckReplayFrame(r, parsedGame, heroName, new TimeSpan(0, 5, 0)))
							.ToArray();

						if (replayFrames.Length == 0)
						{
							Console.WriteLine("No replay frames available for that game.");
						}
						else
						{
							Console.WriteLine("Replay frames vailable:");
							foreach(var rf in replayFrames)
								Console.WriteLine(rf.ToString());
						}

						Console.ReadKey();
					}
				}

				Console.WriteLine("Testing complete. tested: {0}, missing: {1}, failed: {2}", tested, missing, failed);
			}
		}

		private static void ParseOpenHoldemLog()
		{
			Console.WriteLine("OpenHoldem log file name:");
			var fileName = Console.ReadLine();

			if (!File.Exists(fileName))
			{
				Console.WriteLine("File not found");
				return;
			}


			int parsedFramesCount = 0;

			using (var context = new CleverBotDbContext())
			using (var reader = new StreamReader(File.OpenRead(fileName)))
			{
				string currentGameNumber = null;
				int currentGameDealerChair = 0;
				string currentGameHand = null;
				string currentGameTitle = null;
				var gameRegex = new Regex(@"HAND RESET \(num: (\d+) dealer: (\d) cards: ([23456789TJQKAcdhs]{4})\): (.+)");
				var replayFrameRegex = new Regex(@"(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}) - \[CReplayFrame\] Shooting frame \[(\d+)\]");

				while (!reader.EndOfStream)
				{
					var currentLine = reader.ReadLine();

					if (currentLine == null)
						continue;

					var gameMatch = gameRegex.Match(currentLine);

					if (gameMatch.Success)
					{
						currentGameNumber = gameMatch.Groups[1].Value;
						currentGameDealerChair = int.Parse(gameMatch.Groups[2].Value);
						currentGameHand = gameMatch.Groups[3].Value;
						currentGameTitle = gameMatch.Groups[4].Value;
						continue;
					}

					var replayFrameMatch = replayFrameRegex.Match(currentLine);
					if (replayFrameMatch.Success)
					{
						var time = DateTime.Parse(replayFrameMatch.Groups[1].Value);
						var frameNumber = int.Parse(replayFrameMatch.Groups[2].Value);

						var replayFrame = new ReplayFrameInfo
						{
							GameNumber = currentGameNumber,
							GameTitle = currentGameTitle,
							DealerChair = currentGameDealerChair,
							Hand = currentGameHand,
							Time = time,
							FrameNumber = frameNumber
						};

						context.ReplayFrames.Add(replayFrame);
						parsedFramesCount += 1;
					}
				}

				context.SaveChanges();
			}

			Console.WriteLine("Parsing complete. {0} replay frames added.", parsedFramesCount);
		}
	}
}
