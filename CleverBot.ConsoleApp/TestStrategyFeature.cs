﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain;
using CleverBot.Brain.Interfaces;
using CleverBot.Common;
using CleverBot.Common.Enums;

namespace CleverBot.ConsoleApp
{
	public class TestStrategyFeature : FeatureBase
	{
		private Brain.Brain _brain;
		private StringBuilder _log;

		public TestStrategyFeature(IStrategy strategy)
		{
			_brain = new Brain.Brain(strategy);
			_log = new StringBuilder();
		}

		public void Run(string jsonLogFileName, string heroName)
		{
			var preparedGames = GameExtentions.PrepareGames(ImportGames(jsonLogFileName, heroName), BetRound.Preflop);

			var successCount = 0;
			foreach (var item in preparedGames)
			{
				if (Test(item.Key, item.Value))
					successCount += 1;
			}
		}

		public void SaveLog(string fileName)
		{
			File.WriteAllText(fileName, _log.ToString());
		}

		private bool Test(Game game, PlayerAction expectedAction)
		{
			var actualAction = _brain.Think(game);

			var isMatch = actualAction.Action == expectedAction.Action;
				//&& Math.Abs(actualAction.Amount - expectedAction.Amount) < 0.001;
			if (!isMatch)
			{
				_log.AppendFormat("\n---- Game {0} {1} ----\n", game.GameNumber, game.Title ?? "");
				foreach (var line in game.CreateLog())
					_log.AppendLine(line);

				_log.AppendLine();

				foreach (var action in game.Actions)
					_log.AppendLine(action.ToString());

				_log.AppendFormat("Expected: {0}\n", expectedAction);
				_log.AppendFormat("Actual: {0}\n", actualAction);
			}

			return isMatch;
		}


	}
}
