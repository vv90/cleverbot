﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Data;

namespace CleverBot.ConsoleApp
{
	public class FeatureBase
	{
		protected Game[] ImportGames(string jsonLogFileName, string heroName)
		{
			var gameLogManager = new GameLogManager();
			var games = gameLogManager.ReadFromFile(jsonLogFileName).ToArray();


			if (games.Length == 0)
			{
				throw new InvalidOperationException("No games were imported.");
			}

			foreach (var game in games)
			{
				var heroChair = game.GetChair(heroName);
				if (heroChair != null)
				{
					game.HeroChair = heroChair.ChairNumber;
				}
			}

			return games;
		}
	}
}
