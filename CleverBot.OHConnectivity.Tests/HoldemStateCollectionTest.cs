﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.OHConnectivity.Tests
{
	[TestClass]
	public class HoldemStateCollectionTest
	{
		[TestMethod]
		public void TestAdd_AddsStatesCorrectly()
		{
			var states = new HoldemStateCollection(2);

			var state = new ManagedHoldemState { m_title = "title" };
			states.Add(state);

			Assert.AreEqual("title", states.GetState().m_title);
		}

		[TestMethod]
		public void TestAdd_HandlesOverflowCorrectly()
		{
			var states = new HoldemStateCollection(2);

			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());

		}

		[TestMethod]
		public void TestGetState_ReturnsLastAddedState()
		{
			var states = new HoldemStateCollection(2);

			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState { m_title = "title"});

			Assert.AreEqual("title", states.GetState().m_title);
		}

		[TestMethod]
		public void TestGetState_ReturnsStateWithOffsetCorrectly()
		{
			var states = new HoldemStateCollection(5);

			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState());
			states.Add(new ManagedHoldemState { m_title = "title" });
			states.Add(new ManagedHoldemState());

			Assert.AreEqual("title", states.GetState(1).m_title);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void TestGetState_ThrowsIfOffsetIsGreaterThanCapacity()
		{
			var states = new HoldemStateCollection(2);

			states.GetState(3);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void TestGetState_ThrowsIfOffsetIsNegative()
		{
			var states = new HoldemStateCollection(2);

			states.GetState(-1);
		}
	}
}
