﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.OHConnectivity.Tests
{
	[TestClass]
	public class MainControllerTest
	{
		//[TestMethod]
		//public void TestUpdateGame_UpdatesBoardCorrectly()
		//{
		//	var controller = new MainController(new Game());

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 }
		//		}).ToArray();
		//	var state = new ManagedHoldemState
		//	{
		//		m_player = players,
		//		m_cards = new byte[] { 1, 2, 3, 4, 5 }
		//	};

		//	var game = controller.UpdateGame(state);

		//	Assert.AreEqual(new CardMask("3h, 4h, 5h, 6h, 7h"), game.Board);
		//}

		//[TestMethod]
		//public void TestUpdateGame_AddsPlayersCorrectly()
		//{
		//	var controller = new MainController(new Game());

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 }
		//		}).ToArray();

		//	players[0].m_name = "player1";
		//	players[0].m_cards = new byte[] { 1, 2 };
		//	players[0].m_balance = 100.0;

		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 254, 254, 254, 254, 254 },
		//		m_player = players
		//	};

		//	var game = controller.UpdateGame(state);

		//	Assert.AreEqual("player1", game.GetChair(0).Name);
		//	Assert.AreEqual(100, game.GetChair(0).Balance);
		//	Assert.AreEqual(new CardMask("3h, 4h"), game.GetChair(0).Hand);
		//}

		//[TestMethod]
		//public void TestUpdateGame_DoesntRemovePlayerName()
		//{
		//	var game = new Game
		//	{
		//		Chairs = new[]
		//		{
		//			new Chair {ChairNumber = 0, Name = "player0", Hand = new CardMask("Ac, Ad"), Balance = 100}
		//		}.ToList()
		//	};

		//	var controller = new MainController(game);

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 }
		//		}).ToArray();

		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 254, 254, 254, 254, 254 },
		//		m_player = players
		//	};

		//	game = controller.UpdateGame(state);

		//	Assert.IsNotNull(game.GetChair(0).Name);
		//}


		//[TestMethod]
		//public void TestUpdateGame_ChangesPlayersCorrectly()
		//{
		//	var game = new Game
		//	{
		//		Chairs = new[]
		//		{
		//			new Chair {ChairNumber = 0, Name = "player0", Hand = new CardMask("Ac, Ad"), Balance = 100}
		//		}.ToList()
		//	};

		//	var controller = new MainController(game);

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 }
		//		}).ToArray();

		//	players[0].m_name = "player1";
		//	players[0].m_cards = new byte[] { 1, 2 };
		//	//players[0].m_balance = 50.0;

		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 254, 254, 254, 254, 254 },
		//		m_player = players
		//	};

		//	game = controller.UpdateGame(state);

		//	Assert.AreEqual("player1", game.GetChair(0).Name);
		//	//Assert.AreEqual(50, game.GetChair(0).Balance);
		//	Assert.AreEqual(new CardMask("3h, 4h"), game.GetChair(0).Hand);
		//}

		//[TestMethod]
		//public void TestUpdateGame_DoesNotChangePlayerBalance()
		//{
		//	var game = new Game
		//	{
		//		Chairs = new[]
		//		{
		//			new Chair {ChairNumber = 0, Name = "player0", Hand = new CardMask("Ac, Ad"), Balance = 100}
		//		}.ToList()
		//	};

		//	var controller = new MainController(game);

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 }
		//		}).ToArray();

		//	players[0].m_name = "player1";
		//	players[0].m_cards = new byte[] { 1, 2 };
		//	players[0].m_balance = 50.0;

		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 254, 254, 254, 254, 254 },
		//		m_player = players
		//	};

		//	game = controller.UpdateGame(state);

		//	Assert.AreEqual("player1", game.GetChair(0).Name);
		//	Assert.AreEqual(100, game.GetChair(0).Balance);
		//	Assert.AreEqual(new CardMask("3h, 4h"), game.GetChair(0).Hand);
		//}

		//[TestMethod]
		//public void TestUpdateActions_AddsActionsCorrectly()
		//{
		//	var controller = new MainController(new Game
		//	{
		//		SmallBlind = 0.5,
		//		BigBlind = 1,
		//		DealerChair = 0
		//	});

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 }
		//		}).ToArray();

		//	players[0].m_name = "player1";
		//	players[0].m_cards = new byte[] { 1, 2 };
		//	players[0].m_balance = 50.0;
			
		//	players[1].m_name = "player2";
		//	players[1].m_cards = new byte[] { 255, 255 };
		//	players[1].m_balance = 50.0;
		//	players[1].m_currentbet = 0.5;

		//	players[2].m_name = "player3";
		//	players[2].m_cards = new byte[] { 255, 255 };
		//	players[2].m_balance = 50.0;
		//	players[2].m_currentbet = 1;

		//	players[3].m_name = "player4";
		//	players[3].m_cards = new byte[] { 255, 255 };
		//	players[3].m_balance = 50.0;
		//	players[3].m_currentbet = 3;

		//	players[4].m_name = "player5";
		//	players[4].m_cards = new byte[] { 255, 255 };
		//	players[4].m_balance = 50.0;


		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 254, 254, 254, 254, 254 },
		//		m_player = players,
		//		m_dealer_chair = 0
		//	};

		//	var game = controller.UpdateGame(state);
		//	var actions = controller.UpdateActions(state);

		//	Assert.AreEqual("player2", game.Actions[0].Chair.Name);
		//	Assert.AreEqual(0.5, game.Actions[0].Amount);
		//	Assert.AreEqual(PlayerActionEnum.SmallBlind, game.Actions[0].Action);
		//	Assert.AreEqual(BetRound.Preflop, game.Actions[0].BetRound);

		//	Assert.AreEqual("player3", game.Actions[1].Chair.Name);
		//	Assert.AreEqual(1, game.Actions[1].Amount);
		//	Assert.AreEqual(PlayerActionEnum.BigBlind, game.Actions[1].Action);
		//	Assert.AreEqual(BetRound.Preflop, game.Actions[1].BetRound);

		//	Assert.AreEqual("player4", game.Actions[2].Chair.Name);
		//	Assert.AreEqual(3, game.Actions[2].Amount);
		//	Assert.AreEqual(PlayerActionEnum.Raise, game.Actions[2].Action);
		//	Assert.AreEqual(BetRound.Preflop, game.Actions[2].BetRound);
		//}

		//[TestMethod]
		//public void TestUpdateActions_UpdatesActionsAcrossBetRoundsCorrectly()
		//{
		//	var game = new Game
		//	{
		//		SmallBlind = 0.5,
		//		BigBlind = 1,
		//		DealerChair = 4,
		//		Chairs = new List<Chair>
		//		{
		//			new Chair {Name = "player1", Balance = 49.0, ChairNumber = 0, Hand = new CardMask("3h, 4h")},
		//			new Chair {Name = "player2", Balance = 47.0, ChairNumber = 1},
		//			new Chair {Name = "player3", Balance = 50.0, ChairNumber = 2},
		//			new Chair {Name = "player4", Balance = 50.0, ChairNumber = 3},
		//			new Chair {Name = "player5", Balance = 50.0, ChairNumber = 4},
		//			new Chair {Name = "player6", Balance = 49.5, ChairNumber = 5}
		//		}
		//	};

		//	game.Actions = new List<PlayerAction>
		//	{
		//		new PlayerAction(game.GetChair(5), PlayerActionEnum.SmallBlind, 0.5),
		//		new PlayerAction(game.GetChair(0), PlayerActionEnum.BigBlind, 1),
		//		new PlayerAction(game.GetChair(1), PlayerActionEnum.Raise, 3)
		//	};

		//	var controller = new MainController(game);

		//	var players = new ManagedHoldemPlayer[]
		//	{
		//		new ManagedHoldemPlayer { m_balance = 49.0, m_cards = new byte[] { 1, 2 }, m_name = "player1" },
		//		new ManagedHoldemPlayer { m_balance = 41.0, m_cards = new byte[] { 255, 255 }, m_name = "player2", m_currentbet = 3},
		//		new ManagedHoldemPlayer { m_balance = 50.0, m_cards = new byte[] { 255, 255 }, m_name = "player3" },
		//		new ManagedHoldemPlayer { m_balance = 50.0, m_cards = new byte[] { 255, 255 }, m_name = "player4" },
		//		new ManagedHoldemPlayer { m_balance = 50.0, m_cards = new byte[] { 255, 255 }, m_name = "player5" },
		//		new ManagedHoldemPlayer { m_balance = 50.0, m_cards = new byte[] { 255, 255 }, m_name = "player6" },
		//		new ManagedHoldemPlayer { m_cards = new byte[] { 255, 255 } },
		//		new ManagedHoldemPlayer { m_cards = new byte[] { 255, 255 } },
		//		new ManagedHoldemPlayer { m_cards = new byte[] { 255, 255 } },
		//		new ManagedHoldemPlayer { m_cards = new byte[] { 255, 255 } },
		//	};

		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 5, 6, 7, 254, 254 },
		//		m_player = players,
		//		m_dealer_chair = 4
		//	};

		//	game = controller.UpdateGame(state);
		//	var actions = controller.UpdateActions(state);

		//	Assert.AreEqual("player2", actions[0].Chair.Name);
		//	Assert.AreEqual(PlayerActionEnum.Bet, actions[0].Action);
		//	Assert.AreEqual(3, actions[0].Amount);
		//	Assert.AreEqual(BetRound.Flop, actions[0].BetRound);
		//}

		//[TestMethod]
		//public void TestUpdateGame_SetsDealerChairCorrectly()
		//{
		//	var controller = new MainController(new Game());

		//	var players = Enumerable.Range(0, 9)
		//		.Select(i => new ManagedHoldemPlayer
		//		{
		//			m_cards = new byte[] { 254, 254 },
		//		}).ToArray();

		//	players[0].m_name = "player1";
		//	players[0].m_cards = new byte[] { 1, 2 };
		//	players[0].m_balance = 50.0;
		//	players[1].m_name = "player2";
		//	players[1].m_cards = new byte[] { 255, 255 };
		//	players[1].m_balance = 50.0;
		//	players[2].m_name = "player3";
		//	players[2].m_cards = new byte[] { 255, 255 };
		//	players[2].m_balance = 50.0;
		//	players[3].m_name = "player4";
		//	players[3].m_cards = new byte[] { 255, 255 };
		//	players[3].m_balance = 50.0;
		//	players[4].m_name = "player5";
		//	players[4].m_cards = new byte[] { 255, 255 };
		//	players[4].m_balance = 50.0;

		//	var state = new ManagedHoldemState
		//	{
		//		m_cards = new byte[] { 254, 254, 254, 254, 254 },
		//		m_player = players,
		//		m_dealer_chair = 2
		//	};

		//	var game = controller.UpdateGame(state);

		//	Assert.AreEqual(2, game.DealerChair);
		//}
	}
}
