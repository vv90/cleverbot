﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.OHConnectivity.Tests
{
	[TestClass]
	public class GameConstructionTest
	{
		[TestMethod]
		public void TestFromFile()
		{
			var fileNames = Directory.GetFiles("TableStates").ToList();

			var parser = new HoldemManagerLogParser();

			var parsedGames = new Dictionary<Game, List<string>>();
			Game currentGame = null;

			parser.NewGameParsingStartedEvent += (sender, args) =>
			{
				parsedGames.Add(args.Game, new List<string>());
				currentGame = args.Game;
			};

			parser.LineParsedEvent += (sender, args) =>
			{
				parsedGames[currentGame].Add(args.Line);
			};

			parser.Import("HH.txt");

			foreach (var file in fileNames)
			{
				var tableStates = GameConstructionTester.GetTableStates(file);
				var controller = new MainController();

				var gameNumber = tableStates.First().Symbols.HandNumber;

				var expectedGame = parsedGames.FirstOrDefault(g => g.Key.GameNumber == gameNumber).Key;

				foreach (TableState state in tableStates)
				{
					controller.UpdateState(state);
				}

				var actualGame = controller.Game;

				Assert.IsNotNull(expectedGame, string.Format("[{0}/{1}] No game found in hand history", 
					fileNames.IndexOf(file), parsedGames.Count));

				var testGameError = GameConstructionTester.TestGame(expectedGame, actualGame, 1);
				Assert.IsNull(testGameError, string.Format("[{0}/{1}] {2}", 
					fileNames.IndexOf(file), parsedGames.Count, testGameError));
			}
		}

		private string GenerateTestCase(List<string> log, List<TableState> tableStates)
		{
			var sb = new StringBuilder();
			sb.AppendLine("var gameLog = new []");
			sb.AppendLine("{");
			foreach (var line in log)
				sb.AppendFormat("\t\"{0}\",\n", line);
			sb.AppendLine("};");

			sb.AppendLine("var states = new []");
			sb.AppendLine("{");
			foreach (var state in tableStates)
			{
				sb.AppendFormat("\tCreateState(\"{0}\", new [] {{{1}}}, new byte[] {{{2}}}, {3}, {4}, new []\n", 
					state.State.m_title,
					string.Join(", ", state.State.m_pot.Select(d => string.Format("{0:0.00}", d))),
					string.Join(", ", state.State.m_cards),
					state.State.m_dealer_chair,
					state.State.m_playing_bits);
				sb.AppendLine("\t{");
				foreach (var player in state.State.m_player)
				{
					sb.AppendFormat("\t\tCreatePlayer(\"{0}\", {1}, {2}, new byte[] {{{3}}}, {4}),\n",
						player.m_name,
						string.Format("{0:0.00}", player.m_balance),
						string.Format("{0:0.00}", player.m_currentbet),
						string.Join(", ", player.m_cards),
						player.m_is_known);
				}
				sb.AppendFormat("\t}}, CreateSymbols({0}, {1}, {2}, {3}, {4}, \"{5}\")),\n",
					state.Symbols.TurnBits,
					state.Symbols.IsMyTurn ? "true" : "false",
					state.Symbols.SmallBlind,
					state.Symbols.BigBlind,
					state.Symbols.SeatedBits,
					state.Symbols.HandNumber);
			}
			sb.AppendLine("};");

			return sb.ToString();
		}

		internal TableState CreateState(string title, double[] pot, byte[] cards, int dealerChair, byte playingBits,
			ManagedHoldemPlayer[] players, OpenHoldemSymbols symbols)
		{
			var holdemState = new ManagedHoldemState
			{
				m_title = title,
				m_pot = pot,
				m_cards = cards,
				m_dealer_chair = (byte) dealerChair,
				m_playing_bits = playingBits,
				m_player = players
			};
			return new TableState
			{
				State = holdemState,
				Symbols = symbols
			};
		}

		internal ManagedHoldemPlayer CreatePlayer(string name, double balance, double currentBet, byte[] cards, byte known)
		{
			return new ManagedHoldemPlayer
			{
				m_name = name,
				m_balance = balance,
				m_currentbet = currentBet,
				m_cards = cards,
				m_is_known = known
			};
		}

		internal OpenHoldemSymbols CreateSymbols(
			int turnBits, bool isMyTurn, double smallBlind, double bigBlind, int seatedBits, string handNumber)
		{
			return new OpenHoldemSymbols
			{
				TurnBits = turnBits,
				IsMyTurn = isMyTurn,
				SmallBlind = smallBlind,
				BigBlind = bigBlind,
				SeatedBits = seatedBits,
				HandNumber = handNumber
			};
		}

		
		[TestMethod]
		public void TestGame_437654411()
		{
			var gameLog = new[]
			{
				"Game started at: 2015/6/29 7:20:43",
				"Game ID: 437654411 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 1 is the button",
				"Seat 1: cappuccin0vv (1467.75).",
				"Seat 3: Willy24 (1065.92).",
				"Seat 4: Bigvic1 (600).",
				"Seat 5: goodiebad (6485.99).",
				"Seat 6: Thomthumb (3143.66).",
				"Player Willy24 has small blind (5)",
				"Player Bigvic1 has big blind (10)",
				"Player Willy24 received a card.",
				"Player Willy24 received a card.",
				"Player Bigvic1 received a card.",
				"Player Bigvic1 received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [As]",
				"Player cappuccin0vv received card: [3d]",
				"Player goodiebad folds",
				"Player Thomthumb calls (10)",
				"Player cappuccin0vv folds",
				"Player Willy24 calls (5)",
				"Player Bigvic1 raises (10)",
				"Player Thomthumb calls (10)",
				"Player Willy24 calls (10)",
				"*** FLOP ***: [10h Ks 8s]",
				"Player Willy24 bets (10)",
				"Player Bigvic1 raises (60)",
				"Player Thomthumb calls (60)",
				"Player Willy24 calls (50)",
				"*** TURN ***: [10h Ks 8s] [7d]",
				"Player Willy24 checks",
				"Player Bigvic1 bets (118.50)",
				"Player Thomthumb raises (237)",
				"Player Willy24 folds",
				"Player Bigvic1 raises (237)",
				"Player Thomthumb raises (474)",
				"Player Bigvic1 allin (164.50)",
				"Uncalled bet (191) returned to Thomthumb",
				"*** RIVER ***: [10h Ks 8s 7d] [Jc]",
				"------ Summary ------",
				"Pot: 1277. Rake 3",
				"Board: [10h Ks 8s 7d Jc]",
				"Player cappuccin0vv does not show cards.Bets: 0. Collects: 0. Wins: 0.",
				"Player Willy24 does not show cards.Bets: 80. Collects: 0. Loses: 80.",
				"Player Bigvic1 shows: One pair of 10s [10s 2s]. Bets: 600. Collects: 0. Loses: 600.",
				"Player goodiebad does not show cards.Bets: 0. Collects: 0. Wins: 0.",
				"*Player Thomthumb shows: Straight to J [6h 9h]. Bets: 600. Collects: 1277. Wins: 677.",
				"Game ended at: 2015/6/29 7:22:42",
				"",
			};
			var states = new[]
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {25.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 0, 1, new []	
				{
					CreatePlayer("cappuccin0vv", 1467.75, 0.00, new byte[] {51, 14}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 1060.92, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("Blgvic1", 590.00, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodiebad", 6485.99, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3133.66, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(9, true, 5, 10, 61, "437654411")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {25.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 0, 1, new []	
				{
					CreatePlayer("cappuccin0vv", 1467.75, 0.00, new byte[] {51, 14}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 1060.92, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("Blgvic1", 590.00, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodlebad", 6485.99, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3133.66, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(9, true, 5, 10, 61, "437654411")),
			};


			var controller = new MainController();
			foreach(var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437655730()
		{
			var gameLog = new[]
			{
				"Game started at: 2015/6/29 7:22:47",
				"Game ID: 437655730 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 3 is the button",
				"Seat 1: cappuccin0vv (1467.75).",
				"Seat 3: Willy24 (985.92).",
				"Seat 5: goodiebad (6485.99).",
				"Seat 6: Thomthumb (3820.66).",
				"Player goodiebad has small blind (5)",
				"Player Thomthumb has big blind (10)",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [6d]",
				"Player cappuccin0vv received card: [4s]",
				"Player Willy24 received a card.",
				"Player Willy24 received a card.",
				"Player cappuccin0vv folds",
				"Player Willy24 calls (10)",
				"Player goodiebad calls (5)",
				"Player Thomthumb checks",
				"*** FLOP ***: [5s 4h 10s]",
				"Player goodiebad bets (10)",
				"Player Thomthumb calls (10)",
				"Player Willy24 calls (10)",
				"*** TURN ***: [5s 4h 10s] [7d]",
				"Player goodiebad bets (10)",
				"Player Thomthumb calls (10)",
				"Player Willy24 calls (10)",
				"*** RIVER ***: [5s 4h 10s 7d] [9d]",
				"Player goodiebad bets (120)",
				"Player Thomthumb folds",
				"Player Willy24 calls (120)",
				"------ Summary ------",
				"Pot: 327.75. Rake 2.25",
				"Board: [5s 4h 10s 7d 9d]",
				"Player cappuccin0vv does not show cards.Bets: 0. Collects: 0. Wins: 0.",
				"Player Willy24 shows: One pair of 10s [Qh 10c]. Bets: 150. Collects: 0. Loses: 150.",
				"*Player goodiebad shows: Two pairs. 7s and 5s [7h 5c]. Bets: 150. Collects: 327.75. Wins: 177.75.",
				"Player Thomthumb does not show cards.Bets: 30. Collects: 0. Loses: 30.",
				"Game ended at: 2015/6/29 7:23:34",
				"",
			};
			var states = new[]
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {15.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 2, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1467.75, 0.00, new byte[] {17, 41}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 985.92, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("SMALLBLIND", 6480.99, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("BIGBLIND", 3810.66, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(11, true, 5, 10, 61, "437655730")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437656284()
		{
			var gameLog = new[]
			{
				"Game started at: 2015/6/29 7:23:40",
				"Game ID: 437656284 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 5 is the button",
				"Seat 1: cappuccin0vv (1467.75).",
				"Seat 3: Willy24 (835.92).",
				"Seat 5: goodiebad (6663.74).",
				"Seat 6: Thomthumb (3790.66).",
				"Player Thomthumb has small blind (5)",
				"Player cappuccin0vv has big blind (10)",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [2c]",
				"Player cappuccin0vv received card: [6d]",
				"Player Willy24 received a card.",
				"Player Willy24 received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Willy24 calls (10)",
				"Player goodiebad calls (10)",
				"Player Thomthumb calls (5)",
				"Player cappuccin0vv checks",
				"*** FLOP ***: [10d Qh Qc]",
				"Player Thomthumb checks",
				"Player cappuccin0vv checks",
				"Player Willy24 checks",
				"Player goodiebad checks",
				"*** TURN ***: [10d Qh Qc] [8s]",
				"Player Thomthumb checks",
				"Player cappuccin0vv checks",
				"Player Willy24 checks",
				"Player goodiebad checks",
				"*** RIVER ***: [10d Qh Qc 8s] [Qs]",
				"Player Thomthumb checks",
				"Player cappuccin0vv checks",
				"Player Willy24 checks",
				"Player goodiebad checks",
				"Player cappuccin0vv mucks cards",
				"Player goodiebad mucks cards",
				"------ Summary ------",
				"Pot: 38. Rake 2",
				"Board: [10d Qh Qc 8s Qs]",
				"Player cappuccin0vv mucks (does not show: [2c 6d]). Bets: 10. Collects: 0. Loses: 10.",
				"Player Willy24 shows: Three Of Kind of Qs [Jd 3s]. Bets: 10. Collects: 0. Loses: 10.",
				"Player goodiebad mucks (does not show cards). Bets: 10. Collects: 0. Loses: 10.",
				"*Player Thomthumb shows: Three Of Kind of Qs (kicker K) [Jc Kc]. Bets: 10. Collects: 38. Wins: 28.",
				"Game ended at: 2015/6/29 7:24:44",
				"",
			};
			var states = new[]
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {40.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1457.75, 10.00, new byte[] {26, 17}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 825.92, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6653.74, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 3780.66, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 61, "437656284")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {38.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {21, 10, 36, 254, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1457.75, 0.00, new byte[] {26, 17}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 825.92, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6653.74, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 3780.66, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 61, "437656284")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {38.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {21, 10, 36, 45, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1457.75, 0.00, new byte[] {26, 17}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 825.92, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6653.74, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 3780.66, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 61, "437656284")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {38.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {21, 10, 36, 45, 49}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1457.75, 0.00, new byte[] {26, 17}, 3),
					CreatePlayer("", 3303.66, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("Willy4", 825.92, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6653.74, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 3780.66, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 61, "437656284")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437659685()
		{
			var gameLog = new[]
			{
				"Game started at: 2015/6/29 7:29:48",
				"Game ID: 437660154 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 1 is the button",
				"Seat 1: cappuccin0vv (1437.75).",
				"Seat 2: wcoop (305.74).",
				"Seat 5: goodiebad (6682.46).",
				"Seat 6: Thomthumb (4029.49).",
				"Player wcoop has small blind (5)",
				"Player goodiebad has big blind (10)",
				"Player wcoop received a card.",
				"Player wcoop received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [7c]",
				"Player cappuccin0vv received card: [Ad]",
				"Player Thomthumb calls (10)",
				"Player cappuccin0vv raises (35)",
				"Player wcoop calls (30)",
				"Player goodiebad calls (25)",
				"Player Thomthumb calls (25)",
				"*** FLOP ***: [5h Jd 9d]",
				"Player wcoop bets (10)",
				"Player goodiebad folds",
				"Player Thomthumb calls (10)",
				"Player cappuccin0vv folds",
				"*** TURN ***: [5h Jd 9d] [Jc]",
				"Player wcoop bets (105.16)",
				"Player Thomthumb folds",
				"Uncalled bet (105.16) returned to wcoop",
				"Player wcoop mucks cards",
				"------ Summary ------",
				"Pot: 157.75. Rake 2.25",
				"Board: [5h Jd 9d Jc]",
				"Player cappuccin0vv does not show cards.Bets: 35. Collects: 0. Loses: 35.",
				"*Player wcoop mucks (does not show cards). Bets: 45. Collects: 157.75. Wins: 112.75.",
				"Player goodiebad does not show cards.Bets: 35. Collects: 0. Loses: 35.",
				"Player Thomthumb does not show cards.Bets: 45. Collects: 0. Loses: 45.",
				"Game ended at: 2015/6/29 7:30:39",
				"",
			};

			var states = new[]
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {25.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 0, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1437.75, 0.00, new byte[] {31, 25}, 3),
					CreatePlayer("wcoop", 300.74, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6672.46, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 4019.49, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(11, true, 5, 10, 59, "437660154")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {157.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {3, 22, 20, 254, 254}, 0, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1402.75, 0.00, new byte[] {31, 25}, 3),
					CreatePlayer("wcoop", 260.74, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6647.46, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3984.49, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(11, true, 5, 10, 59, "437660154")),
};



			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437661199()
		{
			var gameLog = new []
			{
				"Game started at: 2015/6/29 7:31:31",
				"Game ID: 437661199 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 5 is the button",
				"Seat 1: cappuccin0vv (1402.75).",
				"Seat 2: wcoop (388.49).",
				"Seat 5: goodiebad (6695.21).",
				"Seat 6: Thomthumb (3964.49).",
				"Player Thomthumb has small blind (5)",
				"Player cappuccin0vv has big blind (10)",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [8s]",
				"Player cappuccin0vv received card: [2d]",
				"Player wcoop received a card.",
				"Player wcoop received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player wcoop calls (10)",
				"Player goodiebad folds",
				"Player Thomthumb calls (5)",
				"Player cappuccin0vv checks",
				"*** FLOP ***: [4h 2h 7c]",
				"Player Thomthumb checks",
				"Player cappuccin0vv checks",
				"Player wcoop checks",
				"*** TURN ***: [4h 2h 7c] [7d]",
				"Player Thomthumb checks",
				"Player cappuccin0vv checks",
				"Player wcoop bets (19)",
				"Player Thomthumb calls (19)",
				"Player cappuccin0vv folds",
				"*** RIVER ***: [4h 2h 7c 7d] [6c]",
				"Player Thomthumb checks",
				"Player wcoop bets (43.83)",
				"Player Thomthumb calls (43.83)",
				"Player Thomthumb mucks cards",
				"------ Summary ------",
				"Pot: 153.41. Rake 2.25",
				"Board: [4h 2h 7c 7d 6c]",
				"Player cappuccin0vv does not show cards.Bets: 10. Collects: 0. Loses: 10.",
				"*Player wcoop shows: Three Of Kind of 7s [10s 7s]. Bets: 72.83. Collects: 153.41. Wins: 80.58.",
				"Player goodiebad does not show cards.Bets: 0. Collects: 0. Wins: 0.",
				"Player Thomthumb mucks (does not show cards). Bets: 72.83. Collects: 0. Loses: 72.83.",
				"Game ended at: 2015/6/29 7:32:34",
				"",
			};
			var states = new []
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {30.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1392.75, 10.00, new byte[] {45, 13}, 3),
					CreatePlayer("wcoop", 378.49, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("goodlebad", 6695.21, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3954.49, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 51, "437661199")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {28.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {2, 0, 31, 254, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1392.75, 0.00, new byte[] {45, 13}, 3),
					CreatePlayer("wcoop", 378.49, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("goodlebad", 6695.21, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3954.49, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 51, "437661199")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {28.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {2, 0, 31, 18, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1392.75, 0.00, new byte[] {45, 13}, 3),
					CreatePlayer("wcoop", 378.49, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("goodlebad", 6695.21, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3954.49, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 51, "437661199")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {66.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {2, 0, 31, 18, 254}, 4, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1392.75, 0.00, new byte[] {45, 13}, 3),
					CreatePlayer("wcoop", 359.49, 19.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("goodlebad", 6695.21, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3935.49, 19.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(9, true, 5, 10, 51, "437661199")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437664211()
		{
			var gameLog = new[]
			{
				"Game started at: 2015/6/29 7:36:27",
				"Game ID: 437664211 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 6 is the button",
				"Seat 1: cappuccin0vv (1272.75).",
				"Seat 2: wcoop (522.16).",
				"Seat 4: cowboy2228 (400).",
				"Seat 5: goodiebad (6640.21).",
				"Seat 6: Thomthumb (4006.32).",
				"Player cappuccin0vv has small blind (5)",
				"Player wcoop has big blind (10)",
				"Player cowboy2228 posts (10)",
				"Player cappuccin0vv received card: [Qs]",
				"Player cappuccin0vv received card: [7c]",
				"Player wcoop received a card.",
				"Player wcoop received a card.",
				"Player cowboy2228 received a card.",
				"Player cowboy2228 received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cowboy2228 raises (10)",
				"Player goodiebad calls (20)",
				"Player Thomthumb calls (20)",
				"Player cappuccin0vv folds",
				"Player wcoop calls (10)",
				"*** FLOP ***: [10c 4c Qc]",
				"Player wcoop checks",
				"Player cowboy2228 bets (54.66)",
				"Player goodiebad calls (54.66)",
				"Player Thomthumb folds",
				"Player wcoop calls (54.66)",
				"*** TURN ***: [10c 4c Qc] [9s]",
				"Player wcoop checks",
				"Player cowboy2228 allin (325.34)",
				"Player goodiebad folds",
				"Player wcoop calls (325.34)",
				"*** RIVER ***: [10c 4c Qc 9s] [10d]",
				"------ Summary ------",
				"Pot: 896.66. Rake 3",
				"Board: [10c 4c Qc 9s 10d]",
				"Player cappuccin0vv does not show cards.Bets: 5. Collects: 0. Loses: 5.",
				"Player wcoop shows: One pair of 10s [3c 7h]. Bets: 400. Collects: 0. Loses: 400.",
				"*Player cowboy2228 shows: Two pairs. As and 10s [Ad Ac]. Bets: 400. Collects: 896.66. Wins: 496.66.",
				"Player goodiebad does not show cards.Bets: 74.66. Collects: 0. Loses: 74.66.",
				"Player Thomthumb does not show cards.Bets: 20. Collects: 0. Loses: 20.",
				"Game ended at: 2015/6/29 7:37:30",
				"",
			};
			var states = new[]
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {75.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 5, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1267.75, 5.00, new byte[] {49, 31}, 3),
					CreatePlayer("wcoop", 512.16, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("cowboy8", 380.00, 20.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodlebad", 6620.21, 20.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 3986.32, 20.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(9, true, 5, 10, 59, "437664211")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437665597()
		{
			var gameLog = new []
			{
				"Game started at: 2015/6/29 7:38:44",
				"Game ID: 437665597 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 4 is the button",
				"Seat 1: cappuccin0vv (1232.75).",
				"Seat 4: cowboy2228 (841.66).",
				"Seat 5: goodiebad (6565.55).",
				"Seat 6: Thomthumb (4195.48).",
				"Player goodiebad has small blind (5)",
				"Player Thomthumb has big blind (10)",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [4h]",
				"Player cappuccin0vv received card: [4c]",
				"Player cowboy2228 received a card.",
				"Player cowboy2228 received a card.",
				"Player cappuccin0vv raises (25)",
				"Player cowboy2228 calls (25)",
				"Player goodiebad calls (20)",
				"Player Thomthumb calls (15)",
				"*** FLOP ***: [8c 8s 6d]",
				"Player goodiebad checks",
				"Player Thomthumb checks",
				"Player cappuccin0vv bets (80)",
				"Player cowboy2228 folds",
				"Player goodiebad calls (80)",
				"Player Thomthumb folds",
				"*** TURN ***: [8c 8s 6d] [10c]",
				"Player goodiebad checks",
				"Player cappuccin0vv checks",
				"*** RIVER ***: [8c 8s 6d 10c] [3c]",
				"Player goodiebad bets (128.87)",
				"Player cappuccin0vv folds",
				"Uncalled bet (128.87) returned to goodiebad",
				"Player goodiebad mucks cards",
				"------ Summary ------",
				"Pot: 257.75. Rake 2.25",
				"Board: [8c 8s 6d 10c 3c]",
				"Player cappuccin0vv does not show cards.Bets: 105. Collects: 0. Loses: 105.",
				"Player cowboy2228 does not show cards.Bets: 25. Collects: 0. Loses: 25.",
				"*Player goodiebad mucks (does not show cards). Bets: 105. Collects: 257.75. Wins: 152.75.",
				"Player Thomthumb does not show cards.Bets: 25. Collects: 0. Loses: 25.",
				"Game ended at: 2015/6/29 7:39:41",
				"",
			};
			var states = new []
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {15.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 3, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1232.75, 0.00, new byte[] {2, 28}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("cowboy8", 841.66, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodiebad", 6560.55, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 4185.48, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(11, true, 5, 10, 59, "437665597")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {97.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {32, 45, 17, 254, 254}, 3, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1207.75, 0.00, new byte[] {2, 28}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("cowboy8", 816.66, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodiebad", 6540.55, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 4170.48, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 59, "437665597")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {257.75, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {32, 45, 17, 34, 254}, 3, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1127.75, 0.00, new byte[] {2, 28}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("cowboy8", 816.66, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodiebad", 6460.55, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 4170.48, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 59, "437665597")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {386.62, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {32, 45, 17, 34, 27}, 3, 1, new []
				{
					CreatePlayer("cappuccin0vv", 1127.75, 0.00, new byte[] {2, 28}, 3),
					CreatePlayer("SITTINGOUT", 0.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("", 825.92, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("cowboy8", 816.66, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodiebad", 6331.68, 128.87, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 4170.48, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(9, true, 5, 10, 59, "437665597")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437667580()
		{
			var gameLog = new[]
			{
				"Game started at: 2015/6/29 7:42:2",
				"Game ID: 437667580 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 1 is the button",
				"Seat 1: cappuccin0vv (2439.50).",
				"Seat 3: GRiNCHFACEKiLLa (950).",
				"Seat 4: cowboy2228 (756.66).",
				"Seat 5: goodiebad (6668.30).",
				"Seat 6: Thomthumb (3014.23).",
				"Player GRiNCHFACEKiLLa has small blind (5)",
				"Player cowboy2228 has big blind (10)",
				"Player GRiNCHFACEKiLLa received a card.",
				"Player GRiNCHFACEKiLLa received a card.",
				"Player cowboy2228 received a card.",
				"Player cowboy2228 received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [4d]",
				"Player cappuccin0vv received card: [10d]",
				"Player goodiebad folds",
				"Player Thomthumb calls (10)",
				"Player cappuccin0vv folds",
				"Player GRiNCHFACEKiLLa raises (15)",
				"Player cowboy2228 calls (10)",
				"Player Thomthumb calls (10)",
				"*** FLOP ***: [Qh 6c 8c]",
				"Player GRiNCHFACEKiLLa bets (28.50)",
				"Player cowboy2228 folds",
				"Player Thomthumb calls (28.50)",
				"*** TURN ***: [Qh 6c 8c] [5c]",
				"Player GRiNCHFACEKiLLa bets (57)",
				"Player Thomthumb calls (57)",
				"*** RIVER ***: [Qh 6c 8c 5c] [8s]",
				"Player GRiNCHFACEKiLLa bets (114)",
				"Player Thomthumb folds",
				"Uncalled bet (114) returned to GRiNCHFACEKiLLa",
				"Player GRiNCHFACEKiLLa mucks cards",
				"------ Summary ------",
				"Pot: 228. Rake 3",
				"Board: [Qh 6c 8c 5c 8s]",
				"Player cappuccin0vv does not show cards.Bets: 0. Collects: 0. Wins: 0.",
				"*Player GRiNCHFACEKiLLa mucks (does not show cards). Bets: 105.50. Collects: 228. Wins: 122.50.",
				"Player cowboy2228 does not show cards.Bets: 20. Collects: 0. Loses: 20.",
				"Player goodiebad does not show cards.Bets: 0. Collects: 0. Wins: 0.",
				"Player Thomthumb does not show cards.Bets: 105.50. Collects: 0. Loses: 105.50.",
				"Game ended at: 2015/6/29 7:43:3",
				"",
			};
			var states = new[]
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {25.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 0, 1, new []
				{
					CreatePlayer("cappuccin0vv", 2439.50, 0.00, new byte[] {15, 21}, 3),
					CreatePlayer("SITTINGOUT", 400.00, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("GRillCllFACElIL", 945.00, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("cowboy8", 746.66, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodlebad", 6668.30, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 3004.23, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(11, true, 5, 10, 63, "437667580")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}

		[TestMethod]
		public void TestGame_437668243()
		{
			var gameLog = new []
			{
				"Game started at: 2015/6/29 7:43:7",
				"Game ID: 437668243 5/10 PM Hold'em  5/10 (Hold'em)",
				"Seat 3 is the button",
				"Seat 1: cappuccin0vv (2439.50).",
				"Seat 2: Intern (400).",
				"Seat 3: GRiNCHFACEKiLLa (1072.50).",
				"Seat 4: cowboy2228 (736.66).",
				"Seat 5: goodiebad (6668.30).",
				"Seat 6: Thomthumb (2908.73).",
				"Player cowboy2228 has small blind (5)",
				"Player goodiebad has big blind (10)",
				"Player Intern posts (10)",
				"Player cowboy2228 received a card.",
				"Player cowboy2228 received a card.",
				"Player goodiebad received a card.",
				"Player goodiebad received a card.",
				"Player Thomthumb received a card.",
				"Player Thomthumb received a card.",
				"Player cappuccin0vv received card: [Qd]",
				"Player cappuccin0vv received card: [Qc]",
				"Player Intern received a card.",
				"Player Intern received a card.",
				"Player GRiNCHFACEKiLLa received a card.",
				"Player GRiNCHFACEKiLLa received a card.",
				"Player Thomthumb calls (10)",
				"Player cappuccin0vv raises (50)",
				"Player Intern calls (40)",
				"Player GRiNCHFACEKiLLa calls (50)",
				"Player cowboy2228 calls (45)",
				"Player goodiebad folds",
				"Player Thomthumb calls (40)",
				"*** FLOP ***: [5d 8c 8s]",
				"Player cowboy2228 checks",
				"Player Thomthumb checks",
				"Player cappuccin0vv checks",
				"Player Intern allin (350)",
				"Player GRiNCHFACEKiLLa calls (350)",
				"Player cowboy2228 folds",
				"Player Thomthumb calls (350)",
				"Player cappuccin0vv folds",
				"*** TURN ***: [5d 8c 8s] [Js]",
				"Player Thomthumb checks",
				"Player GRiNCHFACEKiLLa checks",
				"*** RIVER ***: [5d 8c 8s Js] [9s]",
				"Player Thomthumb checks",
				"Player GRiNCHFACEKiLLa checks",
				"Player GRiNCHFACEKiLLa mucks cards",
				"------ Summary ------",
				"Pot: 1307. Rake 3",
				"Board: [5d 8c 8s Js 9s]",
				"Player cappuccin0vv does not show cards.Bets: 50. Collects: 0. Loses: 50.",
				"*Player Intern shows: Two pairs. 8s and 5s [7s 5h]. Bets: 400. Collects: 1307. Wins: 907.",
				"Player GRiNCHFACEKiLLa mucks (does not show cards). Bets: 400. Collects: 0. Loses: 400.",
				"Player cowboy2228 does not show cards.Bets: 50. Collects: 0. Loses: 50.",
				"Player goodiebad does not show cards.Bets: 10. Collects: 0. Loses: 10.",
				"Player Thomthumb shows: Two pairs. 8s and 4s [4d 4s]. Bets: 400. Collects: 0. Loses: 400.",
				"Game ended at: 2015/6/29 7:44:22",
				"",
			};
			var states = new []
			{
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {35.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {254, 254, 254, 254, 254}, 2, 1, new []
				{
					CreatePlayer("cappuccin0vv", 2439.50, 0.00, new byte[] {23, 36}, 3),
					CreatePlayer("Intern", 390.00, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("GRillCllFACElIL", 1072.50, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("cowboy8", 731.66, 5.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodlebad", 6658.30, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("Thomthumb", 2898.73, 10.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(11, true, 5, 10, 63, "437668243")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {257.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {16, 32, 45, 254, 254}, 2, 1, new []
				{
					CreatePlayer("cappuccin0vv", 2389.50, 0.00, new byte[] {23, 36}, 3),
					CreatePlayer("Intern", 350.00, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("GRillCllFACElIL", 1022.50, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("cowboy8", 686.66, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("goodlebad", 6658.30, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 2858.73, 0.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(12, true, 5, 10, 63, "437668243")),
				CreateState(" PM Hold'em  5/10 - No Limit - 5 / 10 Hold'em", new [] {1307.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, new byte[] {16, 32, 45, 254, 254}, 2, 1, new []
				{
					CreatePlayer("cappuccin0vv", 2389.50, 0.00, new byte[] {23, 36}, 3),
					CreatePlayer("Intern", 0.00, 350.00, new byte[] {255, 255}, 3),
					CreatePlayer("GRillCllFACElIL", 672.50, 350.00, new byte[] {255, 255}, 3),
					CreatePlayer("cowboy8", 686.66, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("goodlebad", 6658.30, 0.00, new byte[] {254, 254}, 3),
					CreatePlayer("Thomthumb", 2508.73, 350.00, new byte[] {255, 255}, 3),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
					CreatePlayer("", 0.00, 0.00, new byte[] {254, 254}, 2),
				}, CreateSymbols(9, true, 5, 10, 63, "437668243")),
			};

			var controller = new MainController();
			foreach (var s in states)
				controller.UpdateState(s);

			var actualGame = controller.Game;
			var expectedGame = new HoldemManagerLogParser().ParseLines(gameLog).Single();

			var error = GameConstructionTester.TestGame(expectedGame, actualGame, 1);

			Assert.IsNull(error, error);
		}
	}
}
