﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.OHConnectivity.Tests
{
	[TestClass]
	public class GameConstructorTests
	{
		//[TestMethod]
		//public void TestCurrentChairAndBetRound_NoActions()
		//{
		//	var game = new Game
		//	{
		//		SmallBlind = 1,
		//		BigBlind = 2,
		//		DealerChair = 6,
		//		Chairs = new List<Chair>
		//		{
		//			new Chair { Balance = 100, ChairNumber = 1 },
		//			new Chair { Balance = 100, ChairNumber = 2 },
		//			new Chair { Balance = 100, ChairNumber = 3 },
		//			new Chair { Balance = 100, ChairNumber = 4 },
		//			new Chair { Balance = 100, ChairNumber = 5 },
		//			new Chair { Balance = 100, ChairNumber = 6 }
		//		}
		//	};

		//	var gameConstructor = new GameConstructor(game);

		//	Assert.AreEqual(game.GetChair(1), gameConstructor.CurrentChair);
		//	Assert.AreEqual(BetRound.Preflop, gameConstructor.CurrentBetRound);
		//}

		//[TestMethod]
		//public void TestCurrentChairAndBetRound_Raise()
		//{
		//	var game = new Game
		//	{
		//		SmallBlind = 1,
		//		BigBlind = 2,
		//		DealerChair = 6,
		//		Chairs = new List<Chair>
		//		{
		//			new Chair { Balance = 100, ChairNumber = 1 },
		//			new Chair { Balance = 100, ChairNumber = 2 },
		//			new Chair { Balance = 100, ChairNumber = 3 },
		//			new Chair { Balance = 100, ChairNumber = 4 },
		//			new Chair { Balance = 100, ChairNumber = 5 },
		//			new Chair { Balance = 100, ChairNumber = 6 }
		//		}
		//	};

		//	game.Actions = new List<PlayerAction>
		//	{
		//		new PlayerAction(game.GetChair(1), PlayerActionEnum.SmallBlind, 1),
		//		new PlayerAction(game.GetChair(2), PlayerActionEnum.BigBlind, 2),
		//		new PlayerAction(game.GetChair(3), PlayerActionEnum.Raise, 5)
		//	};

		//	var gameConstructor = new GameConstructor(game);

		//	Assert.AreEqual(game.GetChair(4), gameConstructor.CurrentChair);
		//	Assert.AreEqual(BetRound.Preflop, gameConstructor.CurrentBetRound);
		//}

		//[TestMethod]
		//public void TestCurrentChairAndBetRound_SkipsFolded()
		//{
		//	var game = new Game
		//	{
		//		SmallBlind = 1,
		//		BigBlind = 2,
		//		DealerChair = 6,
		//		Chairs = new List<Chair>
		//		{
		//			new Chair { Balance = 100, ChairNumber = 1 },
		//			new Chair { Balance = 100, ChairNumber = 2 },
		//			new Chair { Balance = 100, ChairNumber = 3 },
		//			new Chair { Balance = 100, ChairNumber = 4 },
		//			new Chair { Balance = 100, ChairNumber = 5 },
		//			new Chair { Balance = 100, ChairNumber = 6 }
		//		}
		//	};

		//	game.Actions = new List<PlayerAction>
		//	{
		//		new PlayerAction(game.GetChair(1), PlayerActionEnum.SmallBlind, 1),
		//		new PlayerAction(game.GetChair(2), PlayerActionEnum.BigBlind, 2),
		//		new PlayerAction(game.GetChair(3), PlayerActionEnum.Fold),
		//		new PlayerAction(game.GetChair(4), PlayerActionEnum.Call, 2),
		//		new PlayerAction(game.GetChair(5), PlayerActionEnum.Call, 2),
		//		new PlayerAction(game.GetChair(6), PlayerActionEnum.Raise, 6),

		//		new PlayerAction(game.GetChair(1), PlayerActionEnum.Call, 6),
		//		new PlayerAction(game.GetChair(2), PlayerActionEnum.Call, 6),
		//	};

		//	var gameConstructor = new GameConstructor(game);

		//	Assert.AreEqual(game.GetChair(4), gameConstructor.CurrentChair);
		//	Assert.AreEqual(BetRound.Preflop, gameConstructor.CurrentBetRound);
		//}

		//[TestMethod]
		//public void TestCurrentChairAndBetRound_CorrectlyStartsNewBetRound()
		//{
		//	var game = new Game
		//	{
		//		SmallBlind = 1,
		//		BigBlind = 2,
		//		DealerChair = 6,
		//		Chairs = new List<Chair>
		//		{
		//			new Chair { Balance = 100, ChairNumber = 1 },
		//			new Chair { Balance = 100, ChairNumber = 2 },
		//			new Chair { Balance = 100, ChairNumber = 3 },
		//			new Chair { Balance = 100, ChairNumber = 4 },
		//			new Chair { Balance = 100, ChairNumber = 5 },
		//			new Chair { Balance = 100, ChairNumber = 6 }
		//		}
		//	};

		//	game.Actions = new List<PlayerAction>
		//	{
		//		new PlayerAction(game.GetChair(1), PlayerActionEnum.SmallBlind, 1),
		//		new PlayerAction(game.GetChair(2), PlayerActionEnum.BigBlind, 2),
		//		new PlayerAction(game.GetChair(3), PlayerActionEnum.Fold),
		//		new PlayerAction(game.GetChair(4), PlayerActionEnum.Call, 2),
		//		new PlayerAction(game.GetChair(5), PlayerActionEnum.Call, 2),
		//		new PlayerAction(game.GetChair(6), PlayerActionEnum.Call, 2),
		//		new PlayerAction(game.GetChair(1), PlayerActionEnum.Call, 2),
		//		new PlayerAction(game.GetChair(2), PlayerActionEnum.Check)
		//	};

		//	var gameConstructor = new GameConstructor(game);

		//	Assert.AreEqual(game.GetChair(1), gameConstructor.CurrentChair);
		//	Assert.AreEqual(BetRound.Flop, gameConstructor.CurrentBetRound);
		//}
	}
}
