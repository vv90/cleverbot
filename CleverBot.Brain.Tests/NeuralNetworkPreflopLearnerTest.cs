﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Brain.Tests
{
	[TestClass]
	public class NeuralNetworkPreflopLearnerTest
	{
	//	[TestMethod]
	//	public void TestGetParameterValues_GetsHandParameterCorrectly()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(0.03, parameterValues[0]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsPositionParameterCorrectly()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(2.0, parameterValues[1]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsLastActionParameterCorrectly_AllFold()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Fold),
	//				new PlayerAction("Player3", PlayerActionEnum.Fold),
	//				new PlayerAction("Player4", PlayerActionEnum.Fold),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold)
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(0, parameterValues[2]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsLastActionParameterCorrectly_SomeoneLimped()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Call, 1),
	//				new PlayerAction("Player3", PlayerActionEnum.Fold),
	//				new PlayerAction("Player4", PlayerActionEnum.Fold),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold)
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(1, parameterValues[2]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsLastActionParameterCorrectly_SomeoneRaised()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Fold),
	//				new PlayerAction("Player4", PlayerActionEnum.Fold),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold)
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(2, parameterValues[2]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsLastActionParameterCorrectly_SomeoneRaised_SomeoneCalled()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Call, 3),
	//				new PlayerAction("Player4", PlayerActionEnum.Fold),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold)
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(2, parameterValues[2]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsLastActionParameterCorrectly_Someone3Bet()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Raise, 9),
	//				new PlayerAction("Player4", PlayerActionEnum.Fold),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold)
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(3, parameterValues[2]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsLastActionParameterCorrectly_Someone3Bet_SomeoneCalled()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Raise, 9),
	//				new PlayerAction("Player4", PlayerActionEnum.Call, 9),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold)
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(3, parameterValues[2]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsHeroActionCorrectly_Raise()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Raise, 9),
	//				new PlayerAction("Player4", PlayerActionEnum.Call, 9),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold),
	//				new PlayerAction("Hero", PlayerActionEnum.Raise, 30),
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(1.0, parameterValues[3]);
	//		Assert.AreEqual(0.0, parameterValues[4]);
	//		Assert.AreEqual(0.0, parameterValues[5]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsHeroActionCorrectly_Call()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Raise, 9),
	//				new PlayerAction("Player4", PlayerActionEnum.Call, 9),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold),
	//				new PlayerAction("Hero", PlayerActionEnum.Call, 9),
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(0.0, parameterValues[3]);
	//		Assert.AreEqual(1.0, parameterValues[4]);
	//		Assert.AreEqual(0.0, parameterValues[5]);
	//	}

	//	[TestMethod]
	//	public void TestGetParameterValues_GetsHeroActionCorrectly_Fold()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Raise, 9),
	//				new PlayerAction("Player4", PlayerActionEnum.Call, 9),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold),
	//				new PlayerAction("Hero", PlayerActionEnum.Fold),
	//			}
	//		};

	//		var parameterValues = learner.GetParameterValues(game);

	//		Assert.AreEqual(0.0, parameterValues[3]);
	//		Assert.AreEqual(0.0, parameterValues[4]);
	//		Assert.AreEqual(1.0, parameterValues[5]);
	//	}

	//	[TestMethod]
	//	public void TestGenerateTrainData_GeneratesCorrectNumberOfDataPoints()
	//	{
	//		var learner = new NeuralNetworkPreflopLearner();
	//		var game = new Game
	//		{
	//			Chairs =
	//			{
	//				new Chair(1, 100, new CardMask("Ac Ad"), "Hero"),
	//				new Chair(1, 100, new CardMask(), "Player1"),
	//				new Chair(1, 100, new CardMask(), "Player2"),
	//				new Chair(1, 100, new CardMask(), "Player3"),
	//				new Chair(1, 100, new CardMask(), "Player4"),
	//				new Chair(1, 100, new CardMask(), "Player5")
	//			},
	//			Actions =
	//			{
	//				new PlayerAction("Hero", PlayerActionEnum.SmallBlind, 0.5),
	//				new PlayerAction("Player1", PlayerActionEnum.BigBlind, 1),
	//				new PlayerAction("Player2", PlayerActionEnum.Raise, 3),
	//				new PlayerAction("Player3", PlayerActionEnum.Raise, 9),
	//				new PlayerAction("Player4", PlayerActionEnum.Call, 9),
	//				new PlayerAction("Player5", PlayerActionEnum.Fold),
	//				new PlayerAction("Hero", PlayerActionEnum.Fold),
	//			}
	//		};

	//		var data = learner.GenerateTrainData(new[] {game});
	//		Assert.AreEqual(2, data.Length);
	//	}
	}
}
