﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Brain.Tests
{
	[TestClass]
	public class DefaultTurnCasesStrategyTest
	{
		private Brain Brain { get { return new Brain(new DefaultTurnCasesStrategy()); } }

		[TestMethod]
		public void TestThink_Inic_NoActions_SecondPair_Check()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Js, 8s, 3d"),
				Turn = new CardMask("Ad"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kc, Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Check, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}


		[TestMethod]
		public void TestThink_Inic_NoActions_GutShot_Check()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Kc, Ts, 3d"),
				Turn = new CardMask("7c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("As, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Check, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NoActions_Draw_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Tc, Qs, 3d"),
				Turn = new CardMask("7c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kc, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NoActions_FirstPair_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8s, 3d"),
				Turn = new CardMask("7d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NoActions_Set_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8s, 3d"),
				Turn = new CardMask("7c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NoActions_GutShot_FoldToDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Kc, Ts, 3d"),
				Turn = new CardMask("7c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("As, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 23, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_SecondPair_FoldToDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8d, Kd"),
				Turn = new CardMask("7c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, Qs"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 24, BetRound.Turn)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NutsTwoPairs_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8d, Jd"),
				Turn = new CardMask("3c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 24, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_FirstPair_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8d, 2d"),
				Turn = new CardMask("7c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 24, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_TwoPairs_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 3d, Kd"),
				Turn = new CardMask("Jc"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 24, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_Draw_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 3d, 5d"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Qd, Kd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 24, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_TwoPairs_CallOverDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, Qc, Ks"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Qd, Kd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 40, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_FirstPair_FoldToOverDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 2c, Ks"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ad, Qd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 40, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_SecondPair_CallUnderDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 2c, Ks"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kd, Jd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 15, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_TwoPairs_FoldToRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, Kd, Qd"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Qs, Ks"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 104, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_FlushDraw_Call2xRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("5s, Kd, 2d"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Qd, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 48, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_StraightDraw_FoldTo2xRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Qs, Kd, 2d"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Td, Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 48, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NutsTwoPairs_3bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, Kd, 5d"),
				Turn = new CardMask("6c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ad, Ks"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 104, BetRound.Turn),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8s, 2s, 3d"),
				Turn = new CardMask("Jd"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 47, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 47, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Turn), 
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_Raise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8s, 2s, 3d"),
				Turn = new CardMask("Jd"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 47, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 47, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 96, BetRound.Turn), 
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_GutShot_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Qc, Js, 3d"),
				Turn = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("As, Ks"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Turn),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FirstPair_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, 8s, 3d"),
				Turn = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("As, Ks"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 56, BetRound.Turn)

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_SecondPair_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, Qc, 3d"),
				Turn = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kd, Ks"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 56, BetRound.Turn)

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FirstPair_3Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Kc, Jc, 3d"),
				Turn = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, As"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 56, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 170, BetRound.Turn)

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Turn, result.BetRound);
		}
	}
}
