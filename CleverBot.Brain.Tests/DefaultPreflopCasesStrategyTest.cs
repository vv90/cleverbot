﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Brain.Tests
{
	[TestClass]
	public class DefaultPreflopCasesStrategyTest
	{
		private Brain Brain { get { return new Brain(new DefaultPreflopCasesStrategy()); } }

		[TestMethod]
		public void TestThink_UTG_AA_Raise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0, 
				HeroChair = 3, 
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask(), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask("Ac,Ad"), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
		}

		[TestMethod]
		public void TestThink_UTG_27o_Fold()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 3,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask(), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask("2c,7d"), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(0, result.Amount);
		}

		[TestMethod]
		public void TestThink_CO_88_CallsWith15bbEffectiveStack()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 5,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask(), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask("8c,8d"), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Raise, 6)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
		}

		[TestMethod]
		public void TestThink_CO_AA_3Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 5,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask(), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask("Ac,Ad"), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Raise, 6)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
		}

		[TestMethod]
		public void TestThink_BU_A7s_FoldsToRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};
			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(0, result.Amount);
		}

		[TestMethod]
		public void TestThink_JJ_FoldTo4Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 18),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 52),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(0, result.Amount);
		}

		[TestMethod]
		public void TestThink_KK_5BetPush()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kc, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 18),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 52),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Allin, result.Action);
		}

		[TestMethod]
		public void TestThink_JJ_FoldTo3Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Jc, Jd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 18)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
		}

		[TestMethod]
		public void TestThink_QQ_4Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Qc, Qd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 18)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
		}

		[TestMethod]
		public void TestThink_AK_6Bet_allin()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 18),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 52),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 104),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Allin, result.Action);
		}

		[TestMethod]
		public void TestThink_AK_Call_5BetPush()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 18),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 52),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Allin, 200),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Allin, result.Action);
		}

		[TestMethod]
		public void TestThink_AK_Call_6BetPush()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 18),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Raise, 54),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 165),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Allin, 200),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Allin, result.Action);
		}

		[TestMethod]
		public void TestThink_BetSize_Raise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(7, result.Amount);
		}

		[TestMethod]
		public void TestThink_BetSize_Raise_1limper()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 2),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(9, result.Amount);
		}

		[TestMethod]
		public void TestThink_BetSize_3bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(21, result.Amount);
		}

		[TestMethod]
		public void TestThink_BetSize_4bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Raise, 18),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(63, result.Amount);
		}

		[TestMethod]
		public void TestThink_BetSize_Raise_4bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 18)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(57, result.Amount);
		}

		[TestMethod]
		public void TestThink_BetSize_BattleForBlinds()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 5,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(6, result.Amount);
		}

		[TestMethod]
		public void TestThink_BetSize_4Bet_BattleForBlinds()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 5,
				HeroChair = 0,
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Kd"), "Player 0"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 0"), PlayerActionEnum.Raise, 6),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 9),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(29, result.Amount);
		}
	}
}
