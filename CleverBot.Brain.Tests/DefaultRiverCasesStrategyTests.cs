﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Brain.Tests
{
	[TestClass]
	public class DefaultRiverCasesStrategyTests
	{
		private Brain Brain { get { return new Brain(new DefaultRiverCasesStrategy()); } }

		[TestMethod]
		public void TestThink_Inic_NoActions_FirstPair_Check()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Js, 8s, 3d"),
				Turn = new CardMask("Ad"),
				River = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Qc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Check, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NoActions_TwoPairs_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8s, Kd"),
				Turn = new CardMask("5d"),
				River = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kc, 8c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Draw_FoldToBet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Kc, 5c, 3d"),
				Turn = new CardMask("7s"),
				River = new CardMask("2d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 23, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 23, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 36, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NutsTwoPairs_Raise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, 7c, 3d"),
				Turn = new CardMask("9s"),
				River = new CardMask("Qd"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ad, Qc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 23, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 23, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 36, BetRound.River),
			};


			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_FirstPair_FoldToDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 8d, Kd"),
				Turn = new CardMask("7c"),
				River = new CardMask("2c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("As, Qs"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 36, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NutsTwoPairs_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 6d, Jd"),
				Turn = new CardMask("3c"),
				River = new CardMask("9c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 36, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}


		[TestMethod]
		public void TestThink_Inic_TwoPairs_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 3d, Kd"),
				Turn = new CardMask("Jc"),
				River = new CardMask("4c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, Js"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Bet, 36, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_NutsTwoPairs_FoldToRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, Kd, Qd"),
				Turn = new CardMask("6c"),
				River = new CardMask("5c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Qs, As"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.River),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.River),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 150, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_Set_3bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, Kd, 5d"),
				Turn = new CardMask("6c"),
				River = new CardMask("4c"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 15, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 15, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 24, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 24, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.River),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.River),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Raise, 150, BetRound.River),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8s, 2s, 3d"),
				Turn = new CardMask("Qc"),
				River = new CardMask("As"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 2),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 10, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 17, BetRound.Turn), 
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 17, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.River), 
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_Raise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8s, 2s, 3d"),
				Turn = new CardMask("Qc"),
				River = new CardMask("As"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 2),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 10, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 17, BetRound.Turn), 
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 17, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 25, BetRound.River), 
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FirstPair_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, Qc, 3d"),
				Turn = new CardMask("2c"),
				River = new CardMask("6s"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, As"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 56, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 56, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.River),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_NutsTwoPairs_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, Kc, 2d"),
				Turn = new CardMask("7s"),
				River = new CardMask("6s"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, As"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 56, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 56, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 112, BetRound.River),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FirstPair_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, Qc, 2d"),
				Turn = new CardMask("7s"),
				River = new CardMask("6s"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("As, Ks"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 36, BetRound.Flop),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Turn),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 56, BetRound.Turn),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 56, BetRound.Turn),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 112, BetRound.River),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.River, result.BetRound);
		}
	}
}
