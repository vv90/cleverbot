﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Brain.Tests
{
	[TestClass]
	public class DefaultFlopCasesStrategyTest
	{
		private Brain Brain { get { return new Brain(new DefaultFlopCasesStrategy()); } }

		[TestMethod]
		public void TestThink_Inic_NoActions_Nothing_Check()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 5,
				HeroChair = 0,
				Flop = new CardMask("Ks, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Check, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_Nothing_Check()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ks, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Check, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_GutShot_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ts, 8s, 6d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(11.25, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_OESD_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("9s, 8s, 6d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(11.25, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_FD_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("9c, 8c, 2d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(11.25, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3rdPair_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("9c, 8s, 7d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(11.25, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_2ndPair_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("9c, 2s, 7d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(11.25, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_1stPair_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ad, 2s, 5d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(11.25, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_OverPair_Fold()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("6d, 2s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_OverPair_Check()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("6d, 2s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Check, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 2s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Bet, result.Action);
			Assert.AreEqual(12.75, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_Raise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 2s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(47, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_OverPair_FoldToRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 2s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_2Pairs_PairOnTheBoard_FoldToRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_2Pairs_PairOnTheBoard_Stack33bbOrLess_CallRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 66, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_SecondPair_Stack16bbOrLess_CallRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 9s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("9c, Td"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 32, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_ThirdPair_Stack8bbOrLess_CallRaise()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 9s, Kd"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("9c, Td"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 16, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_TwoPairs_Stack55bbOrLess_3Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 9s, Kh"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("9c, Kd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 110, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_Nuts2Pairs_3Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Th, Js, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Td, Jd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(105, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_Nuts2Pairs_4Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Th, Js, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Td, Jd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 30, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 90, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_2Pairs_4Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Th, Jh, 3h"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Td, Jd"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_3Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("2h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 30, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(107, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_NoInic_Set_4Bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("2h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("8c, 8d"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 30, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 90, BetRound.Flop),


			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3rdPair_FoldToDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("9c, 8s, 7d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_2ndPair_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("9c, 5s, 7d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,7c"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_2Pairs_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Ac, 5s, Jd"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ad,Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(45, result.Amount);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_OESD_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Qc, Ts, 9d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ad, Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_FD_CallDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("5c, Tc, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac,Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Call, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_GutShot_FoldToDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("Qc, Ts, 2d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ad,Jc"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Bet, 10, BetRound.Flop),
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Fold, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FirstPair_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 9s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kc, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 36, BetRound.Flop)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FlushDraw_RaiseDonk()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("2s, 9s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, As"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Bet, 36, BetRound.Flop)
			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FirstPair_3bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("As, 9s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Kc, Ad"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 111, BetRound.Flop),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}

		[TestMethod]
		public void TestThink_Inic_3betBank_FlushDraw_3bet()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("2s, 9s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ks, As"), "Hero"),
					new Chair(1, 200, new CardMask(), "Player 1"),
					new Chair(2, 200, new CardMask(), "Player 2"),
					new Chair(3, 200, new CardMask(), "Player 3"),
					new Chair(4, 200, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 23),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Call, 23),

				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 36, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Raise, 111, BetRound.Flop),

			};

			var result = Brain.Think(game);

			Assert.AreEqual(PlayerActionEnum.Raise, result.Action);
			Assert.AreEqual(BetRound.Flop, result.BetRound);
		}
	}
}
