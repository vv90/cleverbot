﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Brain.Tests
{
	[TestClass]
	public class DefaultFlopCasesStrategyHandTypeTest
	{
		[TestMethod]
		public void TestEvalType_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, Td");
			var board = new CardMask("7c, Js, Ac");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_OESD()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 9d");
			var board = new CardMask("7c, Td, Ac");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FlushDraw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("2d, Td");
			var board = new CardMask("7d, Kd, Ac");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_PocketPair_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, Ad");
			var board = new CardMask("9d, 6s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 9d");
			var board = new CardMask("Ad, 6s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_SecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 9c");
			var board = new CardMask("9d, Js, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_ThirdPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 6d");
			var board = new CardMask("Jd, 6s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.ThirdPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 9d");
			var board = new CardMask("Ad, 9s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.NutsTwoPairs, target.EvalType(hand, board));
		}


		[TestMethod]
		public void TestEvalType_TwoPairs_PossibleStraght_ReturnsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Tc, 9d");
			var board = new CardMask("Td, 9s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsTwoPairs_PossibleFlush_ReturnsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 9d");
			var board = new CardMask("As, 9s, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FirstPair_PossibleFlush_ReturnSecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 2d");
			var board = new CardMask("As, 9s, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_SecondPair_PossibleFlush_ReturnsThirdPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, 2d");
			var board = new CardMask("As, Ks, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.ThirdPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_ThirdPair_PossibleFlush_ReturnsNothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 2d");
			var board = new CardMask("As, Ks, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FirstPairPlusFlushDraw_ReturnsDraw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qs, As");
			var board = new CardMask("2s, Qd, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_SecondPairPlusFlushDraw_ReturnsDraw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qs, As");
			var board = new CardMask("Ks, Qd, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_ThirdPairPlusFlushDraw_ReturnsDraw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8s, As");
			var board = new CardMask("Ks, Qs, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 9d");
			var board = new CardMask("As, 9s, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9c, 8d");
			var board = new CardMask("Ad, 9s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_ThreeOfAKind_PairOnTheBoard_Trips()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9s, 5d");
			var board = new CardMask("9d, 6s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Trips, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_ThreeOfAKind_PocketPair_Set()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9s, 9d");
			var board = new CardMask("Td, 6s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Set, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9c, 6d");
			var board = new CardMask("5d, 7s, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Straight, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Flush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("2c, 6c");
			var board = new CardMask("5c, Kc, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Flush, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8h, Ks");
			var board = new CardMask("Kd, Kc, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FullHouse, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8h, 8s");
			var board = new CardMask("8d, 8c, Ks");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FourOfAKind, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_StraightFlush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9c, 6c");
			var board = new CardMask("5c, 7c, 8c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightFlush, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PocketPair_PairOnTheBoard_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, Ad");
			var board = new CardMask("9d, 6s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PocketPair_PairOnTheBoard_SecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 8d");
			var board = new CardMask("9d, 6s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));

		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PairOnTheBoard_SecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 6d");
			var board = new CardMask("9d, 6s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PocketPair_PairOnTheBoard_ThirdPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("2c, 2d");
			var board = new CardMask("9d, 6s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.ThirdPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_PocketPair_ThreeOfAKindOnTheBoard_TwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, Ad");
			var board = new CardMask("9d, 9s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_PocketPair_ThreeOfAKindOnTheBoard_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Tc, Td");
			var board = new CardMask("9d, 9s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_PocketPair_ThreeOfAKindOnTheBoard_FirstPair1()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("4c, 4d");
			var board = new CardMask("9d, 9s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_FullHouseOnTheBoard_ReturnsFirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("4c, 4d");
			var board = new CardMask("9d, 9s, 9c, 3c, 3d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_FullHouseOnTheBoard_ReturnsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jc, Jd");
			var board = new CardMask("9d, 9s, 9c, 3c, 3d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_FullHouseOnTheBoard_WithAces_ReturnsNothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jc, Jd");
			var board = new CardMask("9d, 9s, 9c, Ac, Ad");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_FullHouseOnTheBoard_ReturnsNothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("3c, 4d");
			var board = new CardMask("9d, 9s, 9c, 3c, 3d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_ThreeOfAKindOnTheBoardWithOverCard_ReturnsSecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jc, Jd");
			var board = new CardMask("3d, 3s, 3c, Qc, 2d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TheeOfAKind_ThreeOfAKindOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("5c, Ad");
			var board = new CardMask("9d, 9s, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_LowerStraightDraw_With1Card_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 2d");
			var board = new CardMask("8d, 9s, Tc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_LowerStraightDraw_3StraightOnTheBoard_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, Ad");
			var board = new CardMask("8d, 9s, Tc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_HigherStraightDraw_3StraightOnTheBoard_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jc, Ad");
			var board = new CardMask("8d, 9s, Tc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightDraw, target.EvalType(hand, board));
		}
		[TestMethod]
		public void TestEvalType_StaightDraw_WithFakeOut_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8d, 9s, Jc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_StraightDraw_3SuitedBoard_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8s, 5s, Qs");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_GutShot_3SuitedBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8d, Td");
			var board = new CardMask("7c, Jc, Ac");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_LowerStraightDraw_3StraightWithGapOnTheBoard_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8d, 9s, Jc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_LowerStraightDraw_3StraightWith2GapOnTheBoard_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8d, 9s, Qc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_LowerStraightDraw_3StraightWith3GapOnTheBoard_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8d, 9s, Kc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_StraightDraw_1LowerOut_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8d, 9s, Qc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_StraightDraw_NoLowerOuts_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 6d");
			var board = new CardMask("8d, 9s, Kc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsFlushDraw_3SuitedBoard_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, 6d");
			var board = new CardMask("2c, 9c, Kc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsFlushDraw_AceOnTheBoard_3SuitedBoard_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, 6d");
			var board = new CardMask("2c, 9c, Ac");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_SecondNutsFlushDraw_3SuitedBoard_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, 6d");
			var board = new CardMask("2c, 9c, 5c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_SecondNutsFlushDraw_3SuitedBoardWithKing_GutShot()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qc, 6d");
			var board = new CardMask("2c, 9c, Kc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.GutShot, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FlushDraw_3SuitedBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jc, 6d");
			var board = new CardMask("2c, 9c, Kc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Pair_PairOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("5c, Ad");
			var board = new CardMask("9d, Ks, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PairOnTheBoard_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, Ad");
			var board = new CardMask("9d, Ks, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_PocketPair_PairOnTheBoard_FullHouse()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, Kd");
			var board = new CardMask("9d, Ks, 9c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FullHouse, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_PocketPair_ThreeOfAKindOnTheBoard_FullHouse()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Tc, Td");
			var board = new CardMask("9d, 9s, 9c, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FullHouse, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_PocketPair_ThreeOfAKindOnTheBoard_Turn_SecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 8d");
			var board = new CardMask("9d, 9s, 9c, 8s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Flush_FourSuitedOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("5d, 6d");
			var board = new CardMask("9d, 4d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsFlush_FourSuitedOnTheBoard_Flush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ad, 6d");
			var board = new CardMask("9d, 4d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Flush, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_SecondNutsFlush_FourSuitedOnTheBoard_TwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kd, 6d");
			var board = new CardMask("9d, 4d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsFlush_FourSuitedOnTheBoard_WithAce_Flush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kd, 6d");
			var board = new CardMask("Ad, 4d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Flush, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_LowerStraightFlush_FourSuitedOnTheBoard_With4Straight_TwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ad, 6d");
			var board = new CardMask("9d, 7d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_NutsFlush_FourSuitedOnTheBoard_With4Straight_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ad, Kh");
			var board = new CardMask("9d, 7d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_ThirdNutsFlush_FourSuitedOnTheBoard_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qd, Kh");
			var board = new CardMask("9d, 2d, Td, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourthNutsFlush_FourSuitedOnTheBoard_SecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jd, Kh");
			var board = new CardMask("9d, 2d, 3d, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourthNutsFlush_FourSuitedOnTheBoard_JackOnTheBoard_SecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Td, Kh");
			var board = new CardMask("9d, 2d, Jd, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FifthNutsFlush_FourSuitedOnTheBoard_ThirdPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Td, Kh");
			var board = new CardMask("9d, 2d, 3d, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.ThirdPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight_FourSuitedOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("5c, 6c");
			var board = new CardMask("9d, 4d, 7d, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Pair_FourSuitedOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, Ah");
			var board = new CardMask("9d, 4d, 7d, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_FourSuitedOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9c, 4h");
			var board = new CardMask("9d, 4d, 7d, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Set_FourSuitedOnTheBoard_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("9c, 9h");
			var board = new CardMask("9d, 4d, 7d, 8d");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight_SecondNuts_4StraightOnTheBoard_TwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("6c, Ac");
			var board = new CardMask("7d, 8d, 9s, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight_Nuts_4StraightOnTheBoard_Straight()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Jc, Ac");
			var board = new CardMask("7d, 8d, 9s, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Straight, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Set_4StraightOnTheBoard_Draw()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8s, 8c");
			var board = new CardMask("7d, 8d, 9s, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FlushDraw, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_4StraightOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, 8c");
			var board = new CardMask("7d, 8d, 9s, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Pair_4StraightOnTheBoard_Nothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("7c, Ac");
			var board = new CardMask("7d, 8d, 9s, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_3SuitedBoard_FirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qd, Kd");
			var board = new CardMask("Ac, Kc, Qc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PairOnTheBoard_LowerSecondPair_RetrunsFirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("6d, Ad");
			var board = new CardMask("Ac, 9c, 9d, 6s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_PairOnTheBoard_RetrunsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qd, Ad");
			var board = new CardMask("Ac, 9c, 9d, Qs");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		
		[TestMethod]
		public void TestEvalType_TwoPairs_TwoPairsOnTheBoard_ReturnsSecondPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ac, Ad");
			var board = new CardMask("Qc, 9c, 9d, Qs");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_TwoPairsOnTheBoard_ReturnsSecondPair1()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Tc, Td");
			var board = new CardMask("Qc, 9c, 9d, Qs");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_TwoPairs_TwoPairsOnTheBoard_ReturnsSecondPair2()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("8c, 9d");
			var board = new CardMask("Qc, 9c, 9d, Qs");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.SecondPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FullHouse_WithQueen_ThreeOfAKindOnTheBoard_ReturnsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qd, Ad");
			var board = new CardMask("Qc, 9c, 9d, 9s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}



		[TestMethod]
		public void TestEvalType_FullHouse_WithJake_ThreeOfAKindOnTheBoard_ReturnsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qd, Jd");
			var board = new CardMask("Jc, 9c, 9d, 9s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}


		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindOnTheBoard_AceInHand_ReturnsFourOfAKind()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Ad, Jd");
			var board = new CardMask("4c, 4d, 4h, 4s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FourOfAKind, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindOnTheBoard_KingInHand_ReturnsFirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kd, Jd");
			var board = new CardMask("4c, 4d, 4h, 4s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindOnTheBoard_KingInHand_River_ReturnsFirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kd, Jd");
			var board = new CardMask("4c, 4d, 4h, 4s, 2c");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.FirstPair, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindSecondNutsOnTheBoard_River_ReturnsFirstPair()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("2d, 2s");
			var board = new CardMask("4c, 4d, 4h, 4s, Kc");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindOnTheBoard_QueenInHand_ReturnsNothing()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Qd, Jd");
			var board = new CardMask("4c, 4d, 4h, 4s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Nothing, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindNutsOnTheBoard_KingInHand_ReturnsStraightFlush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kd, Jd");
			var board = new CardMask("4c, 4d, 4h, 4s, Ad");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightFlush, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_FourOfAKind_FourOfAKindOnTheBoardKing_ReturnsStraightFlush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("2c, 2d");
			var board = new CardMask("Ac, Ad, Ah, As, Kd");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightFlush, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight_StraightFromTenOnTheBoard_JakeInHand_FlushNotPossible_ReturnsStraight()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, Jd");
			var board = new CardMask("Td, 9c, 8d, 7s, 6s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.Straight, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight_StraightFromTenOnTheBoard_JakeInHand_FlushPossible_ReturnsTwoPairs()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("Kc, Jd");
			var board = new CardMask("Td, 9d, 8d, 7s, 6s");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.TwoPairs, target.EvalType(hand, board));
		}

		[TestMethod]
		public void TestEvalType_Straight_NutsStraightOnTheBoard_ReturnsStraightFlush()
		{
			var target = new DefaultFlopCasesStrategy();
			var hand = new CardMask("2d, 2d");
			var board = new CardMask("Ad, Kc, Qd, Js, Ts");

			Assert.AreEqual(CasesStrategyBase.CasesHandType.StraightFlush, target.EvalType(hand, board));
		}
	}

}
