﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Data;
using CleverBot.Parser;
using Newtonsoft.Json;

namespace CleverBot.OHConnectivity
{
	internal class GameConstructionTestCase
	{
		public IEnumerable<ManagedHoldemState> States { get; set; }
		public Game Game { get; set; }
	}

	internal class GameConstructionTester
	{
		public static string SaveTestCase(IEnumerable<ManagedHoldemState> states, Game game)
		{
			var fileName = string.Format("GameTestCases/{0}_{1}.json", game.GameNumber, game.Title);

			var testCase = new GameConstructionTestCase
			{
				States = states,
				Game = game
			};

			var dataSave = JsonConvert.SerializeObject(testCase, Formatting.Indented,
				new JsonSerializerSettings {ContractResolver = new CardMaskJsonContractResolver()});

			File.WriteAllText(fileName, dataSave);

			return string.Format("Saved test case for game {0}", game.GameNumber);
		}

		public static void SaveGameStates(List<TableState> tableStates)
		{
			var data = JsonConvert.SerializeObject(tableStates, Formatting.Indented);
			var fileName = string.Format("TableStates/{0}.json",
				tableStates.First().Symbols.HandNumber);

			File.WriteAllText(fileName, data);
		}

		public static List<TableState> GetTableStates(string fileName)
		{
			var tableStatesData = File.ReadAllText(fileName);
			var tableStates = JsonConvert.DeserializeObject<List<TableState>>(tableStatesData);
			return tableStates;
		}

		public static string TestGame(Game expected, Game actual, int chairsOffset, int heroChair = 0)
		{
			var betPrecision = 1;
			expected.HeroChair = heroChair + chairsOffset;

			if (expected.GameNumber != actual.GameNumber)
				return string.Format("GameNumber: expected {{{0}}}, actual {{{1}}}", expected.GameNumber, actual.GameNumber);

			if (Math.Abs(expected.BigBlind - actual.BigBlind) > betPrecision)
				return string.Format("BigBlind: expected {{{0}}}, actual {{{1}}}", expected.BigBlind, actual.BigBlind);

			if (Math.Abs(expected.SmallBlind - actual.SmallBlind) > betPrecision)
				return string.Format("SmallBlind: expected {{{0}}}, actual {{{1}}}", expected.SmallBlind, actual.SmallBlind);

			var actualDealerChair = actual.DealerChair + chairsOffset;//((actual.DealerChair + chairsOffset) % 6);
			if (expected.DealerChair != actualDealerChair)
				return string.Format("DealerChair: expected {{{0}}}, actual {{{1}}}", expected.DealerChair, actualDealerChair);

			//var actualHeroChair = ((actual.HeroChair + 2)%6);
			//if (expected.HeroChair != actualHeroChair)
			//	return string.Format("HeroChair: expected {{{0}}}, actual {{{1}}}", expected.HeroChair, actualHeroChair);

			var actualBoard = actual.Board;
			var expectedBoard = expected.Board;

			if ((actualBoard & expectedBoard) != actualBoard)
				return string.Format("Board: expected {{{0}}}, actual {{{1}}}", expected.Board, actual.Board);

			foreach (var expectedChair in expected.Chairs)
			{
				var actualChair = actual.Chairs.SingleOrDefault(c => ((c.ChairNumber + chairsOffset)) == expectedChair.ChairNumber);

				if (actualChair == null)
					return string.Format("Chair {0} not found", expectedChair.ChairNumber);

				var expectedHand = expectedChair.Hand;
				var actualHand = actualChair.Hand;
				if (expectedHand.NumCards() != 0 && actualHand.NumCards() != 0 && (expectedHand & actualHand) != actualHand)
					return string.Format("Chair {0} Hand: expected {{{1}}}, actual {{{2}}}",
						expectedChair.ChairNumber, expectedChair.Hand, actualChair.Hand);

				if (Math.Abs(expectedChair.Balance - actualChair.Balance) > betPrecision)
					return string.Format("Chair {0} Balance: expected {{{1}}}, actual {{{2}}}",
						expectedChair.ChairNumber, expectedChair.Balance, actualChair.Balance);
			}

			var expectedActionsQueue = new Queue<PlayerAction>(expected.Actions);
			var actualActionsQueue = new Queue<PlayerAction>(actual.Actions);

			var count = 0;
			while (expectedActionsQueue.Any())
			{
				var expectedAction = expectedActionsQueue.Dequeue();


				//if (expectedAction.Action == PlayerActionEnum.Post)
				//	continue;

				// if this is the last player action, we can skip the rest 
				if (expectedAction.Chair.ChairNumber == expected.HeroChair &&
					expectedAction == expected.Actions.Last(a => a.Chair.ChairNumber == expected.HeroChair))
					break;

				if (!actualActionsQueue.Any())
					return string.Format("Missing action {0}", expectedAction);

				var actualAction = actualActionsQueue.Dequeue();

				if (expectedAction.BetRound != actualAction.BetRound)
					return string.Format("Action {0} BetRound: expected {{{1}}}, actual {{{2}}}",
						count, expectedAction.BetRound, actualAction.BetRound);

				if (expectedAction.Action != actualAction.Action)
					return string.Format("Action {0} Action: expected {{{1}}}, actual {{{2}}}",
						count, expectedAction.Action, actualAction.Action);

				if (Math.Abs(expectedAction.Amount - actualAction.Amount) > betPrecision)
					return string.Format("Action {0} Amount: expected {{{1}}}, actual {{{2}}}",
						count, expectedAction.Amount, actualAction.Amount);

				var actualChairNumber = ((actualAction.Chair.ChairNumber + chairsOffset));
				if (expectedAction.Chair.ChairNumber != actualChairNumber)
					return string.Format("Action {0} ChairNumber: expected {{{1}}}, actual {{{2}}}",
						count, expectedAction.Chair.ChairNumber, actualChairNumber);

				count += 1;
			}

			//for (int i = 0; i < expected.Actions.Count; i++)
			//{
			//	var expectedAction = expected.Actions[i];
			//	if (expectedAction.Chair.ChairNumber == expected.HeroChair && 
			//		expectedAction == expected.Actions.Last(a => a.Chair.ChairNumber == expected.HeroChair))
			//		break;

			//	if (actual.Actions.Count < i + 1)
			//		return string.Format("Missing action {0}", i);

			//	var actualAction = actual.Actions[i];

			//	if (expectedAction.BetRound != actualAction.BetRound)
			//		return string.Format("Action {0} BetRound: expected {{{1}}}, actual {{{2}}}",
			//			i, expectedAction.BetRound, actualAction.BetRound);

			//	if (expectedAction.Action != actualAction.Action)
			//		return string.Format("Action {0} Action: expected {{{1}}}, actual {{{2}}}",
			//			i, expectedAction.Action, actualAction.Action);

			//	if (Math.Abs(expectedAction.Amount - actualAction.Amount) > betPrecision)
			//		return string.Format("Action {0} Amount: expected {{{1}}}, actual {{{2}}}",
			//			i, expectedAction.Amount, actualAction.Amount);

			//	var actualChairNumber = ((actualAction.Chair.ChairNumber + chairsOffset));
			//	if (expectedAction.Chair.ChairNumber != actualChairNumber)
			//		return string.Format("Action {0} ChairNumber: expected {{{1}}}, actual {{{2}}}",
			//			i, expectedAction.Chair.ChairNumber, actualChairNumber);
			//}

			return null;
		}
	}
}
