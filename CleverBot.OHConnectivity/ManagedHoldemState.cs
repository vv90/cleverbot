﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using CleverBot.Common;

namespace CleverBot.OHConnectivity
{

	//internal struct CardMacros
	//{
	//	public byte Card;
	//	public int Rank { get { return (Card >> 4) & 0x0F; } }
	//	public int Suit { get { return (Card >> 0) & 0x0F; } }
	//	public bool IsCardBack { get { return Card == 0xFF; } }
	//	public bool IsUnknown { get { return Card == 0; } }

	//	public CardMacros(byte card)
	//	{
	//		Card = card;
	//	}
	//}

	internal struct ManagedHoldemPlayer
	{
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
		public string m_name;

		public double m_balance;
		public double m_currentbet;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
		public byte[] m_cards;

		public byte m_is_known;
		public byte m_fillerbyte;

		public bool NameKnown { get { return (m_is_known & 0x01) == 0x01; } }
		public bool BalanceKnown { get { return (m_is_known & 0x02) == 0x02; } }
		public bool IsPlaying { get { return m_cards[0] != 254 && m_cards[1] != 254; } }
		public bool IsSeated { get { return !string.IsNullOrEmpty(m_name); } }


		public CardMask Cards
		{
			get
			{
				return m_cards.Aggregate(new CardMask(),
					(mask, b) => mask |= HoldemStateConsts.HoldemStateCardsDictionary[b]);
			}
		}

		public override string ToString()
		{
			return string.Format("{0}, {1}, {2}, {3}, {4}, {5}",
				m_name,
				m_balance,
				m_currentbet,
				string.Join(", ", m_cards.Select(c => HoldemStateConsts.HoldemStateCardsDictionary[c])),
				m_is_known,
				m_fillerbyte);
		}
	}

	internal struct ManagedHoldemState
	{
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
		public string m_title;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
		public double[] m_pot;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
		public byte[] m_cards;

		public byte m_playing_bits;

		public byte m_fillerbyte;
		public byte m_dealer_chair;

		[MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.Struct, SizeConst = 10)]
		public ManagedHoldemPlayer[] m_player;

		//public CardMask Cards
		//{
		//	get
		//	{
		//		return m_cards.Aggregate(new CardMask(),
		//			(mask, b) => mask |= HoldemStateConsts.HoldemStateCardsDictionary[b]);
		//	}
		//}

		public CardMask Flop
		{
			get
			{
				return m_cards.Take(3).Aggregate(new CardMask(),
					(mask, b) => mask |= HoldemStateConsts.HoldemStateCardsDictionary[b]);
			}
		}

		public CardMask Turn
		{
			get
			{
				return m_cards.Skip(3).Take(1).Aggregate(new CardMask(),
					(mask, b) => mask |= HoldemStateConsts.HoldemStateCardsDictionary[b]);
			}
		}

		public CardMask River
		{
			get
			{
				return m_cards.Skip(4).Take(1).Aggregate(new CardMask(),
					(mask, b) => mask |= HoldemStateConsts.HoldemStateCardsDictionary[b]);
			}
		}

		private byte[] ToByteArray()
		{
			var structSize = Marshal.SizeOf(this);
			var structBytes = new byte[structSize];
			IntPtr structPounter = Marshal.AllocHGlobal(structSize);

			Marshal.StructureToPtr(this, structPounter, true);
			Marshal.Copy(structPounter, structBytes, 0, structSize);
			Marshal.FreeHGlobal(structPounter);

			return structBytes;
		}

		public bool IsIdenticalTo(ManagedHoldemState other)
		{
			var state1Bytes = this.ToByteArray();
			var state2Bytes = other.ToByteArray();

			if (state1Bytes.Length != state2Bytes.Length)
			{
				return false;
			}
			else if (state1Bytes.Where((t, i) => t != state2Bytes[i]).Any())
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public bool IsValid(int heroChair)
		{
			// hero doesn't hold cards
			if (this.m_player[heroChair].Cards.NumCards() != 2)
				return false;

			if (this.m_dealer_chair > 10)
				return false;

			return true;
		}

		public override string ToString()
		{
			return string.Format("{0}, [{1}], [{2}], {3}, {4}, {5}", 
				m_title,
				m_pot == null ? "0" : string.Join(", ", m_pot.Where(p => p > 0)),
				m_cards == null ? "" : string.Join(", ", m_cards.Select(c => HoldemStateConsts.HoldemStateCardsDictionary[c])),
				m_playing_bits,
				m_fillerbyte,
				m_dealer_chair);
		}
	}

	internal class HoldemStateConsts
	{
		public static Dictionary<byte, CardMask> HoldemStateCardsDictionary = new Dictionary<byte, CardMask>
		{
			{0, new CardMask("2h")},
			{1, new CardMask("3h")},
			{2, new CardMask("4h")},
			{3, new CardMask("5h")},
			{4, new CardMask("6h")},
			{5, new CardMask("7h")},
			{6, new CardMask("8h")},
			{7, new CardMask("9h")},
			{8, new CardMask("Th")},
			{9, new CardMask("Jh")},
			{10, new CardMask("Qh")},
			{11, new CardMask("Kh")},
			{12, new CardMask("Ah")},
			{13, new CardMask("2d")},
			{14, new CardMask("3d")},
			{15, new CardMask("4d")},
			{16, new CardMask("5d")},
			{17, new CardMask("6d")},
			{18, new CardMask("7d")},
			{19, new CardMask("8d")},
			{20, new CardMask("9d")},
			{21, new CardMask("Td")},
			{22, new CardMask("Jd")},
			{23, new CardMask("Qd")},
			{24, new CardMask("Kd")},
			{25, new CardMask("Ad")},
			{26, new CardMask("2c")},
			{27, new CardMask("3c")},
			{28, new CardMask("4c")},
			{29, new CardMask("5c")},
			{30, new CardMask("6c")},
			{31, new CardMask("7c")},
			{32, new CardMask("8c")},
			{33, new CardMask("9c")},
			{34, new CardMask("Tc")},
			{35, new CardMask("Jc")},
			{36, new CardMask("Qc")},
			{37, new CardMask("Kc")},
			{38, new CardMask("Ac")},
			{39, new CardMask("2s")},
			{40, new CardMask("3s")},
			{41, new CardMask("4s")},
			{42, new CardMask("5s")},
			{43, new CardMask("6s")},
			{44, new CardMask("7s")},
			{45, new CardMask("8s")},
			{46, new CardMask("9s")},
			{47, new CardMask("Ts")},
			{48, new CardMask("Js")},
			{49, new CardMask("Qs")},
			{50, new CardMask("Ks")},
			{51, new CardMask("As")},
			{254, new CardMask()}, 
			{255, new CardMask()}, 
		};
	}
}
