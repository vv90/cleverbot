﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.OHConnectivity.Exceptions;

namespace CleverBot.OHConnectivity
{
	internal class GameConstructor
	{
		private class ExpectedAction
		{
			public Chair Chair { get; set; }
			public BetRound BetRound { get; set; }

			public override string ToString()
			{
				return string.Format("{0} {1}: {2}", BetRound, Chair.ChairNumber, Chair.Name);
			}
		}

		private List<PlayerAction> _actions = new List<PlayerAction>();
		private PlayerAction _lastAction = null;

		public Game Game { get; private set; }
		//public Chair CurrentChair { get; private set; }
		//public BetRound CurrentBetRound { get; private set; }

		private GameConstructor()
		{
			//CurrentBetRound = BetRound.Preflop;
		}

		public GameConstructor(Game game) : this()
		{
			Game = game;
			//CurrentChair = GetNextActingChair();
		}

		public GameConstructor(TableState tableState) : this()
		{
			Game = NewGame(tableState);
			//CurrentChair = GetNextActingChair();
		}


		public Game UpdateGame(ManagedHoldemState state)
		{
			Game.Flop = state.Flop;
			Game.Turn = state.Turn;
			Game.River = state.River;

			for (int i = 0; i < state.m_player.Length; i++)
			{
				var player = Game.GetChair(i);
				var playerState = state.m_player[i];
				if (player != null && playerState.IsSeated)
				{
					player.Name = playerState.m_name;
					player.Hand = playerState.Cards;
					player.CurrentBalance = playerState.m_balance;
				}
			}

			return Game;
		}

		private Game NewGame(TableState tableState)
		{
			Game = new Game();
			Game.StartTime = DateTime.Now;
			Game.Title = tableState.State.m_title;
			Game.SmallBlind = tableState.Symbols.SmallBlind;
			Game.BigBlind = tableState.Symbols.BigBlind;
			Game.GameNumber = tableState.Symbols.HandNumber;
			Game.DealerChair = tableState.State.m_dealer_chair;

			var seatedBits = tableState.Symbols.SeatedBits;

			for (int i = 0; i < tableState.State.m_player.Length; i++)
			{
				var player = Game.GetChair(i);
				var playerState = tableState.State.m_player[i];

				if ((seatedBits & (1 << i)) != 0 && playerState.m_name != "SITTINGOUT")
				{
					Game.Chairs.Add(new Chair
					{
						ChairNumber = i,
						Balance = playerState.m_balance + playerState.m_currentbet,
						CurrentBalance = playerState.m_balance,
						Hand = playerState.Cards,
						Name = playerState.m_name
					});
				}
				//else if (player != null && playerState.IsSeated)
				//{
				//	player.Name = playerState.m_name;
				//	player.Hand = playerState.Cards;
				//	player.CurrentBalance = playerState.m_balance;
				//}
			}

			return Game;
		}

		private ExpectedAction GetNextActingChair()
		{
			var expectedAction = new ExpectedAction();

			var lastAction = Game.Actions.LastOrDefault();
			var lastActedChair = lastAction != null ? lastAction.Chair : Game.GetDealerChair();
			var betRound = lastAction != null ? lastAction.BetRound : BetRound.Preflop;
			var isBetRoundFinished = IsBetRoundUpdateFinished(betRound);

			Chair nextChair = null;
			if (isBetRoundFinished)
			{
				nextChair = Game.GetNextChair(Game.DealerChair);
				betRound = betRound != BetRound.River ? betRound + 1 : BetRound.River;
			}
			else
			{
				nextChair = Game.GetNextChair(lastActedChair.ChairNumber);
			}
			

			var safetyCount = 0;
			while (Game.Actions.Where(a => a.Chair == nextChair).Any(a => a.Action == PlayerActionEnum.Fold))
			{
				if (safetyCount > Game.Chairs.Count)
					throw new InfiniteLoopException("UpdateActions method seems to have gone into an infinite loop");

				safetyCount += 1;
				nextChair = Game.GetNextChair(nextChair.ChairNumber);
			}

			expectedAction.Chair = nextChair;
			expectedAction.BetRound = betRound;

			return expectedAction;
		}

		private bool IsActionValid(PlayerAction action)
		{
			return true;
		}

		/// <summary>
		/// Updates the Game with new player actions from the provided HoldemState and returns added actions
		/// </summary>
		public List<PlayerAction> UpdateActions(ManagedHoldemState state, int tempVar)
		{
			if (Game.Chairs.Count < 2)
			{
				throw new UpdateActionsException("Not enough chairs in the game");
			}

			var safetyCount = 0;

			var expectedAction = GetNextActingChair();
			var lastExpectedAction = new ExpectedAction { BetRound = Game.BetRound, Chair = Game.GetHeroChair() };

			while (expectedAction.Chair != lastExpectedAction.Chair || 
				expectedAction.BetRound != lastExpectedAction.BetRound ||
				!IsCurrentActionsUpdateComplete(state))
			{
				if (safetyCount > state.m_player.Length * 2)
					throw new InfiniteLoopException("UpdateActions method seems to have gone into an infinite loop");

				safetyCount += 1;
				//var nextChair = GetNextActingChair();

				var playerState = state.m_player[expectedAction.Chair.ChairNumber];
				var expectedActionCopy = expectedAction;
				var currentPlayerLastAction = Game.Actions
						.LastOrDefault(a => a.Chair == expectedAction.Chair && a.BetRound == expectedAction.BetRound);
				var currentPlayerSumOfActions = Game.Actions
						.Where(a => a.Chair == expectedActionCopy.Chair && a.BetRound <= expectedActionCopy.BetRound)
						.GroupBy(a => a.BetRound)
						.Select(g => g.Max(a => a.Amount))
						.Sum();
				var currentPlayerMissingAmount =
					expectedAction.Chair.Balance - playerState.m_balance - currentPlayerSumOfActions - playerState.m_currentbet;

				

				if (!playerState.IsPlaying && Math.Abs(currentPlayerMissingAmount) < 0.001)
				{
					if (Game.Actions
						.Where(a => a.Chair == expectedAction.Chair)
						.All(a => a.Action != PlayerActionEnum.Fold))
					{
						var action = new PlayerAction(expectedAction.Chair, PlayerActionEnum.Fold, 0, expectedAction.BetRound);
						AddAction(action);
					}
				}
				else if (Game.BetRound != expectedAction.BetRound)
				{
					
					var amount = currentPlayerLastAction != null
						? currentPlayerMissingAmount + currentPlayerLastAction.Amount
						: currentPlayerMissingAmount;

					if (currentPlayerMissingAmount > 0.001)
					{
						var action = _lastAction == null || _lastAction.BetRound != expectedAction.BetRound
							? PlayerAction.FirstAction(expectedAction.Chair, amount, expectedAction.BetRound)
							: _lastAction.RaiseAction(expectedAction.Chair, amount);

						AddAction(action);
					}
					else
					{
						var action = new PlayerAction(expectedAction.Chair, PlayerActionEnum.Check, 0, expectedAction.BetRound);
						AddAction(action);
					}
				}
				else
				{
					if (currentPlayerLastAction == null && playerState.m_currentbet > 0 ||
					    currentPlayerLastAction != null && playerState.m_currentbet - currentPlayerLastAction.Amount > 0.001)
					{
						PlayerAction action = null;

						if (Game.Actions.All(a => a.Action != PlayerActionEnum.SmallBlind))
							action = new PlayerAction(expectedAction.Chair, PlayerActionEnum.SmallBlind, Game.SmallBlind);
						else if (Game.Actions.All(a => a.Action != PlayerActionEnum.BigBlind))
							action = new PlayerAction(expectedAction.Chair, PlayerActionEnum.BigBlind, Game.BigBlind);
						else
							action = _lastAction == null || _lastAction.BetRound != expectedAction.BetRound
								? PlayerAction.FirstAction(expectedAction.Chair, playerState.m_currentbet, expectedAction.BetRound)
								: _lastAction.RaiseAction(expectedAction.Chair, playerState.m_currentbet);

						AddAction(action);
					}
					//else if (expectedAction.Chair == lastExpectedAction.Chair &&
					//	expectedAction.BetRound == lastExpectedAction.BetRound)
					//{
					//	var posting = Game.Chairs
					//		.Where(c => Math.Abs(c.Balance - c.CurrentBalance - Game.BigBlind) < 0.001
					//			&& Game.Actions.All(a => a.Chair != c))
					//		.ToList();

					//	if (posting.Count == 1)
					//	{
					//		var action = new PlayerAction(posting.Single(), PlayerActionEnum.Post, Game.BigBlind, BetRound.Preflop);
					//		AddAction(action);
					//	}
					//}
					else
					{
						var action = new PlayerAction(expectedAction.Chair, PlayerActionEnum.Check, 0, expectedAction.BetRound);
						AddAction(action);
					}
				}

				expectedAction = GetNextActingChair();
			}

			return _actions;
		}

		private void AddAction(PlayerAction action)
		{
			if (!IsActionValid(action))
				throw new UpdateActionsException("Action is not valid");

			_actions.Add(action);
			Game.Actions.Add(action);

			if (action.Action != PlayerActionEnum.Fold)
				_lastAction = action;

			//CurrentChair = GetNextActingChair();
		}

		private bool IsCurrentActionsUpdateComplete(ManagedHoldemState state)
		{
			foreach(var chair in Game.Chairs)
			{
				var playerState = state.m_player[chair.ChairNumber];

				var sumOfActions = Game.Actions
					.Where(a => a.Chair == chair)
					.GroupBy(a => a.BetRound)
					.Select(g => g.Max(a => a.Amount))
					.Sum();

				if (Math.Abs(chair.Balance - sumOfActions - playerState.m_balance) > 0.001)
					return false;
			}

			//if (Game.BetRound > BetRound.Preflop && !IsBetRoundUpdateFinished(Game.BetRound - 1))
			//{
			//	return false;
			//}



			return true;
		}

		private bool IsBetRoundUpdateFinished(BetRound betRound)
		{
			var betRoundActions = Game.Actions.Where(a => a.BetRound == betRound).ToArray();
			var lastAggressiveAction = betRoundActions
				.LastOrDefault(a => a.IsAgressive());
			var previousActions = Game.Actions.TakeWhile(a => a != lastAggressiveAction).ToList();

			Chair lastToAct = null;

			if (lastAggressiveAction == null)
			{
				var nextChair = Game.GetDealerChair();
				var safetyCount = 0;
				while (nextChair != Game.GetDealerChair() || safetyCount == 0)
				{
					if (safetyCount > Game.Chairs.Count)
						throw new InfiniteLoopException("Infinite loop");

					safetyCount += 1;
					nextChair = Game.GetNextChair(nextChair.ChairNumber);

					if (Game.Actions.Where(a => a.Chair == nextChair).All(a => a.Action != PlayerActionEnum.Fold))
					{
						lastToAct = nextChair;
					}
				}
				//lastToAct = Game.GetDealerChair();
				//while (Game.Actions.Where(a => a.Chair == lastToAct).Any(a => a.Action == PlayerActionEnum.Fold))
				//{
				//	lastToAct = Game.GetNextChair(lastToAct.ChairNumber);
				//}
			}
			else if (lastAggressiveAction.Action == PlayerActionEnum.BigBlind)
			{
				lastToAct = lastAggressiveAction.Chair;
			}
			else
			{
				var currentChair = lastAggressiveAction.Chair;
				while (Game.GetNextChair(currentChair.ChairNumber) != lastAggressiveAction.Chair)
				{
					currentChair = Game.GetNextChair(currentChair.ChairNumber);
					if (//betRoundActions
						previousActions
						.Where(a => a.Chair == currentChair)
						.All(a => a.Action != PlayerActionEnum.Fold))
					{
						lastToAct = currentChair;
					}
				}
			}

			var lastAction = betRoundActions.LastOrDefault();


			if (lastAction == null || lastAction.Chair != lastToAct)
				return false;

			if (lastAction == lastAggressiveAction)
				return false;

			if (lastAggressiveAction == null)
				return lastAction.Action == PlayerActionEnum.Check && Math.Abs(lastAction.Amount) < 0.001;
			else
				return Math.Abs(lastAction.Amount - lastAggressiveAction.Amount) < 0.001 || 
					lastAction.Action == PlayerActionEnum.Check || 
					lastAction.Action == PlayerActionEnum.Fold;
		}

		public List<PlayerAction> UpdateActions(ManagedHoldemState state)
		{
			var walker = new GameWalker(Game);
			
			var actions = walker.UpdateState(state);

			Game.Actions.AddRange(actions);

			return actions;
		}

		private bool IsActionPossible(PossiblePlayerAction possibleAction, ManagedHoldemState state, Game game)
		{
			throw new NotImplementedException();
		}
	}
}
