.osdb2

// OpenScrape 7.7.5

// 2015-09-29 03:18:27
// 32 bits per pixel

//
// sizes
//

z$clientsize       1000  720

//
// strings
//

s$betsizeconfirmationmethod Enter
s$betsizedeletionmethod     Backspace
s$betsizeinterpretationmethod 0
s$betsizeselectionmethod    Dbl Click
s$nchairs                   6
s$network                   americascardroom
s$potmethod                 1
s$sitename                  americascardroom
s$t0type                    fuzzy
s$t1type                    fuzzy
s$t2type                    fuzzy
s$t3type                    fuzzy
s$titletext                 No Limit
s$ttlimits                  ^*- ^d - ^L - ^s / ^b
s$ttlimits0                 ^*- ^L - ^s / ^b

//
// regions
//

r$c0ante             965 541 971 554 ffffffff  220 T1
r$c0cardface0        333 277 350 315        0    0 H1
r$c0cardface0nocard  333 278 333 278 ff56b13e   30 C
r$c0cardface1        402 277 419 315        0    0 H1
r$c0cardface1nocard  402 278 402 278 ff56b13e   30 C
r$c0cardface2        471 277 488 315        0    0 H1
r$c0cardface2nocard  471 278 471 278 ff56b13e   30 C
r$c0cardface3        540 277 557 315        0    0 H1
r$c0cardface3nocard  541 278 541 278 ff56b13e   30 C
r$c0cardface4        609 277 626 315        0    0 H1
r$c0cardface4nocard  611 278 611 278 ff56b13e   30 C
r$c0handnumber       167   6 242  15 ffffffff  220 T1
r$c0pot0             465 237 569 249 ffffffff  100 T2
r$i0button           617 625 734 669        0    0 N
r$i0label            660 643 690 653 ffffffff  120 T1
r$i0state            660 656 660 656 ff0000ff  160 C
r$i1button           746 625 863 669        0    0 N
r$i1label            750 643 860 653 ffffffff  120 T1
r$i1state            748 649 748 649 ff0000ff  160 C
r$i2button           874 625 991 669        0    0 N
r$i2label            876 643 989 653 ffffffff  120 T1
r$i2state            876 656 876 656 ff0000ff  160 C
r$i3button           923 699 994 714        0    0 N
r$i3edit             923 678 993 693        0    0 N
r$i3label            942 702 980 711 ffcccccc  120 T1
r$i3state            955 706 955 706 ff91cccc    0 C
r$p0active           798 209 798 209 ff2324c6    0 C
r$p0balance          831 225 954 240 ff8be8ff  300 T0
r$p0bet              728 274 797 302 ffffffff  100 T1
r$p0cardback         868 166 868 166 ff2820d2   30 C
r$p0cardface0        828 119 845 157        0    0 H1
r$p0cardface0nocard  836 185 836 185 ff000000  100 C
r$p0cardface1        893 119 910 157        0    0 H1
r$p0cardface1nocard  893 186 893 186        0  100 C
r$p0dealer           789 262 789 262 ff060606    0 C
r$p0name             833 194 955 213 ffffffff  200 T3
r$p0seated           798 209 799 209 ff2324c6   30 C
r$p1active           832 396 832 396 ff2324c6    0 C
r$p1balance          865 412 988 427 ff8be8ff  300 T0
r$p1bet              713 376 791 397 ffffffff  100 T1
r$p1cardback         903 353 903 353 ff2820d2   30 C
r$p1cardface0        863 306 880 344        0    0 H1
r$p1cardface0nocard  871 372 871 372 ff000000  100 C
r$p1cardface1        928 306 945 344        0    0 H1
r$p1cardface1nocard  928 373 928 373        0  100 C
r$p1dealer           771 404 771 404 ff060606    0 C
r$p1name             867 382 989 401 ffffffff  200 T3
r$p1seated           832 396 833 396 ff2324c6   30 C
r$p2active           437 572 437 572 ff2324c6    0 C
r$p2balance          474 588 594 603 ff8be8ff  300 T0
r$p2bet              442 458 518 476 ffffffff  100 T1
r$p2cardback         508 529 508 529 ff2820d2   30 C
r$p2cardface0        468 482 485 520        0    0 H1
r$p2cardface0nocard  476 548 476 548 ff000000  100 C
r$p2cardface1        533 482 550 520        0    0 H1
r$p2cardface1nocard  533 549 533 549        0  100 C
r$p2dealer           443 486 443 486 ff060606    0 C
r$p2name             472 557 594 576 ffffffff  200 T3
r$p2seated           437 572 438 572 ff2324c6   30 C
r$p3active           174 396 174 396 ff2324c6    0 C
r$p3balance           12 412 135 427 ff8be8ff  300 T0
r$p3bet              207 382 289 397 ffffffff  100 T1
r$p3cardback         114 353 114 353 ff2820d2   30 C
r$p3cardface0         17 306  34 344        0    0 H1
r$p3cardface0nocard   25 372  25 372 ff000000  100 C
r$p3cardface1         82 306  99 344        0    0 H1
r$p3cardface1nocard   82 373  82 373        0  100 C
r$p3dealer           220 409 220 409 ff060606    0 C
r$p3name              10 381 132 400 ffffffff  200 T3
r$p3seated           174 396 175 396 ff2324c6   30 C
r$p4active           208 209 208 209 ff2324c6    0 C
r$p4balance           45 225 165 240 ff8be8ff  300 T0
r$p4bet              228 276 313 302 ffffffff  100 T1
r$p4cardback         148 166 148 166 ff2820d2   30 C
r$p4cardface0         51 119  68 157        0    0 H1
r$p4cardface0nocard   59 185  59 185 ff000000  100 C
r$p4cardface1        116 119 133 157        0    0 H1
r$p4cardface1nocard  116 186 116 186        0  100 C
r$p4dealer           202 262 202 262 ff060606    0 C
r$p4name              44 194 166 213 ffffffff  200 T3
r$p4seated           208 209 209 209 ff2324c6   30 C
r$p5active           569 130 569 130 ff2324c6    0 C
r$p5balance          405 146 528 161 ff8be8ff  300 T0
r$p5bet              465 214 569 235 ffffffff  100 T1
r$p5cardback         508  87 508  87 ff2820d2   30 C
r$p5cardface0        411  40 428  78        0    0 H1
r$p5cardface0nocard  419 106 419 106 ff000000  100 C
r$p5cardface1        476  40 493  78        0    0 H1
r$p5cardface1nocard  476 107 476 107        0  100 C
r$p5dealer           434 185 434 185 ff060606    0 C
r$p5name             404 115 526 134 ffffffff  200 T3
r$p5seated           569 130 570 130 ff2324c6   30 C
r$u0seated           978 508 978 508 ffffffff    0 C
r$u1seated           978 512 978 512 ffffffff    0 C
r$u2seated           467 549 467 549 ffffffff    0 C
r$u3seated           978 496 978 496 ffffffff    0 C
r$u4seated           978 500 978 500 ffffffff    0 C
r$u5seated           978 504 978 504 ffffffff    0 C

//
// fonts
//

t0$0 1f8 7fe fff fff c03 801 fff fff 7fe 3fc
t0$1 201 201 601 fff fff fff 801 1 1
t0$4 30 70 1f0 3d0 790 fff fff fff fff 10
t0$8 31c 7be fff fff cc3 861 fff fff 7be 73c
t0$9 3c0 7e1 ff1 ff1 c31 c17 fff ffe 7fc 3f0
t0$3 6 e07 c03 841 841 ce3 fff fff 7be 33c
t0$. 7 7 7 4
t0$7 801 807 83f 8ff bfe ff8 fc0 f00 c00
t0$2 e03 e07 c0f 81f c7f ffd ff9 7f1 381
t0$6 f8 3fe 7fe fff e87 c81 8ff 8ff 8fe 7c
t0$5 f87 f83 f81 f81 8c3 8ff 8ff 87e 87c
t1$1 1 41 ff 1
t1$4 18 18 48 c8 ff 8
t1$4 18 18 68 c8 ff 8
t1$C 18 7e c3 81 81 81
t1$6 1c da 91 91 df
t1$e 1e 2b 29 29 39
t1$c 1e 33 21 21 21
t1$l 1ff
t1$k 1ff 1ff c 3e 23 21
t1$k 1ff 1ff c 3f 23 21
t1$h 1ff 30 20 2f 3f
t1$1 20 40 ff
t1$t 20 fe ff 21 21
t1$a 2 27 29 29 3b 1f
t1$. 3
t1$s 39 2d 2d 27
t1$, 3 c
t1$C 3c 7e c3 81 81 81
t1$6 3e 73 91 91 9e
t1$6 3e 77 91 93 9e 4
t1$d 3f 21 21 1ef 1ff
t1$1 41 41 ff ff 1
t1$4 4 1c 24 64 ff 4 4
t1$o 4 1e 21 21 23 1e
t1$1 41 ff 1
t1$8 4 eb 91 91 eb 4
t1$9 70 89 89 8a 7c
t1$9 79 89 89 ca 7c
t1$0 7e 81 81 81 7e
t1$0 7e 81 81 83 7e
t1$8 7e b9 99 99 6e
t1$0 7e c3 81 c3 7e
t1$7 80 81 86 98 e0 80
t1$3 81 81 91 f3 6e
t1$4 8 18 68 c8 ff 8
t1$7 81 86 98 e0 80
t1$7 81 87 9e f8 e0
t1$3 81 91 91 ee
t1$c 8 1e 33 21 21 23
t1$2 83 85 89 71 1
t1$2 83 85 89 f1 1
t1$2 83 87 8f dd 79 1
t1$i bf
t1$o c 1e 21 21 23 1e
t1$2 c3 87 8f dd 79 1
t1$o c 3f 21 21 23 1e
t1$5 f1 91 91 9e
t1$5 f1 f1 91 9b 8e
t1$F ff 90 90 90 80
t1$B ff 91 91 b1 ef 4
t1$R ff ff 88 8c f7 63 1
t2$3 101 101 111 111 1ff ee
t2$7 101 103 10f 13c 1f0 1c0
t2$3 181 101 111 111 1ff ee
t2$4 18 68 c8 1ff 1ff 8 8
t2$5 1e1 1e1 121 121 13f 11e
t2$. 3 3
t2$6 7e ff 1a1 121 13b 11e
t2$2 81 103 107 11d 1f9 f1
t2$1 81 81 1ff 1ff 1 1
t2$8 ee 1fb 111 111 1fb ee
t2$9 f1 1f9 109 10b 1fe fc
t2$0 fe 1ff 101 101 1ff fe
t3$1 101 101 3ff 1 1
t3$1 101 17f 3ff 1 1
t3$_ 1 1 1 1 1 1 1 1
t3$J 1 201 201 203 3fe
t3$4 18 38 68 c8 188 3ff 8 8
t3$o 18 7e 81 81 81 81 7e 18
t3$e 18 7e 93 91 91 f1 32
t3$c 18 7e c3 81 81 81 c3
t3$8 1de 373 221 221 373 1de
t3$S 1e3 321 221 231 211 31e c
t3$A 1 f 7f 3fc 3e8 3c8 3f8 ff 1f 3
t3$0 1fe 302 201 201 102 1fe
t3$T 200 200 200 3ff 3ff 3ff 200 200 200
t3$i 200 2ff
t3$i 200 2ff 200
t3$Y 200 380 c0 3f 3f c0 380 200
t3$W 200 3c0 3c 7 7 78 380 380 78 7 7 3c 3c0 200
t3$I 201 201 3ff 201
t3$7 201 203 20c 230 2c0 380
t3$I 201 2ff 3ff 201
t3$X 201 303 cc 78 78 cc 303 201
t3$I 201 3ff 3ff 3ff 201
t3$S 2 1e3 3f3 371 231 231 23f 31e 1c
t3$i 281 2ff 200
t3$V 300 3e0 3c 7 3 3c 1e0 300
t3$3 303 201 221 221 353 19e
t3$G 30 fc 102 201 201 211 211 31f
t3$O 30 fc 182 301 201 201 201 102 fc
t3$O 30 fc 1fe 387 201 201 201 387 1fe fc 30
t3$A 3 1e f8 388 388 f8 1e 3
t3$A 3 1f 7f 3f8 3c8 3c8 3fc 7f 1f 3
t3$o 38 7e 81 81 81 83 7e 18
t3$o 38 7e c1 81 81 83 7e 18
t3$5 3e3 3e1 221 221 233 21e
t3$g 3f3 609 409 409 41b 7fc
t3$U 3fe 3 1 1 1 3 3fe
t3$U 3fe 3fe 3ff 1 1 3 3ff 3fe
t3$L 3ff 201 1 1 1 1
t3$D 3ff 201 201 201 201 303 186 fc
t3$P 3ff 210 210 210 210 1e0 c0
t3$R 3ff 210 210 210 21c 3e6 1c3 1
t3$F 3ff 220 220 220 220 220
t3$E 3ff 221 221 221 221 221 200
t3$E 3ff 221 221 221 221 221 201
t3$B 3ff 221 221 221 261 3de c
t3$M 3ff 380 e0 1c 18 c0 380 3ff
t3$D 3ff 3ff 201 201 201 387 1fe fc 30
t3$F 3ff 3ff 220 220 220 220 200
t3$M 3ff 3ff 3e0 1f0 3c 3c 1f0 3ff 3ff 3ff
t3$L 3ff 3ff 3ff 1 1 1 1
t3$D 3ff 3ff 3ff 201 201 387 1fe fc 78
t3$H 3ff 3ff 3ff 20 20 20 3ff 3ff 3ff
t3$R 3ff 3ff 3ff 210 21c 3fe 3ef 1c7 1
t3$E 3ff 3ff 3ff 221 221 221 221
t3$E 3ff 3ff 3ff 221 221 221 221 201
t3$B 3ff 3ff 3ff 221 221 3f3 3ff 1de c
t3$N 3ff 3ff 3ff 3c0 f0 3c 3ff 3ff 3ff
t3$K 3ff 3ff 3ff 78 fc 3df 38f 303 201
t3$y 400 700 e1 3e 18 e0 700 400
t3$j 401 401 17fe
t3$a 4 8f 91 91 91 7f
t3$a 4 8f 91 91 93 7f
t3$G 70 1fe 102 201 201 211 211 31f
t3$C 78 1fc 1fe 387 303 201 201 301 383 186
t3$O 78 1fe 1fe 303 201 201 201 387 1fe fc
t3$G 78 fe 1fe 387 201 211 211 31f 19f
t3$e 7e 93 91 91 71 30
t3$e 7e 93 91 91 f1 32
t3$d 7e c1 81 81 83 7ff
t3$c 7e c3 81 81 81 c2 4
t3$c 7e c3 81 81 81 c3
t3$l 7ff
t3$k 7ff 10 38 44 83 81
t3$k 7ff 10 38 46 83 81
t3$k 7ff 10 38 66 83 81
t3$p 7ff 208 408 408 408 3f0 c0
t3$b 7ff 41 81 81 81 7e 18
t3$b 7ff 41 81 81 83 7e 18
t3$p 7ff 608 408 408 418 3f0 c0
t3$h 7ff c0 80 80 80 7f
t3$t 80 3fe 283 81 81
t3$f 80 3ff 480 480
t3$z 81 87 8d b1 e1 81
t3$4 8 38 68 c8 188 3ff 8 8
t3$a 8f 91 91 91 7f
t3$9 c0 1e1 311 211 213 316 1fc
t3$w c0 fc 7 e f0 e0 e 7 fc 80
t3$x c3 66 38 3c 43 81
t3$v e0 3c 7 7 38 e0
t3$s f1 91 99 8b 6
t3$O fc 102 1 201 201 201 102 fc
t3$C fc 102 201 201 201 201 201 102
t3$C fc 1fe 3cf 303 201 201 201 303 186
t3$u fe 1 1 1 2 ff
t3$u fe 1 1 1 3 ff
t3$6 fe 1a3 321 221 223 21e
t3$r ff 40 80 80
t3$n ff c0 80 80 80 7f
t3$m ff c0 80 80 ff 7f 80 80 80 7f

//
// points
//

p1$   3   12
p1$   3   33
p1$   6    1
p1$   8    9
p1$   8   16
p1$   8   29
p1$  12    7

//
// hash
//

h1$6c                 03d9c20c
h1$9d                 19b40a15
h1$2c                 1cd8bcad
h1$Kc                 26fca5d2
h1$6s                 2ad01879
h1$2h                 32c16638
h1$Ah                 33364111
h1$4s                 35a8b78b
h1$9c                 37af8e24
h1$Jd                 39913561
h1$7c                 3a605f37
h1$9s                 3d6d6cd3
h1$Ks                 3e644ea0
h1$Jh                 4583d6d8
h1$Kd                 48aa004d
h1$8s                 4d2abc9e
h1$4c                 5313eea2
h1$Qh                 532120a4
h1$Td                 5e6939c8
h1$Ac                 62261979
h1$4d                 62a22259
h1$8c                 62c53ca8
h1$Js                 69578f48
h1$Qd                 74b2607f
h1$7d                 7643cf24
h1$Qc                 8564a7d2
h1$2s                 875ec856
h1$5d                 91015ad0
h1$Jc                 924177ef
h1$5s                 944ed285
h1$As                 99aaa3b6
h1$8h                 a6d75992
h1$5h                 aa1a1e84
h1$6h                 aa4c127e
h1$Ts                 b4cf05da
h1$3h                 b754c30a
h1$3c                 b7991662
h1$7h                 bdbc3356
h1$Ad                 cc3dae7e
h1$8d                 cf9d4a30
h1$5c                 d2bfed93
h1$3d                 d4b390a7
h1$Th                 d757441f
h1$3s                 d767fed9
h1$Qs                 e0162478
h1$4h                 e40b4ca4
h1$7s                 e576ae1c
h1$2d                 eb523c20
h1$6d                 f084166e
h1$9h                 f26ca000
h1$Tc                 fa8e97b5
h1$Kh                 fedbbd71

//
// images
//

i$Tc               18  39 
fffffffffffffffffffffffffdfefdfff2f9f2fff7fbf7fffffffffffffffffffffffffffffffffffffffffffbfdfbfff2f8f2fff2f8f2fff5faf5fffefffeffffffffffffffffff
ffffffffffffffffffffffffbdddbdff0a800aff77b977fffdfefdfffffffffffffffffffdfefdffb1d7b1ff53a753ff007d00ff007c00ff1b8b1bff79ba79ffe6f2e6ffffffffff
ffffffffffffffffe2f0e2ff6ab36aff007800ff79ba79fffdfefdfffffffffffefffeff99cb99ff098109ff138713ff5fad5fff68b268ff3e9d3eff007b00ff3f9d3fffe8f4e8ff
add5adff74b874ff3e9d3eff118511ff037f03ff79ba79fffdfefdffffffffffcce5ccff198919ff0d830dffb4d9b4ffffffffffffffffffe7f3e7ff51a651ff027e02ff70b670ff
5dac5dff007400ff007d00ff0c830cff067f06ff79ba79fffefffeffffffffff86c286ff007700ff61af61ffffffffffffffffffffffffffffffffffcae5caff0a820aff2c932cff
f0f8f0ffe6f2e6ffe7f3e7ff70b670ff007800ff79ba79fffffffffff2f8f2ff55a955ff007800ffa6d2a6fffffffffffffffffffffffffffffffffffcfefcff259025ff118611ff
ffffffffffffffffffffffff7dbc7dff007700ff79ba79ffffffffffe6f2e6ff3a9a3aff007900ffc2e0c2ffffffffffffffffffffffffffffffffffffffffff449f44ff007b00ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79ffffffffffdceddcff258f25ff017c01ffcde6cdfffffffffffffffffffffffffffffffffffefffeff5bac5bff007900ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79ffffffffffd6ead6ff168816ff0e840effd3e8d3ffffffffffffffffffffffffffffffffffffffffff6cb46cff007900ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79ffffffffffd3e9d3ff118511ff138713ffd6e9d6ffffffffffffffffffffffffffffffffffffffffff6ab36aff007900ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79ffffffffffd9ebd9ff1c8b1cff0f840fffd5e9d5ffffffffffffffffffffffffffffffffffffffffff68b268ff007900ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79ffffffffffe0efe0ff2e942eff007a00ffcae4cafffffffffffffffffffffffffffffffffffefffeff4ea54eff007900ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79ffffffffffecf5ecff46a146ff007800ffb7dab7ffffffffffffffffffffffffffffffffffffffffff369836ff027d02ff
ffffffffffffffffffffffff7bbb7bff007700ff79ba79fffffffffffbfdfbff6eb66eff007700ff87c287fffefffeffffffffffffffffffffffffffe7f3e7ff168816ff1f8d1fff
ffffffffffffffffffffffff7ebc7eff007700ff7cbb7cffffffffffffffffffb2d8b2ff098009ff2f942ffff3f9f3ffffffffffffffffffffffffff93c893ff027e02ff46a146ff
ebf5ebffdeeedeffdeeedeff6db46dff027902ff6bb36bffddeeddffdceddcffddeeddff57a957ff007a00ff4fa54fffcfe6cfffdfefdfff96ca96ff108510ff128612ffa9d3a9ff
59aa59ff007100ff007500ff007800ff007b00ff007800ff007500ff006a00ff67b267ffebf5ebff53a753ff158715ff007500ff007500ff007c00ff259025ff95c995ffffffffff
b4d9b4ff82bf82ff85c185ff86c186ff86c186ff86c186ff86c186ff7ebc7effb6d9b6fffffffffff5faf5ffbbdcbbff84c084ff83c083ff95c995ffcbe5cbffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd6ead6ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238e23ff8bc38bfff7fbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$Jc               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffff6faf6fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff2f9f2fffcfdfcffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff4da44dff007a00ff017d01ff047f04ff0f850fff168916ffc4e0c4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff95c995ff64b064ff68b268ff58aa58ff1b8b1bff108510ffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe7f3e7ff339633ff0a820affc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff309530ff0b820bffc3e0c3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd6ead6ff319531ff0b820bffc2dfc2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd2e8d2ff2d942dff0b820bffc9e4c9ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefeffffffffffffffffffffffffffffffffffa6d2a6ff0a820aff1d8b1dfff2f9f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffecf6ecff7abb7aff92c792ffddeeddffdfefdfffaad4aaff269026ff007c00ff88c288ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7fbf7ff5fad5fff007300ff007500ff007500ff007a00ff218e21ff72b772fff5fbf5ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcde5cdff8ac38aff84c084ff83c083ff8fc68fffcce5ccffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f1e5ffd6ead6ffd6ead6ffdfefdffffdfefdffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfdfbff99ca99ff2a922aff027d02ff007d00ff1c8c1cff7cbc7cfff2f8f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa9d3a9ff098109ff078007ff108510ff108510ff0c830cff007c00ff7ebd7effffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fbf8ff52a752ff017d01ff108510ff108510ff108510ff108510ff0a820aff299129ffe8f3e8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff49a249ff027d02ff108510ff108510ff108510ff108510ff0b820bff248f24ffe2f0e2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8fcf8fff7fbf7ff68b268ff057f05ff0f850fff108510ff108510ff108510ff0b820bff3e9d3efff0f8f0fff4f9f4ffffffffffffffffffffffffff
fffffffff2f8f2ffa3d1a3ff66b166ff65b065ff54a854ff148714ff0f840fff108510ff108510ff108510ff108510ff439f43ff69b369ff5cac5cff93c893ffeaf5eaffffffffff
f3f9f3ff68b268ff027e02ff017d01ff037e03ff068006ff108510ff108510ff108510ff108510ff108510ff108510ff098109ff027e02ff037e03ff007c00ff4ca44cffeaf4eaff
99cb99ff057f05ff0b820bff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0f840fff037e03ff78ba78ff
6fb56fff007a00ff108510ff108510ff108510ff108510ff108510ff108510ff0f850fff0d840dff0e840eff108510ff108510ff108510ff108510ff108510ff078007ff3c9b3cff
6fb56fff007a00ff108510ff108510ff108510ff108510ff0f850fff148714ff2f952fff178917ff168816ff0e840eff108510ff108510ff108510ff108510ff068006ff3a9a3aff
94c894ff057f05ff0d830dff108510ff108510ff108510ff098209ff359835ff7aba7aff62af62ff5cac5cff027e02ff108510ff108510ff108510ff108510ff057f05ff65b065ff
edf6edff67b167ff027e02ff098109ff0b830bff057f05ff198919ffc4e1c4ffa7d2a7ff77ba77ffdaecdaff309530ff037e03ff0b830bff0b820bff027e02ff3c9c3cffe0efe0ff
fffffffff3f9f3ffa9d4a9ff5ead5eff47a147ff7fbe7fffd0e7d0ffffffffffa9d4a9ff6eb56effffffffffe1f0e1ff81bf81ff4aa24aff4aa34aff8cc48cffe3f0e3ffffffffff
fffffffffffffffffffffffffafcfafff7fbf7ffffffffffffffffffffffffff83c083ff67b167fffffffffffffffffffffffffff7fbf7fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0efe0ff2f952fff259025ffcee6ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff4f9f4fff7fbf7ffe2f0e2ffa9d3a9ff4aa34aff088108ff037f03ff409d40ffadd5adffdfeedffff4f9f4fffbfdfbffffffffffffffffffffffffff
ffffffffffffffff80be80ff3b9b3bff419e41ff319631ff037e03ff007a00ff078007ff078107ff007a00ff078007ff2e942eff3f9d3fff419e41ff6fb66ffff1f8f1ffffffffff
ffffffffffffffffb4d9b4ff97ca97ff98ca98ff9bcc9bffa2cfa2ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa2cfa2ff9bcc9bff99cb99ff96c996ffa5d1a5fff6faf6ffffffffff
i$4h               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefffff3f3fefff1f2fdfffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa2a2f4ff0c0ee1ff090be1ffa4a5f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e6fbff3031e5ff0000deff0a0ce1ffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdffff6b6dedff2526e4ff1f21e3ff0608e0ffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffb4b5f6ff0506deff8e8ff1ff5153eaff0001dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffff5f6feff2224e4ff2123e3ffe8e8fdff5355e9ff0000dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff7a7befff0000dfff8a8cf1ffffffffff494ae8ff0000dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffbebef7ff0406e0ff4344e7fff3f3feffffffffff494ae8ff0000dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeaeafcff3b3de7ff0a0ce0ffd1d2f9ffffffffffffffffff494ae8ff0000dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff8182efff0000ddff7f80efffffffffffffffffffffffffff494ae8ff0000dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffc9c9f9ff1113e1ff4344e7fffafaffffffffffffffffffffffffffff4c4de8ff0000dfffabacf5ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff1f1fdff4345e8ff0000deff8586f0ffc0c0f7ffbebff7ffbebff7ffbebff7ff3839e6ff0202dfff7a7befffbdbdf6ffd9d9faffffffffffffffffffffffffff
ffffffffffffffffd9d9faff0000dcff0000ddff0000dcff0000dbff0000dbff0000dbff0000dbff0000dfff0709e0ff0000ddff0000d8ff5353eafffefeffffffffffffffffffff
fffffffffffffffff2f2feffa4a4f4ffa7a8f4ffa7a8f4ffa7a8f4ffa7a8f4ffa7a8f4ffa7a8f4ff3133e5ff0203dfff6b6dedffa5a5f3ffcbcbf9ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4e4fe9ff0000dfffaeaff5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff494ae8ff0000dfffa3a4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff393ae6ff0000dcff9b9cf3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa3a3f3ff7e7eefffd0d1f9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafefffffffffffffffffffffffffffffffffffffffffffffffffffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$8h               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7fefff1f1fdfff2f2fdfff7f8feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffeeeefdff898af1ff2628e4ff0000ddff0000deff3233e6ff9899f2fff0f0fdffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeaeafcff4d50e9ff0000deff3637e6ff6263ebff5e5febff2123e4ff0000deff5c5eebfff2f2feffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff8385efff0000ddff4f50e9ffe9e9fcffffffffffffffffffd1d1f9ff1b1de3ff0203e0ffa3a3f4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffededfdff4143e7ff0000ddffd1d1f9ffffffffffffffffffffffffffffffffff7f81efff0000ddff6768ecffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe7e7fcff3b3de7ff0000ddffdedefaffffffffffffffffffffffffffffffffff8f90f1ff0000deff6f6fedffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcffff6768ecff0000ddff6f70eefffdfdfffffffffffffffffffff2f2fdff282ae4ff0a0ce1ffbbbcf6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd1d2f9ff2528e4ff0000dfff6b6cedffc2c2f7ffc8c8f8ff5354eaff0204dfff7b7cefffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffddddfbff3638e5ff0000ddff0d0fe1ff181ae3ff0000dfff898af0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeaebfdff7172edff1516e2ff2e30e5ff6566ecff191ae2ff0101dfff3233e6ff9899f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe8e9fcff4b4ce8ff0303dfff7b7ceffff3f3fefffcfcffffebebfcff8181efff0304e0ff0609e0ff9293f1fffdfdffffffffffffffffffffffffffff
fffffffffffffffffdfdffff7f80efff0000dbff6c6eedfffdfdffffffffffffffffffffffffffffffffffff8283f0ff0000deff2e2fe5ffddddfbffffffffffffffffffffffffff
ffffffffffffffffedeefcff3a3ce6ff0000dcffacadf5ffffffffffffffffffffffffffffffffffffffffffe9eafdff0102dfff1315e2ffbebef6ffffffffffffffffffffffffff
ffffffffffffffffeaeafdff2e2fe5ff0000ddffaaabf5ffffffffffffffffffffffffffffffffffffffffffededfdff0001dfff1a1ce3ffc5c6f7ffffffffffffffffffffffffff
fffffffffffffffff8f8feff6163ecff0000ddff5253eafff2f3fdffffffffffffffffffffffffffffffffff9b9cf3ff0000deff4142e7fff0f0fdffffffffffffffffffffffffff
ffffffffffffffffffffffffcbccf9ff1b1ce3ff0000deff4849e8ffc6c6f8ffdedefbffd6d6faff797aeeff0002dfff191be2ffb1b2f5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffc2c3f7ff4042e8ff0b0de1ff0000ddff0000dcff0000dcff090be0ff3739e6ffbbbcf7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff4f4feffb5b6f6ff8889f0ff7e7fefff8889f0ffb1b2f5ffececfdffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafefffffffffffffffffffffffffffffffffffffffffffffffffffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$7c               18  39 
fffffffffffffffffffffffffcfdfcfff1f8f1fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff5faf5fffefffeffffffffffffffffffffffffff
ffffffffffffffffffffffffc1e0c1ff007600ff068006ff068006ff068006ff068006ff068006ff058005ff0d830dff078007ff419e41ffeef6eeffffffffffffffffffffffffff
ffffffffffffffffffffffffd2e8d2ff3e9b3eff4aa34aff4aa34aff4aa34aff4aa34aff4aa34aff4ea44eff329632ff037e03ff4ea44efff5faf5ffffffffffffffffffffffffff
fffffffffffffffffffffffffcfdfcfff2f8f2fff3f9f3fff3f9f3fff3f9f3fff3f9f3fff3f9f3fff6fbf6ff4ba44bff0f840fffa2d0a2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfdfbfff138613ff329632fff6fbf6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff64b064ff037e03ff9acc9affffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc9e4c9ff238e23ff248f24fff1f8f1ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffafdfaff73b773ff007500ff88c388ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5e9d5ff178817ff268f26ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff82be82ff007300ff82c082ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f2e5ff1a891aff1d8b1dffcde6cdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff7bbc7bff007b00ff6fb66ffffcfefcffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffe8f3e8ff1c8b1cff1a8a1affc1dfc1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff89c389ff047e04ff54a854fff6fbf6ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe7f3e7ff319631ff108510ffb5dab5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff8fc78fff078007ff3f9d3fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdeefdeff269026ff007700ffb0d6b0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdbeddbff89c389ff8cc48cfff9fcf9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f1e5ffd6ead6ffd6ead6ffdfefdffffdfefdffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfdfbff99ca99ff2a922aff027d02ff007d00ff1c8c1cff7cbc7cfff2f8f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa9d3a9ff098109ff078007ff108510ff108510ff0c830cff007c00ff7ebd7effffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fbf8ff52a752ff017d01ff108510ff108510ff108510ff108510ff0a820aff299129ffe8f3e8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff49a249ff027d02ff108510ff108510ff108510ff108510ff0b820bff248f24ffe2f0e2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8fcf8fff7fbf7ff68b268ff057f05ff0f850fff108510ff108510ff108510ff0b820bff3e9d3efff0f8f0fff4f9f4ffffffffffffffffffffffffff
fffffffff2f8f2ffa3d1a3ff66b166ff65b065ff54a854ff148714ff0f840fff108510ff108510ff108510ff108510ff439f43ff69b369ff5cac5cff93c893ffeaf5eaffffffffff
f3f9f3ff68b268ff027e02ff017d01ff037e03ff068006ff108510ff108510ff108510ff108510ff108510ff108510ff098109ff027e02ff037e03ff007c00ff4ca44cffeaf4eaff
99cb99ff057f05ff0b820bff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0f840fff037e03ff78ba78ff
6fb56fff007a00ff108510ff108510ff108510ff108510ff108510ff108510ff0f850fff0d840dff0e840eff108510ff108510ff108510ff108510ff108510ff078007ff3c9b3cff
6fb56fff007a00ff108510ff108510ff108510ff108510ff0f850fff148714ff2f952fff178917ff168816ff0e840eff108510ff108510ff108510ff108510ff068006ff3a9a3aff
94c894ff057f05ff0d830dff108510ff108510ff108510ff098209ff359835ff7aba7aff62af62ff5cac5cff027e02ff108510ff108510ff108510ff108510ff057f05ff65b065ff
edf6edff67b167ff027e02ff098109ff0b830bff057f05ff198919ffc4e1c4ffa7d2a7ff77ba77ffdaecdaff309530ff037e03ff0b830bff0b820bff027e02ff3c9c3cffe0efe0ff
fffffffff3f9f3ffa9d4a9ff5ead5eff47a147ff7fbe7fffd0e7d0ffffffffffa9d4a9ff6eb56effffffffffe1f0e1ff81bf81ff4aa24aff4aa34aff8cc48cffe3f0e3ffffffffff
fffffffffffffffffffffffffafcfafff7fbf7ffffffffffffffffffffffffff83c083ff67b167fffffffffffffffffffffffffff7fbf7fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0efe0ff2f952fff259025ffcee6ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff4f9f4fff7fbf7ffe2f0e2ffa9d3a9ff4aa34aff088108ff037f03ff409d40ffadd5adffdfeedffff4f9f4fffbfdfbffffffffffffffffffffffffff
ffffffffffffffff80be80ff3b9b3bff419e41ff319631ff037e03ff007a00ff078007ff078107ff007a00ff078007ff2e942eff3f9d3fff419e41ff6fb66ffff1f8f1ffffffffff
ffffffffffffffffb4d9b4ff97ca97ff98ca98ff9bcc9bffa2cfa2ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa2cfa2ff9bcc9bff99cb99ff96c996ffa5d1a5fff6faf6ffffffffff
i$2s               18  39 
fffffffffffffffffffffffffffffffffffffffffefefefff6f6f6fff1f1f1fff1f1f1fff8f8f8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff9f9f9ffcfcfcfff6a6a6aff1d1d1dff000000ff000000ff353535ff919191fff6f6f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffa0a0a0ff000000ff000000ff333333ff414141ff303030ff010101ff000000ff5f5f5ffff3f3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffacacacff4c4c4cffafafafffe9e9e9fff6f6f6ffe2e2e2ff9d9d9dff0d0d0dff000000ff9a9a9affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff2f2f2ffffffffffffffffffffffffffffffffffffffffff6f6f6fff000000ff555555fff7f7f7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffadadadff000000ff2c2c2cffecececffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa7a7a7ff000000ff3b3b3bfff0f0f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff707070ff000000ff757575fffbfbfbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdadadaff171717ff0b0b0bffc4c4c4ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff555555ff000000ff696969fffbfbfbffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff838383ff000000ff2d2d2dffddddddffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff9b9b9bff000000ff161616ffbbbbbbffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff9c9c9cff070707ff0f0f0fffbfbfbfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff9b9b9bff090909ff030303ffa4a4a4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff9c9c9cff0a0a0aff1d1d1dffb1b1b1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffdfdfdff9c9c9cff030303ff000000ff666666ffb3b3b3ffa7a7a7ffa5a5a5ffa5a5a5ffa5a5a5ffa3a3a3ffe1e1e1ffffffffffffffffffffffffffffffffff
ffffffffffffffffe3e3e3ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffffffffffffffffffffffffffff
ffffffffffffffffeeeeeeff8c8c8cff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7effd3d3d3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff1f1f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffffffffffff
fffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffffffffffff
ffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffffffffffff
ffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffffffffffff
e8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ffffffffff
c5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ffffffffff
bbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ffffffffff
c1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cffffffffff
e7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ffffffffff
ffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffffffffffff
ffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffffffffffff
i$8d               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfbfffff5f4fffff4f3fffff6f5fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffcec8ffff7261ffff3017ffff250bffff3b23ffff8172ffffd9d4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffb8afffff351dffff3b25ffff7969ffff7f70ffff6957ffff2d14ffff3f28ffffcec8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffe8e5ffff4029ffff3f28ffffbab2fffffffffffffffffffffaf9ffff9082ffff2208ffff6857fffff9f8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffb0a6ffff2309ffff7767ffffffffffffffffffffffffffffffffffffe3e0ffff4e39ffff2e15ffffefedffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa69cffff240bffff7d6effffffffffffffffffffffffffffffffffffe7e5ffff5642ffff331bfffff0efffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffd4cfffff3118ffff4b37ffffd5d0ffffffffffffffffffffffffffffa99fffff2006ffff7e6ffffffafaffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9386ffff270dffff5844ffffb2a9ffffddd9ffffaba1ffff3e27ffff523dffffdfdcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffa297ffff2d14ffff2409ffff4c36ffff2910ffff5946ffffeae7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffbeb6ffff5c49ffff3b24ffff7a6affff6553ffff2f16ffff402affff7d6effffe4e1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffaea5ffff331cffff533fffffd6d1fffffbfafffff8f8ffffcbc4ffff5a47ffff270dffff604efffff1eeffffffffffffffffffffffffff
ffffffffffffffffffffffffffdedaffff3e26ffff412affffd7d3ffffffffffffffffffffffffffffffffffffe0dcffff513bffff280fffffa499ffffffffffffffffffffffffff
ffffffffffffffffffffffffffa59affff2f17ffff5641ffffffffffffffffffffffffffffffffffffffffffffffffffff8475ffff2208ffff7c6cffffffffffffffffffffffffff
ffffffffffffffffffffffffff9a8effff2d14ffff5641ffffffffffffffffffffffffffffffffffffffffffffffffffff8678ffff2208ffff8678ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc5beffff3922ffff3b23ffffc0b8ffffffffffffffffffffffffffffffffffffe9e6ffff5d49ffff270dffffbdb6ffffffffffffffffffffffffff
fffffffffffffffffffffffffffefeffff7f70ffff2b12ffff3d25ffffa89dffffe4e0ffffe4e0ffffc0b9ffff5743ffff290fffff7f6ffffff9f8ffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff9f8ffff9184ffff4b35ffff2a11ffff1b00ffff1b00ffff2a10ffff432dffff9082fffff9f8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffffaba2ffff988cffff988cffffada2ffffd8d4ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffff
ffffffffffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffff
ffffffffffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffff
fffaf9ffffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6ff
ffe1deffff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bff
ffffffffffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcff
ffffffffffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffff
ffffffffffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffff
ffffffffffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffff
i$Qs               18  39 
fffffffffffffffffffffffffffffffffffffffff2f2f2ffb4b4b4ff525252ff181818ff181818ff181818ff4f4f4fffb9b9b9fff6f6f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffc9c9c9ff353535ff000000ff0e0e0eff272727ff2b2b2bff282828ff0b0b0bff000000ff383838ffd2d2d2ffffffffffffffffffffffffff
ffffffffffffffffffffffffd1d1d1ff202020ff000000ff4b4b4bffbcbcbcffffffffffffffffffffffffffb5b5b5ff4d4d4dff000000ff171717ffcbcbcbffffffffffffffffff
ffffffffffffffffecececff464646ff000000ff494949fff2f2f2ffffffffffffffffffffffffffffffffffffffffffedededff454545ff000000ff3c3c3cfff3f3f3ffffffffff
ffffffffffffffffabababff020202ff090909ffd6d6d6ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc5c5c5ff0a0a0aff000000ffabababffffffffff
fffffffffefefeff6c6c6cff000000ff444444fffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff3b3b3bff000000ff6a6a6affffffffff
ffffffffeaeaeaff3f3f3fff000000ff7b7b7bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7b7b7bff000000ff444444fff9f9f9ff
ffffffffe3e3e3ff323232ff000000ff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff919191ff000000ff343434ffe6e6e6ff
ffffffffdfdfdfff2a2a2aff000000ff999999ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8f8f8fff000000ff2f2f2fffe1e1e1ff
ffffffffe4e4e4ff343434ff000000ff9a9a9affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff909090ff000000ff2f2f2fffe2e2e2ff
ffffffffedededff444444ff000000ff727272ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff727272ff000000ff474747fffafafaff
ffffffffffffffff777777ff000000ff393939fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcfcff2f2f2fff000000ff737373ffffffffff
ffffffffffffffffb8b8b8ff0a0a0aff030303ffc3c3c3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffacacacff050505ff050505ffbababaffffffffff
fffffffffffffffff6f6f6ff545454ff000000ff2f2f2fffdfdfdfffffffffffffffffffffffffffffffffffffffffffd9d9d9ff272727ff000000ff5c5c5cfffafafaffffffffff
ffffffffffffffffffffffffe3e3e3ff2c2c2cff000000ff272727ff9b9b9bfff1f1f1fffcfcfcfff4f4f4ff8e8e8eff252525ff000000ff3b3b3bffefefefffffffffffffffffff
ffffffffffffffffffffffffffffffffe0e0e0ff565656ff0f0f0fff000000ff000000ff000000ff0b0b0bff010101ff0d0d0dff5a5a5affeeeeeeffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffd2d2d2ff7c7c7cff4b4b4bff484848ff000000ff010101ff9f9f9fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff2b2b2bff000000ff959595ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff727272ff000000ff303030ffd7d7d7ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffe4e4e4ff383838ff000000ff101010ff1e1e1eff1e1e1eff424242ffdededeff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff9f9f9ffe8e8e8ff7e7e7eff2f2f2fff242424ff242424ff464646ffdededeff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffff
ffffffffffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffff
ffffffffffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffff
ffffffffe8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ff
ffffffffc5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffbbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffc1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cff
ffffffffe7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ff
ffffffffffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffff
ffffffffffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffff
i$Ac               18  39 
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffa4d0a4ff248f24ff238e23ffb1d7b1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffbfdfbff59aa59ff047f04ff037f03ff5bab5bfffbfefbffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffe5f2e5ff128512ff389938ff399a39ff148714ffe7f3e7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffa0cfa0ff007c00ff7ebd7eff6fb66fff007a00ffa3d0a3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff4ba24bff128612ffb9dbb9ff9dcd9dff088008ff4da44dffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0f0e0ff198919ff269026ffddedddffcee6ceff1d8c1dff1b8b1bffe1f0e1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff97ca97ff088108ff46a046fffffffffff5faf5ff399a39ff098109ff98ca98ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff52a752ff058005ff82bf82ffffffffffffffffff6fb66fff027e02ff54a854ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd7ebd7ff289228ff128612ffd4ead4ffffffffffffffffffaed5aeff0d830dff2a922affd8ebd8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff96c996ff088108ff379937fffffffffffffffffffffffffffafcfaff2a922aff088108ff97ca97ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffdfefdff5fad5fff007b00ff70b670ffe7f3e7ffe6f2e6ffe6f2e6ffe9f4e9ff58aa58ff007b00ff61af61fffefffeffffffffffffffffffffffffff
ffffffffffffffffffffffffd4e8d4ff2a922aff037e03ff007a00ff007800ff007800ff007800ff007800ff007b00ff047f04ff349834ffddedddffffffffffffffffffffffffff
ffffffffffffffffffffffffa8d2a8ff037b03ff299229ff76b976ff7bbc7bff7bbc7bff7bbc7bff7bbc7bff72b772ff178817ff087f08ffafd6afffffffffffffffffffffffffff
fffffffffffffffff9fcf9ff6db56dff007700ff77b977ffffffffffffffffffffffffffffffffffffffffffffffffff55a855ff007800ff6fb66ffff8fcf8ffffffffffffffffff
ffffffffffffffffe2f0e2ff279027ff027d02ffbfdebfffffffffffffffffffffffffffffffffffffffffffffffffffa1d0a1ff007a00ff2a922affe3f0e3ffffffffffffffffff
ffffffffffffffffb4d9b4ff007900ff3a9a3affe7f3e7ffffffffffffffffffffffffffffffffffffffffffffffffffdeeedeff2f952fff007b00ffb5dab5ffffffffffffffffff
fffffffffbfdfbff50a550ff007100ff72b772fffbfdfbfffffffffffffffffffffffffffffffffffffffffffffffffff7fbf7ff61ae61ff007000ff52a652fffbfdfbffffffffff
fffffffffafdfaff93c893ff81bf81ffd8ebd8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcde6cdff7ebd7eff95c895fffafdfaffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd6ead6ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238e23ff8bc38bfff7fbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$5s               18  39 
fffffffffffffffffffffffffffffffffffffffff7f7f7fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff5f5f5fffefefeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff7f7f7ff323232ff000000ff000000ff000000ff000000ff000000ff000000ff414141ffeeeeeeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff6f6f6ff000000ff161616ff464646ff3e3e3eff3e3e3eff3e3e3eff3d3d3dff757575fff3f3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd2d2d2ff000000ff545454fffbfbfbfff2f2f2fff2f2f2fff2f2f2fff1f1f1fff5f5f5fffefefeffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffb5b5b5ff000000ff777777ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff949494ff010101ff8e8e8effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff7e7e7eff0d0d0dffc1c1c1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff575757ff080808ff707070ff989898ffa9a9a9ffcbcbcbfffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff2b2b2bff000000ff000000ff000000ff000000ff181818ff4e4e4effc3c3c3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffb5b5b5ffc7c7c7ffc8c8c8ffb4b4b4ff7e7e7eff171717ff000000ff181818ffbbbbbbffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe9e9e9ff474747ff000000ff474747ffeeeeeeffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc6c6c6ff080808ff090909ffd0d0d0ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1e1e1ff1f1f1fff000000ffc8c8c8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcbcbcbff080808ff151515ffdadadaffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f9f9ff5e5e5eff000000ff616161fffafafaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff848484ff5f5f5fffa8a8a8ffd8d8d8ffdadadaffa5a5a5ff3e3e3eff000000ff2d2d2dffd0d0d0ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff6c6c6cff151515ff000000ff000000ff000000ff000000ff151515ff525252ffdbdbdbffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcffc9c9c9ff9e9e9eff7e7e7eff7e7e7eff9a9a9affc7c7c7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff5f5f5fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff828282ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c0c0ff141414ff000000ff020202ff898989ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfdfdfff242424ff000000ff000000ff000000ff030303ffa5a5a5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff383838ff000000ff000000ff000000ff000000ff000000ff0a0a0affa7a7a7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ff5d5d5dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0b0b0bffb1b1b1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff1d1d1dffc9c9c9ffffffffffffffffffffffffff
ffffffffffffffffa4a4a4ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff262626ffdbdbdbffffffffffffffffff
ffffffffd3d3d3ff141414ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff4b4b4bfff9f9f9ffffffffff
f3f3f3ff525252ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffff
dcdcdcff060606ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffff
d6d6d6ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6d6d6dffffffffff
dadadaff010101ff000000ff000000ff000000ff000000ff000000ff000000ff494949ff1b1b1bff000000ff000000ff000000ff000000ff000000ff000000ff7d7d7dffffffffff
f2f2f2ff4b4b4bff000000ff000000ff000000ff000000ff000000ff6e6e6eff888888ffbababaff464646ff000000ff000000ff000000ff000000ff222222ffd9d9d9ffffffffff
ffffffffeaeaeaff7c7c7cff252525ff1d1d1dff3c3c3cffa9a9a9ffdfdfdfff2e2e2effe2e2e2fff9f9f9ff969696ff363636ff2e2e2eff636363ffcfcfcfffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff848484ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffadadadff1a1a1aff000000ff060606ff8a8a8afffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff8f8f8ffe1e1e1ffe0e0e0ffacacacff656565ff0f0f0fff000000ff000000ff000000ff000000ff484848ff909090ffbcbcbcffd9d9d9ffddddddffffffffffffffffff
ffffffffcfcfcfff2f2f2fff232323ff111111ff000000ff000000ff050505ff050505ff050505ff030303ff000000ff070707ff161616ff202020ff808080ffffffffffffffffff
i$9c               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfdfbfff5faf5fff2f8f2fff8fcf8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc3e0c3ff59aa59ff1a8a1aff007c00ff409d40ff9acb9afff5faf5ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff95c995ff067f06ff198a19ff5fae5fff68b268ff299229ff007b00ff6cb46cfff8fbf8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffabd4abff098009ff259025ffc2e0c2ffffffffffffffffffd1e8d1ff329632ff047e04ff93c793ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffeff7efff50a550ff007600ffa4d1a4ffffffffffffffffffffffffffffffffffa5d1a5ff037f03ff389a38ffe6f3e6ffffffffffffffffffffffffff
ffffffffffffffffffffffffd8ebd8ff218d21ff1b8a1bffe0efe0fffffffffffffffffffffffffffffffffff6fbf6ff168816ff208e20ffc3e1c3ffffffffffffffffffffffffff
ffffffffffffffffffffffffc2e0c2ff007700ff329632ffe4f1e4fffffffffffffffffffffffffffffffffffdfefdff4aa34aff0d830dffa7d2a7ffffffffffffffffffffffffff
ffffffffffffffffffffffffcbe5cbff0c810cff1b8a1bffe3f1e3ffffffffffffffffffffffffffffffffffffffffff4ba34bff007700ff88c388ffffffffffffffffffffffffff
ffffffffffffffffffffffffe6f2e6ff3d9d3dff007800ffa1cfa1ffffffffffffffffffffffffffffffffffd4e9d4ff238e23ff007800ff88c388ffffffffffffffffffffffffff
fffffffffffffffffffffffffdfefdff90c690ff047f04ff1a8a1affaad4aafffdfefdfffbfdfbffb4d9b4ff329632ff078107ff108510ffa8d3a8ffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff7fbf7ff67b167ff0d840dff0d840dff158715ff1b8b1bff1c8b1cff66b166ff309530ff208d20ffc2e0c2ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffbcddbcff63af63ff49a249ff6bb36bffb4d9b4ffddeeddff0c830cff309630ffe0efe0ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff72b772ff047f04ff71b671ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb0d6b0ff118511ff269026ffdeeedeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffeffa6d2a6ff178817ff088108ff99ca99ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ffd1e8d1ffadd5adff8dc58dff49a249ff037e03ff158815ff94c994ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff2f8f2ff359735ff007200ff0d840dff269026ff5ead5effc8e3c8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ffa3d0a3ff87c287ffabd5abffd5ead5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f1e5ffd6ead6ffd6ead6ffdfefdffffdfefdffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfdfbff99ca99ff2a922aff027d02ff007d00ff1c8c1cff7cbc7cfff2f8f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa9d3a9ff098109ff078007ff108510ff108510ff0c830cff007c00ff7ebd7effffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fbf8ff52a752ff017d01ff108510ff108510ff108510ff108510ff0a820aff299129ffe8f3e8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff49a249ff027d02ff108510ff108510ff108510ff108510ff0b820bff248f24ffe2f0e2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8fcf8fff7fbf7ff68b268ff057f05ff0f850fff108510ff108510ff108510ff0b820bff3e9d3efff0f8f0fff4f9f4ffffffffffffffffffffffffff
fffffffff2f8f2ffa3d1a3ff66b166ff65b065ff54a854ff148714ff0f840fff108510ff108510ff108510ff108510ff439f43ff69b369ff5cac5cff93c893ffeaf5eaffffffffff
f3f9f3ff68b268ff027e02ff017d01ff037e03ff068006ff108510ff108510ff108510ff108510ff108510ff108510ff098109ff027e02ff037e03ff007c00ff4ca44cffeaf4eaff
99cb99ff057f05ff0b820bff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0f840fff037e03ff78ba78ff
6fb56fff007a00ff108510ff108510ff108510ff108510ff108510ff108510ff0f850fff0d840dff0e840eff108510ff108510ff108510ff108510ff108510ff078007ff3c9b3cff
6fb56fff007a00ff108510ff108510ff108510ff108510ff0f850fff148714ff2f952fff178917ff168816ff0e840eff108510ff108510ff108510ff108510ff068006ff3a9a3aff
94c894ff057f05ff0d830dff108510ff108510ff108510ff098209ff359835ff7aba7aff62af62ff5cac5cff027e02ff108510ff108510ff108510ff108510ff057f05ff65b065ff
edf6edff67b167ff027e02ff098109ff0b830bff057f05ff198919ffc4e1c4ffa7d2a7ff77ba77ffdaecdaff309530ff037e03ff0b830bff0b820bff027e02ff3c9c3cffe0efe0ff
fffffffff3f9f3ffa9d4a9ff5ead5eff47a147ff7fbe7fffd0e7d0ffffffffffa9d4a9ff6eb56effffffffffe1f0e1ff81bf81ff4aa24aff4aa34aff8cc48cffe3f0e3ffffffffff
fffffffffffffffffffffffffafcfafff7fbf7ffffffffffffffffffffffffff83c083ff67b167fffffffffffffffffffffffffff7fbf7fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0efe0ff2f952fff259025ffcee6ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff4f9f4fff7fbf7ffe2f0e2ffa9d3a9ff4aa34aff088108ff037f03ff409d40ffadd5adffdfeedffff4f9f4fffbfdfbffffffffffffffffffffffffff
ffffffffffffffff80be80ff3b9b3bff419e41ff319631ff037e03ff007a00ff078007ff078107ff007a00ff078007ff2e942eff3f9d3fff419e41ff6fb66ffff1f8f1ffffffffff
ffffffffffffffffb4d9b4ff97ca97ff98ca98ff9bcc9bffa2cfa2ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa2cfa2ff9bcc9bff99cb99ff96c996ffa5d1a5fff6faf6ffffffffff
i$2d               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffefdfffff7f6fffff4f3fffff4f3fffffaf9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff9f9ffffd6d1ffff8476ffff4732ffff2a11ffff2f16ffff624fffffafa6fffffaf9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffaba2ffff2c13ffff331bffff5f4dffff6957ffff5a46ffff321affff280fffff8b7dfffff8f8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffb6aeffff7161ffffc2bcffffefedfffff7f6ffffe7e4ffffaba0ffff3820ffff290fffffb8b0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefefffff6f5ffffffffffffffffffffffffffffffffffffffffffff8475ffff1f05ffff7f71fffffcfbffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb1a7ffff1e04ffff5e4bfffff6f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffada3ffff1e03ffff6a58fffff8f7ffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfcffff8576ffff1b00ffff9b8ffffffefeffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd9d5ffff3f28ffff3922ffffd8d3ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfaffff705effff2208ffff9487fffffcfcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9689ffff250bffff604dffffe9e7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa89fffff2e16ffff4d38ffffd0caffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa99fffff2f16ffff4129ffffd3ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffa9a0ffff2f16ffff361effffc0b8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffaaa0ffff3017ffff4f3affffc8c2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffdfcffffa99fffff341cffff341dffff8d80ffffc2bbffffb9b1ffffb8afffffb8afffffb8afffffb7aeffffebe8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffe2e0ffff331bffff1e03ffff2309ffff2005ffff1d02ffff1d02ffff1d02ffff1d02ffff1d02ffff1b00ffffbbb4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffeeecffffa196ffff968affff988cffff988cffff988cffff988cffff988cffff988cffff988cffff978bffffe0ddffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffff
ffffffffffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffff
ffffffffffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffff
fffaf9ffffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6ff
ffe1deffff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bff
ffffffffffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcff
ffffffffffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffff
ffffffffffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffff
ffffffffffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffff
i$Kd               18  39 
fffffffffffffffffffffffffff8f7fffff4f2fffff9f8fffffffffffffffffffffffffffffffffffffffffffffffffffff8f7fffff2f1fffff2f0fffffefeffffffffffffffffff
ffffffffffffffffffffffffff7b6bffff2f16ffff9083ffffffffffffffffffffffffffffffffffffffffffffd7d4ffff4e39ffff2910ffff7b6bfffff5f5ffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82ffffffffffffffffffffffffffffffffffffe8e5ffff5945ffff1e03ffff7969fffff5f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82ffffffffffffffffffffffffffffe9e7ffff6351ffff1b00ffff6d5cfffff3f2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82fffffffffffffffffffff3f1ffff6e5dffff2207ffff6c5bfffff1efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8e81fffffffffffffcfbffff7665ffff2409ffff5d4affffe9e7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff9488fffffdfcffff887affff2409ffff5441ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff3017ffff9487ffffa69bffff2c12ffff5a47ffffd8d3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7b6bffff3118ffff4c37ffff3e27ffff280fffff9f94ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7b6bffff2e16ffff3219ffff361fffff3118ffff4630ffffd3ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7b6bffff280fffff624fffffcfc9ffff5d4affff250bffff5542ffffece9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff9184ffffffffffffcec8ffff452effff280fffff7666fffff5f4ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82ffffffffffffffffffffb1a9ffff2e16ffff2d14ffff9285fffffefdffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82ffffffffffffffffffffffffffff998dffff270dffff331bffffafa5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82fffffffffffffffffffffffffffff9f8ffff7262ffff250bffff3e27ffffd2cdffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff7a6affff2d14ffff8f82ffffffffffffffffffffffffffffffffffffe6e3ffff5e4affff260cffff4f3affffe6e3ffffffffffffffffffffffffff
ffffffffffffffffffffffffff6e5dffff1b00ffff8577ffffffffffffffffffffffffffffffffffffffffffffd1ccffff402affff1a00ffff5d4affffe8e5ffffffffffffffffff
ffffffffffffffffffffffffffbcb3ffff9488ffffc6c0ffffffffffffffffffffffffffffffffffffffffffffffffffffc6bfffff9487ffff978affffdbd7ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f6ffff9487fffff3f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff2d15ffffa59affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff3e27ffff2b12ffff4029ffffcfcaffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe9e6ffff533fffff2a11ffff351dffff2d14ffff5c49ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff8f7ffff7463ffff2c13ffff351dffff351dffff341cffff280effff8072fffffefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9b8effff3017ffff351dffff351dffff351dffff351dffff331bffff2b12ffff9b8fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffb1a7ffff3219ffff341bffff351dffff351dffff351dffff351dffff351dffff331bffff3820ffffb0a8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffbdb5ffff371effff3018ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3219ffff3c26ffffc4bdffffffffffffffffffffffffff
ffffffffffcec8ffff432dffff2b12ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2f17ffff452fffffc2bbffffffffffffffffff
ffc7c1ffff4a35ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4630ffffbfb7fffffbfaff
ff5540ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351cffff260cffff5d49ffffe7e4ff
ffe0ddffff5743ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2c13ffff4e39ffffd4cfffffffffff
ffffffffffe2dfffff5843ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3017ffff4e39ffffd9d5ffffffffffffffffff
ffffffffffffffffffd6d0ffff4732ffff2e16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3118ffff4f3affffd9d5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3119ffff351dffff351dffff351dffff351dffff361effff2f16ffff3c25ffffc3bbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa59affff331bffff331bffff351dffff351dffff351dffff3018ffff3018ffffb7aeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff8c7fffff2b12ffff351dffff351dffff331bffff2c13ffff9184fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff6d5cffff270dffff351dffff2e15ffff6c5cfffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd8ffff4e39ffff2910ffff4e39ffffe5e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Qh               18  39 
ffffffffffffffffffffffffffffffffffffffffcfd0f9ff8e8ff2ff2022e3ff1d1fe3ff1b1ce3ff3032e5ff8b8df1ffdfdffbffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8f8feff7c7defff0406e0ff0305e0ff2223e3ff3032e5ff3031e5ff1e20e3ff0103dfff090be0ff8c8df0fff7f7feffffffffffffffffffffffffff
ffffffffffffffffffffffff7374eeff0000ddff1b1ce3ff9293f1ffe9e9fcffffffffffffffffffdedefbff8d8ef1ff1214e1ff0000deff7678eefffafafeffffffffffffffffff
ffffffffffffffffafb0f5ff0203dfff1617e2ffb3b4f6ffffffffffffffffffffffffffffffffffffffffffffffffffa7a7f4ff0d0ee0ff0708dfffb6b7f6ffffffffffffffffff
fffffffffefeffff383ae7ff0000ddff7273eeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6061ebff0000ddff5254e9fff8f8feffffffffff
ffffffffd2d2faff0709e0ff0f10e1ffb3b4f5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffafb0f5ff0203dfff0d0fe1ffe9e9fcffffffffff
ffffffff999af3ff0001dfff292ae5ffd9d9faffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd4d5faff2628e4ff0000dfffbabbf6ffffffffff
ffffffff898af0ff0000dfff393ae6fff0f0fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0e0fbff3436e6ff0000deff9d9ef3ffffffffff
ffffffff8081efff0000deff3839e6ffeeeefdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdffbff3335e6ff0000deff9697f2ffffffffff
ffffffff8b8cf0ff0000dfff3839e6ffefeffdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0e0fbff3436e6ff0000deff9697f2ffffffffff
ffffffff9fa0f3ff0001dfff2425e4ffd3d3f9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd0d1f9ff2022e3ff0000dfffc1c2f7ffffffffff
ffffffffdedefbff0a0de0ff0b0ce0ffacadf4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a7f4ff0000deff1517e2ffeeeefdffffffffff
ffffffffffffffff494be9ff0000ddff6162ecfffcfcfefffffffffffffffffffffffffffffffffffffffffffffffffffafafeff4849e8ff0000ddff6364ebfffafaffffffffffff
ffffffffffffffffc5c5f7ff0506e0ff0b0ce0ff9798f2ffffffffffffffffffffffffffffffffffffffffffffffffff8586efff0000deff1b1ce2ffccccf8ffffffffffffffffff
ffffffffffffffffffffffff898af0ff0000deff0609e0ff6a6becffd0d0f9fffcfcfffffcfcffffc4c5f8ff5c5deaff0102dfff0a0ce0ffa4a6f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff9d9ef3ff2123e3ff0b0de1ff0001dfff0305e0ff0b0de1ff0c0ee1ff0607e0ff2829e4ffaeaff5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe9e9fdffb1b2f6ff5051e9ff4f50e9ff2628e4ff0001dfff4344e8fff8f8feffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa09ff3ff0304dfff282ae4ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcacbf9ff191be3ff0000dfff9091f1ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfbffff8a8bf1ff090be0ff0608e0ff2123e3ff2426e4ff2022e4ff9798f3ffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb2b3f6ff5a5ceaff2a2be4ff2a2be4ff2527e4ff9a9af3ffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$7h               18  39 
fffffffffffffffffffffffffcfcfffff1f1fdfff2f2fdfff2f2fdfff2f2fdfff2f2fdfff2f2fdfff2f2fdfff2f2fdfff1f1fdfff4f4fefffefeffffffffffffffffffffffffffff
ffffffffffffffffffffffffccccf9ff0000ddff0000dfff0000dfff0000dfff0000dfff0000dfff0000dfff0204e0ff0303dfff3032e5ffe9e9fcffffffffffffffffffffffffff
ffffffffffffffffffffffffdadafaff3d3de6ff4344e8ff4344e8ff4344e8ff4344e8ff4344e8ff4748e8ff2b2de5ff0000dfff3a3ce6fff2f2fdffffffffffffffffffffffffff
fffffffffffffffffffffffffdfdfefff1f1fdfff3f2fefff3f2fefff3f2fefff3f2fefff3f2fefffaf9feff4f50e9ff0203dfff9293f2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc5c5f7ff1012e2ff1d1fe3fff0f0fdffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6a6becff0000deff8b8cf1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d2faff2223e3ff1314e1ffe9e9fcffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcffff7a7cefff0000dcff7a7aeefffdfdffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffddddfbff1718e2ff1d1ee2ffd9d8faffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8889f1ff0000dbff7072eefffefeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffebebfdff191be2ff0f11e1ffc1c2f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff8081f0ff0000deff5c5debfffbfaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffecebfdff1c1ee3ff0a0ce1ffb2b3f5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff9192f1ff0000dfff4041e7fff3f3feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffedecfdff3435e6ff0103dfffa7a8f4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff9899f2ff0204e0ff2d2ee5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe4e4fcff2729e4ff0000dcffa2a3f4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdfe0fbff8687f0ff8283effff4f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefffffaf9fefffffffffffffffffffffffffffffffffffffffffffffffffffbfbfefffefdffffffffffffffffffffffffffffffffffff
ffffffffffffffffccccf8ff7d7eefff5b5debff5657eaff6364ecffa3a4f4ffffffffffffffffffc3c4f8ff6f71edff5657eaff595aebff7778eeffb1b2f5fffeffffffffffffff
ffffffff9496f2ff1d1fe2ff0000deff0000deff0000deff0000deff0608e0ff5658eaff797aefff1717e2ff0000deff0000deff0000deff0000deff0f10e1ff6062ebfff4f4fdff
c3c3f8ff191be3ff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0204dfff0001dfff0305e0ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff0002dfff9495f2ff
8080efff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff3b3de7ff
5e5febff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2628e4ff
6667ecff0000ddff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2c2de5ff
a9a9f5ff0c0de0ff0204dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff6566ecff
fbfbfeff6364ecff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff1c1ee3ffe2e3fbff
ffffffffedeefdff4041e7ff0000deff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0001dfff0d0de1ffb6b7f6ffffffffff
ffffffffffffffffd3d4f9ff2f31e5ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0001dfff0e0fe0ffa3a4f4ffffffffffffffffff
ffffffffffffffffffffffffd6d6f9ff3132e5ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0102dfff0d0ee0ffa2a4f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9dafaff4141e6ff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0103e0ff0c0de0ffa1a3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffededfcff484ae8ff0000deff0608e0ff0608e0ff0103dfff0c0ee1ffa2a3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdcdcfbff3133e5ff0000dfff0204dfff0909e0ffa0a2f3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1f9ff1b1ee3ff0000dfff8587f0fffffeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a6f4ff5b5cebfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffff1f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$5h               18  39 
fffffffffffffffffffffffffffffffffffffffffbfbfffff2f2fdfff1f2fdfff2f2fdfff2f2fdfff2f2fdfff2f2fdfff2f2fdfffbfbfeffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff9596f2ff080be0ff0000dfff0000dfff0000dfff0000dfff0000dfff0406e0ffababf5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff7374edff0000ddff3e3fe7ff4446e8ff4344e8ff4344e8ff4142e8ff494ae8ffc1c1f7ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffbfbfeff5f60ebff0000ddffdcdcfbfff4f4fefff3f2fefff3f2fefff2f2fefff3f3fdfffafafeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff7f7feff4a4ce9ff1213e1fff2f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe4e4fbff3436e6ff2b2ee4fff2f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd8d9faff2224e4ff5d5eebffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffc1c2f7ff0b0de0ff3c3ee7ff9798f3ff9e9ff3ffbbbbf6ffe6e6fcffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffa9a9f4ff0000ddff0000dcff0000dbff0000dcff080ae0ff3637e6ff8889f0fffbfcffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffddddfbffb9b9f6ffcacaf8ffc8c8f8ff9fa0f3ff5051e9ff0000deff0000dfff686aecfffbfcfeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8f8feff9e9ff3ff0608e0ff0709e0ffafaff5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4446e8ff0000deff6061ebffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7879eeff0000deff4d4ee8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4b4ce9ff0000dfff7878eeffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb9b9f6ff0c0ee1ff0d0ee1ffc9c9f9ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffdadbfaff5152e9ff8989f0ffc9caf8ffdedefbffc9caf8ff7476eeff0c0fe1ff0606e0ff8182efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd0d1f9ff2829e3ff0a0de0ff0000ddff0000dcff0000ddff0a0ce0ff2f30e5ff9e9ff3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe5e6fcffb3b4f5ff8889f0ff7d7eefff8788f0ffb1b2f5ffebecfcffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefffffaf9fefffffffffffffffffffffffffffffffffffffffffffffffffffbfbfefffefdffffffffffffffffffffffffffffffffffff
ffffffffffffffffccccf8ff7d7eefff5b5debff5657eaff6364ecffa3a4f4ffffffffffffffffffc3c4f8ff6f71edff5657eaff595aebff7778eeffb1b2f5fffeffffffffffffff
ffffffff9496f2ff1d1fe2ff0000deff0000deff0000deff0000deff0608e0ff5658eaff797aefff1717e2ff0000deff0000deff0000deff0000deff0f10e1ff6062ebfff4f4fdff
c3c3f8ff191be3ff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0204dfff0001dfff0305e0ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff0002dfff9495f2ff
8080efff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff3b3de7ff
5e5febff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2628e4ff
6667ecff0000ddff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2c2de5ff
a9a9f5ff0c0de0ff0204dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff6566ecff
fbfbfeff6364ecff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff1c1ee3ffe2e3fbff
ffffffffedeefdff4041e7ff0000deff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0001dfff0d0de1ffb6b7f6ffffffffff
ffffffffffffffffd3d4f9ff2f31e5ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0001dfff0e0fe0ffa3a4f4ffffffffffffffffff
ffffffffffffffffffffffffd6d6f9ff3132e5ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0102dfff0d0ee0ffa2a4f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9dafaff4141e6ff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0103e0ff0c0de0ffa1a3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffededfcff484ae8ff0000deff0608e0ff0608e0ff0103dfff0c0ee1ffa2a3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdcdcfbff3133e5ff0000dfff0204dfff0909e0ffa0a2f3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1f9ff1b1ee3ff0000dfff8587f0fffffeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a6f4ff5b5cebfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffff1f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Kh               18  39 
fffffffffffffffffffffffffdfdfffff2f2fdfff3f3fdfffdfdfffffffffffffffffffffffffffffffffffffffffffffefefffff1f1fdffededfdfff6f6feffffffffffffffffff
ffffffffffffffffffffffffd7d7faff090be0ff1c1fe2ffd7d8faffffffffffffffffffffffffffffffffffffffffff7b7cefff0000dfff1416e2ffb2b2f5ffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffffffffffffffffffffffffff9192f2ff0000deff0d0de1ffb1b1f5ffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffffffffffffffffffa1a1f4ff0405dfff0808e0ffa2a2f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffffffffffb0b1f6ff0a0ce0ff0405dfff9fa0f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffbfc0f7ff1011e1ff0000deff9393f1ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff191ce2ffe4e3fbffdcdcfbff1c1de2ff0000deff8b8cf1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0608e0ff2225e3ffb7b7f6ff3233e5ff090ae0ff7b7ceffff8f8feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0b0de1ff1214e2ff2224e3ff0000deff2123e4ffdedffbffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0e10e1ff0001dfff0708e0ff0305dfff0405dfff6566ecfffbfbffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0c0ee1ff0303deff9b9cf3ff9293f2ff0204e0ff0000dfff8c8cf1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1b1ee2ffdcddfaffffffffff7374edff0000deff0d0ee1ffb8b8f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8fafffffffffff2f3feff4143e8ff0000ddff1d1fe3ffd2d3f9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffffffffffdfe0fbff2c2ee4ff0000deff3739e6ffe2e2fbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffffffffffffffffffb2b4f6ff1314e1ff0000deff5c5ceafffefeffffffffffffffffffffffffffff
ffffffffffffffffffffffffd7d7faff0709e0ff1a1de2ffd7d8faffffffffffffffffffffffffffffffffff9495f2ff0405e0ff0000dfff7d7eefffffffffffffffffffffffffff
ffffffffffffffffffffffffd3d3faff0000ddff0609dfffd3d5faffffffffffffffffffffffffffffffffffffffffff696aecff0000ddff0000deff9798f2fffafaffffffffffff
ffffffffffffffffffffffffebebfcff8182efff8b8cf0ffebebfcfffffffffffffffffffffffffffffffffffffffffff2f2feff8c8ef1ff7b7beeff9fa0f3fff8f8feffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefffffaf9fefffffffffffffffffffffffffffffffffffffffffffffffffffbfbfefffefdffffffffffffffffffffffffffffffffffff
ffffffffffffffffccccf8ff7d7eefff5b5debff5657eaff6364ecffa3a4f4ffffffffffffffffffc3c4f8ff6f71edff5657eaff595aebff7778eeffb1b2f5fffeffffffffffffff
ffffffff9496f2ff1d1fe2ff0000deff0000deff0000deff0000deff0608e0ff5658eaff797aefff1717e2ff0000deff0000deff0000deff0000deff0f10e1ff6062ebfff4f4fdff
c3c3f8ff191be3ff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0204dfff0001dfff0305e0ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff0002dfff9495f2ff
8080efff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff3b3de7ff
5e5febff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2628e4ff
6667ecff0000ddff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2c2de5ff
a9a9f5ff0c0de0ff0204dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff6566ecff
fbfbfeff6364ecff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff1c1ee3ffe2e3fbff
ffffffffedeefdff4041e7ff0000deff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0001dfff0d0de1ffb6b7f6ffffffffff
ffffffffffffffffd3d4f9ff2f31e5ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0001dfff0e0fe0ffa3a4f4ffffffffffffffffff
ffffffffffffffffffffffffd6d6f9ff3132e5ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0102dfff0d0ee0ffa2a4f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9dafaff4141e6ff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0103e0ff0c0de0ffa1a3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffededfcff484ae8ff0000deff0608e0ff0608e0ff0103dfff0c0ee1ffa2a3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdcdcfbff3133e5ff0000dfff0204dfff0909e0ffa0a2f3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1f9ff1b1ee3ff0000dfff8587f0fffffeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a6f4ff5b5cebfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffff1f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$4d               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f8fffff3f2fffff7f6fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5f4ffff5f4cffff2910ffff7060fffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f94ffff2c13ffff2b12ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd7d3ffff4832ffff6250ffff270dffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff4f3ffff6754ffff4630ffffbfb7ffff1f05ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffa599ffff270cffffa196ffffd8d4ffff1d03ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffded9ffff452fffff4d37fffffafaffffd7d2ffff1d02ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffdfcffff7261ffff2a11ffffc4bdffffffffffffd3ceffff1d02ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffb4abffff250bffff8e7ffffffefdffffffffffffd3ceffff1d02ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffe7e4ffff3b24ffff4f3affffe5e1ffffffffffffffffffffd3ceffff1d02ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffcfcffff7a6affff371fffffbcb3ffffffffffffffffffffffffffffdbd7ffff1c01ffff7261fffffcfcffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffaea5ffff321affff5440ffffcdc8ffffcbc4ffffcac4ffffcac4ffffaaa0ffff2309ffff604dffffc0b9ffffcec8fffff5f4ffffffffffffffffffffffffff
ffffffffffffffffff6452ffff1600ffff1f05ffff1300ffff1300ffff1300ffff1300ffff1a00ffff3922ffff2b12ffff1100ffff2106ffffd4cfffffffffffffffffffffffffff
ffffffffffffffffffccc6ffffb3aaffffb7afffffb7afffffb7afffffb7afffffb7afffff9b8fffff260cffff5a47ffffaea5ffffbcb4fffff2f0ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedbffff1b00ffff7363ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd3ceffff1d02ffff6f5efffff2f1ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcaffff0900ffff6250fffff1f0ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe9e6ffff8c7fffffb6adfffff8f8ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffffffffffff
ffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffffffffffff
ffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffffffffffff
ffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6fffffdfcff
ff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bfffff1efff
ffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcffffffffff
ffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffffffffffff
ffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Ks               18  39 
fffffffffffffffffffffffff7f7f7fff1f1f1fff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffff6f6f6fff0f0f0ffeeeeeefffdfdfdffffffffffffffffff
ffffffffffffffffffffffff656565ff030303ff676767ffffffffffffffffffffffffffffffffffffffffffd5d5d5ff2c2c2cff000000ff4f4f4fffefefefffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565ffffffffffffffffffffffffffffffffffe8e8e8ff3c3c3cff000000ff4b4b4bffedededffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565ffffffffffffffffffffffffffe9e9e9ff434343ff000000ff3d3d3dffebebebffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565fffffffffffffffffff4f4f4ff525252ff000000ff3f3f3fffe6e6e6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff646464fffffffffffcfcfcff5d5d5dff000000ff2b2b2bffddddddffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff6b6b6bfffefefeff737373ff000000ff1e1e1effd9d9d9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff6d6d6dff989898ff000000ff2e2e2effc6c6c6ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff646464ff000000ff1c1c1cff0d0d0dff000000ff787878ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff646464ff000000ff000000ff030303ff000000ff141414ffbfbfbfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff646464ff000000ff2d2d2dffc3c3c3ff3b3b3bff000000ff212121ffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff676767ffffffffffc9c9c9ff1a1a1aff000000ff464646fff0f0f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565ffffffffffffffffffa9a9a9ff010101ff000000ff696969fffafafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565ffffffffffffffffffffffffff8a8a8aff000000ff050505ff8d8d8dffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565fffffffffffffffffffffffffffdfdfdff575757ff000000ff090909ffbdbdbdffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff636363ff000000ff656565ffffffffffffffffffffffffffffffffffe6e6e6ff3c3c3cff000000ff1a1a1affd7d7d7ffffffffffffffffffffffffff
ffffffffffffffffffffffff555555ff000000ff585858ffffffffffffffffffffffffffffffffffffffffffcececeff151515ff000000ff2a2a2affdededeffffffffffffffffff
ffffffffffffffffffffffffb0b0b0ff7e7e7effb1b1b1ffffffffffffffffffffffffffffffffffffffffffffffffffbebebeff7e7e7eff7a7a7affcdcdcdffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff5f5f5fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff828282ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c0c0ff141414ff000000ff020202ff898989ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfdfdfff242424ff000000ff000000ff000000ff030303ffa5a5a5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff383838ff000000ff000000ff000000ff000000ff000000ff0a0a0affa7a7a7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ff5d5d5dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0b0b0bffb1b1b1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff1d1d1dffc9c9c9ffffffffffffffffffffffffff
ffffffffffffffffa4a4a4ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff262626ffdbdbdbffffffffffffffffff
ffffffffd3d3d3ff141414ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff4b4b4bfff9f9f9ffffffffff
f3f3f3ff525252ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffff
dcdcdcff060606ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffff
d6d6d6ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6d6d6dffffffffff
dadadaff010101ff000000ff000000ff000000ff000000ff000000ff000000ff494949ff1b1b1bff000000ff000000ff000000ff000000ff000000ff000000ff7d7d7dffffffffff
f2f2f2ff4b4b4bff000000ff000000ff000000ff000000ff000000ff6e6e6eff888888ffbababaff464646ff000000ff000000ff000000ff000000ff222222ffd9d9d9ffffffffff
ffffffffeaeaeaff7c7c7cff252525ff1d1d1dff3c3c3cffa9a9a9ffdfdfdfff2e2e2effe2e2e2fff9f9f9ff969696ff363636ff2e2e2eff636363ffcfcfcfffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff848484ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffadadadff1a1a1aff000000ff060606ff8a8a8afffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff8f8f8ffe1e1e1ffe0e0e0ffacacacff656565ff0f0f0fff000000ff000000ff000000ff000000ff484848ff909090ffbcbcbcffd9d9d9ffddddddffffffffffffffffff
ffffffffcfcfcfff2f2f2fff232323ff111111ff000000ff000000ff050505ff050505ff050505ff030303ff000000ff070707ff161616ff202020ff808080ffffffffffffffffff
i$9s               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7fff2f2f2fff3f3f3fffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffefefefff929292ff2c2c2cff000000ff000000ff5c5c5cffbebebeffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe1e1e1ff484848ff000000ff313131ff5d5d5dff4c4c4cff000000ff101010ffb4b4b4ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcff454545ff000000ff575757ffe7e7e7ffffffffffffffffff909090ff000000ff1f1f1fffddddddffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffb6b6b6ff000000ff1a1a1affe9e9e9fffffffffffffffffffffffffffdfdfdff4a4a4aff000000ff858585fffefefeffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff555555ffffffffffffffffffffffffffffffffffffffffff999999ff000000ff4e4e4efff9f9f9ffffffffffffffffffffffffff
ffffffffffffffffffffffff454545ff000000ff6b6b6bffffffffffffffffffffffffffffffffffffffffffbcbcbcff070707ff222222fff2f2f2ffffffffffffffffffffffffff
ffffffffffffffffffffffff5a5a5aff000000ff525252ffffffffffffffffffffffffffffffffffffffffffbfbfbfff070707ff000000ffebebebffffffffffffffffffffffffff
ffffffffffffffffffffffff9b9b9bff000000ff161616ffe0e0e0ffffffffffffffffffffffffffffffffff7e7e7eff000000ff000000ffebebebffffffffffffffffffffffffff
ffffffffffffffffffffffffefefefff2e2e2eff000000ff414141ffd8d8d8ffffffffffe5e5e5ff787878ff0a0a0aff000000ff282828fff2f2f2ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffc6c6c6ff222222ff000000ff010101ff070707ff0f0f0fff2a2a2aff5c5c5cff070707ff4a4a4afff8f8f8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffefefefff8b8b8bff434343ff454545ff7e7e7effdededeff888888ff000000ff777777fffdfdfdffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeaeaeaff222222ff0e0e0effc8c8c8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f7f7ff595959ff000000ff6a6a6afffbfbfbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffecececff595959ff000000ff2b2b2bffdadadaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeaeaeaffbebebeff9b9b9bff707070ff121212ff000000ff2f2f2fffcececeffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffb1b1b1ff000000ff000000ff080808ff2f2f2fff7f7f7fffecececffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdbdbdbff7e7e7eff909090ffb6b6b6ffebebebffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff5f5f5fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff828282ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c0c0ff141414ff000000ff020202ff898989ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfdfdfff242424ff000000ff000000ff000000ff030303ffa5a5a5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff383838ff000000ff000000ff000000ff000000ff000000ff0a0a0affa7a7a7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ff5d5d5dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0b0b0bffb1b1b1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff1d1d1dffc9c9c9ffffffffffffffffffffffffff
ffffffffffffffffa4a4a4ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff262626ffdbdbdbffffffffffffffffff
ffffffffd3d3d3ff141414ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff4b4b4bfff9f9f9ffffffffff
f3f3f3ff525252ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffff
dcdcdcff060606ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffff
d6d6d6ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6d6d6dffffffffff
dadadaff010101ff000000ff000000ff000000ff000000ff000000ff000000ff494949ff1b1b1bff000000ff000000ff000000ff000000ff000000ff000000ff7d7d7dffffffffff
f2f2f2ff4b4b4bff000000ff000000ff000000ff000000ff000000ff6e6e6eff888888ffbababaff464646ff000000ff000000ff000000ff000000ff222222ffd9d9d9ffffffffff
ffffffffeaeaeaff7c7c7cff252525ff1d1d1dff3c3c3cffa9a9a9ffdfdfdfff2e2e2effe2e2e2fff9f9f9ff969696ff363636ff2e2e2eff636363ffcfcfcfffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff848484ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffadadadff1a1a1aff000000ff060606ff8a8a8afffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff8f8f8ffe1e1e1ffe0e0e0ffacacacff656565ff0f0f0fff000000ff000000ff000000ff000000ff484848ff909090ffbcbcbcffd9d9d9ffddddddffffffffffffffffff
ffffffffcfcfcfff2f2f2fff232323ff111111ff000000ff000000ff050505ff050505ff050505ff030303ff000000ff070707ff161616ff202020ff808080ffffffffffffffffff
i$6c               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9fcf9fff4f9f4fff5faf5ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff4f9f4ffcae4caff87c287ff439f43ff0f850fff45a045ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffd5ead5ff5bab5bff007800ff088108ff369836ff419e41ff7fbe7fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffb2d7b2ff2a922aff007900ff52a752ffb3d8b3ffe0efe0fff3f9f3fffdfefdffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd3e9d3ff2a922aff027e02ff8dc58dfff3f9f3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffefefeff6bb46bff007900ff7cbc7cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffc9e4c9ff208b20ff208e20ffeef6eeffedf6edffcfe7cfffc6e2c6ffd3e9d3fff1f8f1ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffefffeff89c389ff007500ff76b976ffa3d0a3ff3b9b3bff007900ff007000ff0d840dff55a855ffbbddbbffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff7fbf7ff60ae60ff027c02ff3c9c3cff1c8b1cff64b064ff9ece9eff93c893ff409e40ff007900ff1c8b1cffbbdcbbffffffffffffffffffffffffffffffffff
ffffffffffffffffe8f3e8ff3e9d3eff057f05ff238f23ffbbdcbbfffcfefcffffffffffffffffffe9f4e9ff5bab5bff007b00ff50a650ffeff7efffffffffffffffffffffffffff
ffffffffffffffffe1efe1ff2b932bff007b00ff90c690ffffffffffffffffffffffffffffffffffffffffffd8ebd8ff0a820aff1a8b1affc7e3c7ffffffffffffffffffffffffff
ffffffffffffffffe6f2e6ff389938ff007700ffc8e3c8fffffffffffffffffffffffffffffffffffffffffff2f8f2ff3d9c3dff007900ffacd5acffffffffffffffffffffffffff
fffffffffffffffff5faf5ff5ead5eff007600ffb2d8b2fffffffffffffffffffffffffffffffffffffffffff2f8f2ff389938ff047b04ffb1d7b1ffffffffffffffffffffffffff
fffffffffffffffffdfefdff7fbd7fff007500ff6ab36afffdfdfdffffffffffffffffffffffffffffffffffddeeddff0e840eff289228ffd2e8d2ffffffffffffffffffffffffff
ffffffffffffffffffffffffcae5caff238e23ff0c830cffc8e3c8ffffffffffffffffffffffffffffffffff7ebd7eff007800ff63af63fffafdfaffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff88c388ff027d02ff1c8b1cff91c791ffdfeedfffcee6ceff69b369ff047f04ff2c932cffc8e3c8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff94c894ff329632ff0d840dff007600ff007800ff158715ff449f44ffc5e1c5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdfefdfffacd5acff87c287ff8dc58dffb7dab7fff0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd6ead6ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238e23ff8bc38bfff7fbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$7d               18  39 
fffffffffffffffffffffffffff7f6fffff4f2fffff4f3fffff4f3fffff4f3fffff4f3fffff4f3fffff4f3fffff4f3fffff4f2fffffbfaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff5f4cffff2208ffff2c13ffff2c13ffff2c13ffff2c13ffff2c13ffff2e15ffff361effff2e15ffffaea5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff8b7dffff5f4cffff6654ffff6654ffff6654ffff6654ffff6957ffff6351ffff4029ffff2b11ffffc1baffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffff7f6fffff4f3fffff5f4fffff5f4fffff5f4fffff5f4fffff8f8ffffc4bdffff3018ffff5e4bfffff3f3ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdffff7e6effff1d02ffffaea4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedaffff2f17ffff5a47ffffe9e5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff270effff9b90ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1ddffff361fffff4933ffffe0dbffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8273ffff2c14ffff8d7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdcffff4630ffff3c25ffffdfdbffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9588ffff280fffff7a6bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffe0dbffff4e39ffff331bffffd5d1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffff9e93ffff2106ffff7565ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffeceaffff5643ffff290fffffc7c1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffaea4ffff2106ffff6f5efffff7f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff2f0ffff5641ffff270fffffbcb4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff1300ffff614fffffefedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffb7afffff9184ffffcac3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f6ffff9487fffff3f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff2d15ffffa59affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff3e27ffff2b12ffff4029ffffcfcaffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe9e6ffff533fffff2a11ffff351dffff2d14ffff5c49ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff8f7ffff7463ffff2c13ffff351dffff351dffff341cffff280effff8072fffffefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9b8effff3017ffff351dffff351dffff351dffff351dffff331bffff2b12ffff9b8fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffb1a7ffff3219ffff341bffff351dffff351dffff351dffff351dffff351dffff331bffff3820ffffb0a8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffbdb5ffff371effff3018ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3219ffff3c26ffffc4bdffffffffffffffffffffffffff
ffffffffffcec8ffff432dffff2b12ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2f17ffff452fffffc2bbffffffffffffffffff
ffc7c1ffff4a35ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4630ffffbfb7fffffbfaff
ff5540ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351cffff260cffff5d49ffffe7e4ff
ffe0ddffff5743ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2c13ffff4e39ffffd4cfffffffffff
ffffffffffe2dfffff5843ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3017ffff4e39ffffd9d5ffffffffffffffffff
ffffffffffffffffffd6d0ffff4732ffff2e16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3118ffff4f3affffd9d5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3119ffff351dffff351dffff351dffff351dffff361effff2f16ffff3c25ffffc3bbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa59affff331bffff331bffff351dffff351dffff351dffff3018ffff3018ffffb7aeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff8c7fffff2b12ffff351dffff351dffff331bffff2c13ffff9184fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff6d5cffff270dffff351dffff2e15ffff6c5cfffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd8ffff4e39ffff2910ffff4e39ffffe5e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$3d               18  39 
fffffffffffffffffffffffffffffffffffffffffffcfcfffff7f6fffff4f2fffff5f4fffffbfaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffeeecffffb7aeffff7464ffff4029ffff2409ffff331bffff6c5affffb8b0fffffbfaffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffaca2ffff2810ffff4e38ffff7565ffff7f70ffff6654ffff270fffff2f17ffffa397fffffcfcffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffdfdbffff9c90ffffdfdbfffffffffffffffffffff5f4ffff8c7fffff260cffff4631ffffdbd7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefecffff452fffff2208ffffbbb3ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5f4ffff5541ffff250affffbfb7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdcffff2c14ffff4c36ffffdfdbffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5d0ffff6856ffff2309ffff9385fffffeffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffdfdffffb5adffff8071ffff7767ffff4832ffff361fffff9588fffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffcfbffff7b6cffff1c01ffff270dffff2c13ffff3820ffff887affffe8e5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff6f5ffffefedffffe5e2ffffb5adffff715fffff2f16ffff533effffd9d5ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfbffff8577ffff2208ffff7160fffff5f4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdddaffff3e27ffff2f17ffffe0dcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd9d5ffff3c25ffff2d15ffffdfdcffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdffff9588ffff2207ffff6552fffff1efffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff8d7fffff6c59ffffb2a9ffffd9d5ffffe4e1ffffcac3ffff7f6fffff2a11ffff321affffbeb6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff8778ffff442dffff2e15ffff1d03ffff1a00ffff240affff3c24ffff5c49ffffb9b1ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffdbd6ffffb2a9ffff9b8fffff978bffffa59affffc8c1ffffeeedffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f6ffff9487fffff3f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff2d15ffffa59affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff3e27ffff2b12ffff4029ffffcfcaffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe9e6ffff533fffff2a11ffff351dffff2d14ffff5c49ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff8f7ffff7463ffff2c13ffff351dffff351dffff341cffff280effff8072fffffefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9b8effff3017ffff351dffff351dffff351dffff351dffff331bffff2b12ffff9b8fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffb1a7ffff3219ffff341bffff351dffff351dffff351dffff351dffff351dffff331bffff3820ffffb0a8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffbdb5ffff371effff3018ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3219ffff3c26ffffc4bdffffffffffffffffffffffffff
ffffffffffcec8ffff432dffff2b12ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2f17ffff452fffffc2bbffffffffffffffffff
ffc7c1ffff4a35ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4630ffffbfb7fffffbfaff
ff5540ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351cffff260cffff5d49ffffe7e4ff
ffe0ddffff5743ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2c13ffff4e39ffffd4cfffffffffff
ffffffffffe2dfffff5843ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3017ffff4e39ffffd9d5ffffffffffffffffff
ffffffffffffffffffd6d0ffff4732ffff2e16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3118ffff4f3affffd9d5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3119ffff351dffff351dffff351dffff351dffff361effff2f16ffff3c25ffffc3bbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa59affff331bffff331bffff351dffff351dffff351dffff3018ffff3018ffffb7aeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff8c7fffff2b12ffff351dffff351dffff331bffff2c13ffff9184fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff6d5cffff270dffff351dffff2e15ffff6c5cfffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd8ffff4e39ffff2910ffff4e39ffffe5e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$2c               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffff9fcf9fff3f9f3fff2f8f2fff5faf5fffdfefdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe7f3e7ff98cb98ff46a146ff0e840eff017d01ff279127ff6fb56fffddeeddffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe4f2e4ff409d40ff007800ff2c942cff4ca34cff48a148ff249024ff007d00ff2a922affcde5cdffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe6f2e6ff64b064ff87c287ffd7ebd7fff5faf5fff0f7f0ffc9e4c9ff45a045ff007c00ff449f44ffeff7efffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f9f3ffffffffffffffffffffffffffffffffffffffffffc8e3c8ff118511ff208d20ffc7e3c7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f8d1fff108510ffa4d1a4ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfefcff1d8c1dff168816ffb0d7b0ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc6e2c6ff108510ff2b932bffe2f1e2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff66b066ff057f05ff6ab36affffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffafd7afff118511ff248f24ffddeeddffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd2e8d2ff2c932cff057f05ffa0cfa0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffdeefdeff419d41ff047d04ff7fbe7ffffdfffdffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdfeedfff3f9d3fff007900ff78bb78fffdfefdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdbeddbff3e9d3eff007800ff5eac5efff4faf4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdceddcff409e40ff068006ff7abb7afff4f9f4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffddeeddff429f42ff017d01ff3b9b3bffabd4abffafd6afffaad4aaffaad4aaffaad4aaffa5d1a5ffc5e1c5fffdfefdffffffffffffffffffffffffff
ffffffffffffffffffffffff73b773ff006f00ff007a00ff007900ff007700ff007700ff007700ff007700ff007700ff006f00ff45a045fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffbbdcbbff7ebd7eff86c186ff86c186ff86c186ff86c186ff86c186ff86c186ff86c186ff7ebe7effabd4abfffbfdfbffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd6ead6ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238e23ff8bc38bfff7fbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$4c               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfefdfff3f9f3fff2f9f2fffbfdfbffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9acb9aff118511ff178917ffb0d6b0ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfefdfff2f942fff017e01ff178917ffafd6afffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffafdfaff67b267ff319631ff238f23ff158715ffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffacd5acff057c05ff9ccc9cff4aa24aff0f840fffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffeff7efff218d21ff309530ffeef6eeff4ba34bff0d830dffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff73b873ff037e03ff9bcc9bffffffffff419e41ff0d830dffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffb7dab7ff098109ff57a957fff6fbf6ffffffffff419e41ff0d830dffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe6f2e6ff3a9b3aff1d8c1dffdbeddbffffffffffffffffff419e41ff0d830dffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff7abb7aff007a00ff90c690ffffffffffffffffffffffffff419e41ff0d830dffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffc1e0c1ff158715ff52a652ffffffffffffffffffffffffffffffffff449f44ff0d830dffb7dab7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffebf5ebff429f42ff027c02ff92c892ffc2e0c2ffc1dfc1ffc1dfc1ffc1dfc1ff349734ff0e840eff86c186ffbdddbdffddeeddffffffffffffffffffffffffff
ffffffffffffffffcde6cdff007200ff007a00ff007300ff007000ff007000ff007000ff007000ff088108ff118511ff007700ff006700ff67b267ffffffffffffffffffffffffff
ffffffffffffffffeef7eeffa4d1a4ffaad4aaffaad4aaffaad4aaffaad4aaffaad4aaffaad4aaff309530ff0e840eff77b977ffa5d1a5ffd1e8d1ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff44a044ff0d830dffbadcbaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff419e41ff0d830dffafd6afffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff309630ff007800ffa8d2a8ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9fce9fff84c084ffd6ead6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd6ead6ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238e23ff8bc38bfff7fbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$Ad               18  39 
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefedffff6d5bffff412affff6b59fffff3f1ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc6bfffff3c25ffff280fffff3a22ffffbeb7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9486ffff270dffff6f5effff2b12ffff8a7cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff6452ffff3018ffffc4bdffff2a11ffff5e4bffffedecffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffcac4ffff3219ffff6250ffffebe9ffff523effff331affffc2bbffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff1b00ffff9083fffffbfaffff8b7cffff1e03ffff9487fffffffeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff1efffff614effff2007ffffc5beffffffffffffbdb5ffff2309ffff5b47ffffeceaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffd5d1ffff2c13ffff4c37ffffe2dfffffffffffffe3dfffff452effff2b11ffffcdc7ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefeffff9a8dffff1d03ffff8476fffffbfbfffffffffffff6f4ffff7463ffff2006ffff9184fffffdfdffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff9f9ffff5440ffff2a11ffffb2a9ffffffffffffffffffffffffffffb0a7ffff260cffff4e39fffff5f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffd4ceffff3018ffff4832ffffc5beffffeae7ffffeae7ffffeae8ffffc0b8ffff4029ffff2d15ffffcfc9ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff8f82ffff321affff2a11ffff2106ffff1f05ffff1f05ffff1f05ffff2106ffff2c13ffff321affff9588ffffffffffffffffffffffffff
fffffffffffffffffffffffffffefeffff5642ffff3219ffff6b59ffff9082ffff9082ffff9082ffff9082ffff9082ffff614fffff2e15ffff5541fffffdfcffffffffffffffffff
ffffffffffffffffffffffffffd5d0ffff3c26ffff3f29ffffd7d3ffffffffffffffffffffffffffffffffffffffffffffc6bfffff3a24ffff3820ffffcfc9ffffffffffffffffff
ffffffffffffffffffffffffff9589ffff2c13ffff5e4cfffffbfbfffffffffffffffffffffffffffffffffffffffffffff6f5ffff5744ffff2a10ffff8d80ffffffffffffffffff
fffffffffffffffffffcfcffff6452ffff290fffff9b8fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff988cffff2b11ffff5c49fffffafaffffffffff
ffffffffffffffffffc4bdffff3018ffff2e14ffffd8d3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd3ceffff2d14ffff2c14ffffbbb4ffffffffff
ffffffffffffffffffd3ceffff9487ffffaaa0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa69cffff9387ffffcec9ffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffff
ffffffffffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffff
ffffffffffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffff
fffaf9ffffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6ff
ffe1deffff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bff
ffffffffffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcff
ffffffffffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffff
ffffffffffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffff
ffffffffffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffff
i$6h               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f9fefff4f4fefff4f5feffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff6f6feffcbcbf9ff8889f1ff3e40e7ff0b0ce0ff3132e5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffd8d9faff5b5debff0000deff0000dfff2c2ee5ff3d3de7ff7072edffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffb7b8f6ff2829e4ff0000ddff4647e8ffacacf5ffdddefbfff2f2fefffcfdffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd8d8faff2a2ce5ff0000ddff7f80efffefeffdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffefeffff7172edff0000ddff6a6cecffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd2d2f9ff1e1fe2ff1112e1ffe4e5fcffefeffdffcfcff9ffc4c4f8ffd0d0f9ffefeffdffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff9193f1ff0000dcff686aecffa3a4f4ff393be7ff0000ddff0000dbff0002dfff4a4be9ffb3b4f6ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffafafeff6567ecff0000deff3335e6ff1416e2ff5658ebff9a9bf2ff9192f1ff3e3fe7ff0000ddff0c0fe1ffadaef5ffffffffffffffffffffffffffffffffff
ffffffffffffffffefeffdff3f40e7ff0000dfff1517e2ffafb0f5fff9f9feffffffffffffffffffeaeafcff5c5deaff0000ddff3e3fe7ffeaeafdffffffffffffffffffffffffff
ffffffffffffffffe8e9fcff292be5ff0000ddff8181f0ffffffffffffffffffffffffffffffffffffffffffe0e0fbff0708e0ff0c0ee1ffb8b9f6ffffffffffffffffffffffffff
ffffffffffffffffecedfdff383ae6ff0000ddffb9b9f7fffffffffffffffffffffffffffffffffffffffffff8f8feff3c3ee6ff0000ddff9a9bf2ffffffffffffffffffffffffff
fffffffffffffffff9f9feff6264ecff0000dcffa3a3f4fffffffffffffffffffffffffffffffffffffffffff8f8feff3738e6ff0000deff9ea0f3ffffffffffffffffffffffffff
fffffffffffffffffefeffff8687f1ff0000dcff5a5bebfff8f9feffffffffffffffffffffffffffffffffffe4e5fcff0d0ee1ff1719e3ffc6c6f8ffffffffffffffffffffffffff
ffffffffffffffffffffffffd2d2f9ff2123e3ff0606dfffbbbcf7ffffffffffffffffffffffffffffffffff8283efff0000ddff4e50e9fff9f9feffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff8f90f2ff0202deff0e10e1ff8687f0ffdcddfbffd0d1f9ff696aecff0000dfff1e1fe2ffbbbcf6ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffcfcffff9696f2ff2c2ee5ff0406e0ff0000dcff0000ddff090ce0ff3739e6ffbbbbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffe2e2fcffababf5ff8384efff8788f0ffb1b2f5ffececfdffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafefffffffffffffffffffffffffffffffffffffffffffffffffffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Jh               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffff7f7fefff2f2fdfff3f3fdfff3f3fdfff3f3fdfff3f3fdfffbfbffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff5f5febff0001dfff0608e0ff0a0be0ff1617e2ff1819e2ffbdbdf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff9f9ff3ff6768ecff6b6bedff5f60ebff2425e4ff1112e2ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0f0fdff3f40e7ff0a0be1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdedefbff3c3de7ff0b0ce1ffbcbcf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdffbff3d3ee7ff0b0ce1ffbbbbf6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdbdbfbff3a3be7ff0b0ce1ffc1c2f7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefeffffffffffffffffffffffffffffffffffffb5b5f6ff1314e2ff1b1be2ffeaeafcffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff1f1fdff8181efff8f90f1ffdcddfaffe0e0fbffb3b3f5ff3233e6ff0304e0ff8182effffefeffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffafafeff6d6eedff0000deff0000deff0000deff0000dfff2425e3ff6f70edfff1f1feffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd2d2faff8f8ff1ff8687f0ff8687f0ff9090f1ffc9c9f8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefffffaf9fefffffffffffffffffffffffffffffffffffffffffffffffffffbfbfefffefdffffffffffffffffffffffffffffffffffff
ffffffffffffffffccccf8ff7d7eefff5b5debff5657eaff6364ecffa3a4f4ffffffffffffffffffc3c4f8ff6f71edff5657eaff595aebff7778eeffb1b2f5fffeffffffffffffff
ffffffff9496f2ff1d1fe2ff0000deff0000deff0000deff0000deff0608e0ff5658eaff797aefff1717e2ff0000deff0000deff0000deff0000deff0f10e1ff6062ebfff4f4fdff
c3c3f8ff191be3ff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0204dfff0001dfff0305e0ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff0002dfff9495f2ff
8080efff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff3b3de7ff
5e5febff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2628e4ff
6667ecff0000ddff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2c2de5ff
a9a9f5ff0c0de0ff0204dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff6566ecff
fbfbfeff6364ecff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff1c1ee3ffe2e3fbff
ffffffffedeefdff4041e7ff0000deff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0001dfff0d0de1ffb6b7f6ffffffffff
ffffffffffffffffd3d4f9ff2f31e5ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0001dfff0e0fe0ffa3a4f4ffffffffffffffffff
ffffffffffffffffffffffffd6d6f9ff3132e5ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0102dfff0d0ee0ffa2a4f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9dafaff4141e6ff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0103e0ff0c0de0ffa1a3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffededfcff484ae8ff0000deff0608e0ff0608e0ff0103dfff0c0ee1ffa2a3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdcdcfbff3133e5ff0000dfff0204dfff0909e0ffa0a2f3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1f9ff1b1ee3ff0000dfff8587f0fffffeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a6f4ff5b5cebfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffff1f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Qc               18  39 
fffffffffffffffffffffffffffffffffefffeffcee6ceff8cc48cff269026ff269026ff248f24ff3c9c3cff95c995ffe3f1e3ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6ff7abb7aff0b820bff0f840fff2c932cff389a38ff389938ff259025ff0a810aff158815ff9bcc9bfffafcfaffffffffffffffffffffffffff
fffffffffffffffffcfefcff6fb56fff007a00ff289128ff9ccd9cffedf6edffffffffffffffffffdaecdaff8cc48cff178917ff017d01ff89c389fffdfefdffffffffffffffffff
ffffffffffffffffa8d2a8ff047e04ff1f8d1fffbfdebfffffffffffffffffffffffffffffffffffffffffffffffffffa1cfa1ff0e840eff0f850fffc2e0c2ffffffffffffffffff
fffffffff9fcf9ff379837ff007b00ff84c084fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffeff5bab5bff007800ff62af62fffcfefcffffffffff
ffffffffcbe5cbff0a810aff1d8c1dffc0dfc0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa5d1a5ff088008ff1e8c1efff3f9f3ffffffffff
ffffffff94c894ff037f03ff3a9b3affe0f0e0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcde6cdff289128ff098109ffc7e2c7ffffffffff
ffffffff85c085ff017d01ff4ca44cfff5faf5ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd9ecd9ff339833ff078007ffabd4abffffffffff
ffffffff7bbc7bff017d01ff4aa34afff3f9f3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd8ebd8ff329732ff078007ffa4d0a4ffffffffff
ffffffff87c187ff017e01ff4aa34afff4f9f4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd9ebd9ff339733ff078007ffa4d1a4ffffffffff
ffffffff9acb9aff047f04ff359935ffdbeddbffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc9e3c9ff238e23ff0a810affcde6cdffffffffff
ffffffffd7ebd7ff0d830dff168916ffbadcbaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9ccc9cff047e04ff269026fff7fbf7ffffffffff
fffffffffcfdfcff47a147ff007a00ff73b773fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5faf5ff469f46ff007800ff72b772fffdfefdffffffffff
ffffffffffffffffbcddbcff088008ff128612ffa4d0a4ffffffffffffffffffffffffffffffffffffffffffffffffff81be81ff007d00ff279027ffd6ead6ffffffffffffffffff
ffffffffffffffffffffffff83c083ff007d00ff148714ff77b977ffd6ead6fffcfdfcfffcfdfcffc1dfc1ff5dac5dff088108ff168816ffb5d9b5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff99cb99ff279127ff148714ff098109ff0d840dff158815ff168816ff0f840fff349834ffbbddbbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe7f3e7ffaed6aeff55a855ff56a956ff2a922aff067f06ff53a753fffcfdfcffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff95c995ff057f05ff3b9a3bffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc3e0c3ff1b8b1bff0b820bff9dcd9dffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8bc48bff128612ff128612ff2b932bff2d942dff2c932cffa4d1a4ffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd7ebd7ffe2f0e2ffafd6afff5cac5cff2e942eff319631ff319531ffa6d2a6ffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238f23ff90c690ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$Jd               18  39 
fffffffffffffffffffffffffffffffffffffffffffdfdfffff4f3fffff4f3fffff4f3fffff4f3fffff4f2fffff8f7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffd5cfffff2b12ffff260cffff280fffff3017ffff2a12ffff7666fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe6e3ffff8072ffff7e6effff7f70ffff5a46ffff260dffff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa398ffff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9a8effff240affff7564fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9b8fffff240affff7463fffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9589ffff2309ffff7868fffffdfdffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffefeffffffffffffffffffffffffffffffffffffffffffff604dffff240affff9589ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffc2bcffff8475ffffcbc5ffffe4e0ffffdcd8ffff8373ffff270effff4f3affffd9d4ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffcac4ffff361effff1a00ffff1b00ffff1a00ffff3219ffff5a47ffffc0b9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffff4f2ffffb3aaffff978bffff988cffff9689ffffb7afffffeceaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f6ffff9487fffff3f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff2d15ffffa59affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff3e27ffff2b12ffff4029ffffcfcaffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe9e6ffff533fffff2a11ffff351dffff2d14ffff5c49ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff8f7ffff7463ffff2c13ffff351dffff351dffff341cffff280effff8072fffffefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9b8effff3017ffff351dffff351dffff351dffff351dffff331bffff2b12ffff9b8fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffb1a7ffff3219ffff341bffff351dffff351dffff351dffff351dffff351dffff331bffff3820ffffb0a8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffbdb5ffff371effff3018ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3219ffff3c26ffffc4bdffffffffffffffffffffffffff
ffffffffffcec8ffff432dffff2b12ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2f17ffff452fffffc2bbffffffffffffffffff
ffc7c1ffff4a35ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4630ffffbfb7fffffbfaff
ff5540ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351cffff260cffff5d49ffffe7e4ff
ffe0ddffff5743ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2c13ffff4e39ffffd4cfffffffffff
ffffffffffe2dfffff5843ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3017ffff4e39ffffd9d5ffffffffffffffffff
ffffffffffffffffffd6d0ffff4732ffff2e16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3118ffff4f3affffd9d5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3119ffff351dffff351dffff351dffff351dffff361effff2f16ffff3c25ffffc3bbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa59affff331bffff331bffff351dffff351dffff351dffff3018ffff3018ffffb7aeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff8c7fffff2b12ffff351dffff351dffff331bffff2c13ffff9184fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff6d5cffff270dffff351dffff2e15ffff6c5cfffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd8ffff4e39ffff2910ffff4e39ffffe5e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$5d               18  39 
fffffffffffffffffffffffffffffffffffffffffff8f7fffff4f3fffff4f3fffff4f3fffff4f3fffff4f3fffff4f2fffff8f7fffffffeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff3f2ffff5642ffff2f17ffff2b12ffff2c13ffff2c13ffff2c13ffff2208ffff7161fffff6f5ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff1efffff2b12ffff4a35ffff6d5bffff6654ffff6654ffff6654ffff5f4cffff978bfffff8f7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffd5cfffff2207ffff8576fffffcfcfffff5f4fffff5f4fffff5f4fffff5f3fffff7f7fffffefeffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffbdb4ffff2a11ffffa094ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa298ffff341cffffb0a6ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9083ffff432cffffd7d2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff7160ffff3d26ffff9386ffffaca2ffffbdb5ffffd8d3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff4e39ffff2209ffff1900ffff1500ffff290fffff4832ffff7766ffffd5d0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffc3bbffffd3ceffffd3ceffffc2bbffff9689ffff432cffff2409ffff4d37ffffd0cbffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe9e6ffff6552ffff2006ffff7767fffff5f4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc7c1ffff2b12ffff3a23ffffe3dfffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdcffff4b35ffff280fffffdedaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff2c13ffff4d37ffffe9e7ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6f5ffff7766ffff1c01ffff8c7efffffcfcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff968affff8171ffffbdb6ffffe1ddffffe1ddffffb4aaffff604dffff270cffff5d49ffffe0dcffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff8374ffff3e27ffff2c13ffff1a00ffff1a00ffff2d14ffff432dffff7c6cffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffbfbffffd3cdffffb0a6ffff968affff968affffb1a8ffffd6d1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f6ffff9487fffff3f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff2d15ffffa59affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff3e27ffff2b12ffff4029ffffcfcaffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe9e6ffff533fffff2a11ffff351dffff2d14ffff5c49ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff8f7ffff7463ffff2c13ffff351dffff351dffff341cffff280effff8072fffffefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9b8effff3017ffff351dffff351dffff351dffff351dffff331bffff2b12ffff9b8fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffb1a7ffff3219ffff341bffff351dffff351dffff351dffff351dffff351dffff331bffff3820ffffb0a8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffbdb5ffff371effff3018ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3219ffff3c26ffffc4bdffffffffffffffffffffffffff
ffffffffffcec8ffff432dffff2b12ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2f17ffff452fffffc2bbffffffffffffffffff
ffc7c1ffff4a35ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4630ffffbfb7fffffbfaff
ff5540ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351cffff260cffff5d49ffffe7e4ff
ffe0ddffff5743ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2c13ffff4e39ffffd4cfffffffffff
ffffffffffe2dfffff5843ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3017ffff4e39ffffd9d5ffffffffffffffffff
ffffffffffffffffffd6d0ffff4732ffff2e16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3118ffff4f3affffd9d5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3119ffff351dffff351dffff351dffff351dffff361effff2f16ffff3c25ffffc3bbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa59affff331bffff331bffff351dffff351dffff351dffff3018ffff3018ffffb7aeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff8c7fffff2b12ffff351dffff351dffff331bffff2c13ffff9184fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff6d5cffff270dffff351dffff2e15ffff6c5cfffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd8ffff4e39ffff2910ffff4e39ffffe5e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Ah               18  39 
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffaaabf5ff2020e3ff2020e2ffa2a3f4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff5c5deaff0000deff0000deff494be9fff7f7feffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffededfdff1112e1ff2a2be4ff3737e6ff0809e0ffdcddfbffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffa7a8f4ff0000dfff6d6eedff7779efff0000dcff9596f2ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff4f51e9ff0507e0ffababf4ffa6a7f4ff0305dfff3c3de7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe5e5fcff1a1ce3ff1315e2ffd3d4f9ffd6d6faff1c1ee3ff090be0ffdadafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff9c9df2ff0506dfff3032e6fffffffffff9f9feff3e40e7ff0000dfff898af0ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff5759eaff0000deff7172edffffffffffffffffff7677eeff0000deff4142e8fffdfdffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffddddfbff2729e4ff0102dfffc9c9f8ffffffffffffffffffb5b5f6ff080ae0ff181ae2ffcdcef8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff9e9ff3ff0304e0ff2426e3fffffffffffffffffffffffffffcfcffff2b2ce4ff0202deff8586f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffefeffff6466ecff0000ddff5f61ebffe6e6fcffe5e5fcffe5e5fcffe7e8fcff595aeaff0000deff4d4fe9fffbfcffffffffffffffffffffffffffff
ffffffffffffffffffffffffdbdbfaff2628e4ff0000dfff0000ddff0000ddff0000ddff0000ddff0000ddff0000ddff0000dfff2325e4ffd3d3faffffffffffffffffffffffffff
ffffffffffffffffffffffffb2b2f6ff0000deff1c1ee3ff6d6fedff7677eeff7677eeff7677eeff7677eeff7071edff0f12e2ff0101deff9c9ef2ffffffffffffffffffffffffff
fffffffffffffffffbfbfeff7375eeff0000ddff6768ecffffffffffffffffffffffffffffffffffffffffffffffffff5859eaff0000ddff5d5eebfff6f6feffffffffffffffffff
ffffffffffffffffeaeafcff2627e4ff0000deffb0b1f5ffffffffffffffffffffffffffffffffffffffffffffffffffaaaaf4ff0000deff1b1ce3ffd9d9fbffffffffffffffffff
ffffffffffffffffbdbdf7ff0000deff2a2ce4ffdedefbffffffffffffffffffffffffffffffffffffffffffffffffffe4e5fbff2f30e5ff0000ddffa7a7f5ffffffffffffffffff
fffffffffefeffff5456eaff0000daff5f60ecfff9f9fefffffffffffffffffffffffffffffffffffffffffffffffffff9f9feff696aedff0000daff4143e8fff6f6feffffffffff
fffffffffdfdffff9495f2ff7e7eeeffd0d0f9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd2d3faff7e7eeeff8c8df1fff6f6feffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafefffffffffffffffffffffffffffffffffffffffffffffffffffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Js               18  39 
fffffffffffffffffffffffffffffffffffffffffdfdfdfff2f2f2fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff5f5f5fffefefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffd6d6d6ff000000ff000000ff000000ff000000ff030303ff474747fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe6e6e6ff636363ff5e5e5eff5e5e5eff323232ff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff979797ff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff454545fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8b8b8bff000000ff444444fff4f4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff858585ff000000ff494949fffafafaffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffefefeffffffffffffffffffffffffffffffffffffffffff414141ff000000ff6c6c6cffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffbababaff626262ffb9b9b9ffdcdcdcffd6d6d6ff6b6b6bff000000ff181818ffc8c8c8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffc5c5c5ff080808ff000000ff000000ff000000ff000000ff2c2c2cffa7a7a7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f3f3ffa3a3a3ff7e7e7eff7e7e7eff7e7e7effa1a1a1ffe4e4e4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff5f5f5fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff828282ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c0c0ff141414ff000000ff020202ff898989ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfdfdfff242424ff000000ff000000ff000000ff030303ffa5a5a5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff383838ff000000ff000000ff000000ff000000ff000000ff0a0a0affa7a7a7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ff5d5d5dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0b0b0bffb1b1b1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff1d1d1dffc9c9c9ffffffffffffffffffffffffff
ffffffffffffffffa4a4a4ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff262626ffdbdbdbffffffffffffffffff
ffffffffd3d3d3ff141414ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff4b4b4bfff9f9f9ffffffffff
f3f3f3ff525252ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffff
dcdcdcff060606ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffff
d6d6d6ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6d6d6dffffffffff
dadadaff010101ff000000ff000000ff000000ff000000ff000000ff000000ff494949ff1b1b1bff000000ff000000ff000000ff000000ff000000ff000000ff7d7d7dffffffffff
f2f2f2ff4b4b4bff000000ff000000ff000000ff000000ff000000ff6e6e6eff888888ffbababaff464646ff000000ff000000ff000000ff000000ff222222ffd9d9d9ffffffffff
ffffffffeaeaeaff7c7c7cff252525ff1d1d1dff3c3c3cffa9a9a9ffdfdfdfff2e2e2effe2e2e2fff9f9f9ff969696ff363636ff2e2e2eff636363ffcfcfcfffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff848484ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffadadadff1a1a1aff000000ff060606ff8a8a8afffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff8f8f8ffe1e1e1ffe0e0e0ffacacacff656565ff0f0f0fff000000ff000000ff000000ff000000ff484848ff909090ffbcbcbcffd9d9d9ffddddddffffffffffffffffff
ffffffffcfcfcfff2f2f2fff232323ff111111ff000000ff000000ff050505ff050505ff050505ff030303ff000000ff070707ff161616ff202020ff808080ffffffffffffffffff
i$5c               18  39 
fffffffffffffffffffffffffffffffffffffffffbfdfbfff3f9f3fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff2f8f2fff3f9f3fffbfdfbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff8dc58dff0f840fff057f05ff068006ff068006ff068006ff027e02ff138713ffb7dbb7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff68b268ff007b00ff47a147ff4ba34bff4aa34aff4aa34aff47a147ff54a854ffcae4caffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fcf8ff57aa57ff017d01ffe5f2e5fff3f9f3fff3f9f3fff3f9f3fff3f8f3fff3f9f3fffbfdfbffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f9f3ff46a146ff228e22fffafcfaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdeeedeff339733ff3f9d3ffff7fbf7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd2e8d2ff259025ff6fb66fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffbadbbaff138713ff4aa34aff9ccd9cffa3d0a3ffc0dfc0ffe9f4e9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff9fce9fff007800ff007700ff007100ff007600ff158715ff419e41ff94c894ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9ecd9ffbadcbaffcce5ccffc9e3c9ffa0cfa0ff50a550ff017d01ff0a820aff79ba79ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8fbf8ff99cb99ff0c830cff168816ffbbdcbbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff429e42ff017d01ff74b874ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff74b974ff017c01ff62af62ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff47a147ff078107ff8ac38affffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb4d9b4ff108510ff1e8c1effd2e8d2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd3e9d3ff55a855ff91c791ffcee6ceffdfefdfffc8e3c8ff74b874ff128612ff118611ff90c790ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcae4caff289128ff138613ff007800ff007400ff007900ff168816ff399a39ffa9d3a9ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe5f2e5ffb4d9b4ff8bc48bff82bf82ff8ec58effb8dbb8fff0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f1e5ffd6ead6ffd6ead6ffdfefdffffdfefdffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfdfbff99ca99ff2a922aff027d02ff007d00ff1c8c1cff7cbc7cfff2f8f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa9d3a9ff098109ff078007ff108510ff108510ff0c830cff007c00ff7ebd7effffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fbf8ff52a752ff017d01ff108510ff108510ff108510ff108510ff0a820aff299129ffe8f3e8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff49a249ff027d02ff108510ff108510ff108510ff108510ff0b820bff248f24ffe2f0e2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8fcf8fff7fbf7ff68b268ff057f05ff0f850fff108510ff108510ff108510ff0b820bff3e9d3efff0f8f0fff4f9f4ffffffffffffffffffffffffff
fffffffff2f8f2ffa3d1a3ff66b166ff65b065ff54a854ff148714ff0f840fff108510ff108510ff108510ff108510ff439f43ff69b369ff5cac5cff93c893ffeaf5eaffffffffff
f3f9f3ff68b268ff027e02ff017d01ff037e03ff068006ff108510ff108510ff108510ff108510ff108510ff108510ff098109ff027e02ff037e03ff007c00ff4ca44cffeaf4eaff
99cb99ff057f05ff0b820bff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0f840fff037e03ff78ba78ff
6fb56fff007a00ff108510ff108510ff108510ff108510ff108510ff108510ff0f850fff0d840dff0e840eff108510ff108510ff108510ff108510ff108510ff078007ff3c9b3cff
6fb56fff007a00ff108510ff108510ff108510ff108510ff0f850fff148714ff2f952fff178917ff168816ff0e840eff108510ff108510ff108510ff108510ff068006ff3a9a3aff
94c894ff057f05ff0d830dff108510ff108510ff108510ff098209ff359835ff7aba7aff62af62ff5cac5cff027e02ff108510ff108510ff108510ff108510ff057f05ff65b065ff
edf6edff67b167ff027e02ff098109ff0b830bff057f05ff198919ffc4e1c4ffa7d2a7ff77ba77ffdaecdaff309530ff037e03ff0b830bff0b820bff027e02ff3c9c3cffe0efe0ff
fffffffff3f9f3ffa9d4a9ff5ead5eff47a147ff7fbe7fffd0e7d0ffffffffffa9d4a9ff6eb56effffffffffe1f0e1ff81bf81ff4aa24aff4aa34aff8cc48cffe3f0e3ffffffffff
fffffffffffffffffffffffffafcfafff7fbf7ffffffffffffffffffffffffff83c083ff67b167fffffffffffffffffffffffffff7fbf7fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0efe0ff2f952fff259025ffcee6ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff4f9f4fff7fbf7ffe2f0e2ffa9d3a9ff4aa34aff088108ff037f03ff409d40ffadd5adffdfeedffff4f9f4fffbfdfbffffffffffffffffffffffffff
ffffffffffffffff80be80ff3b9b3bff419e41ff319631ff037e03ff007a00ff078007ff078107ff007a00ff078007ff2e942eff3f9d3fff419e41ff6fb66ffff1f8f1ffffffffff
ffffffffffffffffb4d9b4ff97ca97ff98ca98ff9bcc9bffa2cfa2ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa2cfa2ff9bcc9bff99cb99ff96c996ffa5d1a5fff6faf6ffffffffff
i$6s               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdfff6f6f6fff1f1f1fffbfbfbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1e1e1ffabababff616161ff232323ff000000ffb2b2b2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffa0a0a0ff1d1d1dff000000ff111111ff313131ff424242ffccccccffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffafafaff686868ff000000ff080808ff7a7a7affc6c6c6ffe7e7e7fff6f6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff8d8d8dff000000ff2a2a2affc1c1c1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffdfdfdfff060606ff131313ffc0c0c0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff6d6d6dff000000ff6b6b6bffffffffffdbdbdbffc5c5c5ffc5c5c5ffdbdbdbffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffefefefff1b1b1bff090909ffa2a2a2ff747474ff080808ff000000ff000000ff1a1a1aff737373ffe4e4e4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffc3c3c3ff121212ff090909ff292929ff181818ff848484ff9a9a9aff6f6f6fff0c0c0cff000000ff4b4b4bffefefefffffffffffffffffffffffffff
ffffffffffffffffffffffff9d9d9dff050505ff000000ff4e4e4effe0e0e0ffffffffffffffffffffffffffb5b5b5ff191919ff000000ff9b9b9bffffffffffffffffffffffffff
ffffffffffffffffffffffff898989ff000000ff131313ffd7d7d7ffffffffffffffffffffffffffffffffffffffffff6f6f6fff000000ff444444ffffffffffffffffffffffffff
ffffffffffffffffffffffff979797ff020202ff212121ffffffffffffffffffffffffffffffffffffffffffffffffffa5a5a5ff000000ff101010fffefefeffffffffffffffffff
ffffffffffffffffffffffffc0c0c0ff0d0d0dff191919fff3f3f3ffffffffffffffffffffffffffffffffffffffffffa2a2a2ff010101ff171717fffefefeffffffffffffffffff
ffffffffffffffffffffffffe6e6e6ff181818ff040404ffb0b0b0ffffffffffffffffffffffffffffffffffffffffff7a7a7aff000000ff5a5a5affffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff727272ff000000ff444444fff7f7f7ffffffffffffffffffffffffffdcdcdcff292929ff000000ffb8b8b8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffefefefff292929ff000000ff3b3b3bffbababaffdcdcdcffa7a7a7ff2a2a2aff000000ff5f5f5ffff7f7f7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffd8d8d8ff585858ff131313ff000000ff000000ff000000ff181818ff696969fff2f2f2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffc3c3c3ff919191ff7e7e7eff949494ffcacacaffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff1f1f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffff
ffffffffffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffff
ffffffffffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffff
ffffffffe8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ff
ffffffffc5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffbbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffc1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cff
ffffffffe7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ff
ffffffffffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffff
ffffffffffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffff
i$8s               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfbfbfff3f3f3fff1f1f1fff3f3f3fffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc7c7c7ff545454ff000000ff000000ff050505ff5c5c5cffc7c7c7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffaeaeaeff0b0b0bff080808ff545454ff5e5e5eff454545ff000000ff0e0e0effb8b8b8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffebebebff171717ff0d0d0dff9d9d9dfffffffffffffffffffcfcfcff7e7e7eff000000ff363636fff1f1f1ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffa6a6a6ff000000ff444444ffffffffffffffffffffffffffffffffffe4e4e4ff282828ff000000ffdfdfdfffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff9a9a9aff000000ff4c4c4cffffffffffffffffffffffffffffffffffe8e8e8ff333333ff000000ffe2e2e2ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd2d2d2ff040404ff191919ffc0c0c0ffffffffffffffffffffffffffa0a0a0ff000000ff525252fff5f5f5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff818181ff000000ff292929ff989898ffd4d4d4ff9c9c9cff141414ff272727ffd0d0d0ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff929292ff000000ff000000ff1d1d1dff000000ff282828ffdadadaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffb3b3b3ff363636ff060606ff535353ff414141ff000000ff111111ff555555ffd7d7d7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffa3a3a3ff070707ff1e1e1effc3c3c3fff9f9f9fff7f7f7ffc3c3c3ff393939ff000000ff303030ffe6e6e6ffffffffffffffffffffffffff
ffffffffffffffffffffffffddddddff151515ff070707ffc4c4c4ffffffffffffffffffffffffffffffffffdfdfdfff2b2b2bff000000ff818181ffffffffffffffffffffffffff
ffffffffffffffffffffffff9a9a9aff020202ff1e1e1efffdfdfdffffffffffffffffffffffffffffffffffffffffff747474ff000000ff4e4e4effffffffffffffffffffffffff
ffffffffffffffffffffffff8d8d8dff000000ff1e1e1efffcfcfcffffffffffffffffffffffffffffffffffffffffff777777ff000000ff5b5b5bffffffffffffffffffffffffff
ffffffffffffffffffffffffc0c0c0ff0d0d0dff020202ffa6a6a6ffffffffffffffffffffffffffffffffffe8e8e8ff3d3d3dff000000ffa1a1a1ffffffffffffffffffffffffff
fffffffffffffffffffffffffefefeff696969ff000000ff060606ff888888ffdcdcdcffdcdcdcffb5b5b5ff353535ff000000ff555555fff2f2f2ffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffcfcfcff7c7c7cff212121ff000000ff000000ff000000ff000000ff181818ff696969fff2f2f2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffd5d5d5ff999999ff7e7e7eff7e7e7eff949494ffcacacaffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff1f1f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffff
ffffffffffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffff
ffffffffffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffff
ffffffffe8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ff
ffffffffc5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffbbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffc1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cff
ffffffffe7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ff
ffffffffffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffff
ffffffffffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffff
i$9h               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcfffff5f5fefff1f1fdfff8f8feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc6c6f8ff5657eaff1415e1ff0000ddff3234e6ff9192f1fff1f2fdffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffbfbfeff9a9bf3ff0405e0ff0c0ee0ff5657eaff6263ebff2527e4ff0000deff5b5cebfff5f5feffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffb3b3f6ff090adfff191ae1ffb7b8f5ffffffffffffffffffd4d5faff3133e5ff0000deff8283efffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff2f2feff5254eaff0000dcff9293f1ffffffffffffffffffffffffffffffffffacadf5ff0203dfff2527e4ffe1e1fbffffffffffffffffffffffffff
ffffffffffffffffffffffffdfdffbff1d1fe3ff0d0fe1ffd3d3f9fffffffffffffffffffffffffffffffffffdfdffff1518e2ff1012e1ffb7b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffccccf9ff0000dcff2023e4ffdadbfaffffffffffffffffffffffffffffffffffffffffff4d4fe8ff0305dfff9798f2ffffffffffffffffffffffffff
ffffffffffffffffffffffffd4d4faff0a0bdfff1113e1ffd6d7faffffffffffffffffffffffffffffffffffffffffff4e4fe9ff0000ddff7476eeffffffffffffffffffffffffff
ffffffffffffffffffffffffececfcff3d3fe8ff0000dcff9091f2ffffffffffffffffffffffffffffffffffdadafbff2122e3ff0000ddff7476eeffffffffffffffffffffffffff
fffffffffffffffffffffffffefeffff9698f2ff0000deff0d0de1ff9fa0f3fffafafefffdfdffffb7b7f6ff2f31e5ff0000dfff0608dfff9799f2ffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffafaffff6c6dedff0808e0ff0204e0ff0b0ce1ff1113e1ff1213e1ff5d5eebff2e2fe5ff1113e1ffb6b6f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c1f7ff5f61ebff4242e8ff6162ebffadaef5ffe1e2fcff0d0fe0ff1e20e3ffd9d9fbffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7879eeff0000deff5d5febffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb4b5f6ff0e0fe1ff1415e1ffd4d4faffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffacacf5ff1315e1ff0000deff8a8bf1ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffafbfeffd3d3faffadadf5ff8a8bf1ff4748e8ff0000deff0b0ce1ff8889f0fffdfeffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff6f6feff393ae6ff0000daff0204e0ff1b1de2ff5253eaffc0c1f7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffbfbfeffa5a6f4ff7f80efffa6a6f4ffcfd0f9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefffffaf9fefffffffffffffffffffffffffffffffffffffffffffffffffffbfbfefffefdffffffffffffffffffffffffffffffffffff
ffffffffffffffffccccf8ff7d7eefff5b5debff5657eaff6364ecffa3a4f4ffffffffffffffffffc3c4f8ff6f71edff5657eaff595aebff7778eeffb1b2f5fffeffffffffffffff
ffffffff9496f2ff1d1fe2ff0000deff0000deff0000deff0000deff0608e0ff5658eaff797aefff1717e2ff0000deff0000deff0000deff0000deff0f10e1ff6062ebfff4f4fdff
c3c3f8ff191be3ff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0204dfff0001dfff0305e0ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff0002dfff9495f2ff
8080efff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff3b3de7ff
5e5febff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2628e4ff
6667ecff0000ddff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2c2de5ff
a9a9f5ff0c0de0ff0204dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff6566ecff
fbfbfeff6364ecff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff1c1ee3ffe2e3fbff
ffffffffedeefdff4041e7ff0000deff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0001dfff0d0de1ffb6b7f6ffffffffff
ffffffffffffffffd3d4f9ff2f31e5ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0001dfff0e0fe0ffa3a4f4ffffffffffffffffff
ffffffffffffffffffffffffd6d6f9ff3132e5ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0102dfff0d0ee0ffa2a4f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9dafaff4141e6ff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0103e0ff0c0de0ffa1a3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffededfcff484ae8ff0000deff0608e0ff0608e0ff0103dfff0c0ee1ffa2a3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdcdcfbff3133e5ff0000dfff0204dfff0909e0ffa0a2f3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1f9ff1b1ee3ff0000dfff8587f0fffffeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a6f4ff5b5cebfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffff1f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Qd               18  39 
fffffffffffffffffffffffffffffffffffffffffff3f1ffffc2baffff705effff412affff4731ffff4832ffff7968ffffccc6fffff9f9ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffcfc9ffff5844ffff2b12ffff412bffff5541ffff5743ffff543fffff3c25ffff2b12ffff6553ffffe1ddffffffffffffffffffffffffff
ffffffffffffffffffffffffffd3ceffff452fffff2910ffff7666ffffcec8ffffffffffffffffffffffffffffc0b9ffff6c5affff280effff4b36ffffddd9ffffffffffffffffff
ffffffffffffffffffedebffff6351ffff270effff7666fffff8f7ffffffffffffffffffffffffffffffffffffffffffffedeaffff6554ffff270effff6e5dfffff8f8ffffffffff
ffffffffffffffffffb3aaffff2f16ffff4029ffffe6e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcac4ffff351dffff3b23ffffc5beffffffffff
fffffffffffefdffff8071ffff2308ffff7463ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5b47ffff260cffff9488ffffffffff
ffffffffffeae7ffff604dffff270dffffa094ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e80ffff240affff7463fffffdfcff
ffffffffffe3dfffff5642ffff270dffffbab2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa094ffff270effff6554ffffefedff
ffffffffffdfdbffff513cffff280effffb8afffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9e92ffff270effff614fffffebe9ff
ffffffffffe3e0ffff5843ffff270dffffb9b0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f93ffff270effff614fffffeceaff
ffffffffffedeaffff6350ffff260cffff988cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8777ffff2309ffff7867fffffefdff
ffffffffffffffffff887affff2309ffff6b58fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfbffff533effff290fffff9b8fffffffffff
ffffffffffffffffffbeb6ffff361effff3922ffffd7d2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb4acffff2f17ffff4029ffffd0cbffffffffff
fffffffffffffffffff6f5ffff6d5dffff270dffff614effffeae8ffffffffffffffffffffffffffffffffffffffffffffdcd7ffff4e39ffff2a11ffff8779fffffdfdffffffffff
ffffffffffffffffffffffffffe3dfffff4f3affff270dffff5845ffffb5abfffff7f6fffffcfcfffff4f2ffffa095ffff4e38ffff2a10ffff6b5afffff4f3ffffffffffffffffff
ffffffffffffffffffffffffffffffffffe2dfffff7362ffff3e26ffff341cffff3018ffff341bffff3d26ffff361effff3b25ffff8172fffff6f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdad6ffff9285ffff6958ffff6c5affff2f17ffff3821ffffbdb5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4f3affff2a11ffffb7afffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdffff8677ffff2006ffff6451ffffe3e0ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe3dfffff5a46ffff2d13ffff432cffff4d38ffff452fffff6f5effffe9e6ff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487ffffffffffffe8e5ffff9488ffff5845ffff503affff4a34ffff7262ffffeae7ff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffff
ffffffffffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffff
ffffffffffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffff
fffaf9ffffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6ff
ffe1deffff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bff
ffffffffffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcff
ffffffffffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffff
ffffffffffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffff
ffffffffffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffff
i$3s               18  39 
fffffffffffffffffffffffffffffffffffffffffcfcfcfff5f5f5fff1f1f1fff2f2f2fff9f9f9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffecececffa8a8a8ff545454ff121212ff000000ff000000ff414141ff9e9e9efff7f7f7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffa1a1a1ff000000ff1c1c1cff4f4f4fff5e5e5eff424242ff000000ff000000ff7f7f7ffffafafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffdcdcdcff828282ffd0d0d0fffffffffffffffffff7f7f7ff777777ff000000ff0d0d0dffc7c7c7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff4f4f4ff1d1d1dff000000ff9a9a9affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffafafaff2f2f2fff000000ff9e9e9effffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe2e2e2ff030303ff151515ffcdcdcdffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcececeff4a4a4aff000000ff6a6a6afffdfdfdffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefeffa9a9a9ff656565ff565656ff1d1d1dff000000ff6e6e6effebebebffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefeff656565ff000000ff000000ff000000ff020202ff626262ffdcdcdcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff4f4f4ffecececffe0e0e0ffa7a7a7ff505050ff020202ff232323ffc6c6c6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff707070ff000000ff3f3f3fffedededffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1e1e1ff0f0f0fff010101ffcbcbcbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdbdbdbff0e0e0eff000000ffcacacaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff858585ff000000ff313131ffe6e6e6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff444444ff999999ffccccccffdcdcdcffbfbfbfff656565ff000000ff070707ffa0a0a0ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff717171ff1d1d1dff000000ff000000ff000000ff000000ff090909ff2d2d2dff9f9f9fffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd2d2d2ffa1a1a1ff828282ff7e7e7eff8b8b8bffb7b7b7ffe7e7e7ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff5f5f5fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff828282ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c0c0ff141414ff000000ff020202ff898989ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfdfdfff242424ff000000ff000000ff000000ff030303ffa5a5a5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff383838ff000000ff000000ff000000ff000000ff000000ff0a0a0affa7a7a7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ff5d5d5dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0b0b0bffb1b1b1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff1d1d1dffc9c9c9ffffffffffffffffffffffffff
ffffffffffffffffa4a4a4ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff262626ffdbdbdbffffffffffffffffff
ffffffffd3d3d3ff141414ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff4b4b4bfff9f9f9ffffffffff
f3f3f3ff525252ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffff
dcdcdcff060606ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffff
d6d6d6ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6d6d6dffffffffff
dadadaff010101ff000000ff000000ff000000ff000000ff000000ff000000ff494949ff1b1b1bff000000ff000000ff000000ff000000ff000000ff000000ff7d7d7dffffffffff
f2f2f2ff4b4b4bff000000ff000000ff000000ff000000ff000000ff6e6e6eff888888ffbababaff464646ff000000ff000000ff000000ff000000ff222222ffd9d9d9ffffffffff
ffffffffeaeaeaff7c7c7cff252525ff1d1d1dff3c3c3cffa9a9a9ffdfdfdfff2e2e2effe2e2e2fff9f9f9ff969696ff363636ff2e2e2eff636363ffcfcfcfffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff848484ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffadadadff1a1a1aff000000ff060606ff8a8a8afffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff8f8f8ffe1e1e1ffe0e0e0ffacacacff656565ff0f0f0fff000000ff000000ff000000ff000000ff484848ff909090ffbcbcbcffd9d9d9ffddddddffffffffffffffffff
ffffffffcfcfcfff2f2f2fff232323ff111111ff000000ff000000ff050505ff050505ff050505ff030303ff000000ff070707ff161616ff202020ff808080ffffffffffffffffff
i$4s               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8f8f8fff1f1f1fff5f5f5fffefefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f9f9ff3f3f3fff030303ff404040ffe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff909090ff000000ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd6d6d6ff1b1b1bff393939ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffff6f6f6ff4b4b4bff0b0b0bffb2b2b2ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff9a9a9aff000000ff7b7b7bffd9d9d9ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdcdcdcff262626ff151515fff0f0f0ffdbdbdbff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff565656ff040404ffa9a9a9ffffffffffd6d6d6ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffaaaaaaff000000ff5f5f5ffffbfbfbffffffffffd5d5d5ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe9e9e9ff111111ff1d1d1dffd5d5d5ffffffffffffffffffd5d5d5ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffcfcfcff636363ff010101ff9e9e9effffffffffffffffffffffffffe0e0e0ff000000ff414141fff5f5f5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffa5a5a5ff050505ff1f1f1fffbebebeffbdbdbdffbdbdbdffbdbdbdff9e9e9eff000000ff2e2e2effacacacffbfbfbffff0f0f0ffffffffffffffffffffffffff
ffffffffffffffff4d4d4dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ffbdbdbdffffffffffffffffffffffffff
ffffffffffffffffc4c4c4ffa5a5a5ffa5a5a5ffa5a5a5ffa5a5a5ffa5a5a5ffa5a5a5ff8a8a8aff000000ff282828ff969696ffa8a8a8ffebebebffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe4e4e4ff000000ff424242fffafafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd5d5d5ff000000ff3e3e3effe9e9e9ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1d1ff000000ff2d2d2dffe7e7e7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeaeaeaff7e7e7eff9d9d9dfff4f4f4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff1f1f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffffffffffff
fffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffffffffffff
ffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffffffffffff
ffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffffffffffff
e8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ffffffffff
c5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ffffffffff
bbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ffffffffff
c1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cffffffffff
e7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ffffffffff
ffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffffffffffff
ffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffffffffffff
i$Kc               18  39 
fffffffffffffffffffffffffcfefcfff2f8f2fff4f9f4fffdfefdfffffffffffffffffffffffffffffffffffffffffffdfefdfff2f8f2ffeef6eefff8fbf8ffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0e840eff2d942dffe2f0e2ffffffffffffffffffffffffffffffffffffffffff77b977ff037f03ff238f23ffbeddbeffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffffffffffffffffffffffffff8cc48cff007c00ff1b8b1bffbcddbcffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffffffffffffffffff9bcc9bff047f04ff118611ffafd7afffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffffffffffaad3aaff0f840fff108510ffadd6adffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe3f0e3ffffffffffb8dbb8ff138613ff068006ffa0cea0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2a932affeff7efffd4e9d4ff1c8b1cff047e04ff99cb99ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0b830bff329632ffbbdcbbff319631ff118511ff8bc48bfffbfdfbffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff108510ff1d8c1dff2a922aff047e04ff349834ffe8f4e8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff138613ff088108ff118511ff0d830dff088108ff76b976fffdfefdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff118611ff078107ffa9d3a9ff8dc58dff088108ff078007ff9bcc9bffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2c932cffe7f3e7ffffffffff6eb46eff007c00ff1a8a1affc4e1c4ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffedf6edff3f9d3fff007b00ff2e952effdceedcffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffffffffffd8ecd8ff2b942bff007b00ff48a148ffe8f3e8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffffffffffffffffffacd5acff158815ff007c00ff6db56dffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcee6ceff0c830cff2b932bffe2f0e2ffffffffffffffffffffffffffffffffff91c691ff0a810aff078007ff8ec58effffffffffffffffffffffffff
ffffffffffffffffffffffffcae4caff007800ff188a18ffdfefdfffffffffffffffffffffffffffffffffffffffffff65af65ff007900ff047f04ffa6d2a6fffcfdfcffffffffff
ffffffffffffffffffffffffe6f2e6ff84c084ff93c893fff0f7f0ffffffffffffffffffffffffffffffffffffffffffeef6eeff8ec58eff7bbc7bffa8d2a8fffbfdfbffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f1e5ffd6ead6ffd6ead6ffdfefdffffdfefdffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfdfbff99ca99ff2a922aff027d02ff007d00ff1c8c1cff7cbc7cfff2f8f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa9d3a9ff098109ff078007ff108510ff108510ff0c830cff007c00ff7ebd7effffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fbf8ff52a752ff017d01ff108510ff108510ff108510ff108510ff0a820aff299129ffe8f3e8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff49a249ff027d02ff108510ff108510ff108510ff108510ff0b820bff248f24ffe2f0e2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8fcf8fff7fbf7ff68b268ff057f05ff0f850fff108510ff108510ff108510ff0b820bff3e9d3efff0f8f0fff4f9f4ffffffffffffffffffffffffff
fffffffff2f8f2ffa3d1a3ff66b166ff65b065ff54a854ff148714ff0f840fff108510ff108510ff108510ff108510ff439f43ff69b369ff5cac5cff93c893ffeaf5eaffffffffff
f3f9f3ff68b268ff027e02ff017d01ff037e03ff068006ff108510ff108510ff108510ff108510ff108510ff108510ff098109ff027e02ff037e03ff007c00ff4ca44cffeaf4eaff
99cb99ff057f05ff0b820bff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0f840fff037e03ff78ba78ff
6fb56fff007a00ff108510ff108510ff108510ff108510ff108510ff108510ff0f850fff0d840dff0e840eff108510ff108510ff108510ff108510ff108510ff078007ff3c9b3cff
6fb56fff007a00ff108510ff108510ff108510ff108510ff0f850fff148714ff2f952fff178917ff168816ff0e840eff108510ff108510ff108510ff108510ff068006ff3a9a3aff
94c894ff057f05ff0d830dff108510ff108510ff108510ff098209ff359835ff7aba7aff62af62ff5cac5cff027e02ff108510ff108510ff108510ff108510ff057f05ff65b065ff
edf6edff67b167ff027e02ff098109ff0b830bff057f05ff198919ffc4e1c4ffa7d2a7ff77ba77ffdaecdaff309530ff037e03ff0b830bff0b820bff027e02ff3c9c3cffe0efe0ff
fffffffff3f9f3ffa9d4a9ff5ead5eff47a147ff7fbe7fffd0e7d0ffffffffffa9d4a9ff6eb56effffffffffe1f0e1ff81bf81ff4aa24aff4aa34aff8cc48cffe3f0e3ffffffffff
fffffffffffffffffffffffffafcfafff7fbf7ffffffffffffffffffffffffff83c083ff67b167fffffffffffffffffffffffffff7fbf7fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0efe0ff2f952fff259025ffcee6ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff4f9f4fff7fbf7ffe2f0e2ffa9d3a9ff4aa34aff088108ff037f03ff409d40ffadd5adffdfeedffff4f9f4fffbfdfbffffffffffffffffffffffffff
ffffffffffffffff80be80ff3b9b3bff419e41ff319631ff037e03ff007a00ff078007ff078107ff007a00ff078007ff2e942eff3f9d3fff419e41ff6fb66ffff1f8f1ffffffffff
ffffffffffffffffb4d9b4ff97ca97ff98ca98ff9bcc9bffa2cfa2ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa2cfa2ff9bcc9bff99cb99ff96c996ffa5d1a5fff6faf6ffffffffff
i$6d               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfffff8f7fffff4f3fffffcfcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe6e3ffffbab1ffff7e6fffff4f3affff230affffcdc7ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffbfbffffaea4ffff432dffff1a00ffff442effff5d4affff614effffdedaffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff8274ffff290fffff3b24ffff9a8effffd5d0ffffedebfffffafaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9c90ffff2005ffff5a47ffffd3cfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffdfdbffff331affff4832ffffd6d1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff8171ffff2a11ffff9488ffffffffffffe1deffffd1cbffffd2ccffffe4e1ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffefedffff412bffff442effffb8afffff8c7effff371fffff1400ffff1b00ffff4c37ffff978affffedeaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3f27ffff533fffff4d37ffffa096ffffafa5ffff897bffff3a22ffff2207ffff7767fffff6f5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffa89effff371fffff2f16ffff7a6affffe9e6fffffffffffffffffffffefeffffbdb5ffff422bffff2c13ffffb8b1ffffffffffffffffffffffffff
ffffffffffffffffffffffffff968affff2d14ffff4934ffffe5e2ffffffffffffffffffffffffffffffffffffffffffff8273ffff240affff7363ffffffffffffffffffffffffff
ffffffffffffffffffffffffffa298ffff2e16ffff5a46ffffffffffffffffffffffffffffffffffffffffffffffffffffaea3ffff331bffff4a35ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc5beffff3821ffff523efffff8f7ffffffffffffffffffffffffffffffffffffffffffffaaa0ffff3119ffff513cffffffffffffffffffffffffff
ffffffffffffffffffffffffffe5e2ffff3f29ffff3d26ffffc8c1ffffffffffffffffffffffffffffffffffffffffffff897bffff270effff8577ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff8678ffff270cffff7463fffffcfbffffffffffffffffffffffffffffddd9ffff4f39ffff280fffffd0caffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffedeaffff4d39ffff2a10ffff6a58ffffcdc7ffffe4e1ffffb5acffff4f3affff280fffff8779fffffcfcffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffdbd7ffff7666ffff3f29ffff250bffff1a00ffff2a11ffff432dffff9082fffff8f7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffffa69cffff978bffffaca2ffffd8d4ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffff
ffffffffffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffff
ffffffffffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffff
fffaf9ffffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6ff
ffe1deffff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bff
ffffffffffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcff
ffffffffffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffff
ffffffffffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffff
ffffffffffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffff
i$Th               18  39 
fffffffffffffffffffffffffefefffff2f2fdfff7f7fefffffffffffffffffffffffffffffffffffffffffffbfbfefff2f2fdfff1f1fdfff4f4fefffdfeffffffffffffffffffff
ffffffffffffffffffffffffc7c8f8ff0304dfff6768ecfffafafefffffffffffffffffffefeffffb3b3f6ff5152e9ff0000deff0000ddff0e0fe1ff6d6eedffe0e1fbffffffffff
ffffffffffffffffe5e5fcff6f6feeff0000ddff696bedfffafafeffffffffffffffffffa0a1f4ff0709dfff0607e0ff5657eaff6263ebff3c3de7ff0000deff2c2ee4ffe4e4fbff
b1b2f5ff7070edff3b3ce7ff080ae0ff0000deff696bedfffafafeffffffffffd3d4f9ff1517e2ff0304dfffa7a8f4ffffffffffffffffffececfcff5253e9ff0000deff5c5deaff
6667ecff0000dcff0000deff0103dfff0000deff696bedfffbfbffffffffffff8e90f1ff0000dcff5052eaffffffffffffffffffffffffffffffffffd1d2faff0a0be0ff191be3ff
f1f1fdffe6e6fcffe6e6fcff7677eeff0000dcff696bedfffefefffff8f7feff5759eaff0000dcff9596f2ffffffffffffffffffffffffffffffffffffffffff2728e4ff0406e0ff
ffffffffffffffffffffffff8485f0ff0000dcff696bedffffffffffededfdff3a3be6ff0000ddffb2b3f6ffffffffffffffffffffffffffffffffffffffffff4648e8ff0000deff
ffffffffffffffffffffffff8283f0ff0000dcff696bedffffffffffe4e5fcff2123e3ff0000ddffbebef7ffffffffffffffffffffffffffffffffffffffffff5e60ebff0000ddff
ffffffffffffffffffffffff8283f0ff0000dcff696bedffffffffffdfe0fbff1113e1ff0203dfffc5c5f8ffffffffffffffffffffffffffffffffffffffffff7172edff0000ddff
ffffffffffffffffffffffff8283f0ff0000dcff696bedffffffffffdedffbff0c0ee1ff0305e0ffc8c8f8ffffffffffffffffffffffffffffffffffffffffff6f70edff0000ddff
ffffffffffffffffffffffff8283f0ff0000dcff696bedffffffffffe2e3fbff181ae2ff0204dfffc6c7f8ffffffffffffffffffffffffffffffffffffffffff6c6dedff0000ddff
ffffffffffffffffffffffff8283f0ff0000dcff696bedffffffffffe8e9fcff2c2de5ff0000ddffb9baf7ffffffffffffffffffffffffffffffffffffffffff5052e9ff0000ddff
ffffffffffffffffffffffff8283f0ff0000dcff696bedfffffffffff3f2fdff4748e8ff0000ddffa6a7f4ffffffffffffffffffffffffffffffffffffffffff393ae6ff0000deff
ffffffffffffffffffffffff8283f0ff0000dcff696bedfffcfcfffffdfdffff7375eeff0000dcff7779eefffcfcffffffffffffffffffffffffffffecedfdff1618e2ff0f10e1ff
ffffffffffffffffffffffff8586f0ff0000dcff6b6dedffffffffffffffffffbbbcf7ff0405dfff1d1fe3ffeaeafcffffffffffffffffffffffffff989af2ff0000dfff3132e6ff
ececfdffdcdcfbffddddfbff7172eeff0101ddff5c5eebffd9d9faffdcdcfbffdfdffbff5b5debff0000ddff4042e7ffc9c9f8ffdedefbff9b9cf2ff0a0ce0ff0305e0ff9c9cf2ff
6263ecff0000dbff0000dcff0000ddff0000ddff0000ddff0000dcff0000daff5052e9fff1f1feff5354eaff0e0fe1ff0000dcff0000dcff0000deff191be3ff8788f0ffffffffff
b8b9f6ff7e7eefff8081efff8182efff8182efff8182efff8182efff7e7eeeffabacf5fffffffffff8f7feffbcbcf6ff7f80efff7e7fefff8e8ff1ffc6c6f8ffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafefffffffffffffffffffffffffffffffffffffffffffffffffffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$3h               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffff8f8fefff2f2fdfff2f2fdfff5f5fefffefeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffbfbffffd1d2faff7b7cefff3537e6ff0000deff0000deff2021e3ff7273eeffdbdcfbffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffeeeefdff3739e6ff0000deff4042e7ff6162ebff5f60ebff1b1de3ff0000ddff2d2fe5ffdddefbffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffbfbffffa2a3f4ff9fa0f3fff4f5feffffffffffffffffffc5c6f8ff2829e4ff0000deff6869ecffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8182efff0000dfff2224e3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9697f1ff0103dfff292be5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6668ecff0000ddff7374eeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffededfdff9899f2ff0f10e1ff1415e1ffd7d7faffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdddefbff7778eeff6565ecff3f41e7ff0406dfff3535e5ffc1c2f7ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffc3c4f7ff0b0de1ff0000dcff0000deff0000deff2e30e5ffacadf5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfbfeffeeeefdffececfdffcacaf9ff7f80efff2123e3ff0404e0ff7374edfffcfcffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc4c4f7ff1c1ee3ff0507e0ffa8a8f5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6769ecff0000deff5556eaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6364ecff0000deff5153e9ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefeffffffffffffffffffffffffffffffffffffffffffffd4d4faff2224e4ff0102dfff9898f2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd8d8faff3b3ce6ff7475eeffbcbcf7ffdddefbffdadafbff9b9cf2ff2526e3ff0000deff3d3fe7fff8f8feffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd1d2f9ff3131e5ff0e10e1ff0000ddff0000dcff0000dcff0001dfff1d1fe3ff6668ecffe0e1fcffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff1f1feffb7b8f6ff8f8ff1ff7f80efff8283efffa3a4f3ffcdcef9ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffefefffffaf9fefffffffffffffffffffffffffffffffffffffffffffffffffffbfbfefffefdffffffffffffffffffffffffffffffffffff
ffffffffffffffffccccf8ff7d7eefff5b5debff5657eaff6364ecffa3a4f4ffffffffffffffffffc3c4f8ff6f71edff5657eaff595aebff7778eeffb1b2f5fffeffffffffffffff
ffffffff9496f2ff1d1fe2ff0000deff0000deff0000deff0000deff0608e0ff5658eaff797aefff1717e2ff0000deff0000deff0000deff0000deff0f10e1ff6062ebfff4f4fdff
c3c3f8ff191be3ff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0204dfff0001dfff0305e0ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff0002dfff9495f2ff
8080efff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff3b3de7ff
5e5febff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2628e4ff
6667ecff0000ddff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff2c2de5ff
a9a9f5ff0c0de0ff0204dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0103dfff6566ecff
fbfbfeff6364ecff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff1c1ee3ffe2e3fbff
ffffffffedeefdff4041e7ff0000deff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0001dfff0d0de1ffb6b7f6ffffffffff
ffffffffffffffffd3d4f9ff2f31e5ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0001dfff0e0fe0ffa3a4f4ffffffffffffffffff
ffffffffffffffffffffffffd6d6f9ff3132e5ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0102dfff0d0ee0ffa2a4f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd9dafaff4141e6ff0000dfff0507e0ff0608e0ff0608e0ff0608e0ff0103e0ff0c0de0ffa1a3f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffededfcff484ae8ff0000deff0608e0ff0608e0ff0103dfff0c0ee1ffa2a3f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffdcdcfbff3133e5ff0000dfff0204dfff0909e0ffa0a2f3ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffd1d1f9ff1b1ee3ff0000dfff8587f0fffffeffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffa6a6f4ff5b5cebfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefffff1f2fdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Td               18  39 
fffffffffffffffffffffffffffffffffff9f8fffff5f4fffffdfcfffffffffffffffffffffffffffffffffffffffffffff7f6fffff4f2fffff4f3fffff9f9ffffffffffffffffff
ffffffffffffffffffffffffffffffffff6956ffff432dffffd2cdffffffffffffffffffffffffffffe9e7ffff988cffff432cffff2309ffff2910ffff5e4bffffbcb5fffffdfcff
ffffffffffffffffffffffffffc7c1ffff3c25ffff422bffffd2cdffffffffffffffffffffeceaffff624fffff240affff5c49ffff8070ffff7c6cffff3921ffff280effffaca2ff
fff1efffff9487ffff7b6bffff432dffff2f16ffff452fffffd2cdffffffffffffffffffff8475ffff2208ffff6d5cffffebe9ffffffffffffffffffffb6adffff3d26ffff3a22ff
ffe3e0ffff2b12ffff2105ffff2c13ffff3118ffff452fffffd2cdffffffffffffedebffff412affff3b24ffffc6c0ffffffffffffffffffffffffffffffffffff8475ffff1a00ff
fffcfcffffeceaffffebe8ffffcec9ffff3f29ffff412bffffd2cdffffffffffffbeb6ffff341cffff533efffff2f0ffffffffffffffffffffffffffffffffffffaea5ffff250bff
ffffffffffffffffffffffffffe2dfffff412bffff412bffffd2cdffffffffffffa298ffff2e15ffff5c49ffffffffffffffffffffffffffffffffffffffffffffbdb6ffff351dff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffff8e81ffff2a10ffff6351ffffffffffffffffffffffffffffffffffffffffffffcac4ffff402aff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffff8172ffff2910ffff7261ffffffffffffffffffffffffffffffffffffffffffffd3cdffff4832ff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffff7d6dffff2910ffff7666ffffffffffffffffffffffffffffffffffffffffffffd2ccffff4731ff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffff8779ffff2a11ffff7363ffffffffffffffffffffffffffffffffffffffffffffd1cbffff4630ff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffff978bffff2b12ffff5f4cffffffffffffffffffffffffffffffffffffffffffffc3bcffff3a23ff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffffaea5ffff3119ffff5945fffffcfcffffffffffffffffffffffffffffffffffffb8b1ffff2e15ff
ffffffffffffffffffffffffffdfdcffff412bffff412bffffd2cdffffffffffffd7d2ffff3a22ffff4933ffffddd8ffffffffffffffffffffffffffffffffffff9b8fffff2006ff
ffffffffffffffffffffffffffe3e1ffff412bffff412bffffd6d1ffffffffffffffffffff6451ffff2b12ffff9e93ffffffffffffffffffffffffffffebe9ffff604dffff2006ff
fffcfbffffe5e2ffffe3e0ffffc8c2ffff3f29ffff3f29ffffbdb5ffffe4e0ffffe4e1ffffbeb5ffff341cffff371fffffa69dffffe4e1ffffdedaffff6856ffff2209ffff6a58ff
ffe3dfffff270dffff1900ffff1d02ffff2309ffff2309ffff1d02ffff1b00ffff1600ffffe9e6ffffb2a9ffff4d38ffff280effff1900ffff1c01ffff3118ffff6754ffffe1ddff
fff2f1ffff9e92ffff978bffff988cffff988cffff988cffff988cffff988cffff9689fffff5f4ffffffffffffe3e0ffffa99effff9488ffff998dffffb7affffff3f1ffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffedecffff9487fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8779ffff3119ffffb7afffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbab2ffff371effff2b12ffff4b35ffffdcd8ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd7ffff4631ffff2c13ffff351dffff2a11ffff6c5bfffff0efffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffefedffff6553ffff2b12ffff351dffff351dffff331bffff280fffff968affffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffefdffff8a7cffff2d14ffff351dffff351dffff351dffff351dffff331bffff3018ffffada4ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9f93ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff331bffff3e27ffffc1baffffffffffffffffffffffffff
ffffffffffffffffffffffffffaaa1ffff3119ffff331bffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff462fffffd2ccffffffffffffffffff
ffffffffffffffffffbfb7ffff3a23ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4f3affffcfcaffffffffff
fffaf9ffffb9b0ffff402affff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2c13ffff4f3affffcdc6ff
ffe1deffff4730ffff2910ffff351effff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff260cffff6c5bff
ffffffffffd5cfffff4a34ffff2b11ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff341cffff2a11ffff5a47ffffe0dcff
ffffffffffffffffffd6d1ffff4b35ffff2f16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2e16ffff5a48ffffe3e0ffffffffff
ffffffffffffffffffffffffffc6bfffff3d27ffff3118ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3018ffff5a47ffffe6e4ffffffffffffffffff
ffffffffffffffffffffffffffffffffffb8b0ffff361effff331bffff351dffff351dffff351dffff351dffff361effff2e16ffff462fffffd1ccffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff9488ffff2f16ffff341cffff351dffff351dffff361effff2f17ffff3820ffffc9c2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffbfbffff7a6bffff2911ffff351dffff351dffff321affff2e16ffffa69bffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffff1efffff5c49ffff2910ffff341cffff2d14ffff7f6ffffff8f7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcdc7ffff432dffff290fffff5c48fffff1f0ffffffffffffffffffffffffffffffffffffffffffffffffff
i$8c               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff7fbf7fff2f8f2fff3f9f3fff8fcf8ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffecf6ecff88c288ff2c932cff007b00ff068006ff3f9d3fffa1cfa1fff3f9f3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe7f3e7ff4da54dff007d00ff439f43ff68b268ff62af62ff259025ff007c00ff6db46dfff5faf5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff7dbd7dff007c00ff5fad5fffeef7eeffffffffffffffffffcee6ceff1d8b1dff0f850fffb2d8b2ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe8f4e8ff409d40ff007b00ffdeeedeffffffffffffffffffffffffffffffffff7bbc7bff007900ff7dbd7dffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe1f0e1ff3b9a3bff007c00ffeaf5eaffffffffffffffffffffffffffffffffff89c389ff007a00ff83c083ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffafcfaff61ae61ff007a00ff7ebe7efffffffffffffffffffffffffff0f7f0ff289128ff1b8b1bffc8e3c8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcde6cdff299229ff0a820aff76b976ffc7e2c7ffc7e2c7ff51a651ff0e830eff8ac38affffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdaecdaff369836ff007900ff198a19ff1f8c1fff0a820aff97ca97ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe6f3e6ff71b671ff1b8a1bff3b9b3bff69b369ff1e8c1eff0a820aff3e9c3effa3d0a3ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe3f1e3ff49a249ff047f04ff89c389fff4f9f4fffcfdfcffe7f3e7ff7ebd7eff0a820aff138713ffa2cfa2fffefefeffffffffffffffffffffffffff
fffffffffffffffffcfefcff78ba78ff007500ff7cbc7cffffffffffffffffffffffffffffffffffffffffff7fbe7fff007c00ff3f9d3fffe5f2e5ffffffffffffffffffffffffff
ffffffffffffffffe8f3e8ff3b9b3bff007800ffbcddbcffffffffffffffffffffffffffffffffffffffffffe1f0e1ff047f04ff228e22ffcbe5cbffffffffffffffffffffffffff
ffffffffffffffffe2f0e2ff2f952fff007900ffbadcbaffffffffffffffffffffffffffffffffffffffffffe5f2e5ff037e03ff2a932affd2e8d2ffffffffffffffffffffffffff
fffffffffffffffff4faf4ff5dac5dff007900ff62af62fff8fbf8ffffffffffffffffffffffffffffffffff96ca96ff007a00ff54a854fff4f9f4ffffffffffffffffffffffffff
ffffffffffffffffffffffffc3e0c3ff1f8d1fff007d00ff58a958ffcce5ccffdfefdfffd4e9d4ff77b977ff078107ff289128ffbedebeffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffbedebeff43a043ff138713ff007800ff007500ff007800ff148714ff439f43ffc5e1c5ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff1f8f1ffb5dab5ff8cc48cff83c083ff8fc58fffb7dab7fff0f7f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1f0e1ffd6ead6ffd6ead6ffe2f0e2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff7fbf7ff89c389ff228f22ff017d01ff017d01ff238e23ff8bc38bfff7fbf7ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff93c793ff037e03ff0a820aff108510ff108510ff0a820aff037f03ff94c894ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f8f0ff3b9b3bff067f06ff108510ff108510ff108510ff108510ff067f06ff3d9c3dfff1f8f1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffeff7efff359835ff068006ff108510ff108510ff108510ff108510ff068006ff379837fff0f7f0ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6faf6fff7fbf7ff51a751ff078107ff108510ff108510ff108510ff108510ff078007ff53a753fff8fbf8fff6faf6ffffffffffffffffffffffffff
ffffffffecf5ecff9bcc9bff61ae61ff67b267ff4ba44bff118511ff0f840fff108510ff108510ff0f840fff118511ff4ca44cff67b267ff61ae61ff9bcc9bfff1f8f1ffffffffff
ebf5ebff55a955ff007d00ff037e03ff027e02ff088108ff108510ff108510ff108510ff108510ff108510ff108510ff088108ff027e02ff037e03ff007d00ff5fad5ffff2f8f2ff
80be80ff037f03ff0d840dff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0d830dff057f05ff8fc68fff
51a651ff017e01ff108510ff108510ff108510ff108510ff108510ff0f850fff0f850fff0c830cff0e840eff108510ff108510ff108510ff108510ff108510ff017e01ff56a956ff
50a650ff017d01ff108510ff108510ff108510ff108510ff0e830eff1a8a1aff2c932cff198a19ff138713ff0f840fff108510ff108510ff108510ff108510ff017d01ff54a854ff
7abb7aff037e03ff0f850fff108510ff108510ff108510ff078007ff4aa34aff6ab46aff73b773ff44a044ff068006ff108510ff108510ff108510ff0f840fff057f05ff7dbc7dff
e7f3e7ff54a854ff017e01ff0a820aff0b820bff037f03ff249024ffd4e9d4ff8bc48bff92c892ffcfe6cfff238f23ff057f05ff0b830bff0a820aff027e02ff4da44dffe7f3e7ff
ffffffffeef6eeffa1cfa1ff56a956ff4ba44bff86c186ffd9ecd9ffffffffff8ac38aff8bc48bffffffffffd7ebd7ff79ba79ff46a146ff51a651ff95c995ffeaf4eaffffffffff
fffffffffffffffffffffffff9fcf9fff7fbf7ffffffffffffffffffffffffff69b269ff81be81fffffffffffffffffffefefefff6fbf6fff8fbf8ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffcee6ceff228e22ff329632ffdfefdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff3f9f3fff7faf7ffddeeddffa3d0a3ff3e9c3eff088108ff057f05ff4da54dffb3d8b3ffe3f0e3fff4f9f4fffefefeffffffffffffffffffffffffff
fffffffffafcfaff71b771ff3b9b3bff419e41ff2c932cff007c00ff007b00ff078007ff078007ff007900ff0b820bff319631ff3f9d3fff449f44ff7bbb7bfffafdfaffffffffff
fffffffffcfdfcffadd5adff97c997ff98ca98ff9ccc9cffa3d0a3ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa1cfa1ff9bcc9bff99cb99ff96c996ffacd4acfffcfdfcffffffffff
i$9d               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9f8fffff5f3fffff6f4fffffdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff1efffffa195ffff5540ffff2c12ffff371fffff8071ffffd1cdffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffe4e0ffff6351ffff2309ffff5f4cffff7f70ffff6e5cffff2a11ffff3f28ffffcdc7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffaf9ffff6351ffff270dffff8374ffffefeefffffffffffffefeffff9f94ffff270effff513cffffebe9ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffbeb6ffff2c13ffff533efffff4f2fffffffffffffffffffffffffffff9f8ffff6958ffff1e04ffffa99effffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffff8c7dffff2e15ffff8374ffffffffffffffffffffffffffffffffffffffffffffa298ffff2005ffff7b6bfffffdfcffffffffffffffffffffffffff
ffffffffffffffffffffffffff614effff2910ffff9487ffffffffffffffffffffffffffffffffffffffffffffc1baffff3820ffff5743fffffaf9ffffffffffffffffffffffffff
ffffffffffffffffffffffffff7362ffff280fffff8172ffffffffffffffffffffffffffffffffffffffffffffc3bcffff3820ffff331cfffff7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffa89effff2e15ffff503cffffebe9ffffffffffffffffffffffffffffffffffff9083ffff2c13ffff361ffffff7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffedebffff523effff2a11ffff715fffffe4e1ffffffffffffe7e4ffff8d7fffff351dffff3017ffff5c48fffffaf9ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffccc6ffff4a34ffff321affff361effff3b24ffff3d26ffff5b47ffff7b6bffff371fffff7968fffffdfcffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff0eeffff9f93ffff6957ffff6c5bffff9c91ffffe9e6ffff988bffff1900ffff9c91ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe6e3ffff4530ffff3921ffffdcd8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5f3ffff7363ffff1e04ffff9588fffffdfdffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeceaffff7463ffff2309ffff5c48ffffe7e5ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffedeaffffc9c3ffffaea5ffff8b7dffff3f28ffff2a11ffff604cffffdedbffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffb9b1ffff1900ffff280fffff3d26ffff5b47ffff9f93fffff4f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffdfdbffff978affffa99fffffc7c0fffff2f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f6ffff9487fffff3f2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff9c90ffff2d15ffffa59affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffccc6ffff3e27ffff2b12ffff4029ffffcfcaffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffe9e6ffff533fffff2a11ffff351dffff2d14ffff5c49ffffe7e4ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffff8f7ffff7463ffff2c13ffff351dffff351dffff341cffff280effff8072fffffefeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffff9b8effff3017ffff351dffff351dffff351dffff351dffff331bffff2b12ffff9b8fffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffb1a7ffff3219ffff341bffff351dffff351dffff351dffff351dffff351dffff331bffff3820ffffb0a8ffffffffffffffffffffffffffffffffff
ffffffffffffffffffbdb5ffff371effff3018ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3219ffff3c26ffffc4bdffffffffffffffffffffffffff
ffffffffffcec8ffff432dffff2b12ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2f17ffff452fffffc2bbffffffffffffffffff
ffc7c1ffff4a35ffff2e15ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2d15ffff4630ffffbfb7fffffbfaff
ff5540ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351cffff260cffff5d49ffffe7e4ff
ffe0ddffff5743ffff2910ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff2c13ffff4e39ffffd4cfffffffffff
ffffffffffe2dfffff5843ffff2d14ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3017ffff4e39ffffd9d5ffffffffffffffffff
ffffffffffffffffffd6d0ffff4732ffff2e16ffff351dffff351dffff351dffff351dffff351dffff351dffff351dffff3118ffff4f3affffd9d5ffffffffffffffffffffffffff
ffffffffffffffffffffffffffc7c1ffff3e27ffff3119ffff351dffff351dffff351dffff351dffff361effff2f16ffff3c25ffffc3bbffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffa59affff331bffff331bffff351dffff351dffff351dffff3018ffff3018ffffb7aeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffff8c7fffff2b12ffff351dffff351dffff331bffff2c13ffff9184fffffefeffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffff8f7ffff6d5cffff270dffff351dffff2e15ffff6c5cfffff2f0ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffdbd8ffff4e39ffff2910ffff4e39ffffe5e3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
i$Ts               18  39 
fffffffffffffffffffffffffffffffff8f8f8fff2f2f2fffbfbfbfffffffffffffffffffffffffffffffffffffffffff5f5f5fff1f1f1fff1f1f1fff7f7f7ffffffffffffffffff
ffffffffffffffffffffffffffffffff4f4f4fff0b0b0bffbebebeffffffffffffffffffffffffffe9e9e9ff858585ff181818ff000000ff000000ff2f2f2fffa3a3a3fffafafaff
ffffffffffffffffffffffffc0c0c0ff131313ff090909ffbebebeffffffffffffffffffedededff424242ff000000ff2c2c2cff5e5e5eff5c5c5cff0d0d0dff000000ff898989ff
f3f3f3ff7c7c7cff5d5d5dff151515ff000000ff0f0f0fffbebebeffffffffffffffffff6e6e6eff000000ff3b3b3bffe0e0e0ffffffffffffffffffacacacff181818ff0e0e0eff
e8e8e8ff000000ff000000ff000000ff000000ff0f0f0fffbebebeffffffffffeeeeeeff191919ff050505ffabababffffffffffffffffffffffffffffffffff6e6e6eff000000ff
fdfdfdffe8e8e8ffe6e6e6ffc9c9c9ff171717ff090909ffbebebeffffffffffb7b7b7ff050505ff191919ffe8e8e8ffffffffffffffffffffffffffffffffffa7a7a7ff000000ff
ffffffffffffffffffffffffe3e3e3ff1a1a1aff080808ffbebebeffffffffff969696ff000000ff232323ffffffffffffffffffffffffffffffffffffffffffb8b8b8ff030303ff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffff7e7e7eff000000ff2d2d2dffffffffffffffffffffffffffffffffffffffffffc6c6c6ff141414ff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffff6e6e6eff000000ff404040ffffffffffffffffffffffffffffffffffffffffffd0d0d0ff1f1f1fff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffff696969ff000000ff464646ffffffffffffffffffffffffffffffffffffffffffcfcfcfff1d1d1dff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffff757575ff000000ff414141ffffffffffffffffffffffffffffffffffffffffffcececeff1c1c1cff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffff888888ff000000ff272727ffffffffffffffffffffffffffffffffffffffffffbebebeff090909ff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffffa4a4a4ff010101ff202020fff9f9f9ffffffffffffffffffffffffffffffffffb3b3b3ff000000ff
ffffffffffffffffffffffffdfdfdfff1a1a1aff080808ffbebebeffffffffffd5d5d5ff0e0e0eff0e0e0effcacacaffffffffffffffffffffffffffffffffff8e8e8eff000000ff
ffffffffffffffffffffffffe5e5e5ff1b1b1bff080808ffc3c3c3ffffffffffffffffff464646ff000000ff767676ffffffffffffffffffffffffffecececff3f3f3fff000000ff
fcfcfcffdfdfdfffdcdcdcffc1c1c1ff161616ff070707ffa4a4a4ffdcdcdcffddddddffb3b3b3ff090909ff000000ff868686ffdcdcdcffdadadaff4b4b4bff000000ff3b3b3bff
e7e7e7ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ffd8d8d8ffa9a9a9ff282828ff000000ff000000ff000000ff000000ff373737ffd2d2d2ff
f4f4f4ff888888ff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7eff7e7e7effecececffffffffffdfdfdfff959595ff7e7e7eff7e7e7effa1a1a1ffebebebffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff1f1f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffff
ffffffffffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffff
ffffffffffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffff
ffffffffe8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ff
ffffffffc5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffbbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffc1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cff
ffffffffe7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ff
ffffffffffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffff
ffffffffffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffff
i$7s               18  39 
fffffffffffffffffffffffff5f5f5fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff1f1f1fff9f9f9ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff434343ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff030303ff8d8d8dffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff777777ff3c3c3cff3e3e3eff3e3e3eff3e3e3eff3e3e3eff414141ff3c3c3cff0d0d0dff000000ffa5a5a5ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff6f6f6fff0f0f0fff2f2f2fff2f2f2fff2f2f2fff2f2f2fff5f5f5ffc0c0c0ff0f0f0fff2a2a2affeaeaeaffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff676767ff000000ff898989ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0e0e0ff030303ff292929ffdadadaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff737373ff000000ff747474ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe1e1e1ff0c0c0cff0e0e0effcfcfcfffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6f6f6fff000000ff616161ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffddddddff212121ff020202ffcececeffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff858585ff000000ff4b4b4bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffe0e0e0ff282828ff050505ffc0c0c0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff939393ff000000ff454545fffafafaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffeeeeeeff323232ff000000ffacacacffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa5a5a5ff000000ff3f3f3ffff0f0f0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff5f5f5ff333333ff010101ff9d9d9dffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff919191ff000000ff2b2b2bffe6e6e6ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffaaaaaaff7e7e7effb3b3b3ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff5f5f5fffdfdfdfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff828282ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc0c0c0ff141414ff000000ff020202ff898989ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfdfdfff242424ff000000ff000000ff000000ff030303ffa5a5a5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff383838ff000000ff000000ff000000ff000000ff000000ff0a0a0affa7a7a7ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff7f7f7ff5d5d5dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff0b0b0bffb1b1b1ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffff797979ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff1d1d1dffc9c9c9ffffffffffffffffffffffffff
ffffffffffffffffa4a4a4ff070707ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff262626ffdbdbdbffffffffffffffffff
ffffffffd3d3d3ff141414ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff4b4b4bfff9f9f9ffffffffff
f3f3f3ff525252ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff9e9e9effffffffff
dcdcdcff060606ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffff
d6d6d6ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6d6d6dffffffffff
dadadaff010101ff000000ff000000ff000000ff000000ff000000ff000000ff494949ff1b1b1bff000000ff000000ff000000ff000000ff000000ff000000ff7d7d7dffffffffff
f2f2f2ff4b4b4bff000000ff000000ff000000ff000000ff000000ff6e6e6eff888888ffbababaff464646ff000000ff000000ff000000ff000000ff222222ffd9d9d9ffffffffff
ffffffffeaeaeaff7c7c7cff252525ff1d1d1dff3c3c3cffa9a9a9ffdfdfdfff2e2e2effe2e2e2fff9f9f9ff969696ff363636ff2e2e2eff636363ffcfcfcfffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff848484ff000000ff686868ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffadadadff1a1a1aff000000ff060606ff8a8a8afffcfcfcffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffff8f8f8ffe1e1e1ffe0e0e0ffacacacff656565ff0f0f0fff000000ff000000ff000000ff000000ff484848ff909090ffbcbcbcffd9d9d9ffddddddffffffffffffffffff
ffffffffcfcfcfff2f2f2fff232323ff111111ff000000ff000000ff050505ff050505ff050505ff030303ff000000ff070707ff161616ff202020ff808080ffffffffffffffffff
i$3c               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffff8fcf8fff2f9f2fff2f8f2fff6faf6ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffafdfaffcfe6cfff7bbb7bff3b9b3bff027e02ff017d01ff2d942dff7dbd7dffe1f0e1ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffeaf4eaff369836ff088108ff4aa34aff68b268ff63b063ff1f8c1fff007a00ff3e9c3effe5f2e5ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff9fcf9ffa1cfa1ffa9d3a9fff7fbf7ffffffffffffffffffbfdebfff299229ff007c00ff79bb79ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7abb7aff027d02ff379937ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8fc68fff088108ff3e9d3effffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff61ae61ff007b00ff84c084ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeaf5eaff94c894ff118511ff228e22ffe0efe0ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffd9ecd9ff78ba78ff6ab36aff449f44ff0d830dff3f9d3fffcae4caffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffbbdcbbff0e840eff007800ff037e03ff078007ff3d9b3dffb5d9b5ffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffafdfaffeef6eeffedf6edffc8e3c8ff7fbe7fff269026ff0e840eff83c083fffcfefcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffc0dfc0ff1e8c1eff148714ffb5d9b5ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff63b063ff017d01ff6ab36affffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff5fad5fff007c00ff66b166ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfe7cfff248f24ff0e840effa7d2a7ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffd1e8d1ff409d40ff7ebd7effc1dfc1ffdfefdfffd9ecd9ff9acb9aff279127ff007c00ff4fa54ffffcfefcffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffcbe5cbff309530ff158815ff007a00ff007500ff007600ff0c830cff289228ff73b773ffe6f3e6ffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff0f7f0ffb7dbb7ff92c792ff84c084ff88c288ffaad3aaffd2e8d2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5f1e5ffd6ead6ffd6ead6ffdfefdffffdfefdffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffbfdfbff99ca99ff2a922aff027d02ff007d00ff1c8c1cff7cbc7cfff2f8f2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffa9d3a9ff098109ff078007ff108510ff108510ff0c830cff007c00ff7ebd7effffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff8fbf8ff52a752ff017d01ff108510ff108510ff108510ff108510ff0a820aff299129ffe8f3e8ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff9fcf9ff49a249ff027d02ff108510ff108510ff108510ff108510ff0b820bff248f24ffe2f0e2ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff8fcf8fff7fbf7ff68b268ff057f05ff0f850fff108510ff108510ff108510ff0b820bff3e9d3efff0f8f0fff4f9f4ffffffffffffffffffffffffff
fffffffff2f8f2ffa3d1a3ff66b166ff65b065ff54a854ff148714ff0f840fff108510ff108510ff108510ff108510ff439f43ff69b369ff5cac5cff93c893ffeaf5eaffffffffff
f3f9f3ff68b268ff027e02ff017d01ff037e03ff068006ff108510ff108510ff108510ff108510ff108510ff108510ff098109ff027e02ff037e03ff007c00ff4ca44cffeaf4eaff
99cb99ff057f05ff0b820bff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff108510ff0f840fff037e03ff78ba78ff
6fb56fff007a00ff108510ff108510ff108510ff108510ff108510ff108510ff0f850fff0d840dff0e840eff108510ff108510ff108510ff108510ff108510ff078007ff3c9b3cff
6fb56fff007a00ff108510ff108510ff108510ff108510ff0f850fff148714ff2f952fff178917ff168816ff0e840eff108510ff108510ff108510ff108510ff068006ff3a9a3aff
94c894ff057f05ff0d830dff108510ff108510ff108510ff098209ff359835ff7aba7aff62af62ff5cac5cff027e02ff108510ff108510ff108510ff108510ff057f05ff65b065ff
edf6edff67b167ff027e02ff098109ff0b830bff057f05ff198919ffc4e1c4ffa7d2a7ff77ba77ffdaecdaff309530ff037e03ff0b830bff0b820bff027e02ff3c9c3cffe0efe0ff
fffffffff3f9f3ffa9d4a9ff5ead5eff47a147ff7fbe7fffd0e7d0ffffffffffa9d4a9ff6eb56effffffffffe1f0e1ff81bf81ff4aa24aff4aa34aff8cc48cffe3f0e3ffffffffff
fffffffffffffffffffffffffafcfafff7fbf7ffffffffffffffffffffffffff83c083ff67b167fffffffffffffffffffffffffff7fbf7fff7fbf7ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe0efe0ff2f952fff259025ffcee6ceffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffff4f9f4fff7fbf7ffe2f0e2ffa9d3a9ff4aa34aff088108ff037f03ff409d40ffadd5adffdfeedffff4f9f4fffbfdfbffffffffffffffffffffffffff
ffffffffffffffff80be80ff3b9b3bff419e41ff319631ff037e03ff007a00ff078007ff078107ff007a00ff078007ff2e942eff3f9d3fff419e41ff6fb66ffff1f8f1ffffffffff
ffffffffffffffffb4d9b4ff97ca97ff98ca98ff9bcc9bffa2cfa2ffa5d1a5ffa5d1a5ffa5d1a5ffa5d1a5ffa2cfa2ff9bcc9bff99cb99ff96c996ffa5d1a5fff6faf6ffffffffff
i$As               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff505050ff202020ff3b3b3bffe9e9e9ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffc2c2c2ff111111ff000000ff030303ffa2a2a2ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff878787ff000000ff474747ff000000ff5c5c5cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffff1f1f1ff464646ff000000ffb3b3b3ff000000ff292929ffe2e2e2ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffc8c8c8ff070707ff2c2c2cffe6e6e6ff303030ff040404ffa4a4a4ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffff8e8e8eff000000ff666666fffafafaff787878ff000000ff696969fffefefeffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffff4f4f4ff3e3e3eff000000ffa7a7a7ffffffffffb8b8b8ff000000ff272727ffe0e0e0ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffd5d5d5ff020202ff141414ffd0d0d0ffffffffffe5e5e5ff191919ff000000ffb3b3b3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff8a8a8aff000000ff575757fff8f8f8fffffffffff7f7f7ff5a5a5aff000000ff6a6a6afffafafaffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffff303030ff000000ff8f8f8fffffffffffffffffffffffffffaaaaaaff000000ff191919ffe9e9e9ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffd3d3d3ff010101ff121212ffadadadffe4e4e4ffe4e4e4ffe5e5e5ffb9b9b9ff151515ff000000ffb6b6b6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff7e7e7eff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff6e6e6effffffffffffffffffffffffff
ffffffffffffffffffffffffffffffff363636ff000000ff404040ff727272ff727272ff727272ff727272ff727272ff3f3f3fff000000ff1e1e1efff8f8f8ffffffffffffffffff
ffffffffffffffffffffffffd3d3d3ff121212ff060606ffc3c3c3ffffffffffffffffffffffffffffffffffffffffffc3c3c3ff101010ff000000ffb8b8b8ffffffffffffffffff
ffffffffffffffffffffffff878787ff010101ff292929fff9f9f9fffffffffffffffffffffffffffffffffffffffffff9f9f9ff363636ff000000ff636363ffffffffffffffffff
fffffffffffffffffcfcfcff4a4a4aff000000ff777777ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff878787ff000000ff252525fff4f4f4ffffffffff
ffffffffffffffffbfbfbfff020202ff000000ffc4c4c4ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcfcfff000000ff000000ff9e9e9effffffffff
ffffffffffffffffcfcfcfff7a7a7aff8e8e8effffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff959595ff7c7c7cffbbbbbbffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfdfdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe5e5e5ff5d5d5dfff1f1f1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffefefeff696969ff000000ff7f7f7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffaaaaaaff0b0b0bff000000ff0a0a0aff9e9e9effffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcdcdcdff161616ff000000ff000000ff000000ff0a0a0affbababaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe4e4e4ff262626ff000000ff000000ff000000ff000000ff000000ff151515ffbcbcbcffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3f3ff4a4a4aff000000ff000000ff000000ff000000ff000000ff000000ff000000ff161616ffc6c6c6ffffffffffffffffffffffffff
fffffffffffffffffffffffffcfcfcff636363ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff292929ffd9d9d9ffffffffffffffffff
ffffffffffffffffffffffff8d8d8dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff373737ffe5e5e5ffffffffff
ffffffffffffffffbebebeff090909ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff626262ffffffffff
ffffffffe8e8e8ff3d3d3dff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff060606ffb8b8b8ff
ffffffffc5c5c5ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffbbbbbbff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff000000ff909090ff
ffffffffc1c1c1ff000000ff000000ff000000ff000000ff000000ff000000ff010101ff4d4d4dff111111ff000000ff000000ff000000ff000000ff000000ff000000ff9c9c9cff
ffffffffe7e7e7ff373737ff000000ff000000ff000000ff000000ff000000ff7f7f7fff848484ffbababaff363636ff000000ff000000ff000000ff000000ff333333ffe7e7e7ff
ffffffffffffffffe0e0e0ff707070ff202020ff1d1d1dff434343ffb7b7b7ffd1d1d1ff2d2d2dfff8f8f8ffefefefff898989ff303030ff323232ff6d6d6dffdadadaffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff696969ff000000ff7e7e7effffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffff999999ff101010ff000000ff0f0f0fff9c9c9cffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffff5f5f5ffe0e0e0ffdfdfdfffa4a4a4ff5c5c5cff070707ff000000ff000000ff000000ff030303ff515151ff989898ffc0c0c0ffd9d9d9ffe1e1e1ffffffffff
ffffffffffffffffbababaff282828ff222222ff0e0e0eff000000ff010101ff050505ff050505ff050505ff020202ff000000ff0a0a0aff181818ff202020ff969696ffffffffff
i$2h               18  39 
fffffffffffffffffffffffffffffffffffffffffffffffff9f9fefff3f3fefff1f1fdfff4f5fefffcfcffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe8e9fcff999af3ff4243e7ff0608e0ff0000deff191be3ff6465ecffd6d7faffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe9e9fcff4042e7ff0000ddff2123e4ff4445e8ff4243e8ff1e20e3ff0000deff1c1de3ffc3c3f8ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffeaeafcff6263ebff7b7defffd2d3fafff5f5fefff1f0feffcacbf8ff4648e8ff0000deff3233e5ffebebfdffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffff3f3feffffffffffffffffffffffffffffffffffffffffffd0d0f9ff0f11e1ff0f11e1ffbabbf6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff2022e3ff0103dfff9394f1ffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffefeffff1e20e3ff0709e0ffa1a1f3ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff0d0fe1ff191be3ffd9d8faffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff6c6cecff0000dfff5959eaffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffb7b8f6ff0d0ee1ff1415e2ffd4d4faffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd6d7faff2d2ee4ff0000deff9394f2ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffe4e4fcff4445e8ff0000ddff7071edfffafafeffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffe3e3fcff4042e8ff0000ddff6667ecfff9f9feffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffdfe0fbff4041e8ff0000ddff4c4de9ffefeffdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffe1e1fcff4143e8ff0000dfff6c6cedffeeeffdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffe3e3fcff4445e7ff0000deff2c2de5ffa5a5f4ffacacf5ffa7a8f4ffa7a8f4ffa7a8f4ffa3a3f4ffbebff7fffbfcffffffffffffffffffffffffffff
ffffffffffffffffffffffff7c7defff0000dbff0000ddff0000ddff0000dcff0000dcff0000dcff0000dcff0000dcff0000daff3031e5fff3f3feffffffffffffffffffffffffff
ffffffffffffffffffffffffbfbff7ff7e7eeeff8182efff8182efff8182efff8182efff8182efff8182efff8182efff7e7eeeffa1a2f3fff9f9feffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffdfdfefffafafefffffffffffffffffffffffffffffffffffffffffffffffffffafafeffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffc1c2f7ff7878eeff595beaff5758eaff6768ecffaeaff5ffffffffffffffffffb8b9f6ff6a6becff5657eaff5c5debff7c7defffbbbbf7ffffffffffffffffff
ffffffff8182efff1719e2ff0000deff0000deff0000deff0000deff0a0ce0ff6465ecff6f70eeff1011e1ff0000deff0000deff0000deff0000deff1315e2ff7172edfffdfdffff
b1b1f6ff0d0fe1ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0002dfff0002dfff0204e0ff0001dfff0608e0ff0608e0ff0608e0ff0507e0ff0000dfff0709e0ffa8a9f4ff
6363ebff0000deff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0507e0ff0507e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff5456eaff
4647e8ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff3f41e7ff
4a4ce9ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000deff4446e8ff
8f91f1ff0608e0ff0305e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0407e0ff0304e0ff7b7cefff
f9f9feff4b4de9ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff2d2ee5fff0f1fdff
ffffffffe2e2fcff2d2fe5ff0000dfff0609e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0609e0ff0000dfff1518e2ffc9c9f8ffffffffff
ffffffffffffffffc4c5f7ff2223e3ff0000deff0709e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0709e0ff0000deff1819e2ffb6b7f6ffffffffffffffffff
ffffffffffffffffffffffffc7c7f8ff2324e3ff0000deff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0608e0ff0001dfff1819e1ffb6b7f6ffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffcaccf9ff3131e5ff0000dfff0608e0ff0608e0ff0608e0ff0608e0ff0000dfff1618e2ffb4b4f6ffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffe0e1fbff3637e6ff0000deff0608e0ff0608e0ff0001dfff1517e2ffb5b6f6ffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffcecef9ff2224e3ff0001dfff0103dfff0e10e1ffb3b5f6ffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfc0f7ff1011e1ff0405e0ff9b9cf2ffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff8e8ef1ff6c6dedffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbfafefff5f5feffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

