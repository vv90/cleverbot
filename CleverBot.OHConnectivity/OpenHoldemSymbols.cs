﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.OHConnectivity
{
	public class OpenHoldemSymbols
	{
		public int TurnBits { get; set; }
		public bool IsMyTurn { get; set; }
		public double SmallBlind { get; set; }
		public double BigBlind { get; set; }
		public int SeatedBits { get; set; }
		public string HandNumber { get; set; }
	}
}
