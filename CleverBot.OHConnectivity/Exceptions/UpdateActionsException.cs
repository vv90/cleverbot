﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.OHConnectivity.Exceptions
{
	public class UpdateActionsException : Exception
	{
		public UpdateActionsException()
		{
		}

		public UpdateActionsException(string message) : base(message)
		{
		}
	}
}
