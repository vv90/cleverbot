﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.OHConnectivity.Exceptions
{
	public class InfiniteLoopException : Exception
	{
		public InfiniteLoopException()
		{
		}

		public InfiniteLoopException(string message)
			: base(message)
		{
		}
	}
}
