﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.OHConnectivity
{
	internal class HoldemStateCollection
	{
		private ManagedHoldemState[] _states;
		private int _positionIndex;

		public HoldemStateCollection(int capacity)
		{
			_states = new ManagedHoldemState[capacity];
			_positionIndex = -1;
		}

		public int Capacity { get { return _states.Length; } }

		public void Add(ManagedHoldemState state)
		{
			_positionIndex = (_positionIndex + 1) % Capacity;
			_states[_positionIndex] = state;

		}

		public ManagedHoldemState GetState(int offset = 0)
		{
			if (offset >= Capacity)
			{
				throw new ArgumentOutOfRangeException("offset", "Offset cannot be greater than capacity");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Offset cannot be negative");
			}

			return _states[Math.Abs((_positionIndex - offset) % Capacity)];
		}
	}
}
