﻿using System;
using System.Collections.Generic;
using System.Linq;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.OHConnectivity.Exceptions;

namespace CleverBot.OHConnectivity
{
	public class PossiblePlayerAction
	{
		public Chair Chair { get; set; }
		public PlayerActionEnum Action { get; set; }
		//public double LowerPossibleAmount { get; set; }
		//public double HigherPossibleAmount { get; set; }
		public BetRound BetRound { get; set; }

		public PossiblePlayerAction()
		{
		}

		public PossiblePlayerAction(Chair chair, PlayerActionEnum action, BetRound betRound)
		{
			Chair = chair;
			Action = action;
			BetRound = betRound;
		}

		public override string ToString()
		{
			return string.Format("{{{0}: {1}: {2}}}", BetRound, Chair.Name, Action);
		}
	}

	public class ActionNode
	{
		public PossiblePlayerAction PossibleAction { get; set; }
		public List<PlayerAction> PreviousActions { get; set; }

		public ActionNode()
		{

		}

		//public ActionNode(PossiblePlayerAction possibleAction, IEnumerable<PlayerAction> previousActions)
		//{
		//	PossibleAction = possibleAction;
		//	PreviousActions = new List<PlayerAction>(previousActions);


		//}

		public override string ToString()
		{
			return PossibleAction.ToString();
		}
	}

	//public class TableState
	//{
	//	public BetRound BetRound { get; set; }
	//	public List<ChairState> ChairStates { get; set; }
	//}

	//public class ChairState
	//{
	//	public int ChairNumber { get; set; }
	//	public double CurrentBet { get; set; }
	//	public double CurrentBalance { get; set; }
	//}

	internal class GameWalker
	{
		//public double BigBlind { get; private set; }
		//public double SmallBlind { get; private set; }
		//public List<PlayerAction> CurrentActions { get; private set; }

		private Game _game;
		private List<ActionNode> _currentNodes;

		public GameWalker(Game game)
		{
			//BigBlind = game.BigBlind;
			//SmallBlind = game.SmallBlind;

			//CurrentActions = new List<PlayerAction>();

			//if (game.Actions.Any())
			//	CurrentActions.AddRange(game.Actions);

			_game = game;
			_currentNodes = new List<ActionNode>
			{
				new ActionNode
				{
					//PossibleAction = null, 
					PreviousActions = new List<PlayerAction>(_game.Actions)
				}
			};
		}

		//public List<PlayerAction> ResolveActions()
		//{
		//	var node = _currentNodes.FirstOrDefault();
		//	return node != null ? node.PreviousActions : new List<PlayerAction>();
		//}

		public List<PlayerAction> UpdateState(ManagedHoldemState state)
		{
			var safetyThreshold = 100;
			var safetyCount = 0;

			var terminalNodes = new List<ActionNode>();

			// put the current nodes here and expand them until all of them are terminal for the current state
			var tempNodes = new List<ActionNode>(_currentNodes);
			//List<ActionNode> succededNodes = new List<ActionNode>();
			ActionNode succededNode = null;

			while (safetyCount < safetyThreshold && tempNodes.Any() && succededNode == null)
			{
				safetyCount += 1;

				var updatedNodes = new List<ActionNode>();
				
				foreach (var node in tempNodes)
				{
					if (IsStateProcessingComplete(node.PreviousActions, state))
					{
						succededNode = node;
						break;
					}

					var nodeChildren = ExpandNode(node, state);

					if (nodeChildren.Count > 0)
					{
						updatedNodes.AddRange(nodeChildren);
					}
					else
					{
						terminalNodes.Add(node);
					}
				}
				tempNodes = updatedNodes;
			}

			if (safetyCount >= safetyThreshold)
				throw new Exception("Game walker update took too many steps to complete and was interrupted");

			if (succededNode == null)
				throw new UpdateActionsException("Could not find a valid actions sequence");
			//_currentNodes = terminalNodes;
			//return succededNodes.Single().PreviousActions;
			return succededNode.PreviousActions.Where(a => !_game.Actions.Contains(a)).ToList();
		}

		private List<ActionNode> ExpandNode(ActionNode parentNode, ManagedHoldemState state)
		{
			var betRound = parentNode.PreviousActions.Any()
				? parentNode.PreviousActions.Max(a => a.BetRound)
				: BetRound.Preflop;

			var chair = GetNextActingChair(parentNode.PreviousActions, betRound);

			if (chair == null && betRound == BetRound.River)
				return new List<ActionNode>();

			if (chair == null)
			{
				betRound += 1;
				chair = GetNextActingChair(parentNode.PreviousActions, betRound);
			}

			var possibleActions = new List<PossiblePlayerAction>();

			if (betRound == BetRound.Preflop)
			{
				possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.SmallBlind, BetRound.Preflop));
				possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.BigBlind, BetRound.Preflop));
				possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Straddle, BetRound.Preflop));
				possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Post, BetRound.Preflop));
			}

			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Fold, betRound));
			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Check, betRound));
			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Call, betRound));
			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Bet, betRound));
			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Raise, betRound));
			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Cap, betRound));
			possibleActions.Add(new PossiblePlayerAction(chair, PlayerActionEnum.Allin, betRound));

			var resultNodes = new List<ActionNode>();

			foreach (var pa in possibleActions)
			{
				var amount = ValidatePossibleAction(pa, parentNode.PreviousActions, state);

				if (amount.HasValue)
				{
					resultNodes.Add(new ActionNode
					{
						PossibleAction = pa,
						PreviousActions = new List<PlayerAction>(parentNode.PreviousActions)
						{
							new PlayerAction(pa.Chair, pa.Action, amount.Value, pa.BetRound)
						}
					});
				}
			}

			return resultNodes;
		}

		private double? ValidatePossibleAction(PossiblePlayerAction possibleAction,
			List<PlayerAction> currentActions,
			ManagedHoldemState state)
		{


			var playerChair = possibleAction.Chair;
			var playerState = state.m_player[playerChair.ChairNumber];
			var currentPlayerActions = currentActions.Where(a => a.Chair == possibleAction.Chair).ToList();
			var currentBetRoundActions = currentActions.Where(a => a.BetRound == possibleAction.BetRound).ToList();
			var currentPlayerSumOfBets = currentActions.GetPlayerSumOfBets(possibleAction.Chair);
			var currentPlayerMissingAmount = playerChair.Balance - playerState.m_balance - currentPlayerSumOfBets;
			//var postedChair = DetectPostedChair(state);

			var maxBet = currentBetRoundActions
				.Select(a => a.Amount)
				.DefaultIfEmpty()
				.Max();

			var currentPlayerMaxBet = currentPlayerActions
				.Where(a => a.BetRound == possibleAction.BetRound)
				.Select(a => a.Amount)
				.DefaultIfEmpty()
				.Max();

			if (currentPlayerActions.Any(a => a.Action == PlayerActionEnum.Fold))
				return null;

			if (!playerState.IsPlaying &&
			    Math.Abs(currentPlayerMissingAmount) < 0.001 &&
			    possibleAction.Action != PlayerActionEnum.Fold)
			{
				return null;
			}

			if (currentActions.All(a => a.Action != PlayerActionEnum.SmallBlind) &&
				possibleAction.Action != PlayerActionEnum.SmallBlind)
				return null;

			if (currentActions.All(a => a.Action != PlayerActionEnum.BigBlind) &&
				possibleAction.Action != PlayerActionEnum.BigBlind && possibleAction.Action != PlayerActionEnum.SmallBlind)
				return null;

			//if (playerChair == postedChair && possibleAction.Action != PlayerActionEnum.Post)
			//	return null;


			switch (possibleAction.Action)
			{
				case PlayerActionEnum.Fold:
					if (playerState.IsPlaying)
						return null;
					if (Math.Abs(maxBet - currentPlayerMaxBet) < 0.001)
						return null;
					if (currentPlayerMissingAmount > 0.001)
						return null;

					return 0;


				case PlayerActionEnum.Check:
					if (!currentActions.Any())
						return null;
					if (Math.Abs(maxBet - currentPlayerMaxBet) > 0.001)
						return null;
					//if (currentPlayerMissingAmount > 0.001)
					//	return null;

					return 0;


				case PlayerActionEnum.SmallBlind:
					if (currentActions.Any(a => a.Action == PlayerActionEnum.SmallBlind || a.Action == PlayerActionEnum.BigBlind))
						return null;
					if (currentPlayerActions.Any())
						return null;
					//if (_game.SmallBlind - currentPlayerMissingAmount > 0.001)
					//	return null;

					return _game.SmallBlind;


				case PlayerActionEnum.BigBlind:
					if (currentActions.All(a => a.Action != PlayerActionEnum.SmallBlind))
						return null;
					if (currentActions.Any(a => a.Action == PlayerActionEnum.BigBlind))
						return null;
					if (currentPlayerActions.Any())
						return null;
					//if (Math.Abs(currentPlayerMissingAmount - _game.BigBlind) > 0.001)
					//	return null;

					return _game.BigBlind;


				case PlayerActionEnum.Straddle:
					//if (currentPlayerActions.Any())
					//	return null;
					//if (currentActions.Any(a => 
					//	a.Action != PlayerActionEnum.SmallBlind && 
					//	a.Action != PlayerActionEnum.BigBlind && 
					//	a.Action != PlayerActionEnum.Post))
					//	return null;
					//if (currentActions.All(a => a.Action != PlayerActionEnum.SmallBlind) ||
					//	currentActions.All(a => a.Action != PlayerActionEnum.BigBlind))
					//	return null;

					//return _game.BigBlind * 2;
					return null;

				case PlayerActionEnum.Post:
					//if (postedChair != playerChair)
					//	return null;
					//if (currentPlayerActions.Any())
					//	return null;
					//if (Math.Abs(currentPlayerMissingAmount - _game.BigBlind) > 0.001)
					//	return null;

					//return _game.BigBlind;
					return null;


				case PlayerActionEnum.Call:
					if (Math.Abs(maxBet - currentPlayerMaxBet) < 0.001)
						return null;
					//if (Math.Abs(currentPlayerMissingAmount - maxBet) > 0.001)
					//	return null;

					return maxBet;


				case PlayerActionEnum.Bet:
					//if (Math.Abs(maxBet - currentPlayerMaxBet) < 0.001)
					//	return null;
					if (currentBetRoundActions.Any(a => a.Action == PlayerActionEnum.Bet || a.Action == PlayerActionEnum.Raise))
						return null;
					if (currentBetRoundActions.Any(a => a.Amount > 0))
						return null;
					if (currentPlayerMissingAmount < _game.BigBlind)
						return null;

					return currentPlayerMissingAmount;


				case PlayerActionEnum.Raise:
					if (Math.Abs(maxBet - currentPlayerMaxBet) < 0.001)
						return null;
					if (currentPlayerMissingAmount < 2 * _game.BigBlind)
						return null;
					if (currentPlayerMissingAmount < 2 * maxBet)
						return null;

					return currentPlayerMissingAmount;


				case PlayerActionEnum.Cap:
					return null;


				case PlayerActionEnum.Allin:
					if (maxBet > playerChair.Balance)
						return playerChair.Balance;
					else
						return null;
			}

			//actionAmount = 0;
			return 0;
		}

		private Chair GetNextActingChair(List<PlayerAction> currentActions, BetRound betRound)
		{
			if (IsBetRoundFinished(currentActions, betRound))
				return null;


			var lastAction = currentActions.LastOrDefault(a => a.BetRound == betRound);

			var startingChair = lastAction == null ? _game.GetDealerChair() : lastAction.Chair;

			var nextActingChair = _game.GetNextChair(startingChair.ChairNumber);

			while (nextActingChair != startingChair && currentActions
				.Where(a => a.Chair == nextActingChair)
				.Any(a => a.Action == PlayerActionEnum.Fold))
			{
				nextActingChair = _game.GetNextChair(nextActingChair.ChairNumber);
			}

			if (nextActingChair == startingChair)
				throw new UpdateActionsException("Could not determine next acting chair");

			return nextActingChair;
			//if (lastAction == null)
			//{
			//	return _game.GetNextChair(_game.DealerChair);
			//}
			//else
			//{
			//	return _game.GetNextChair(lastAction.Chair.ChairNumber);
			//}
		}

		private bool IsBetRoundFinished(List<PlayerAction> currentActions, BetRound betRound)
		{
			foreach (var chair in _game.Chairs)
			{
				var didFold = currentActions
					.Where(a => a.Chair == chair)
					.Any(a => a.Action == PlayerActionEnum.Fold);

				if (didFold)
					continue;

				var didActThisRound = currentActions
					.Where(a => a.BetRound == betRound && a.Chair == chair)
					.Any(a => a.Action != PlayerActionEnum.SmallBlind &&
							  a.Action != PlayerActionEnum.BigBlind &&
							  a.Action != PlayerActionEnum.Post &&
							  a.Action != PlayerActionEnum.Straddle);

				var maxBet = currentActions
					.Where(a => a.BetRound == betRound)
					.Select(a => a.Amount)
					.DefaultIfEmpty()
					.Max();

				var chairMaxBet = currentActions
					.Where(a => a.BetRound == betRound && a.Chair == chair)
					.Select(a => a.Amount)
					.DefaultIfEmpty()
					.Max();

				if (!didActThisRound || Math.Abs(maxBet - chairMaxBet) > 0.001)
					return false;
			}

			return true;
		}

		private bool IsStateProcessingComplete(List<PlayerAction> currentActions, ManagedHoldemState state)
		{
			foreach (var chair in _game.Chairs)
			{
				var playerState = state.m_player[chair.ChairNumber];

				var sumOfBets = currentActions.GetPlayerSumOfBets(chair);

				var missingAmount = chair.Balance - playerState.m_balance;

				if (Math.Abs(sumOfBets - missingAmount) > 0.001)
					return false;
			}

			
			var lastAction = currentActions.LastOrDefault();
			if (lastAction != null)
			{
				Chair lastToAct = null;
				var currentChair = _game.GetNextChair(_game.HeroChair);

				while (currentChair != _game.GetHeroChair())
				{
					if (currentActions.Where(c => c.Chair == currentChair).All(c => c.Action != PlayerActionEnum.Fold))
					{
						lastToAct = currentChair;
					}
					currentChair = _game.GetNextChair(currentChair.ChairNumber);
				}

				if (lastAction.Chair != lastToAct || lastAction.BetRound != _game.BetRound)
					return false;
			}

			return true;
		}

		private Chair DetectPostedChair(ManagedHoldemState state)
		{
			var currentChair = _game.GetDealerChair();
			var maxBet = 0.0;
			Chair chairMissingAction = null;

			do
			{
				currentChair = _game.GetNextChair(currentChair.ChairNumber);

				var playerState = state.m_player[currentChair.ChairNumber];
				if (!playerState.IsPlaying)
					continue;

				if (playerState.m_currentbet > maxBet)
					maxBet = playerState.m_currentbet;

				if (playerState.m_currentbet < maxBet)
					chairMissingAction = currentChair;

				if (chairMissingAction != null && Math.Abs(playerState.m_currentbet - _game.BigBlind) < 0.001)
					return currentChair;
			} while (currentChair != _game.GetDealerChair());

			return null;
		}
	}

	public static class PlayerActionsEnumerableExtentions
	{
		public static double GetPlayerSumOfBets(this IEnumerable<PlayerAction> actions, Chair chair)
		{
			return actions
				.Where(a => a.Chair == chair)
				.GroupBy(a => a.BetRound)
				.Select(g => g.Select(a => a.Amount).DefaultIfEmpty().Max())
				.Sum();
		}
	}
}
