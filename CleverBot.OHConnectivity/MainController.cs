﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using CleverBot.Brain.Strategies;
using CleverBot.Common;
using CleverBot.Common.Enums;
using CleverBot.Common.Utilities;
using CleverBot.Data;
using CleverBot.OHConnectivity.Exceptions;
using Newtonsoft.Json;

namespace CleverBot.OHConnectivity
{
	public class MainController
	{

		private static MainController _instance;

		private HoldemStateCollection _states = new HoldemStateCollection(256);
		private bool _isTestMode;
		private IEnumerable<Game> _referenceHandHistory;
		private bool _isCurrentGameCorrect;
		private List<TableState> _currentTableStates = new List<TableState>();
		private GameLogManager _logManager = new GameLogManager();
		//private GameBuilder _gameBuilder;
		private GameConstructor _gameConstructor = null;

		private CasesStrategy _casesStrategy = new CasesStrategy();
		
		private PlayerAction _lastAction;
		private PlayerAction _lastHeroAction;
		private BetRound _lastBetRound;

		[UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
		private delegate double GetSymbolFromDllDelegate(string symbol);
		private GetSymbolFromDllDelegate getSymbolFromDll;

		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		private delegate string GetHandNumberFromDllDelegate();
		private GetHandNumberFromDllDelegate getHandNumberFromDll;

		//private NeuralNetworkPreflopLearner _learner = new NeuralNetworkPreflopLearner();

		public int HeroChair { get; set; }
		public Game Game { get { return _gameConstructor.Game; } }

		public static MainController GetInstance(IntPtr getSymbolFromDll, IntPtr getHandNumberFromDll)
		{
			return _instance ?? (_instance = new MainController(getSymbolFromDll, getHandNumberFromDll));
		}

		internal MainController(IntPtr getSymbolFromDll, IntPtr getHandNumberFromDll)
		{
			this.getSymbolFromDll = (GetSymbolFromDllDelegate)Marshal.GetDelegateForFunctionPointer(getSymbolFromDll, typeof(GetSymbolFromDllDelegate));
			this.getHandNumberFromDll = (GetHandNumberFromDllDelegate)Marshal.GetDelegateForFunctionPointer(getHandNumberFromDll, typeof(GetHandNumberFromDllDelegate));

			Database.SetInitializer(new MigrateDatabaseToLatestVersion<CleverBotDbContext, Data.Migrations.Configuration>());

			ConsoleManager.Show();
		}

		//internal MainController(Game game)
		//{
		//	//_gameBuilder = new GameBuilder(game);
		//	Game = game;
		//}

		internal MainController()
		{
			_isTestMode = true;
		}

		internal double GetOpenHoldemSymbol(Symbols symbol)
		{
			return getSymbolFromDll(symbol.ToString());
		}

		internal string GetHandNumber()
		{
			return getHandNumberFromDll();
		}

		private bool IsNewGame(Game game, ManagedHoldemState state, int heroChair)
		{
			// hero cards have changed
			if (state.m_player[heroChair].Cards != game.GetChair(heroChair).Hand)
				return true;



			return false;
		}

		//internal Game UpdateGame(ManagedHoldemState state)
		//{
		//	Game.Flop = state.Flop;
		//	Game.Turn = state.Turn;
		//	Game.River = state.River;

		//	for (int i = 0; i < state.m_player.Length; i++)
		//	{
		//		var player = Game.GetChair(i);
		//		var playerState = state.m_player[i];
		//		if (player != null && playerState.IsSeated)
		//		{
		//			player.Name = playerState.m_name;
		//			player.Hand = playerState.Cards;
		//			player.CurrentBalance = playerState.m_balance;
		//		}
		//	}

		//	return Game;
		//}

		//internal Game NewGame(TableState tableState)
		//{
		//	Game = new Game();
		//	Game.StartTime = DateTime.Now;
		//	Game.Title = tableState.State.m_title;
		//	Game.SmallBlind = tableState.Symbols.SmallBlind;
		//	Game.BigBlind = tableState.Symbols.BigBlind;
		//	Game.GameNumber = tableState.Symbols.HandNumber;
		//	Game.DealerChair = tableState.State.m_dealer_chair;

		//	var seatedBits = tableState.Symbols.SeatedBits;

		//	for (int i = 0; i < tableState.State.m_player.Length; i++)
		//	{
		//		var player = Game.GetChair(i);
		//		var playerState = tableState.State.m_player[i];

		//		if ((seatedBits & (1 << i)) != 0 && playerState.m_name != "SITTINGOUT")
		//		{
		//			Game.Chairs.Add(new Chair
		//			{
		//				ChairNumber = i,
		//				Balance = playerState.m_balance + playerState.m_currentbet,
		//				CurrentBalance = playerState.m_balance,
		//				Hand = playerState.Cards,
		//				Name = playerState.m_name
		//			});
		//		}
		//		//else if (player != null && playerState.IsSeated)
		//		//{
		//		//	player.Name = playerState.m_name;
		//		//	player.Hand = playerState.Cards;
		//		//	player.CurrentBalance = playerState.m_balance;
		//		//}
		//	}

		//	return Game;
		//}

		//internal class ChairEnumerator
		//{
		//	private Game _game;
		//	private int _currentChairNumber;

		//	public ChairEnumerator(Game game)
		//	{
		//		_game = game;
		//		_currentChairNumber = _game.DealerChair;
		//	}

		//	public Chair GetChair()
		//	{
		//		var currentChair = _game.GetNextChair(_currentChairNumber);
		//		_currentChairNumber = currentChair.ChairNumber;
		//		return currentChair;
		//	}
		//}

		internal OpenHoldemSymbols GetSymbols()
		{
			var symbols = new OpenHoldemSymbols();

			symbols.TurnBits = (int)GetOpenHoldemSymbol(Symbols.myturnbits);
			symbols.IsMyTurn = (int)GetOpenHoldemSymbol(Symbols.ismyturn) != 0;
			symbols.SmallBlind = GetOpenHoldemSymbol(Symbols.sblind);
			symbols.BigBlind = GetOpenHoldemSymbol(Symbols.bblind);
			symbols.SeatedBits = (int)GetOpenHoldemSymbol(Symbols.playersseatedbits);
			symbols.HandNumber = GetHandNumber();

			return symbols;
		}

		//internal List<PlayerAction> GoAroundTableOnce(Chair startChair, ManagedHoldemState state)
		//{
		//	var currentChair = startChair;
		//	var actions = new List<PlayerAction>();
		//	var safetyCount = 0;

		//	var lastAction = Game.Actions
		//		.LastOrDefault(a => a.BetRound == Game.BetRound);

		//	while (Game.GetNextChair(currentChair.ChairNumber).ChairNumber != Game.HeroChair)
		//	{
		//		if (safetyCount > Game.Chairs.Count)
		//			throw new UpdateActionsException("UpdateActions method seems to have gone into an infinite loop");

		//		safetyCount += 1;

		//		var playerState = state.m_player[currentChair.ChairNumber];

		//		if (!playerState.IsPlaying)
		//		{
		//			if (Game.Actions
		//				.Where(a => a.Chair == currentChair)
		//				.All(a => a.Action != PlayerActionEnum.Fold))
		//			{
		//				var action = new PlayerAction(currentChair, PlayerActionEnum.Fold, 0, Game.BetRound);
		//				actions.Add(action);
		//			}
		//		}
		//		else
		//		{
		//			var currentPlayerLastAction = Game.Actions
		//				.LastOrDefault(a => a.Chair == currentChair && a.BetRound == Game.BetRound);

		//			if (currentPlayerLastAction == null && playerState.m_currentbet > 0 ||
		//				currentPlayerLastAction != null && playerState.m_currentbet - currentPlayerLastAction.Amount > 0.001)
		//			{
		//				PlayerAction action = null;

		//				if (Game.Actions.All(a => a.Action != PlayerActionEnum.SmallBlind))
		//					action = new PlayerAction(currentChair, PlayerActionEnum.SmallBlind, Game.SmallBlind);
		//				else if (Game.Actions.All(a => a.Action != PlayerActionEnum.BigBlind))
		//					action = new PlayerAction(currentChair, PlayerActionEnum.BigBlind, Game.BigBlind);
		//				else
		//					action = lastAction == null || lastAction.BetRound != Game.BetRound
		//						? PlayerAction.FirstAction(currentChair, playerState.m_currentbet, Game.BetRound)
		//						: lastAction.RaiseAction(currentChair, playerState.m_currentbet);

		//				actions.Add(action);
		//				lastAction = action;
		//			}
		//		}

		//		currentChair = Game.GetNextChair(currentChair.ChairNumber);
		//	}

		//	Game.Actions.AddRange(actions);

		//	return actions;
		//}

		/// <summary>
		/// Updates the Game with new player actions from the provided HoldemState and returns added actions
		/// </summary>
		//internal List<PlayerAction> UpdateActions(ManagedHoldemState state)
		//{
		//	if (Game.Chairs.Count < 2)
		//	{
		//		throw new UpdateActionsException("Not enough chairs in the game");
		//	}
		//	var actions = new List<PlayerAction>();

		//	var startChair = Game.Actions.LastOrDefault(a => a.BetRound == Game.BetRound) == null
		//		? Game.GetNextChair(Game.GetDealerChair().ChairNumber)
		//		: Game.GetNextChair(Game.GetHeroChair().ChairNumber);

		//	actions.AddRange(GoAroundTableOnce(startChair, state));

		//	//var chairsQueue = new Queue<Chair>();
		//	//var startChairNumber = lastAction != null
		//	//	? lastAction.Chair.ChairNumber
		//	//	: Game.GetChair(Game.DealerChair).ChairNumber;
		//	//var currentChair = Game.GetNextChair(startChairNumber);

		//	//for (int i = 0; i < Game.Chairs.Count; i++)
		//	//{
		//	//	chairsQueue.Enqueue(currentChair);
		//	//	currentChair = Game.GetNextChair(currentChair.ChairNumber);
		//	//}

		//	//while (chairsQueue.Any())
		//	//{
		//	//	currentChair = chairsQueue.Dequeue();
		//	//	var playerState = state.m_player[currentChair.ChairNumber];
		//	//	//PlayerAction currentAction = null;
		//	//	var currentPlayerLastAction = Game.Actions
		//	//		.LastOrDefault(a => a.Chair == currentChair && a.BetRound == Game.BetRound);

		//	//	if (Game.Actions.Any(a => a.Chair == currentChair && a.Action == PlayerActionEnum.Fold))
		//	//	{
		//	//		continue;
		//	//	}

		//	//	// calculate the difference between the last known balance and current balance
		//	//	var currentPlayerSumOfActions = playerState.m_currentbet + Game.Actions.Concat(actions)
		//	//		.Where(a => a.Chair == currentChair && a.BetRound != Game.BetRound)
		//	//		.GroupBy(a => a.BetRound)
		//	//		.Select(g => g.Max(a => a.Amount))
		//	//		.Sum();

		//	//	var currentPlayerBalanceDifference = currentChair.Balance - playerState.m_balance;

		//	//	// if saved or visible actions do not account for the balance difference
		//	//	if (Math.Abs(currentPlayerBalanceDifference - currentPlayerSumOfActions) > 0.001)
		//	//	{
		//	//		var amount = Math.Round(currentPlayerBalanceDifference - playerState.m_currentbet, 3);
		//	//		var lastActionsAllBetRounds = Game.Actions.Concat(actions)
		//	//			.LastOrDefault(a => a.Action != PlayerActionEnum.Fold);
		//	//		var action = lastActionsAllBetRounds == null
		//	//			? PlayerAction.FirstAction(currentChair, amount, Game.BetRound - 1)
		//	//			: lastActionsAllBetRounds.RaiseAction(currentChair, amount);
		//	//		actions.Add(action);
		//	//		lastAction = action;
		//	//		chairsQueue.Enqueue(currentChair);
		//	//	}

		//	//	else if (currentPlayerLastAction == null && playerState.m_currentbet > 0 ||
		//	//		currentPlayerLastAction != null && currentPlayerLastAction.Amount < playerState.m_currentbet)
		//	//	{
		//	//		var action = lastAction == null || lastAction.BetRound != Game.BetRound
		//	//			? PlayerAction.FirstAction(currentChair, playerState.m_currentbet, Game.BetRound)
		//	//			: lastAction.RaiseAction(currentChair, playerState.m_currentbet);

		//	//		// if player on the small blind has already made another bet
		//	//		if (action.Action == PlayerActionEnum.SmallBlind && action.Amount > Game.SmallBlind)
		//	//		{
		//	//			// add the small blind action and enqueue this chair to be processed again
		//	//			action.Amount = Game.SmallBlind;
		//	//			chairsQueue.Enqueue(currentChair);
		//	//		}

		//	//		actions.Add(action);
		//	//		lastAction = action;
		//	//	}

		//	//	// if there is a bet visible on one of chairs in the queue
		//	//	else if (playerState.IsPlaying && Math.Abs(playerState.m_currentbet) < 0.001 &&
		//	//			 chairsQueue.Select(c => Game.Actions.LastOrDefault(a => a.BetRound == Game.BetRound && a.Chair == c))
		//	//				.Any(a => a != null && state.m_player[a.Chair.ChairNumber].m_currentbet - a.Amount > 0.001))
		//	//	{
		//	//		var action = new PlayerAction(currentChair, PlayerActionEnum.Check, 0, Game.BetRound);
		//	//		actions.Add(action);
		//	//	}

		//	//	if (!playerState.IsPlaying)
		//	//	{
		//	//		var action = new PlayerAction(currentChair, PlayerActionEnum.Fold, 0, Game.BetRound);
		//	//		actions.Add(action);
		//	//	}
		//	//}

		//	return actions;
		//}

		public double ProcessQuery(string query)
		{
			try
			{
				if (_lastAction == null || Game.Actions.LastOrDefault() != _lastAction || Game.BetRound != _lastBetRound)
				{
					_lastAction = Game.Actions.LastOrDefault();
					_lastHeroAction = _casesStrategy.Think(Game);
					_lastBetRound = Game.BetRound;

					Console.WriteLine("[[[ACTING]]] - {0}", _lastHeroAction);
				}

				if (query == "dll$iswait")
					return 0;

				if (query == "dll$alli")
					return _lastHeroAction.Action == PlayerActionEnum.Allin ? 1.0 : 0.0;

				else if (query == "dll$rais")
					return _lastHeroAction.Action == PlayerActionEnum.Raise ? 1.0 : 0.0;

				else if (query == "dll$call")
					return _lastHeroAction.Action == PlayerActionEnum.Call ? 1.0 : 0.0;

				else if (query == "dll$check")
					return _lastHeroAction.Action == PlayerActionEnum.Check ? 1.0 : 0.0;

				else if (query == "dll$betsize")
					return _lastHeroAction.Amount;

				else if (query == "dll$fold")
					return _lastHeroAction.Action == PlayerActionEnum.Fold ? 1.0 : 0.0;
			}
			catch (Exception ex)
			{
				if (_isTestMode)
					throw;

				Console.WriteLine(ex.Message);
				using (var writer = new StreamWriter(File.OpenWrite("ErrorLog.txt")))
				{
					writer.WriteLine("[{0}]: {1}", DateTime.Now.ToString("G"), ex.Message);
				}
			}

			return 0;
		}

		public void UpdateState(IntPtr pstate)
		{
			var tableState = new TableState
			{
				State = (ManagedHoldemState)Marshal.PtrToStructure(pstate, typeof(ManagedHoldemState)),
				Symbols = GetSymbols()
			};

			UpdateState(tableState);
		}

		internal void UpdateState(TableState tableState)
		{
			try
			{
				var turnBits = tableState.Symbols.TurnBits;
				var isMyTurn = tableState.Symbols.IsMyTurn;
				var heroChair = 0;

				if (tableState.State.IsIdenticalTo(_states.GetState()))
					return;
				if (!tableState.State.IsValid(heroChair))
					return;
				if (!isMyTurn)
					return;
				



				// if we got so far than the holdem state is correct and contains relevant information

				var isNewGame = false;
				// start new game
				if (_gameConstructor == null || Game == null || IsNewGame(Game, tableState.State, heroChair))
				{

					if (_gameConstructor != null && Game != null)
					{
						//_logManager.WriteToDatabase(_game, true);
						//_logManager.WriteToFile("currentGamesLog.txt", _game);

						//if (_isTestMode && _isCurrentGameCorrect)
						//{
						GameConstructionTester.SaveGameStates(_currentTableStates);
						//Console.WriteLine(message);
						//}
					}

					_currentTableStates = new List<TableState>();
					_gameConstructor = new GameConstructor(tableState);

					Console.WriteLine("\n---- Starting new game [{0}] {1}/{2}----",Game.GameNumber, Game.SmallBlind, Game.BigBlind);
					isNewGame = true;
				}

				

				_gameConstructor.UpdateGame(tableState.State);
				var actions = _gameConstructor.UpdateActions(tableState.State);

				if (isNewGame)
					foreach (var l in Game.CreateLog())
						Console.WriteLine(l);

				if (actions.Count > 0)
				{
					Console.WriteLine();
					foreach (var a in actions)
						Console.WriteLine(a);
				}

				_states.Add(tableState.State);



				_currentTableStates.Add(tableState);
				//if (_isTestMode)
				//{
				//	//var referenceGame = _referenceHandHistory.SingleOrDefault(g => g.GameNumber == _game.GameNumber);

					
				//	//if (referenceGame == null)
				//	//{
				//	//	Console.WriteLine("Warning! Reference game {0} not found!", _game.GameNumber);
				//	//}
				//	//else
				//	//{
				//	//	var error = GameConstructionTester.TestGame(referenceGame, _game);

				//	//	if (error != null)
				//	//	{
				//	//		Console.WriteLine("Error! {0}: {1}", _game.GameNumber, error);
				//	//	}

				//	//	_isCurrentGameCorrect = (error == null);
				//	//}
				//}
			}
			catch (Exception ex)
			{
				if (_isTestMode)
					throw;

				Console.WriteLine(ex.Message);
				using (var writer = new StreamWriter(File.OpenWrite("ErrorLog.txt")))
				{
					writer.WriteLine("[{0}]: {1}", DateTime.Now.ToString("G"), ex.Message);
				}
			}
		}
	}
}
