﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.OHConnectivity
{
	internal class GameStates
	{
		public DateTime DateTime { get; set; }
		public List<TableState> TableStates { get; set; }

		public GameStates()
		{
			DateTime = DateTime.Now;
			TableStates = new List<TableState>();
		}
	}

	internal class TableState
	{
		public ManagedHoldemState State { get; set; }
		public OpenHoldemSymbols Symbols { get; set; }
	}
}
