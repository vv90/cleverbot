﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;
using CleverBot.Common.Utilities;

namespace CleverBot.PokerEvalImport
{
	public partial class PokerEval
	{
		public static HandType EvalType(CardMask mask)
		{
			return (HandType) Eval_Type(mask);
		}

		public static CardRank[] GetRanks(int value)
		{
			var ranks = new CardRank[5];

			for (int i = 0; i < 5; i++)
			{
				var currentRank = (CardRank)((value & (15 << (i*4))) >> (i*4));
				ranks[i] = currentRank;
			}

			return ranks;
		}

		public static HandType GetType(int pokerValue)
		{
			var bitString = pokerValue.BitString();
			return (HandType) ((pokerValue & (15 << 24)) >> 24);
		}
	}
}
