﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common;

namespace CleverBot.PokerEvalImport
{
	public partial class PokerEval
	{
		[DllImport(@"poker-eval-export.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int Eval_N(CardMask mask);

		[DllImport(@"poker-eval-export.dll", CallingConvention = CallingConvention.Cdecl)]
		private static extern int Eval_Type(CardMask mask);

		[DllImport(@"poker-eval-export.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern CardMask Mask(int index);

		//[DllImport(@"poker-eval-export.dll", CallingConvention = CallingConvention.Cdecl)]
		//private static extern IntPtr maskString(CardMask mask);

		//[DllImport(@"poker-eval-export.dll", CallingConvention = CallingConvention.Cdecl)]
		//public static extern int NumCards(CardMask mask);

		[DllImport(@"poker-eval-export.dll", CallingConvention = CallingConvention.Cdecl)]
		private static extern unsafe double
			CalculateMonteCarlo(CardMask hand, CardMask board, CardMask dead,
								int trialsNum, int seed,
								CardMask* range1, int limit1,
								CardMask* range2, int limit2,
								CardMask* range3, int limit3,
								CardMask* range4, int limit4,
								CardMask* range5, int limit5,
								CardMask* range6, int limit6,
								CardMask* range7, int limit7,
								CardMask* range8, int limit8,
								CardMask* range9, int limit9);

		public static double CalculateMC(CardMask hand, CardMask board, CardMask dead,
										int trialsNum, int seed,
										HandRange range1,
										HandRange range2 = null,
										HandRange range3 = null,
										HandRange range4 = null,
										HandRange range5 = null,
										HandRange range6 = null,
										HandRange range7 = null,
										HandRange range8 = null,
										HandRange range9 = null)
		{
			if ((hand.cards_n & board.cards_n) != 0)
				throw new ArgumentException("hand & board != 0");

			unsafe
			{
				return CalculateMonteCarlo(hand, board, dead, trialsNum, seed,
					range1.Pfirst(), range1.Count,
					range2 == null ? null : range2.Pfirst(), range2 == null ? 0 : range2.Count,
					range3 == null ? null : range3.Pfirst(), range3 == null ? 0 : range3.Count,
					range4 == null ? null : range4.Pfirst(), range4 == null ? 0 : range4.Count,
					range5 == null ? null : range5.Pfirst(), range5 == null ? 0 : range5.Count,
					range6 == null ? null : range6.Pfirst(), range6 == null ? 0 : range6.Count,
					range7 == null ? null : range7.Pfirst(), range7 == null ? 0 : range7.Count,
					range8 == null ? null : range8.Pfirst(), range8 == null ? 0 : range8.Count,
					range9 == null ? null : range9.Pfirst(), range9 == null ? 0 : range9.Count);
			}
		}
	}
}
