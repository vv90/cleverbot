﻿using System;
using System.Linq;
using CleverBot.Common;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Data.Tests
{
	[TestClass]
	public class TestGameLog
	{
		[TestMethod]
		public void TestWriteToFileReadFromFileRoundtrip()
		{
			var game = new Game
			{
				GameNumber = "12345",
				Title = "GameTitle",
				StartTime = new DateTime(2014, 1, 1),
				SmallBlind = 1,
				BigBlind = 2,
				Flop = new CardMask("Ac Jc 8s"),
				DealerChair = 1,
				HeroChair = 2,
				Chairs = new[]
				{
					new Chair(1, 100, new CardMask("As, 5d"), "Player1"),
					new Chair(2, 100, new CardMask("4c, 9c"), "Player2")
				}.ToList()
			};

			game.Actions = new[]
			{
				new PlayerAction(game.GetChair("Player1"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player2"), PlayerActionEnum.SmallBlind, 1),
			}.ToList();

			var gameLog = new GameLogManager();

			gameLog.WriteToFile("TestGameLogFile.txt", Enumerable.Repeat(game, 2));

			var games = gameLog.ReadFromFile("TestGameLogFile.txt").ToArray();

			//Test game 1 properties
			Assert.AreEqual("12345", games[0].GameNumber);
			Assert.AreEqual("GameTitle", games[0].Title);
			Assert.AreEqual(new DateTime(2014, 1, 1), games[0].StartTime);
			Assert.AreEqual(1, games[0].SmallBlind);
			Assert.AreEqual(2, games[0].BigBlind);
			Assert.AreEqual(new CardMask("Ac Jc 8s"), games[0].Board);
			Assert.AreEqual(1, games[0].DealerChair);
			Assert.AreEqual(2, games[0].HeroChair);

			Assert.AreEqual(1, games[0].Chairs[0].ChairNumber);
			Assert.AreEqual(100, games[0].Chairs[0].Balance);
			Assert.AreEqual(new CardMask("As, 5d"), games[0].Chairs[0].Hand);
			Assert.AreEqual("Player1", games[0].Chairs[0].Name);
			//Assert.AreEqual(true, games[0].Chairs[0].IsDealer);
			//Assert.AreEqual(true, games[0].Chairs[0].IsHero);
			//Assert.AreEqual(Position.BU, games[0].Chairs[0].Position);

			Assert.AreEqual(2, games[0].Chairs[1].ChairNumber);
			Assert.AreEqual(100, games[0].Chairs[1].Balance);
			Assert.AreEqual(new CardMask("4c, 9c"), games[0].Chairs[1].Hand);
			Assert.AreEqual("Player2", games[0].Chairs[1].Name);
			//Assert.AreEqual(false, games[0].Chairs[1].IsDealer);
			//Assert.AreEqual(false, games[0].Chairs[1].IsHero);
			//Assert.AreEqual(Position.SB, games[0].Chairs[1].Position);

			Assert.AreEqual("Player1", games[0].Actions[0].Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, games[0].Actions[0].Action);
			Assert.AreEqual(2, games[0].Actions[0].Amount);

			Assert.AreEqual("Player2", games[0].Actions[1].Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, games[0].Actions[1].Action);
			Assert.AreEqual(1, games[0].Actions[1].Amount);

			//Test game 2 properties
			Assert.AreEqual("12345", games[1].GameNumber);
			Assert.AreEqual("GameTitle", games[1].Title);
			Assert.AreEqual(new DateTime(2014, 1, 1), games[1].StartTime);
			Assert.AreEqual(1, games[1].SmallBlind);
			Assert.AreEqual(2, games[1].BigBlind);
			Assert.AreEqual(new CardMask("Ac Jc 8s"), games[1].Board);
			Assert.AreEqual(1, games[1].DealerChair);
			Assert.AreEqual(2, games[1].HeroChair);

			Assert.AreEqual(1, games[1].Chairs[0].ChairNumber);
			Assert.AreEqual(100, games[1].Chairs[0].Balance);
			Assert.AreEqual(new CardMask("As, 5d"), games[1].Chairs[0].Hand);
			Assert.AreEqual("Player1", games[1].Chairs[0].Name);
			//Assert.AreEqual(true, games[1].Chairs[0].IsDealer);
			//Assert.AreEqual(true, games[1].Chairs[0].IsHero);
			//Assert.AreEqual(Position.BU, games[1].Chairs[0].Position);

			Assert.AreEqual(2, games[1].Chairs[1].ChairNumber);
			Assert.AreEqual(100, games[1].Chairs[1].Balance);
			Assert.AreEqual(new CardMask("4c, 9c"), games[1].Chairs[1].Hand);
			Assert.AreEqual("Player2", games[1].Chairs[1].Name);
			//Assert.AreEqual(false, games[1].Chairs[1].IsDealer);
			//Assert.AreEqual(false, games[1].Chairs[1].IsHero);
			//Assert.AreEqual(Position.SB, games[1].Chairs[1].Position);

			Assert.AreEqual("Player1", games[1].Actions[0].Chair.Name);
			Assert.AreEqual(PlayerActionEnum.BigBlind, games[1].Actions[0].Action);
			Assert.AreEqual(2, games[1].Actions[0].Amount);

			Assert.AreEqual("Player2", games[1].Actions[1].Chair.Name);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, games[1].Actions[1].Action);
			Assert.AreEqual(1, games[1].Actions[1].Amount);
		}
	}
}
