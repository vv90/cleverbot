﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;

namespace CleverBot.Common
{
	public class GameTree
	{
		private Game _game;
		//private List<GameTreeNode> _nodes;

		public GameTree(Game game)
		{
			_game = game;
			//_nodes = new List<GameTreeNode>();
		}

		public List<GameTreeNode> GetSuccessors()
		{
			var successors = new List<GameTreeNode>();
			if (_game.Actions.Count == 0)
			{
				var chair = _game.GetNextChair(_game.DealerChair);
				successors.Add(
					new GameTreeNode (
						new PlayerAction(chair, PlayerActionEnum.SmallBlind, _game.SmallBlind)));
			}

			return successors;
		}

		public bool IsActionValid(PlayerAction playerAction)
		{
			var successors = GetSuccessors();
			return successors.Any(s => s.PlayerAction.Chair == playerAction.Chair &&
			                           s.PlayerAction.Action == playerAction.Action &&
			                           Math.Abs(s.PlayerAction.Amount - playerAction.Amount) < 0.001 &&
			                           s.PlayerAction.BetRound == playerAction.BetRound);
		}
	}

	public class GameTreeNode
	{
		private List<GameTreeNode> _successors;

		public PlayerAction PlayerAction { get; set; }

		public GameTreeNode(PlayerAction playerAction)
		{
			PlayerAction = playerAction;
		}
	}
}
