﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using CleverBot.Common.Utilities;

namespace CleverBot.Common
{
	#region poker-eval format reference

	//mask format
	//0xhhhhddddccccssss
	//000A KQJT 9876 5432 | 000A KQJT 9876 5432 | 000A KQJT 9876 5432 | 000A KQJT 9876 5432
	//0000 0000 0000 0000 | 0000 0000 0000 0000 | 0000 0000 0000 0000 | 0000 0000 0000 0000
	//		hearts		  |		 diamonds		|		clubs		  |		  spades		

	//indexes
	//	h		d		c		s
	//2	0		13		26		39
	//3	1		14		27		40
	//4	2		15		28		41
	//5	3		16		29		42
	//6	4		17		30		43
	//7	5		18		31		44
	//8	6		19		32		45
	//9	7		20		33		46
	//T	8		21		34		47
	//J	9		22		35		48
	//Q	10		23		36		49
	//K	11		24		37		50
	//A	12		25		38		51

	#endregion

	#region Enums

	public enum CardRank
	{
		Two = 0,
		Three = 1,
		Four = 2,
		Five = 3,
		Six = 4,
		Seven = 5,
		Eight = 6,
		Nine = 7,
		Ten = 8,
		Jack = 9,
		Queen = 10,
		King = 11,
		Ace = 12
	}

	public enum CardSuit
	{
		Spades = 0,
		Clubs = 1,
		Diamonds = 2,
		Hearts = 3
	}

	#endregion

	[StructLayout(LayoutKind.Explicit)]
	public struct cardsStruct
	{
		[FieldOffset(0)]
		public ushort spades;

		[FieldOffset(2)]
		public ushort clubs;

		[FieldOffset(4)]
		public ushort diamonds;

		[FieldOffset(6)]
		public ushort hearts;
	}

	[StructLayout(LayoutKind.Explicit)]
	public struct CardMask
	{
		[FieldOffset(0)]
		public ulong cards_n;

		[FieldOffset(0)]
		public cardsStruct cards;

		public CardMask(ulong ul)
		{
			cards = new cardsStruct();
			cards_n = ul;
		}

		public CardMask(string maskString)
		{
			CardMask mask = CardMask.Parse(maskString);
			cards = mask.cards;
			cards_n = mask.cards_n;
		}

		public CardMask(CardRank rank, CardSuit suit)
		{
			//CardMask mask = PokerEval.Mask((int)rank + 13 * (int)suit);
			var mask =  (1UL << ((int)rank + ((int)suit * 16)));

			cards = new cardsStruct();
			cards_n = mask;
		}

		public int NumCards()
		{
			return cards_n.CountBits();
		}

		/// <summary>
		/// Determines whether all the cards in the mask have the same suit
		/// </summary>
		/// <returns>True for suited, false for offsuited or empty mask</returns>
		public bool IsSuited()
		{
			if (cards_n == 0)
				return false;

			for (int i = 0; i < 4; i++)
				if ((cards_n & ((ulong)0xFFFF << 16 * i)).CountBits() == cards_n.CountBits())
					return true;

			return false;
		}

		public int GetMaxSuitedCount()
		{
			var maxCount = 0;

			for (int i = 0; i < 4; i++)
			{
				var currentCount = (cards_n & ((ulong) 0xFFFF << 16*i)).CountBits();
				if (currentCount > maxCount)
					maxCount = currentCount;
			}

			return maxCount;
		}

		public int GetRankBits()
		{
			var rankBits = ((cards_n & ((ulong)0xFFFF << 0)) >> 0) |
						   ((cards_n & ((ulong)0xFFFF << 16)) >> 16) |
						   ((cards_n & ((ulong)0xFFFF << 32)) >> 32) |
						   ((cards_n & ((ulong)0xFFFF << 48)) >> 48);

			return (int)rankBits;
		}

		public bool IsAllSameRank()
		{
			return GetRankBits().CountBits() == 1;
		}

		public CardRank[] GetRanks()
		{
			var rankBits = GetRankBits();

			return Enumerable.Range(0, 13)
				.Where(i => (rankBits & (1 << i)) != 0)
				.Select(i => (CardRank)i)
				.ToArray();
		}

		public static CardMask Empty = new CardMask(0UL);

		public static CardMask Parse(string maskString)
		{
			if (maskString == null)
				throw new ArgumentNullException("maskString");

			maskString = maskString
				.Replace("10h", "Th")
				.Replace("10d", "Td")
				.Replace("10c", "Tc")
				.Replace("10s", "Ts");

			CardMask result = new CardMask();
			string[] splitInput = maskString.Split(',', ' ');

			foreach (string s in splitInput)
			{
				if (string.IsNullOrWhiteSpace(s))
					continue;

				try
				{
					result |= MaskStringDictionary[s];
				}
				catch (KeyNotFoundException ex)
				{
					throw new ArgumentException(string.Format("\"{0}\"",s), "maskString", ex);
				}
			}

			return result;
		}

		public static bool TryParse(string maskString, out CardMask cardMask)
		{
			try
			{
				cardMask = Parse(maskString);
				return true;
			}
			catch (Exception)
			{
				cardMask = new CardMask();
				return false;
			}
		}

		public static CardMask operator &(CardMask m1, CardMask m2)
		{
			return new CardMask(m1.cards_n & m2.cards_n);
		}

		public static CardMask operator |(CardMask m1, CardMask m2)
		{
			return new CardMask(m1.cards_n | m2.cards_n);
		}

		public static bool operator ==(CardMask m1, CardMask m2)
		{
			return m1.cards_n == m2.cards_n;
		}

		public static bool operator !=(CardMask m1, CardMask m2)
		{
			return !(m1 == m2);
		}

		public override string ToString()
		{
			var cardMask = this;
			return string.Join(", ", MaskStringDictionary.Keys
				.Where(s => (MaskStringDictionary[s] & cardMask).NumCards() != 0));
		}

		#region String, CardMask dictionary

		public readonly static Dictionary<string, CardMask> MaskStringDictionary = new Dictionary<string, CardMask>() 
		{
			{"2h", new CardMask(0x0001000000000000UL)},
			{"3h", new CardMask(0x0002000000000000UL)},
			{"4h", new CardMask(0x0004000000000000UL)},
			{"5h", new CardMask(0x0008000000000000UL)},
			{"6h", new CardMask(0x0010000000000000UL)},
			{"7h", new CardMask(0x0020000000000000UL)},
			{"8h", new CardMask(0x0040000000000000UL)},
			{"9h", new CardMask(0x0080000000000000UL)},
			{"Th", new CardMask(0x0100000000000000UL)},
			{"Jh", new CardMask(0x0200000000000000UL)},
			{"Qh", new CardMask(0x0400000000000000UL)},
			{"Kh", new CardMask(0x0800000000000000UL)},
			{"Ah", new CardMask(0x1000000000000000UL)},
			{"2d", new CardMask(0x0000000100000000UL)},
			{"3d", new CardMask(0x0000000200000000UL)},
			{"4d", new CardMask(0x0000000400000000UL)},
			{"5d", new CardMask(0x0000000800000000UL)},
			{"6d", new CardMask(0x0000001000000000UL)},
			{"7d", new CardMask(0x0000002000000000UL)},
			{"8d", new CardMask(0x0000004000000000UL)},
			{"9d", new CardMask(0x0000008000000000UL)},
			{"Td", new CardMask(0x0000010000000000UL)},
			{"Jd", new CardMask(0x0000020000000000UL)},
			{"Qd", new CardMask(0x0000040000000000UL)},
			{"Kd", new CardMask(0x0000080000000000UL)},
			{"Ad", new CardMask(0x0000100000000000UL)},
			{"2c", new CardMask(0x0000000000010000UL)},
			{"3c", new CardMask(0x0000000000020000UL)},
			{"4c", new CardMask(0x0000000000040000UL)},
			{"5c", new CardMask(0x0000000000080000UL)},
			{"6c", new CardMask(0x0000000000100000UL)},
			{"7c", new CardMask(0x0000000000200000UL)},
			{"8c", new CardMask(0x0000000000400000UL)},
			{"9c", new CardMask(0x0000000000800000UL)},
			{"Tc", new CardMask(0x0000000001000000UL)},
			{"Jc", new CardMask(0x0000000002000000UL)},
			{"Qc", new CardMask(0x0000000004000000UL)},
			{"Kc", new CardMask(0x0000000008000000UL)},
			{"Ac", new CardMask(0x0000000010000000UL)},
			{"2s", new CardMask(0x0000000000000001UL)},
			{"3s", new CardMask(0x0000000000000002UL)},
			{"4s", new CardMask(0x0000000000000004UL)},
			{"5s", new CardMask(0x0000000000000008UL)},
			{"6s", new CardMask(0x0000000000000010UL)},
			{"7s", new CardMask(0x0000000000000020UL)},
			{"8s", new CardMask(0x0000000000000040UL)},
			{"9s", new CardMask(0x0000000000000080UL)},
			{"Ts", new CardMask(0x0000000000000100UL)},
			{"Js", new CardMask(0x0000000000000200UL)},
			{"Qs", new CardMask(0x0000000000000400UL)},
			{"Ks", new CardMask(0x0000000000000800UL)},
			{"As", new CardMask(0x0000000000001000UL)}
		};

		#endregion
	}
}
