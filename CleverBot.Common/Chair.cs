﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;

namespace CleverBot.Common
{
	public class Chair
	{
		public Chair()
		{
		}

		public Chair(int chairNumber, double balance, CardMask hand, string name)
		{
			ChairNumber = chairNumber;
			Balance = balance;
			Hand = hand; 
			Name = name;
			//IsDealer = isDealer;
			//IsHero = isHero;
			//Position = position;
		}

		public int ChairNumber { get; set; }
		public double Balance { get; set; }
		public double CurrentBalance { get; set; }
		public CardMask Hand { get; set; }
		public string Name { get; set; }
		//public bool IsDealer { get; set; }
		//public bool IsHero { get; set; }
		public Player Player { get; set; }
		//public Position Position { get; set; }

		public override string ToString()
		{
			return string.Format("{{{0}: [{1}] {2} [{3}]}}", ChairNumber, Hand.NumCards() > 0 ? Hand.ToString() : "", Name, Balance);
		}
	}
}
