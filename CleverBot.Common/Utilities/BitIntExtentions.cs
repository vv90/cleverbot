﻿using System;
using System.Linq;

namespace CleverBot.Common.Utilities
{
	public static class BitIntExtentions
	{
		public static string BitString(this UInt64 n, int limit = 64, int tabs = 4, bool reverse = false)
		{
			string sReturn = "";
			for (int i = limit - 1; i >= 0; i--)
			{
				if ((n & ((UInt64)1 << i)) != 0)
					sReturn += "1";
				else
					sReturn += "0";

				if (tabs != 0 && i % tabs == 0)
					sReturn += " ";
			}

			if (reverse)
				return new string(sReturn.Reverse().ToArray());
			else
				return sReturn;
		}

		public static string BitString(this int n, int show = 32, int tabs = 4, bool reverse = false)
		{
			return ((UInt64)n).BitString(show, tabs, reverse);
		}

		public static int CountBits(this ulong value)
		{
			value = value - ((value >> 1) & 0x5555555555555555);
			value = (value & 0x3333333333333333) + ((value >> 2) & 0x3333333333333333);
			value = (value & 0xf0f0f0f0f0f0f0f) + ((value >> 4) & 0xf0f0f0f0f0f0f0f);
			value = (value & 0xff00ff00ff00ff) + ((value >> 8) & 0xff00ff00ff00ff);
			value = (value & 0xffff0000ffff) + ((value >> 16) & 0xffff0000ffff);
			return (int)((value & 0xffffffff) + ((value >> 32) & 0xffffffff));
		}

		public static int CountBits(this int value)
		{
			return CountBits((ulong)value);
		}

		public static int CountBits(this uint value)
		{
			return CountBits((ulong)value);
		}

		public static int BitLog2(this int v)
		{
			return BitLog2((ulong)v);
		}

		public static int BitLog2(this uint v)
		{
			return BitLog2((ulong)v);
		}

		public static int BitLog2(this ulong v)
		{
			if (v == 0)
				throw new ArgumentException("Cannot take Log2(0)");

			ulong r; // result of log2(v) will go here
			ulong shift;

			r = (v > 0xFFFFFFFF ? (ulong)1 : 0) << 5; v >>= (int)r;
			shift = (v > 0xFFFF ? (ulong)1 : 0) << 4; v >>= (int)shift; r |= shift;
			shift = (v > 0xFF ? (ulong)1 : 0) << 3; v >>= (int)shift; r |= shift;
			shift = (v > 0xF ? (ulong)1 : 0) << 2; v >>= (int)shift; r |= shift;
			shift = (v > 0x3 ? (ulong)1 : 0) << 1; v >>= (int)shift; r |= shift;
			r |= (v >> 1);

			return (int)r;
		}
	}
}
