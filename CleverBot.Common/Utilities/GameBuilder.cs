﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CleverBot.Common.Enums;
using CleverBot.Common.Exceptions;

namespace CleverBot.Common.Utilities
{
	public class GamePropertyValue
	{
		public GamePropertyValue(GameProperty property, object value)
		{
			//var propertyType = GameProperty.GamePropertyTypeDictionary[property];
			if (value != null && property.Type != value.GetType())
			{
				throw new Exception("Invalid property type");
			}

			Property = property;
			Value = value;
		}

		public GameProperty Property { get; private set; }
		public object Value { get; private set; }

		public override string ToString()
		{
			return string.Format("{{{0}: {1}}}", Property, Value);
		}
	}

	public class GameBuilder
	{
		public int AssignmentCount { get; private set; }
		public Game Game { get; private set; }

		public GameBuilder()
		{
			Game = new Game();
		}

		public GameBuilder(Game game)
		{
			Game = game;
		}

		public void SetProperties(params GamePropertyValue[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}

			AssignmentCount += 1;

			var chairNumberPropertyValue = values.FirstOrDefault(p => p.Property == GameProperty.ChairNumber);
			var playerNamePropertyValue = values.FirstOrDefault(p => p.Property == GameProperty.PlayerName);

			Chair chair = null;
			if (chairNumberPropertyValue != null)
			{
				chair = CreateChairIfNotExists(Game, (int) chairNumberPropertyValue.Value);
			}
			else if (playerNamePropertyValue != null)
			{
				chair = Game.GetChair((string) playerNamePropertyValue.Value);
			}

			PlayerAction action = null;

			foreach (var val in values)
			{
				if (val.Property.Level == GamePropertyLevel.Game)
				{
					if (!GamePropertySetters.ContainsKey(val.Property))
					{
						throw new KeyNotFoundException(
							"Game property was not found in the default setters dictionary");
					}

					GamePropertySetters[val.Property](Game, val.Value);
				}
				else if (val.Property.Level == GamePropertyLevel.Chair)
				{
					if (!ChairPropertySetters.ContainsKey(val.Property))
					{
						throw new KeyNotFoundException(
							"Chair property was not found in the default setters dictionary");
					}
					if (chair == null)
					{
						throw new IncompleteInformationException("Chair could not be determined");
					}

					ChairPropertySetters[val.Property](chair, Game, val.Value);
				}
				else if (val.Property.Level == GamePropertyLevel.Action)
				{
					if (!PlayerActionPropertySetters.ContainsKey(val.Property))
					{
						throw new KeyNotFoundException(
							"PlayerAction property was not found in the default setters dictionary");
					}
					if (chair == null)
					{
						throw new IncompleteInformationException("Chair could not be determined");
					}
					if (action == null)
					{
						action = new PlayerAction { Chair = chair };
						Game.Actions.Add(action);
					}

					PlayerActionPropertySetters[val.Property](action, Game, val.Value);
				}
			}
		}

		private static Chair CreateChairIfNotExists(Game game, int chairNumber)
		{
			var chair = game.GetChair(chairNumber);
			if (chair == null)
			{
				chair = new Chair { ChairNumber = chairNumber };
				game.Chairs.Add(chair);
			}

			return chair;
		}

		private static Dictionary<int, Tuple<double, double>> GameLimitsDictionary =
			new Dictionary<int, Tuple<double, double>>
			{
				{2, new Tuple<double, double>(0.02, 0.01)},
				{4, new Tuple<double, double>(0.04, 0.02)},
				{5, new Tuple<double, double>(0.05, 0.02)},
				{10, new Tuple<double, double>(0.1, 0.05)},
				{16, new Tuple<double, double>(0.16, 0.08)},
				{25, new Tuple<double, double>(0.25, 0.1)},
				{50, new Tuple<double, double>(0.5, 0.25)},
				{100, new Tuple<double, double>(1.0, 0.5)},
				{200, new Tuple<double, double>(2, 1)},
				{300, new Tuple<double, double>(3, 1.5)},
				{400, new Tuple<double, double>(4, 2)},
				{500, new Tuple<double, double>(5, 2.5)},
			};

		private static readonly Dictionary<GameProperty, Action<Game, object>> GamePropertySetters =
			new Dictionary<GameProperty, Action<Game, object>>
			{
				{
					GameProperty.GameStartDateTime,
					(game, startTime) => { game.StartTime = (DateTime) startTime; }
				},
				{
					GameProperty.GameTitle,
					(game, title) => { game.Title = (string) title; }
				},
				{
					GameProperty.GameId, 
					(game, gameId) => { game.GameNumber = (string) gameId; }
				},
				{
					GameProperty.SmallBlind, 
					(game, smallBlind) => { game.SmallBlind = (double) smallBlind; }
				},
				{
					GameProperty.BigBlind, 
					(game, bigBlind) => { game.BigBlind = (double) bigBlind; }
				},
				{
					GameProperty.Board,
					(game, board) =>
					{
						var boardString = Regex.Replace((string)board, @"[\W]", " ");

						var cards = boardString.Split(',', ' ')
							.Where(s => !string.IsNullOrWhiteSpace(s))
							.Select(s => new CardMask(s))
							.ToArray();

						//var boardMask = cards.Aggregate(game.Board, (mask, item) => mask |= item);

						if (cards.Aggregate(game.Board, (mask, item) => mask |= item).NumCards() > 5)
							throw new ArgumentException("Invalid number of cards on the board");

						if (cards.Length >= 3)
						{
							game.Flop = cards[0] | cards[1] | cards[2];
						}

						if (cards.Length > 3)
							game.Turn = cards[3];

						if(cards.Length > 4)
							game.River = cards[4];

						if (cards.Length != 1 || game.Flop.NumCards() != 3)
							return;

						if (game.Turn.NumCards() == 1 &&
							game.River.NumCards() == 0)
						{
							game.River = cards[0];
						}

						if (game.Turn.NumCards() == 0 &&
						    game.River.NumCards() == 0)
						{
							game.Turn = cards[0];
						}
					}
				},
				//{
				//	GameProperty.BetRound,
				//	(game, betRound) => { game.BetRound = (BetRound) betRound; }
				//},
				{
					GameProperty.DealerChair,
					(game, dealerChair) => { game.DealerChair = (int) dealerChair; }
				},
				{
					GameProperty.GameLimit,
					(game, gameLimit) =>
					{
						var lim = (double) gameLimit;
						if (GameLimitsDictionary.ContainsKey((int) lim))
						{
							game.BigBlind = GameLimitsDictionary[(int) lim].Item1;
							game.SmallBlind = GameLimitsDictionary[(int) lim].Item2;
						}
						else
						{
							game.BigBlind = lim/100;
							game.SmallBlind = lim/200;
						}
					}
				},
				{
					GameProperty.NoProperty,
					(game, noProperty) => { }
				}
			};

		private static readonly Dictionary<GameProperty, Action<Chair, Game, object>> ChairPropertySetters =
			new Dictionary<GameProperty, Action<Chair, Game, object>>
			{
				{
					GameProperty.ChairNumber, 
					(chair, game, number) => { chair.ChairNumber = (int) number; }
				},
				{
					GameProperty.PlayerName,
					(chair, game, name) => { chair.Name = (string) name; }
				},
				{
					GameProperty.Balance, 
					(chair, game, balance) => { chair.Balance = (double) balance; }
				},
				//{
				//	GameProperty.Position, 
				//	(chair, game, position) => { chair.IsDealer = (Position) position == Position.BU; }
				//},
				{
					GameProperty.Hand,
					(chair, game, hand) =>
					{
						var cardMask = (CardMask) hand;
						if (cardMask.NumCards() == 1 && chair.Hand.NumCards() == 1)
						{
							chair.Hand |= cardMask;
						}
						else
						{
							chair.Hand = cardMask;
						}
					}
				}
			};

		private static readonly Dictionary<GameProperty, Action<PlayerAction, Game, object>> PlayerActionPropertySetters =
			new Dictionary<GameProperty, Action<PlayerAction, Game, object>>
			{
				{
					GameProperty.Action,
					(playerAction, game, action) =>
					{
						playerAction.Action = (PlayerActionEnum) action;
						playerAction.BetRound = game.BetRound;
					}
				},
				{
					GameProperty.BetAmount,
					(playerAction, game, amount) =>
					{
						var lastAction = game.Actions
							.Where(a => a.BetRound == playerAction.BetRound)
							.LastOrDefault(a => a.Chair == playerAction.Chair && a != playerAction);

						if (lastAction != null)
							playerAction.Amount = (double) amount + lastAction.Amount;
						else
							playerAction.Amount = (double) amount;


						//if (playerAction.Action == PlayerActionEnum.Call)
						//	playerAction.Amount += (double) amount;
						//else
						//	playerAction.Amount = (double) amount;
					}
				}
			};
	}
}
