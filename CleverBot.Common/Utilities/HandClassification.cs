﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.Common.Utilities
{
	public partial class HandClassification
	{
		public static int ClassifyHand(CardMask hand)
		{
			if(hand.NumCards() != 2)
				throw new ArgumentException("Card mask should contain 2 cards in order to be classified as hand", "hand");

			return HandClassesDictionary[hand];
		}
	}
}
