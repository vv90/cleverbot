﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.Common.Exceptions
{
	public class GameUpdateException : Exception
	{
		public GameUpdateException()
			: base()
		{
		}

		public GameUpdateException(string message)
			: base(message)
		{
			
		}
	}

	public class UnexpectedPropertyClassType : GameUpdateException
	{
		public UnexpectedPropertyClassType()
			: base()
		{	
		}

		public UnexpectedPropertyClassType(string message)
			: base()
		{
		}
	}

	public class IncompleteInformationException : GameUpdateException
	{
		public IncompleteInformationException()
			: base()
		{
		}

		public IncompleteInformationException(string message)
			: base(message)
		{
		}
	}
}
