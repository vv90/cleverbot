﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using CleverBot.Common.Enums;
using CleverBot.Common.Exceptions;

namespace CleverBot.Common
{
	public class Game
	{
		//public static readonly Position[][] PositionTable = 
		//{
		//	new Position[]{},
		//	new []{ Position.NotDefined },
		//	new []{ Position.SB, Position.BB },
		//	new []{ Position.BU, Position.SB, Position.BB },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.CO },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.MP, Position.CO },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.MP, Position.CO },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.MP, Position.MP, Position.CO },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.UTG, Position.MP, Position.MP, Position.CO },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.UTG, Position.MP, Position.MP, Position.MP, Position.CO },
		//	new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.UTG, Position.UTG, Position.MP, Position.MP, Position.MP, Position.CO }
		//};

		public Game()
		{
			Chairs = new List<Chair>();
			Actions = new List<PlayerAction>();
		}

		//public Game(long id, string title, DateTime startTime, double smallBlind, double bigBlind, 
		//	CardMask board, List<Chair> chairs, List<PlayerAction> actions)
		//	: this()
		//{
		//	Id = id;
		//	Title = title;
		//	StartTime = startTime;
		//	SmallBlind = smallBlind;
		//	BigBlind = bigBlind;
		//	Board = board;
		//	Chairs = chairs;
		//	Actions = actions;
		//}

		public string GameNumber { get; set; }
		public string Title { get; set; }
		public DateTime StartTime { get; set; }
		public double SmallBlind { get; set; }
		public double BigBlind { get; set; }
		public int DealerChair { get; set; }
		public int HeroChair { get; set; }
		public CardMask Flop { get; set; }
		public CardMask Turn { get; set; }
		public CardMask River { get; set; }
		public CardMask Board { get { return Flop | Turn | River; } }
		public List<Chair> Chairs { get; set; }
		public List<PlayerAction> Actions { get; set; }



		public BetRound BetRound
		{
			get
			{
				if(Board.NumCards() == 0)
					return BetRound.Preflop;
				else if (Board.NumCards() == 3)
					return BetRound.Flop;
				else if (Board.NumCards() == 4)
					return BetRound.Turn;
				else if (Board.NumCards() == 5)
					return BetRound.River;
				else
					throw new Exception("Incorrect number of cards");
			}
		}


		public Chair GetHeroChair()
		{
			return GetChair(HeroChair);
		}

		public Chair GetDealerChair()
		{
			return GetChair(DealerChair);
		}

		public Chair GetChair(string name)
		{
			return Chairs.FirstOrDefault(c => c.Name == name);
		}

		public Chair GetChair(int chairNumber)
		{
			return Chairs.FirstOrDefault(c => c.ChairNumber == chairNumber);
		}

		public Chair GetNextChair(int chairNumber)
		{
			if(Chairs.All(c => c.ChairNumber != chairNumber))
				throw new ArgumentException("Chair not found", "chairNumber");

			var orderedChairs = Chairs.OrderBy(c => c.ChairNumber).ToList();
			var chairIndex = orderedChairs.IndexOf(GetChair(chairNumber));
			var nextChair = orderedChairs[(chairIndex + 1)%orderedChairs.Count];
			return nextChair;
		}

		public int DealerOffset(int chairNumber)
		{
			var offset = 0;
			var orderedChairs = Chairs.OrderBy(c => c.ChairNumber).ToList();
			var dealerChairIndex = orderedChairs.IndexOf(GetChair(DealerChair));
			while (offset < Chairs.Count)
			{
				var currentChair = orderedChairs[(dealerChairIndex + offset)%orderedChairs.Count];
				if (currentChair.ChairNumber == chairNumber)
				{
					return offset;
				}

				offset += 1;
			}

			throw new ArgumentException("Chair not found", "chairNumber");
		}

		public double EffectiveStack()
		{
			var playingOpponents = Chairs
				.Where(c => c.ChairNumber != HeroChair)
				.Where(c => Actions
					.Where(a => a.Chair.ChairNumber == c.ChairNumber)
					.All(a => a.Action != PlayerActionEnum.Fold));
				

			return Math.Min(playingOpponents.Max(a => a.Balance), GetChair(HeroChair).Balance);
		}


		public override string ToString()
		{
			var heroChair = GetChair(HeroChair);
			return string.Format("{{{0}: [{1}][{2}]}}", GameNumber, heroChair == null ? "" : heroChair.Hand.ToString(), Board);
		}

		public string[] CreateLog()
		{
			var lines = Chairs
				.OrderBy(c => c.ChairNumber)
				.Select(c => string.Format("Chair {0}{1}: {2} ({3}) {4}", 
					c.ChairNumber, 
					DealerChair == c.ChairNumber ? "*" : "", 
					c.Name,
					c.Balance, 
					c.Hand.NumCards() == 2 ? c.Hand.ToString() : ""))
				.ToArray();

			return lines;
		}

		public Game Clone()
		{
			var clone = (Game)this.MemberwiseClone();
			return clone;
		}
	}
}
