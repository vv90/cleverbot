﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.Common
{
	public enum Position
	{
		NotDefined = 0,
		BU = 1,
		SB = 2,
		BB = 3,
		UTG = 4,
		MP = 5,
		CO = 6
	}

	public class PositionUtils
	{
		public static readonly Position[][] PositionTable = 
		{
			new Position[]{},
			new []{ Position.NotDefined },
			new []{ Position.SB, Position.BB },
			new []{ Position.BU, Position.SB, Position.BB },
			new []{ Position.BU, Position.SB, Position.BB, Position.CO },
			new []{ Position.BU, Position.SB, Position.BB, Position.MP, Position.CO },
			new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.MP, Position.CO },
			new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.MP, Position.MP, Position.CO },
			new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.UTG, Position.MP, Position.MP, Position.CO },
			new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.UTG, Position.MP, Position.MP, Position.MP, Position.CO },
			new []{ Position.BU, Position.SB, Position.BB, Position.UTG, Position.UTG, Position.UTG, Position.MP, Position.MP, Position.MP, Position.CO }
		};

		public static Position GetPosition(int numberOfPlayers, int dealerOffset)
		{
			return numberOfPlayers == 0 
				? Position.NotDefined 
				: PositionTable[numberOfPlayers][dealerOffset];
		}
	}
}
