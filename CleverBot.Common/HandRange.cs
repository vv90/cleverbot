﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace CleverBot.Common
{
	public partial class HandRange : IEnumerable<CardMask>
	{
		#region Fields

		private IntPtr _pthis;
		private int _count;

		#region Static

		public const int MaxCount = 1326;
		static readonly List<char> Ranks = new List<char> 
			{ '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A' };
		static readonly int[][] AllSuits = new int[][] 
			{ 
				new int[] {0, 0}, new int[] {0, 1}, new int[] {0, 2}, new int[] {0, 3},
				new int[] {1, 0}, new int[] {1, 1}, new int[] {1, 2}, new int[] {1, 3},
				new int[] {2, 0}, new int[] {2, 1}, new int[] {2, 2}, new int[] {2, 3},
				new int[] {3, 0}, new int[] {3, 1}, new int[] {3, 2}, new int[] {3, 3}
			};
		static readonly int[][] Suited = new int[][]
			{
				new int[] {0, 0}, new int[] {1, 1}, new int[] {2, 2}, new int[] {3, 3}
			};
		static readonly int[][] Offsuited = new int[][]
			{
				new int[] {0, 1}, new int[] {0, 2}, new int[] {0, 3},
				new int[] {1, 0}, new int[] {1, 2}, new int[] {1, 3},
				new int[] {2, 0}, new int[] {2, 1}, new int[] {2, 3},
				new int[] {3, 0}, new int[] {3, 1}, new int[] {3, 2}
			};
		static readonly int[][] PairSuites = new int[][]
			{
				new int[] {0, 1}, new int[] {0, 2}, new int[] {0, 3},
				new int[] {1, 2}, new int[] {1, 3},
				new int[] {2, 3}
			};

		static readonly Regex singleHandRegEx = new Regex(@"^([AKQJT98765432][cdhs]){2}$");
		static readonly Regex singleSidedRangeRegEx = new Regex(@"^([AKQJT98765432]{2}[so]?[+-]?)$");
		static readonly Regex doubleSidedRangeRegEx = new Regex(@"^([AKQJT98765432]{2}[so]?-[AKQJT98765432]{2}[so]?)$");

		#endregion

		#endregion

		#region Constructor

		public HandRange()
		{
			unsafe
			{
				_pthis = Marshal.AllocHGlobal(sizeof(CardMask) * MaxCount);
			}

			_count = 0;
		}

		~HandRange()
		{
			Marshal.FreeHGlobal(_pthis);
		}

		#endregion

		#region Public

		/// <summary>
		/// Parses hand range from the input string
		/// </summary>
		/// <param name="input">String format is:
		/// XxYy - (AcAd) exact hand
		/// XY - (AK) all hands with A and K of any suit
		/// XYs - (K6s) suited hands
		/// XYo - (54o) offsuited hands
		/// XY+ (K2+) all hands with K and (2..Q) of any suit (may also be used with "s" and "o" modifiers)
		/// XX+ any pocket pair f X and higher
		/// XY-XZ - (AK-A3) specified range
		/// following expressions are also valid: XX-YY, XYs-XZs, XYo-XZo, XY-;
		/// </param>
		/// <returns></returns>
		public static HandRange Parse(string input)
		{
			if (input == null)
				throw new ArgumentNullException("input");

			if (input.Length < 2)
				throw new ArgumentException("input");

			input = input.Replace("Broadway", "TT+, ATs+, KTs+, QTs+, JTs, ATo+, KTo+, QTo+, JTo");

			//if (input.Length == 0)
			//	throw new ArgumentException("input");
			//
			//if (!singleHandRegEx.IsMatch(input) &&
			//	!singleSidedRangeRegEx.IsMatch(input) &&
			//	!doubleSidedRangeRegEx.IsMatch(input))
			//{
			//	throw new ArgumentException("input");
			//}

			HandRange result = new HandRange();

			string[] inputParts = input.Replace("\n","").Replace(" ", "").Split(',');

			foreach (string str in inputParts)
				result.AddRange(ParseSingleRange(str));

			return result;
		}

		public static HandRange StdRange()
		{
			HandRange result = new HandRange();

			unsafe
			{
				CardMask* pfirst = result.Pfirst();

				for (int i = 0; i < HandRange.MaxCount; i++)
					pfirst[i] = new CardMask(HandRange.StdRangeHexTable[i]);

				result._count = HandRange.MaxCount;
			}

			return result;
		}

		/// <summary>
		/// Gets current number of masks in range
		/// </summary>
		public int Count
		{
			get { return _count; }
		}

		public unsafe CardMask* Pfirst()
		{
			return (CardMask*)_pthis.ToPointer();
		}

		/// <summary>
		/// Adds specifyed hand to a hand range if that range does'n contain such hand already
		/// </summary>
		/// <param name="hand"></param>
		public void Add(CardMask handMask)
		{
			if (handMask.NumCards() != 2)
				throw new ArgumentException("handMask numCards is not 2");

			if (this.Count == 1326)
				return;

			unsafe
			{
				CardMask* pfirst = this.Pfirst();

				for (int i = 0; i < _count; i++)
					if (pfirst[i].cards_n == handMask.cards_n)
						return;

				pfirst[_count++] = handMask;
			}
		}

		public void AddRange(HandRange range)
		{
			foreach (CardMask mask in range)
				this.Add(mask);
		}

		public CardMask Get(int index)
		{
			if (index >= _count)
				throw new IndexOutOfRangeException();

			unsafe
			{
				CardMask* pfirst = this.Pfirst();

				return pfirst[index];
			}
		}

		/// <summary>
		/// Returns index of specified hand mask if the range contains it, otherwise returns -1
		/// </summary>
		/// <param name="handMask"></param>
		/// <returns></returns>
		public int IndexOf(CardMask handMask)
		{
			if (handMask.NumCards() != 2)
				return -1;

			unsafe
			{
				CardMask* pfirst = this.Pfirst();

				for (int i = 0; i < _count; i++)
					if (pfirst[i].cards_n == handMask.cards_n)
						return i;
			}

			return -1;
		}

		#endregion

		#region Class invariants

		public bool Invariant()
		{
			if (this._count < 0 || this._count > HandRange.MaxCount)
				return false;

			if (this._pthis == null)
				return false;

			unsafe
			{
				CardMask* pfirst = this.Pfirst();

				for (int i = 0; i < _count; i++)
					for (int j = i + 1; j < _count; j++)
						if (pfirst[i].cards_n == pfirst[j].cards_n)
							return false;
			}

			return true;
		}

		#endregion

		#region IEnumerator

		public IEnumerator<CardMask> GetEnumerator()
		{
			return Enumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return Enumerator();
		}

		IEnumerator<CardMask> Enumerator()
		{
			for (int i = 0; i < this.Count; i++)
				yield return this.Get(i);
			yield break;
		}

		#endregion

		#region Helper methods

		private static HandRange ParseSingleRange(string input)
		{
			if (input == null)
				throw new ArgumentNullException("input");
			if (input[0] == input[1] && input.Contains('s'))
				throw new ArgumentException(input, "input");

			HandRange result = new HandRange();

			if (singleHandRegEx.IsMatch(input))
			{
				result.Add(CardMask.Parse(input.Substring(0, 2)) | CardMask.Parse(input.Substring(2, 2)));
			}
			else if (singleSidedRangeRegEx.IsMatch(input))
			{
				RangeWireframe wireframe = new RangeWireframe(input);

				if (wireframe.rankLo == wireframe.rankHi &&
					wireframe.suitCombinations != PairSuites)
					throw new ArgumentException(input, "input");

				bool isPair = (wireframe.rankLo == wireframe.rankHi);
				int rankHi = isPair ? 13 : wireframe.rankHi;

				if (input.Contains('+'))
					while (wireframe.rankLo < rankHi)
					{
						result.AddRange(wireframe.GenerateRange());

						wireframe.rankLo++;
						if (isPair)
							wireframe.rankHi++;
					}
				else if (input.Contains('-'))
					while (wireframe.rankLo > -1)
					{
						result.AddRange(wireframe.GenerateRange());

						wireframe.rankLo--;
						if (isPair)
							wireframe.rankHi--;
					}
				else
					result.AddRange(wireframe.GenerateRange());

			}
			else if (doubleSidedRangeRegEx.IsMatch(input))
			{
				string[] upperLower = input.Split('-');

				if (upperLower.Length != 2)
					throw new ArgumentException(input, "input");

				RangeWireframe lower = new RangeWireframe(upperLower[0]);
				RangeWireframe upper = new RangeWireframe(upperLower[1]);

				if (lower.rankLo > upper.rankLo)
				{
					RangeWireframe temp = lower;
					lower = upper;
					upper = temp;
				}

				if (lower.suitCombinations != upper.suitCombinations)
					throw new ArgumentException(input, "input");

				if (lower.rankHi == upper.rankHi)
				{
					while (upper.rankLo >= lower.rankLo)
					{
						result.AddRange(upper.GenerateRange());
						upper.rankLo -= 1;
					}
				}
				else if (lower.rankHi - lower.rankLo == upper.rankHi - upper.rankLo)
				{
					while (upper.rankLo >= lower.rankLo)
					{
						result.AddRange(upper.GenerateRange());
						upper.rankLo -= 1;
						upper.rankHi -= 1;
					}
				}
				else
					throw new ArgumentException(input, "input");
			}
			else
				throw new ArgumentException(input, "input");

			return result;
		}

		#endregion

		#region Helper structures

		struct RangeWireframe
		{
			public int rankHi;
			public int rankLo;
			//public bool? isSuited;
			///// <summary>
			///// true for "+", false for "-", null for "none"
			///// </summary>
			//public bool? boundaryModifier;

			public int[][] suitCombinations;

			public RangeWireframe(string input)
			{
				//boundaryModifier = null;
				//isSuited = null;

				rankHi = Ranks.IndexOf(input[0]);
				rankLo = Ranks.IndexOf(input[1]);

				if (rankHi < rankLo)
				{
					var temp = rankHi;
					rankHi = rankLo;
					rankLo = temp;
				}

				if (rankHi == rankLo)
					suitCombinations = PairSuites;
				else if (input.Contains('s'))
					suitCombinations = Suited;
				else if (input.Contains('o'))
					suitCombinations = Offsuited;
				else
					suitCombinations = AllSuits;
			}

			public HandRange GenerateRange()
			{
				HandRange result = new HandRange();

				foreach (int[] suits in suitCombinations)
				{
					result.Add(new CardMask((CardRank)rankHi, (CardSuit)suits[0]) | new CardMask((CardRank)rankLo, (CardSuit)suits[1]));
						//PokerEval.Mask(suits[0] * 13 + rankHi) |
								//PokerEval.Mask(suits[1] * 13 + rankLo));
				}

				return result;
			}
		}

		#endregion
	}
}
