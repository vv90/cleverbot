﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleverBot.Common.Enums
{
	public enum PlayerActionEnum
	{
		Fold,
		Check,
		SmallBlind,
		BigBlind,
		Straddle,
		Post,
		Call,
		Bet,
		Raise,
		Cap,
		Allin
	}
}
