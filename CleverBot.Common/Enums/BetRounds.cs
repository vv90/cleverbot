﻿namespace CleverBot.Common.Enums
{
	public enum BetRound
	{
		Preflop,
		Flop,
		Turn,
		River
	}
}
