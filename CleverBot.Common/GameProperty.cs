﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;

namespace CleverBot.Common
{
	//public enum GamePropertyEnum
	//{
	//	GameId = 0,
	//	GameStartDateTime,
	//	SmallBlind,
	//	BigBlind,
	//	ChairNumber,
	//	PlayerName,
	//	Balance,
	//	Action,
	//	Position
	//}

	public enum GamePropertyLevel
	{
		Game = 0,
		Chair,
		Action
	}

	public class GameProperty
	{
		private GameProperty()
		{
		}

		public string Name { get; private set; }
		public Type Type { get; private set; }
		public GamePropertyLevel Level { get; private set; }


		public static readonly GameProperty GameId = new GameProperty
		{
			Name = "GameId",
			Type = typeof (string),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty GameStartDateTime = new GameProperty
		{
			Name = "GameStartDateTime",
			Type = typeof (DateTime),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty GameTitle = new GameProperty
		{
			Name = "GameTitle",
			Type = typeof (string),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty GameLimit = new GameProperty
		{
			Name = "GameLimit",
			Type = typeof(double),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty SmallBlind = new GameProperty
		{
			Name = "SmallBlind",
			Type = typeof (double),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty BigBlind = new GameProperty
		{
			Name = "BigBlind",
			Type = typeof (double),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty Board = new GameProperty
		{
			Name = "Board",
			Type = typeof(string),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty BetRound = new GameProperty
		{
			Name = "BetRound",
			Type = typeof(BetRound),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty DealerChair = new GameProperty
		{
			Name = "DealerChair",
			Type = typeof (int),
			Level = GamePropertyLevel.Game
		};

		public static readonly GameProperty ChairNumber = new GameProperty
		{
			Name = "ChairNumber",
			Type = typeof (int),
			Level = GamePropertyLevel.Chair
		};

		public static readonly GameProperty PlayerName = new GameProperty
		{
			Name = "PlayerName",
			Type = typeof (string),
			Level = GamePropertyLevel.Chair
		};

		public static readonly GameProperty Balance = new GameProperty
		{
			Name = "Balance",
			Type = typeof (double),
			Level = GamePropertyLevel.Chair
		};

		//public static readonly GameProperty Position = new GameProperty
		//{
		//	Name = "Position",
		//	Type = typeof(Position),
		//	Level = GamePropertyLevel.Chair
		//};

		public static readonly GameProperty Hand = new GameProperty
		{
			Name = "Hand",
			Type = typeof(CardMask),
			Level = GamePropertyLevel.Chair
		};

		public static readonly GameProperty Action = new GameProperty
		{
			Name = "Action",
			Type = typeof (PlayerActionEnum),
			Level = GamePropertyLevel.Action
		};
		
		public static readonly GameProperty BetAmount = new GameProperty
		{
			Name = "BetAmount",
			Type = typeof(double),
			Level = GamePropertyLevel.Action
		};

		public static readonly GameProperty NoProperty = new GameProperty
		{
			Name = "NoProperty",
			Type = typeof(string),
			Level = GamePropertyLevel.Game
		};

		public override string ToString()
		{
			return string.Format("[{0}]{1}", Type.Name, Name);
		}
	}
}
