﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;

namespace CleverBot.Common
{
	public class PlayerAction
	{
		public PlayerAction()
		{
		}

		public PlayerAction(Chair chair, PlayerActionEnum action, double amount = 0, BetRound betRound = BetRound.Preflop)
		{
			//PlayerName = playerName;
			Chair = chair;
			Action = action;
			Amount = amount;
			BetRound = betRound;
		}

		public Chair Chair { get; set; }
		//public string PlayerName { get; set; }
		public PlayerActionEnum Action { get; set; }
		public double Amount { get; set; }
		public BetRound BetRound { get; set; }

		public static PlayerAction FirstAction(Chair chair, double amount, BetRound betRound)
		{
			PlayerActionEnum action;

			if (betRound == BetRound.Preflop)
				action = PlayerActionEnum.SmallBlind;
			else if (amount > 0)
				action = PlayerActionEnum.Bet;
			else 
				action = PlayerActionEnum.Check;

			return new PlayerAction(chair, action, amount, betRound);
		}

		public bool IsAgressive()
		{
			return Action == PlayerActionEnum.BigBlind ||
			       Action == PlayerActionEnum.Bet ||
			       Action == PlayerActionEnum.Raise ||
			       Action == PlayerActionEnum.Cap ||
			       Action == PlayerActionEnum.Allin;
		}

		public PlayerAction RaiseAction(Chair chair, double amount)
		{
			if(chair.ChairNumber == Chair.ChairNumber)
				throw new ArgumentException("Chair cannot be the same", "chair");

			PlayerActionEnum action;

			if(Amount - amount > 0.001 )
				throw new ArgumentException("Amount cannot be reduced", "amount");
			else if (Math.Abs(Amount - amount) < 0.001)
			{
				action = PlayerActionEnum.Call;
			}
			else
			{
				switch (Action)
				{
					case PlayerActionEnum.SmallBlind:
						action = PlayerActionEnum.BigBlind;
						break;
					case PlayerActionEnum.Check:
						action = PlayerActionEnum.Bet;
						break;
					default:
						action = PlayerActionEnum.Raise;
						break;
				}
			}

			return new PlayerAction(chair, action, amount, BetRound);
		}

		public PlayerAction CallAction(Chair chair)
		{
			if (chair.ChairNumber == Chair.ChairNumber)
				throw new ArgumentException("Chair cannot be the same", "chair");

			return new PlayerAction(chair, PlayerActionEnum.Call, Amount, BetRound);
		}

		public override string ToString()
		{
			return string.Format("{{{0}: {1}: {2} {3}}}",BetRound, Chair.Name, Action, Amount);
		}
	}
}
