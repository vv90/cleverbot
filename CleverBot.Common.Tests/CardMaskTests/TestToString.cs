﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.CardMaskTests
{
	public partial class CardMaskTest
	{
		[TestMethod]
		public void Test_ToString_ReturnsEmptyStringForEmptyMask()
		{
			var cardMask = new CardMask();
			
			Assert.AreEqual(string.Empty, cardMask.ToString());
		}

		[TestMethod]
		public void Test_ToString_ReturnsFullStringForFullMask()
		{
			var cardMask = new CardMask();

			foreach (var mask in CardMask.MaskStringDictionary.Values)
			{
				cardMask |= mask;
			}

			Assert.IsTrue(CardMask.MaskStringDictionary.Keys.All(k => cardMask.ToString().Contains(k)));
		}

		[TestMethod]
		public void Test_ToString_ReturnsTheRightStringForEachMask()
		{
			foreach (var keyMask in CardMask.MaskStringDictionary)
			{
				var cardMask = keyMask.Value;

				Assert.AreEqual(keyMask.Key, cardMask.ToString());
			}
		}
	}
}
