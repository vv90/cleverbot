﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.CardMaskTests
{
	public partial class CardMaskTest
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TestParse_Null()
		{
			string input = null;
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(CardMask.Empty, result);
		}

		[TestMethod]
		public void TestParse_EmptyString()
		{
			string input = "";
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(CardMask.Empty, result);
		}

		[TestMethod]
		public void TestParse_2s()
		{
			string input = "2s";
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(1UL, result.cards_n);
		}

		[TestMethod]
		public void TestParse_2c()
		{
			string input = "2c";
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(1UL, result.cards.clubs);
		}

		[TestMethod]
		public void TestParse_2s2c2d2h()
		{
			string input = "2s, 2c, 2d,  2h ";
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(1UL, result.cards.clubs, "clubs");
			Assert.AreEqual(1UL, result.cards.diamonds, "diamonds");
			Assert.AreEqual(1UL, result.cards.hearts, "hearts");
			Assert.AreEqual(1UL, result.cards.spades, "spades");
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_TrowsOnInvalidInput()
		{
			string input = "0";
			CardMask result = CardMask.Parse(input);
		}

		[TestMethod]
		public void TestParse_ParsesSpaceDelimetedCardsCorrectly()
		{
			string input = "2c 2d";

			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(1UL, result.cards.clubs, "clubs");
			Assert.AreEqual(1UL, result.cards.diamonds, "diamonds");
		}

		[TestMethod]
		public void TestParse_ParsesMistiSpaceDelimetedCardsCorrectly()
		{
			string input = "2c   2d";

			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(1UL, result.cards.clubs, "clubs");
			Assert.AreEqual(1UL, result.cards.diamonds, "diamonds");
		}

		[TestMethod]
		public void TestParse_ParsesMixedDetimetedCardsCorrectly()
		{
			string input = "2s, 2c 2d,  2h ";
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(1UL, result.cards.clubs, "clubs");
			Assert.AreEqual(1UL, result.cards.diamonds, "diamonds");
			Assert.AreEqual(1UL, result.cards.hearts, "hearts");
			Assert.AreEqual(1UL, result.cards.spades, "spades");
		}

		[TestMethod]
		public void TestParse_ParsesDigitEncodedTensCorrectly()
		{
			string input = "10d, 10s, 10c, 10h";
			CardMask result = CardMask.Parse(input);

			Assert.AreEqual(0x0100010001000100UL, result.cards_n);
		}
	}
}
