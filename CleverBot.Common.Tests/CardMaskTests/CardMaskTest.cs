﻿using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.CardMaskTests
{
	[TestClass]
	public partial class CardMaskTest
	{
		#region Parameterized tests

		public void TestMaskAndSuitsUnion(CardMask mask)
		{
			ushort spades = (ushort)(mask.cards_n & 0x000000000000FFFFUL);
			ushort clubs = (ushort)((mask.cards_n & 0x00000000FFFF0000UL) >> 16);
			ushort diamonds = (ushort)((mask.cards_n & 0x0000FFFF00000000UL) >> 32);
			ushort hearts = (ushort)((mask.cards_n & 0xFFFF000000000000UL) >> 48);

			Assert.AreEqual(spades, mask.cards.spades);
			Assert.AreEqual(clubs, mask.cards.clubs);
			Assert.AreEqual(diamonds, mask.cards.diamonds);
			Assert.AreEqual(hearts, mask.cards.hearts);
		}

		#endregion

		[TestMethod]
		public void Test_ManagedAndUnmanagedSizeofMatch()
		{
			unsafe
			{
				Assert.AreEqual(sizeof(CardMask), Marshal.SizeOf(typeof(CardMask)));
			}
		}

		[TestMethod]
		public void Test_MaskAndSuitsUnion()
		{
			foreach (var mask in CardMask.MaskStringDictionary.Values)
			{
				var cardMask = mask;
				TestMaskAndSuitsUnion(cardMask);
			}
		}

		[TestMethod]
		public void Test_Constructor_RankSuit_8d()
		{
			Assert.AreEqual(CardMask.Parse("8d"), new CardMask(CardRank.Eight, CardSuit.Diamonds));
		}

		[TestMethod]
		public void Test_IsSuited_FalseForEmptyMask()
		{
			CardMask target = new CardMask();
			Assert.AreEqual(false, target.IsSuited());
		}

		[TestMethod]
		public void Test_IsSuited_TrueForSuited()
		{
			CardMask target = CardMask.Parse("Ac, Tc, 9c, 3c");
			Assert.AreEqual(true, target.IsSuited());
		}

		[TestMethod]
		public void Test_IsSuited_FalseForNotSuited()
		{
			CardMask target = CardMask.Parse("Ac, Tc, 9d");
			Assert.AreEqual(false, target.IsSuited());
		}

		[TestMethod]
		public void Test_IsAllSameRank_TrueIfAllRanksAreSame()
		{
			var target = new CardMask("Ac, Ad, As, Ah");
			Assert.AreEqual(true, target.IsAllSameRank());
		}

		[TestMethod]
		public void Test_IsAllSameRank_FalseForDifferentRanks()
		{
			var target = new CardMask("Ac, Td");
			Assert.AreEqual(false, target.IsAllSameRank());
		}


		[TestMethod]
		public void Test_GetRanks_ReturnsRankCorrectly()
		{
			var target = new CardMask("Ac");

			Assert.AreEqual(CardRank.Ace, target.GetRanks().Single());
		}

		[TestMethod]
		public void Test_GetRanks_ReturnsAllRanks()
		{
			var target = new CardMask("Ac, Kd, Qd, Js, Ts, 9h, 8d, 7s, 6h, 5h, 4c, 3d, 2c");

			var ranks = target.GetRanks();

			Assert.IsTrue(ranks.Contains(CardRank.Ace));
			Assert.IsTrue(ranks.Contains(CardRank.King));
			Assert.IsTrue(ranks.Contains(CardRank.Queen));
			Assert.IsTrue(ranks.Contains(CardRank.Jack));
			Assert.IsTrue(ranks.Contains(CardRank.Ten));
			Assert.IsTrue(ranks.Contains(CardRank.Nine));
			Assert.IsTrue(ranks.Contains(CardRank.Eight));
			Assert.IsTrue(ranks.Contains(CardRank.Seven));
			Assert.IsTrue(ranks.Contains(CardRank.Six));
			Assert.IsTrue(ranks.Contains(CardRank.Five));
			Assert.IsTrue(ranks.Contains(CardRank.Four));
			Assert.IsTrue(ranks.Contains(CardRank.Three));
			Assert.IsTrue(ranks.Contains(CardRank.Two));
		}

		[TestMethod]
		public void Test_GetMaxSuitedCount()
		{
			var target = new CardMask("Ac, Kc, Jc, 9c, Ts, 4s");

			Assert.AreEqual(4, target.GetMaxSuitedCount());
		}
	}
}
