﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.HandRangeTests
{
	public partial class HandRangeTest
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestAdd_ThrowsInvalidMaskException()
		{
			HandRange target = new HandRange();
			CardMask invalidMask = new CardMask();

			target.Add(invalidMask);
		}

		[TestMethod]
		public void Test_AddTwice()
		{
			HandRange target = new HandRange();
			CardMask handMask = CardMask.Parse("Ac, 2d");

			AssertInvariant(target);

			int limit;

			target.Add(handMask);
			limit = target.Count;

			target.Add(handMask);

			Assert.AreEqual(limit, target.Count);
		}

		[TestMethod]
		public void TestAdd_NonRepeatingSequence()
		{
			HandRange target = new HandRange();
			int curLimit = target.Count;

			for (int i = 0; i < HandRange.MaxCount; i++)
			{
				target.Add(new CardMask(HandRange.StdRangeHexTable[i]));
				Assert.AreEqual(++curLimit, target.Count);
			}
		}
	}
}