﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.HandRangeTests
{
	public partial class HandRangeTest
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TestParse_Null()
		{
			string input = null;
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_EmptyString()
		{
			string input = string.Empty;
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_InvalidString1()
		{
			string input = "invalid input";
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_InvalidString2()
		{
			string input = "AKQ";
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_InvalidString3()
		{
			string input = "AK-2J";
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_InvalidString4()
		{
			string input = "AAs";
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestParse_InvalidString5()
		{
			string input = "AK+AQ";
			HandRange result;

			result = HandRange.Parse(input);
		}

		[TestMethod]
		public void TestParse_SingleHand()
		{
			string input = "AcKd";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kd"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_TwoHands()
		{
			string input = "AcKd, AcKs";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kd"));
			expected.Add(CardMask.Parse("Ac, Ks"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_AnySuits()
		{
			string input = "AK";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kc"));
			expected.Add(CardMask.Parse("Ac, Kd"));
			expected.Add(CardMask.Parse("Ac, Kh"));
			expected.Add(CardMask.Parse("Ac, Ks"));

			expected.Add(CardMask.Parse("Ad, Kc"));
			expected.Add(CardMask.Parse("Ad, Kd"));
			expected.Add(CardMask.Parse("Ad, Kh"));
			expected.Add(CardMask.Parse("Ad, Ks"));

			expected.Add(CardMask.Parse("Ah, Kc"));
			expected.Add(CardMask.Parse("Ah, Kd"));
			expected.Add(CardMask.Parse("Ah, Kh"));
			expected.Add(CardMask.Parse("Ah, Ks"));

			expected.Add(CardMask.Parse("As, Kc"));
			expected.Add(CardMask.Parse("As, Kd"));
			expected.Add(CardMask.Parse("As, Kh"));
			expected.Add(CardMask.Parse("As, Ks"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_AnySuitsReversed()
		{
			string input = "KA";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kc"));
			expected.Add(CardMask.Parse("Ac, Kd"));
			expected.Add(CardMask.Parse("Ac, Kh"));
			expected.Add(CardMask.Parse("Ac, Ks"));

			expected.Add(CardMask.Parse("Ad, Kc"));
			expected.Add(CardMask.Parse("Ad, Kd"));
			expected.Add(CardMask.Parse("Ad, Kh"));
			expected.Add(CardMask.Parse("Ad, Ks"));

			expected.Add(CardMask.Parse("Ah, Kc"));
			expected.Add(CardMask.Parse("Ah, Kd"));
			expected.Add(CardMask.Parse("Ah, Kh"));
			expected.Add(CardMask.Parse("Ah, Ks"));

			expected.Add(CardMask.Parse("As, Kc"));
			expected.Add(CardMask.Parse("As, Kd"));
			expected.Add(CardMask.Parse("As, Kh"));
			expected.Add(CardMask.Parse("As, Ks"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_Suited()
		{
			string input = "AKs";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kc"));
			expected.Add(CardMask.Parse("Ad, Kd"));
			expected.Add(CardMask.Parse("Ah, Kh"));
			expected.Add(CardMask.Parse("As, Ks"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_SuitedReversed()
		{
			string input = "KAs";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kc"));
			expected.Add(CardMask.Parse("Ad, Kd"));
			expected.Add(CardMask.Parse("Ah, Kh"));
			expected.Add(CardMask.Parse("As, Ks"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_Offsuited()
		{
			string input = "AKo";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Kd"));
			expected.Add(CardMask.Parse("Ac, Kh"));
			expected.Add(CardMask.Parse("Ac, Ks"));

			expected.Add(CardMask.Parse("Ad, Kc"));
			expected.Add(CardMask.Parse("Ad, Kh"));
			expected.Add(CardMask.Parse("Ad, Ks"));

			expected.Add(CardMask.Parse("Ah, Kc"));
			expected.Add(CardMask.Parse("Ah, Kd"));
			expected.Add(CardMask.Parse("Ah, Ks"));

			expected.Add(CardMask.Parse("As, Kc"));
			expected.Add(CardMask.Parse("As, Kd"));
			expected.Add(CardMask.Parse("As, Kh"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_PocketPairs()
		{
			string input = "AA";
			HandRange actual;
			HandRange expected = new HandRange();
			expected.Add(CardMask.Parse("Ac, Ad"));
			expected.Add(CardMask.Parse("Ac, Ah"));
			expected.Add(CardMask.Parse("Ac, As"));

			expected.Add(CardMask.Parse("Ad, Ah"));
			expected.Add(CardMask.Parse("Ad, As"));

			expected.Add(CardMask.Parse("Ah, As"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_PocketPairsPlus()
		{
			string input = "QQ+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("AA"));
			expected.AddRange(HandRange.Parse("KK"));
			expected.AddRange(HandRange.Parse("QQ"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_SuitedPlus()
		{
			string input = "K9s+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("K9s"));
			expected.AddRange(HandRange.Parse("KTs")); 
			expected.AddRange(HandRange.Parse("KJs"));
			expected.AddRange(HandRange.Parse("KQs"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_SuitedPlusReversed()
		{
			string input = "9Ks+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("K9s"));
			expected.AddRange(HandRange.Parse("KTs"));
			expected.AddRange(HandRange.Parse("KJs"));
			expected.AddRange(HandRange.Parse("KQs"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_OffsuitedPlus()
		{
			string input = "K9o+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("K9o"));
			expected.AddRange(HandRange.Parse("KTo"));
			expected.AddRange(HandRange.Parse("KJo"));
			expected.AddRange(HandRange.Parse("KQo"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_AnySuitsPlus()
		{
			string input = "K9+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("K9"));
			expected.AddRange(HandRange.Parse("KT"));
			expected.AddRange(HandRange.Parse("KJ"));
			expected.AddRange(HandRange.Parse("KQ"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_PocketPairsMinus()
		{
			string input = "55-";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("55"));
			expected.AddRange(HandRange.Parse("44"));
			expected.AddRange(HandRange.Parse("33"));
			expected.AddRange(HandRange.Parse("22"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_AnySuitsRange()
		{
			string input = "AK-AT";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("AK"));
			expected.AddRange(HandRange.Parse("AQ"));
			expected.AddRange(HandRange.Parse("AJ"));
			expected.AddRange(HandRange.Parse("AT"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_AnySuitsRangeReversed()
		{
			string input = "AT-AK";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("AK"));
			expected.AddRange(HandRange.Parse("AQ"));
			expected.AddRange(HandRange.Parse("AJ"));
			expected.AddRange(HandRange.Parse("AT"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_Composite()
		{
			string input = "AA, AJ+, A9o+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("AA"));
			expected.AddRange(HandRange.Parse("AJ+"));
			expected.AddRange(HandRange.Parse("A9o+"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_CompositWithLineBrakes()
		{
			string input = "AA\n, AJ+,\n A9o+";
			HandRange actual;
			HandRange expected = new HandRange();

			expected.AddRange(HandRange.Parse("AA"));
			expected.AddRange(HandRange.Parse("AJ+"));
			expected.AddRange(HandRange.Parse("A9o+"));

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_Broadway()
		{
			string input = "Broadway";
			HandRange actual;
			HandRange expected = HandRange.Parse("TT+, ATs+, KTs+, QTs+, JTs, ATo+, KTo+, QTo+, JTo");

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_DoubleSidedSuited()
		{
			string input = "AKs-ATs";

			HandRange actual = HandRange.Parse(input);
			HandRange expected = HandRange.Parse("AKs, AQs, AJs, ATs");

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_RangeOfConnectors()
		{
			string input = "45s-TJs";
			HandRange actual;
			HandRange expected = HandRange.Parse("JTs, T9s, 98s, 87s, 76s, 65s, 54s");

			actual = HandRange.Parse(input);

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_RangeOfPairs()
		{
			string input = "22-TT";

			HandRange actual = HandRange.Parse(input);
			HandRange expected = HandRange.Parse("22, 33, 44, 55, 66, 77, 88, 99, TT");

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_CompositeRangeWithBroadway()
		{
			string input = "55-99, Broadway";

			HandRange actual = HandRange.Parse(input);
			HandRange expected = HandRange.Parse("55+, ATs+, KTs+, QTs+, JTs, ATo+, KTo+, QTo+, JTo");

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}

		[TestMethod]
		public void TestParse_SingleGapConnectors()
		{
			string input = "75s-J9s";

			HandRange actual = HandRange.Parse(input);
			HandRange expected = HandRange.Parse("75s, 86s, 97s, T8s, J9s");

			Assert.AreNotEqual(0, actual.Count, "Count is zero");
			AssertRangesMatch(expected, actual);
		}
	}
}
