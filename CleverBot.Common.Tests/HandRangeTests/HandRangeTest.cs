﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.HandRangeTests
{
	[TestClass]
	public partial class HandRangeTest
	{
		#region Helper methods

		private void AssertInvariant(HandRange target)
		{
			Assert.IsTrue(target.Invariant(), "Invariant");
		}

		void AssertRangesMatch(HandRange expected, HandRange actual)
		{
			Assert.AreEqual(expected.Count, actual.Count, "Count");

			foreach (CardMask mask in actual)
				Assert.AreNotEqual(-1, expected.IndexOf(mask), mask.ToString());

		}

		#endregion

		[TestMethod]
		public void TestStdRange()
		{
			HandRange target = HandRange.StdRange();

			AssertInvariant(target);

			Assert.AreEqual(HandRange.MaxCount, target.Count);

			for (int i = 0; i < target.Count; i++)
				Assert.AreNotEqual(-1, new CardMask(HandRange.StdRangeHexTable[i]));
		}

		[TestMethod]
		public void TestGet()
		{
			HandRange target = HandRange.StdRange();

			for (int i = 0; i < target.Count; i++)
				Assert.AreEqual(HandRange.StdRangeHexTable[i], target.Get(i).cards_n);
		}

		[TestMethod]
		public void TestAddRange()
		{
			HandRange target = new HandRange();
			HandRange rangeToAdd = new HandRange();

			rangeToAdd.Add(CardMask.Parse("Ac,Ad"));
			rangeToAdd.Add(CardMask.Parse("Ac,Ah"));
			rangeToAdd.Add(CardMask.Parse("Ac,As"));

			target.AddRange(rangeToAdd);

			Assert.AreNotEqual(0, target.Count, "Count is 0");
			AssertRangesMatch(target, rangeToAdd);
		}
	}
}
