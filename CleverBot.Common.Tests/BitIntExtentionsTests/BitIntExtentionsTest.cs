﻿using System;
using CleverBot.Common.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.BitIntExtentionsTests
{
	[TestClass]
	public class BitIntExtentionsTest
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestBitLog2_Zero()
		{
			ulong target = 0;

			BitIntExtentions.BitLog2(target);
		}

		[TestMethod]
		public void TestBitLog2_One()
		{
			ulong target = 1;

			Assert.AreEqual(0, BitIntExtentions.BitLog2(target));
		}

		[TestMethod]
		public void TestBitLog2_Four()
		{
			ulong target = 4;

			Assert.AreEqual(2, BitIntExtentions.BitLog2(target));
		}

		[TestMethod]
		public void TestBitLog2_Five()
		{
			ulong target = 5;

			Assert.AreEqual(2, BitIntExtentions.BitLog2(target));
		}
	}
}
