﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests
{
	[TestClass]
	public class GameTreeTest
	{
		[TestMethod]
		public void TestIsActionValid()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 70, new CardMask(), "Player 1"),
					new Chair(2, 66, new CardMask(), "Player 2"),
					new Chair(3, 30, new CardMask(), "Player 3"),
					new Chair(4, 220, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			var tree = new GameTree(game);
			var action = new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1);
			var isValid = tree.IsActionValid(action);

			Assert.AreEqual(true, isValid);
		}

		[TestMethod]
		public void TestGetSuccessors()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 70, new CardMask(), "Player 1"),
					new Chair(2, 66, new CardMask(), "Player 2"),
					new Chair(3, 30, new CardMask(), "Player 3"),
					new Chair(4, 220, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			var tree = new GameTree(game);
			var successors = tree.GetSuccessors();

			Assert.AreEqual(game.GetChair("Player 1"), successors.Single().PlayerAction.Chair);
			Assert.AreEqual(PlayerActionEnum.SmallBlind, successors.Single().PlayerAction.Action);
			Assert.AreEqual(1, successors.Single().PlayerAction.Amount);
			Assert.AreEqual(BetRound.Preflop, successors.Single().PlayerAction.BetRound);
		}
	}
}
