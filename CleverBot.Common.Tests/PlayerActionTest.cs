﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests
{
	[TestClass]
	public class PlayerActionTest
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestNextAction_ThowsExceptionIfChairNumberIsTheSame()
		{
			var chair = new Chair() { ChairNumber = 1 };
			var action = new PlayerAction(chair, PlayerActionEnum.SmallBlind, 1);

			var reaction = action.RaiseAction(chair, 2);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestNextAction_ThrowsExceptionIfAmountHasReduced()
		{
			var action = new PlayerAction(new Chair(), PlayerActionEnum.SmallBlind, 1);

			var reaction = action.RaiseAction(new Chair(), 0.5);
		}

		[TestMethod]
		public void TestNextAction_ReturnsBigBlindAfterSmallBlind()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.SmallBlind, 1);

			var reaction = action.RaiseAction(new Chair(), 2);

			Assert.AreEqual(PlayerActionEnum.BigBlind, reaction.Action);
			Assert.AreEqual(2, reaction.Amount);
		}

		[TestMethod]
		public void TestNextAction_ReturnsRaiseAfterBigBlind()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.BigBlind, 1);

			var reaction = action.RaiseAction(new Chair(), 2);

			Assert.AreEqual(PlayerActionEnum.Raise, reaction.Action);
			Assert.AreEqual(2, reaction.Amount);
		}

		[TestMethod]
		public void TestNextAction_ReturnsRaiseAfterBet()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.Bet, 1);

			var reaction = action.RaiseAction(new Chair(), 2);

			Assert.AreEqual(PlayerActionEnum.Raise, reaction.Action);
			Assert.AreEqual(2, reaction.Amount);
		}

		[TestMethod]
		public void TestNextAction_ReturnsRaiseAfterCall()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.Call, 1);

			var reaction = action.RaiseAction(new Chair(), 2);

			Assert.AreEqual(PlayerActionEnum.Raise, reaction.Action);
			Assert.AreEqual(2, reaction.Amount);
		}

		[TestMethod]
		public void TestNextAction_ReturnRaiseAfterRaise()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.Raise, 1);

			var reaction = action.RaiseAction(new Chair(), 2);

			Assert.AreEqual(PlayerActionEnum.Raise, reaction.Action);
			Assert.AreEqual(2, reaction.Amount);
		}

		[TestMethod]
		public void TestNextAction_SetsBetRoundCorrectly()
		{
			var action = new PlayerAction(new Chair {ChairNumber = 1}, PlayerActionEnum.Bet, 1, BetRound.Flop);

			var reaction = action.RaiseAction(new Chair(), 2);

			Assert.AreEqual(BetRound.Flop, reaction.BetRound);
		}

		[TestMethod]
		public void TestCallAction_SetsBetRoundCorrectly()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.Raise, 1, BetRound.Flop);

			var reaction = action.CallAction(new Chair());

			Assert.AreEqual(BetRound.Flop, reaction.BetRound);
		}

		[TestMethod]
		public void TestCallAction_SetsAmountCorrectly()
		{
			var action = new PlayerAction(new Chair { ChairNumber = 1 }, PlayerActionEnum.Raise, 1, BetRound.Flop);

			var reaction = action.CallAction(new Chair());

			Assert.AreEqual(1, reaction.Amount);
		}
	}
}
