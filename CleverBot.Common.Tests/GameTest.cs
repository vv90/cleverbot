﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests
{
	[TestClass]
	public class GameTest
	{
		//[TestMethod]
		//public void TestGetPosition_ReturnsPositionsCorrectly()
		//{
		//	var game = new Game
		//	{
		//		Chairs = new[]
		//		{
		//			new Chair { ChairNumber = 1, IsDealer = true },
		//			new Chair { ChairNumber = 2, IsDealer = false },
		//			new Chair { ChairNumber = 3, IsDealer = false },
		//			new Chair { ChairNumber = 4, IsDealer = false },
		//			new Chair { ChairNumber = 5, IsDealer = false },
		//			new Chair { ChairNumber = 6, IsDealer = false }
		//		}
		//	};

		//	Assert.AreEqual(Position.BU, game.GetPosition(1));
		//	Assert.AreEqual(Position.SB, game.GetPosition(2));
		//	Assert.AreEqual(Position.BB, game.GetPosition(3));
		//	Assert.AreEqual(Position.UTG, game.GetPosition(4));
		//	Assert.AreEqual(Position.MP, game.GetPosition(5));
		//	Assert.AreEqual(Position.CO, game.GetPosition(6));
		//}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestDealerOffset_ThrowsExceptionIfChairNotFound()
		{
			var game = new Game
			{
				DealerChair = 1,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var dealerOffset = game.DealerOffset(3);
		}

		[TestMethod]
		public void TestDealerOffset_ReturnsZeroForDealer()
		{
			var game = new Game
			{
				DealerChair = 1,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 3 },
					new Chair { ChairNumber = 4 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var dealerOffset = game.DealerOffset(1);

			Assert.AreEqual(0, dealerOffset);
		}

		[TestMethod]
		public void TestDealerOffset_WorksForChairsAfterDealer()
		{
			var game = new Game
			{
				DealerChair = 0,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 3 },
					new Chair { ChairNumber = 4 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var dealerOffset = game.DealerOffset(5);

			Assert.AreEqual(5, dealerOffset);
		}

		[TestMethod]
		public void TestDealerOffset_WorksForChairsBeforeDealer()
		{
			var game = new Game
			{
				DealerChair = 5,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 3 },
					new Chair { ChairNumber = 4 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var dealerOffset = game.DealerOffset(4);

			Assert.AreEqual(5, dealerOffset);
		}

		[TestMethod]
		public void TestDealerOffset_WorksWithMissingChairNumbers()
		{
			var game = new Game
			{
				DealerChair = 5,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 4 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var dealerOffset = game.DealerOffset(4);

			Assert.AreEqual(3, dealerOffset);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestGetNextChair_ThrowsExceptionForInvalidChairNumber()
		{
			var game = new Game
			{
				DealerChair = 1,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var chair = game.GetNextChair(3);
		}

		[TestMethod]
		public void TestGetNextChair_ReturnsCorrectChair()
		{
			var game = new Game
			{
				DealerChair = 5,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 3 },
					new Chair { ChairNumber = 4 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var chair = game.GetNextChair(1);

			Assert.AreEqual(2, chair.ChairNumber);
		}

		[TestMethod]
		public void TestGetNextChair_WorksCorrectlyForTheLastChairNumber()
		{
			var game = new Game
			{
				DealerChair = 5,
				Chairs = new[]
				{
					new Chair { ChairNumber = 0 },
					new Chair { ChairNumber = 1 },
					new Chair { ChairNumber = 2 },
					new Chair { ChairNumber = 3 },
					new Chair { ChairNumber = 4 },
					new Chair { ChairNumber = 5 }
				}.ToList()
			};

			var chair = game.GetNextChair(5);

			Assert.AreEqual(0, chair.ChairNumber);
		}

		[TestMethod]
		public void TestEffectiveStack_ReturnsMaxmumPlaying()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 70, new CardMask(), "Player 1"),
					new Chair(2, 66, new CardMask(), "Player 2"),
					new Chair(3, 30, new CardMask(), "Player 3"),
					new Chair(4, 220, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop)
			};

			Assert.AreEqual(70.0, game.EffectiveStack());
		}

		[TestMethod]
		public void TestEffectiveStack_DoesNotReturMoreThanHeroStack()
		{
			var game = new Game
			{
				SmallBlind = 1,
				BigBlind = 2,
				DealerChair = 0,
				HeroChair = 0,
				Flop = new CardMask("8h, 8s, 3d"),
				Chairs = new List<Chair>
				{
					new Chair(0, 200, new CardMask("Ac, Ad"), "Hero"),
					new Chair(1, 70, new CardMask(), "Player 1"),
					new Chair(2, 66, new CardMask(), "Player 2"),
					new Chair(3, 30, new CardMask(), "Player 3"),
					new Chair(4, 220, new CardMask(), "Player 4"),
					new Chair(5, 200, new CardMask(), "Player 5"),
				}
			};

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.SmallBlind, 1),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.BigBlind, 2),
				new PlayerAction(game.GetChair("Player 3"), PlayerActionEnum.Fold),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Call, 2),
				new PlayerAction(game.GetChair("Player 5"), PlayerActionEnum.Fold),

				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Raise, 7),
				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Call, 7),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Call, 7),

				new PlayerAction(game.GetChair("Player 1"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 4"), PlayerActionEnum.Check, 0, BetRound.Flop),
				new PlayerAction(game.GetChair("Hero"), PlayerActionEnum.Bet, 10, BetRound.Flop),
				new PlayerAction(game.GetChair("Player 2"), PlayerActionEnum.Raise, 30, BetRound.Flop)
			};

			Assert.AreEqual(200.0, game.EffectiveStack());
		}
	}
}
