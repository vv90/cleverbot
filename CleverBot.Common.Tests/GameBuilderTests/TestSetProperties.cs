﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CleverBot.Common.Enums;
using CleverBot.Common.Exceptions;
using CleverBot.Common.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CleverBot.Common.Tests.GameBuilderTests
{

	[TestClass]
	public class TestSetProperties
	{
		[TestMethod]
		public void TestSetProperties_SetGameId()
		{
			var gameBuilder = new GameBuilder();
			var propertyValue = new GamePropertyValue(GameProperty.GameId, "123");

			gameBuilder.SetProperties(propertyValue);

			Assert.AreEqual("123", gameBuilder.Game.GameNumber);
		}

		[TestMethod]
		public void TestSetProperties_SetsBoardCorrectly()
		{
			var gameBuilder = new GameBuilder();
			var propertyValue = new GamePropertyValue(GameProperty.Board, "Ac, 6s, 4d, 9s, Tc");

			gameBuilder.SetProperties(propertyValue);

			Assert.AreEqual(new CardMask("Ac, 6s, 4d, 9s, Tc"), gameBuilder.Game.Board);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestSetProperties_ThrowsIfBoardHasMoreThan5Cards()
		{
			var gameBuilder = new GameBuilder();
			var propertyValue = new GamePropertyValue(GameProperty.Board, "Ac, 6s, 4d, 9s, Tc");

			gameBuilder.SetProperties(propertyValue);

			var propertyValue2 = new GamePropertyValue(GameProperty.Board, "7h");

			gameBuilder.SetProperties(propertyValue2);
		}

		[TestMethod]
		public void TestSetProperties_SkipsEmptyValuesInput()
		{
			var gameBuilder = new GameBuilder();
			gameBuilder.SetProperties(new GamePropertyValue[0]);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TestSetProperties_ThrowsArgumentNullException()
		{
			var gameBuilder = new GameBuilder();
			gameBuilder.SetProperties(null);
		}

		[TestMethod]
		public void TestSetProperties_SetChair()
		{
			var gameBuilder = new GameBuilder();
			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, "Player")
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(1, gameBuilder.Game.Chairs.Single().ChairNumber);
			Assert.AreEqual("Player", gameBuilder.Game.Chairs.Single().Name);
		}

		[TestMethod]
		public void TestSetProperties_UpdateDoesNotDuplicateChair()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.Balance, 100.0)
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.Balance, 90.0)
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(1, gameBuilder.Game.Chairs.Count);
		}

		[TestMethod]
		public void TestSetProperties_UpdateDoesNotReplaceChair()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.Balance, 100.0)
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, "Player")
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(100, gameBuilder.Game.Chairs.Single().Balance);
		}

		[TestMethod]
		public void TestSetProperties_SetPlayerAction()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, "Player")
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.PlayerName, "Player"),
				new GamePropertyValue(GameProperty.Action, PlayerActionEnum.Fold)
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(PlayerActionEnum.Fold, gameBuilder.Game.Actions.Single().Action);
		}

		[TestMethod]
		[ExpectedException(typeof(IncompleteInformationException))]
		public void TestSetProperties_UpdateActionThrowsWithoutNameSpecified()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, "Player")
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.Action, PlayerActionEnum.Fold)
			};

			gameBuilder.SetProperties(values);
		}

		[TestMethod]
		[ExpectedException(typeof(IncompleteInformationException))]
		public void TestSetProperties_UpdateActionThrowsIfPlayerNameNotFound()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, "Player")
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.PlayerName, "Player111"),
				new GamePropertyValue(GameProperty.Action, PlayerActionEnum.Fold)
			};

			gameBuilder.SetProperties(values);
		}

		[TestMethod]
		public void TestSetProperties_SetGameLimitNL25()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.GameLimit, 25.0)
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(0.25, gameBuilder.Game.BigBlind);
			Assert.AreEqual(0.1, gameBuilder.Game.SmallBlind);
		}

		[TestMethod]
		public void TestSetProperties_SetGameLimitNL2()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.GameLimit, 2.0)
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(0.02, gameBuilder.Game.BigBlind);
			Assert.AreEqual(0.01, gameBuilder.Game.SmallBlind);
		}

		[TestMethod]
		public void TestSetProperties_SetGameLimitNL5()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.GameLimit, 5.0)
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(0.05, gameBuilder.Game.BigBlind);
			Assert.AreEqual(0.02, gameBuilder.Game.SmallBlind);
		}

		[TestMethod]
		public void TestSetProperties_SetGameLimitNL2000()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.GameLimit, 2000.0)
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(20, gameBuilder.Game.BigBlind);
			Assert.AreEqual(10, gameBuilder.Game.SmallBlind);
		}

		[TestMethod]
		public void TestSetProperties_PlayerActionIsSetWithTheCorrectBetRound()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, "Player")
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.Board, "Ac Ad 9s"),
			};

			gameBuilder.SetProperties(values);

			values = new[]
			{
				new GamePropertyValue(GameProperty.PlayerName, "Player"),
				new GamePropertyValue(GameProperty.Action, PlayerActionEnum.Fold),
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(BetRound.Flop, gameBuilder.Game.Actions.Single().BetRound);
		}

		[TestMethod]
		public void TestSetProperties_SetsNullValueWithoutError()
		{
			var gameBuilder = new GameBuilder();

			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 1),
				new GamePropertyValue(GameProperty.PlayerName, null)
			};

			gameBuilder.SetProperties(values);

			Assert.IsNull(gameBuilder.Game.GetChair(1).Name);
		}

		[TestMethod]
		public void TestSetProperties_AmountIsSetToTheSumOfAllPreviousAndCurrentBets()
		{
			var game = new Game();

			var chair = new Chair(0, 100, CardMask.Empty, "player 0");

			game.Chairs = new List<Chair> { chair };

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(chair, PlayerActionEnum.SmallBlind, 5)
			};

			var gameBuilder = new GameBuilder(game);



			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 0),
				new GamePropertyValue(GameProperty.Action, PlayerActionEnum.Call),
				new GamePropertyValue(GameProperty.BetAmount, 5.0),
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(10, game.Actions.Last().Amount);
		}

		[TestMethod]
		public void TestSetProperties_DoesNotAddAmountFromPreviousBetRounds()
		{
			var game = new Game { Flop = new CardMask("Ac, Kd, 3s") };

			var chair = new Chair(0, 100, CardMask.Empty, "player 0");

			game.Chairs = new List<Chair> { chair };

			game.Actions = new List<PlayerAction>
			{
				new PlayerAction(chair, PlayerActionEnum.SmallBlind, 5)
			};

			var gameBuilder = new GameBuilder(game);



			var values = new[]
			{
				new GamePropertyValue(GameProperty.ChairNumber, 0),
				new GamePropertyValue(GameProperty.Action, PlayerActionEnum.Bet),
				new GamePropertyValue(GameProperty.BetAmount, 5.0),
			};

			gameBuilder.SetProperties(values);

			Assert.AreEqual(5, game.Actions.Last().Amount);
		}
	}
}
